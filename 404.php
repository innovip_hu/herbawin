<?php 
  header('HTTP/1.0 404 Not Found', true, 404);
	session_start();
	ob_start();
?>
<!DOCTYPE html>
<html class="wide smoothscroll wow-animation" lang="hu">
  <head>
  	<?php
  		$oldal = "404";
  		include 'config.php';
  		include $gyoker.'/module/mod_head.php';
  	?>

      <title><?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">  	
  </head>
  <body>
  	<?php include $gyoker.'/module/mod_body_start.php'; ?>
    <!-- Page-->
    <div class="page">
      <!-- Page preloader-->

		<?php include $gyoker.'/module/mod_header.php'; ?>

      <section class="section section-bredcrumbs bg-image-breadcrumbs-1">
        <div class="shell">
          <div class="range range-center">
            <div class="cell-sm-10 cell-xl-8">
              <div class="breadcrumb-wrapper"><img src="<?=$domain?>/images/image-icon-1-49x43.png" alt="" width="49" height="43"/>
                <h2>404</h2>
                <ol class="breadcrumbs-custom">
                  <li><a href="<?=$domain?>">Főoldal</a></li>
                  <li>404
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Company overview-->
      <section class="section section-lg bg-white">
        <div class="shell text-center">
          <div class="range range-center">
            <div class="cell-sm-7 cell-lg-5">
              <h1 class="heading-decorative"><span><img src="images/typography-image-1-83x72.png" alt="" width="83" height="72"/>404</span></h1>
              <h6>A keresett oldal nem található</h6>
              <p class="big offset-1">Kérjük ellenőrizze a linket és próbálja újra.</p><a class="button button-primary offset-1" href="<?=$domain?>">Vissza a kezdőlapra</a>
            </div>
          </div>
        </div>
      </section>


	<?php include $gyoker.'/module/mod_footer.php'; ?>

    </div>
	<?php include $gyoker.'/module/mod_body_end.php'; ?>
  </body>
</html>