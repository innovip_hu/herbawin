<p><strong>ADATKEZELÉSI TÁJÉKOZTATÓ</strong></p>

<p><strong>AZ ÉRINTETT TERMÉSZETES SZEMÉLY JOGAIRÓL </strong></p>

<p><strong>SZEMÉLYES ADATAI KEZELÉSE VONATKOZÁSÁBAN</strong></p>

<p>&nbsp;</p>

<p><strong>BEVEZETÉS</strong></p>

<p><em>A természetes személyeknek a személyes adatok kezelése tekintetében történő védelméről és az ilyen adatok szabad áramlásáról, valamint a 95/46/EK rendelet hatályon kívül helyezéséről szóló</em><em> </em><em>AZ EURÓPAI PARLAMENT ÉS A TANÁCS (EU) 2016/679 RENDELETE (a továbbiakban: Rendelet) </em><em>előírja, hogy az Adatkezelő megfelelő intézkedéseket hoz annak érdekében, hogy az érintett részére a személyes adatok kezelésére vonatkozó, minden egyes tájékoztatást tömör, átlátható, érthető és könnyen hozzáférhető formában, világosan és közérthetően megfogalmazva nyújtsa, továbbá hogy az Adatkezelő elősegíti az érintett jogainak a gyakorlását. </em></p>

<p><em>Az érintett előzetes tájékoztatási kötelezettségét az információs önrendelkezési jogról és az információszabadságról 2011. évi CXII. törvény is előírja. Az alábbiakban olvasható tájékoztatással e jogszabályi kötelezettségünknek teszünk eleget. </em></p>

<p><em>A tájékoztatást közzé kell tenni a társaság honlapján, vagy az érintett személy részére kérésére meg kell küldeni. </em></p>

<p>&nbsp;</p>

<p><strong>I. FEJEZET</strong></p>

<p><strong>AZ ADATKEZELŐ MEGNEVEZÉSE</strong></p>

<p>E tájékoztatás kiadója, egyben az Adatkezelő:</p>

<p>Cégnév: Második Lehetőség Kereskedelmi Szociális Szövetkezet</p>

<p>Székhely: 6725 Szeged, Alföldi utca 45. ½.</p>

<p>Képviseli: Durszt Anikó Blanka</p>

<p>Elérhetőségek: e-mail cím: <u><a href="mailto:herbawin.webshop@gmail.com">herbawin.webshop@gmail.com</a></u>, telefon: +3670/618-9013</p>

<p><strong>(a továbbiakban: Társaság) </strong></p>

<p>&nbsp;</p>

<p><strong>II. FEJEZET</strong></p>

<p><strong>ADATFELDOLGOZÓK MEGNEVEZÉSE </strong></p>

<p><strong>Adatfeldolgozó</strong>: az a természetes vagy jogi személy, közhatalmi szerv, ügynökség vagy bármely egyéb szerv, amely az adatkezelő nevében személyes adatokat kezel; (Rendelet 4. cikk 8.)</p>

<p>Az adatfeldolgozó igénybevételéhez nem kell az érintett előzetes beleegyezése, de szükséges a tájékoztatása. Ennek megfelelően a következő tájékoztatást adjuk:</p>

<p>&nbsp;</p>

<p><strong>III. FEJEZET</strong></p>

<p><strong>AZ ADATKEZELÉS JOGSZERŰSÉGÉNEK BIZTOSÍTÁSA</strong></p>

<p><strong>1. Adatkezelés az érintett hozzájárulása alapján </strong></p>

<p>1.1. Amennyiben a Társaság hozzájáruláson alapuló adatkezelést kíván végezni, az érintett hozzájárulását személyes adatai kezeléséhez az adatkezelési szabályzatban meghatározott adatkérő lap szerinti tartalommal és tájékoztatással kell kérni.</p>

<p>1.2. Hozzájárulásnak minősül az is, ha az érintett a Társaság internetes honlapjának megtekintése során bejelöl egy erre vonatkozó négyzetet, az információs társadalommal összefüggő szolgáltatások igénybevétele során erre vonatkozó technikai beállításokat hajt végre, valamint bármely egyéb olyan nyilatkozat vagy cselekedet is, amely az adott összefüggésben az érintett hozzájárulását személyes adatainak tervezett kezeléséhez egyértelműen jelzi. A hallgatás, az előre bejelölt négyzet vagy a nem cselekvés ezért nem minősül hozzájárulásnak.</p>

<p>1.3. A hozzájárulás az ugyanazon cél vagy célok érdekében végzett összes adatkezelési tevékenységre kiterjed. Ha az adatkezelés egyszerre több célt is szolgál, akkor a hozzájárulást az összes adatkezelési célra vonatkozóan meg kell adni.</p>

<p>1.4. Ha az érintett hozzájárulását olyan írásbeli nyilatkozat keretében adja meg, amely más ügyekre is vonatkozik – például értékesítési, szolgáltatási szerződés megkötése - a hozzájárulás iránti kérelmet ezektől a más ügyektől egyértelműen megkülönböztethető módon kell előadni, érthető és könnyen hozzáférhető formában, világos és egyszerű nyelvezettel. Az érintett hozzájárulását tartalmazó ilyen nyilatkozat bármely olyan része, amely sérti a Rendeletet, kötelező erővel nem bír.</p>

<p>1.5. A Társaság nem kötheti szerződés megkötését, teljesítését olyan személyes adatok kezeléséhez való hozzájárulás megadásához, amelyek nem szükségesek a szerződés teljesítéséhez.</p>

<p>1.6. A hozzájárulás visszavonását ugyanolyan egyszerű módon kell lehetővé tenni, mint annak megadását.</p>

<p>1.7. Ha a személyes adat felvételére az érintett hozzájárulásával került sor, az adatkezelő a felvett adatokat törvény eltérő rendelkezésének hiányában a rá vonatkozó jogi kötelezettség teljesítése céljából további külön hozzájárulás nélkül, valamint az érintett hozzájárulásának visszavonását követően is kezelheti.</p>

<p>&nbsp;</p>

<p><strong>2. Jogi kötelezettség teljesítésén alapuló adatkezelés </strong></p>

<p>2.1. A jogi kötelezettségen alapuló adatkezelés esetén a kezelhető adatok körére, az adatkezelés céljára, az adatok tárolásának időtartamára, a címzettekre az alapul szolgáló jogszabály rendelkezései irányadók.</p>

<p>2.2 A jogi kötelezettség teljesítése jogcímén alapuló adatkezelés az érintett hozzájárulásától független, mivel az adatkezelést jogszabály határozza meg. Az érintettel az adatkezelés megkezdése előtt ezesetben közölni kell, hogy az adatkezelés kötelező, továbbá az érintettet az adatkezelés megkezdése előtt egyértelműen és részletesen tájékoztatni kell az adatai kezelésével kapcsolatos minden tényről, így különösen az adatkezelés céljáról és jogalapjáról, az adatkezelésre és az adatfeldolgozásra jogosult személyéről, az adatkezelés időtartamáról, arról, ha az érintett személyes adatait az adatkezelő a rá vonatkozó jogi kötelezettség alapján kezeli, illetve arról, hogy kik ismerhetik meg az adatokat. A tájékoztatásnak ki kell terjednie az érintett adatkezeléssel kapcsolatos jogaira és jogorvoslati lehetőségeire is. Kötelező adatkezelés esetén a tájékoztatás megtörténhet az előbbi információkat tartalmazó jogszabályi rendelkezésekre való utalás nyilvánosságra hozatalával is.</p>

<p>&nbsp;</p>

<p><strong>3. Jogos érdeken alapuló adatkezelés</strong></p>

<p>3.1. A Társaság vagy valamely harmadik fél jogos érdeke jogalapot teremthet az adatkezelésre, feltéve, hogy az érintett érdekei, alapvető jogai és szabadságai nem élveznek elsőbbséget. Figyelembe kell venni az érintett adatkezelővel való kapcsolata alapján észszerű elvárásait, így a személyes adatok kapcsolattartási, akár közvetlen üzletszerzési célú kezelése is jogos érdeken alapulónak tekinthető.</p>

<p>&nbsp;</p>

<p><strong>4. Tájékoztatás ügyféladatok, szerződő partnerek, kapcsolattartók adatainak kezeléséről</strong></p>

<p>4.1. A Társaság szerződés teljesítése jogcímén a szerződés előkészítése, megkötése, teljesítése, megszűnése, szerződési kedvezmény nyújtása – összefoglalva a közös érdekkörben felmerülő gazdasági folyamatok támogatása – céljából kezelheti a vele szerződéses viszonyban álló természetes személy nevét, születési nevét, születési idejét, anyja nevét, lakcímét, adóazonosító jelét, adószámát, vállalkozói, őstermelői igazolvány számát, személyi igazolvány számát, lakcímét, székhely, telephely címét, telefonszámát, e-mail címét, honlap-címét, bankszámlaszámát, vevőszámát (ügyfélszámát, rendelésszámát), online azonosítóját (vevők, szállítók listája, törzsvásárlási listák), egészségügyi alkalmassági iratait, bizonyítványát és egyéb egészségügyi adatait, leletetit. Ezen adatkezelés jogszerűnek minősül akkor is, ha az adatkezelés a szerződés megkötését megelőzően az érintett kérésére történő lépések megtételéhez szükséges. A személyes adatok címzettjei: a Társaság ügyfélkiszolgálással kapcsolatos feladatokat ellátó munkavállalói, könyvelési, adózási, üzletkötési feladatokat ellátó munkavállalói, és adatfeldolgozói. A személyes adatok tárolásának időtartama a Társaság hosszú távú üzleti kapcsolataira tekintettel a szerződés megszűnését követő 5 év.</p>

<p>4.2. A természetes személy szerződő fél szerződésben megadott adatai számviteli, adózási célú kezelésének jogalapja jogi kötelezettség teljesítése, ebben a körben az adattárolás időtartama 8 év.</p>

<p>4.3. A Társaság a vele szerződő jogi személy képviseletében eljáró – a szerződést aláíró– természetes személy szerződésben megadott személyes adatait, továbbá lakcímét, e-mail címét és telefonszámát, online azonosítóját szerződés előkészítése, kapcsolattartás, a szerződésből eredő jogok és kötelezettségek gyakorlása – összefoglalva a közös érdekkörben felmerülő gazdasági folyamatok támogatása – céljából szerződés teljesítése jogcímén kezeli. Ezen adatok tárolásának időtartama a szerződés megszűnését követő 5 év. A jogos érdeken alapuló adatkezelés esetén az érintett kiemelt joga, hogy tiltakozzon az adatkezelés ellen.</p>

<p>4.4. A Társaság a vele kötött szerződésben kapcsolatartóként megjelölt – nem aláíró – természetes személy nevét, címét, telefonszámát, e-mail címét, online azonosítóját kapcsolattartás, szerződésből eredő jogok és kötelezettségek gyakorlása – összefoglalva a közös érdekkörben felmerülő gazdasági folyamatok támogatása – céljából szerződés teljesítése jogcímén kezeli, figyelemmel arra, hogy a kapcsolattartó a szerződő féllel foglalkoztatásra irányuló jogviszonyban áll, így ezen adatkezelés az érinett jogait nem érinti hátrányosan. A szerződő fél kijelenti, hogy a kapcsolattartói minőséghez kapcsolódó adatkezelésről az érintett kapcsolatartót tájékoztatta. Ezen adatok tárolásának időtartama a kapcsolattartói minőség fennállását követő 5 év.</p>

<p>4.5. Valamennyi érintett vonatkozásában a személyes adatok címzettjei: a Társaság ügyvezetője, ügyfélkiszolgálással kapcsolatos feladatokat ellátó munkavállalói, kapcsolattartói, könyvelési, adózási, üzletkötési feladatokat ellátó munkavállalói, és adatfeldolgozói.</p>

<p>4.6. A személyes adatok adatfeldolgozásra átadásra kerülhetnek adózás, könyvelés céljából a társaság által megbízott könyvelő irodának, postázás szállítás céljából a megbízott futárszolgálatnak, vagyonvédelem céljából a társaság vagyonvédelmi megbízottjának.</p>

<p>4.7. Az adatkezelés jogszerűnek minősül, ha arra valamely szerződés vagy szerződéskötési szándék keretében van szükség (Preambulum 44.) ha az a szerződés megkötését megelőzően az érintett kérésére történő lépések megtételéhez szükséges (6 cikk (1) b./). Így szerződés teljesítése jogcímén az e pontban írtak szerint kezelhetők a szerződési ajánlatok keretében gyűjtött személyes adatok is. Ajánlattételkor, illetve fogadáskor erről a Társaság köteles az ajánlattevőt, illetve az ajánlat címzettjét tájékoztatni kell.</p>

<p>&nbsp;</p>

<p><strong>Tájékoztatás jogi kötelezettség teljesítésén alapuló adatkezelésről</strong></p>

<p>5.1. A jogi kötelezettségen alapuló adatkezelés esetén a kezelhető adatok körére, az adatkezelés céljára, az adatok tárolásának időtartamára, a címzettekre az alapul szolgáló jogszabály rendelkezései irányadók.</p>

<p>5.2. A jogi kötelezettség teljesítése jogcímén alapuló adatkezelés az érintett hozzájárulásától független, mivel az adatkezelést jogszabály határozza meg. Az érintettel az adatkezelés megkezdése előtt ezesetben közölni kell, hogy az adatkezelés kötelező, továbbá az érintettet az adatkezelés megkezdése előtt egyértelműen és részletesen tájékoztatni kell az adatai kezelésével kapcsolatos minden tényről, így különösen az adatkezelés céljáról és jogalapjáról, az adatkezelésre és az adatfeldolgozásra jogosult személyéről, az adatkezelés időtartamáról, arról, ha az érintett személyes adatait az adatkezelő a rá vonatkozó jogi kötelezettség alapján kezeli, illetve arról, hogy kik ismerhetik meg az adatokat. A tájékoztatásnak ki kell terjednie az érintett adatkezeléssel kapcsolatos jogaira és jogorvoslati lehetőségeire is. Kötelező adatkezelés esetén a tájékoztatás megtörténhet az előbbi információkat tartalmazó jogszabályi rendelkezésekre való utalás nyilvánosságra hozatalával is.</p>

<p>&nbsp;</p>

<p><strong>6. Tájékoztatás adó- és számviteli kötelezettségek teljesítése céljából végzett adatkezelésről</strong></p>

<p>6.1. A Társaság jogi kötelezettség teljesítése jogcímén, törvényben előírt adó és számviteli kötelezettségek teljesítése (könyvelés, adózás) céljából kezeli a vele üzleti kapcsolatba lépő természetes személyek törvényben meghatározott adatait. A kezelt adatok az általános forgalmi adóról szóló 2017. évi CXXVII. tv. 169.§, és 202.§-a alapján különösen: adószám, név, cím, adózási státusz, a számvitelről szóló 2000. évi C. törvény 167.§-a alapján: név, cím, a gazdasági műveletet elrendelő személy vagy szervezet megjelölése, az utalványozó és a rendelkezés végrehajtását igazoló személy, valamint a szervezettől függően az ellenőr aláírása; a készletmozgások bizonylatain és a pénzkezelési bizonylatokon az átvevő, az ellennyugtákon a befizető aláírása, a személyi jövedelemadóról szóló 1995. évi CXVII. törvény alapján: vállalkozói igazolvány száma, őstermelői igazolvány száma, adóazonosító jel.</p>

<p>6.2. A személyes adatok tárolásának időtartama a jogalapot adó jogviszony megszűnését követő 8 év.</p>

<p>6.3. A személyes adatok címzettjei: a Társaság adózási, könyvviteli, bérszámfejtési, társadalombiztosítási feladatait ellátó munkavállalói és adatfeldolgozói.</p>

<p>&nbsp;</p>

<p><strong>7. </strong><strong>Az érintett vagy más természetes személy létfontosságú érdekeinek védelme miatti adatkezelés</strong></p>

<p>(1) Az érintett életének vagy más létfontosságú érdekének vagy más természetes személy érdekeinek védelme szintén jogalapot teremthet az adatkezelésre. Ilyen eset áll fenn természetes személy esetén, ha egészségügyi szolgáltatást vesz igénybe vagy járványok terjedésének megállítása miatt kerül sor az adatkezelésre.</p>

<p>&nbsp;</p>

<p><strong>8. Tájékoztatás a Levéltári törvény szerint maradandó értékű iratokra vonatkozó adatkezelésről</strong></p>

<p>8.1. A Társaság jogi kötelezettsége teljesítése jogcímén kezeli a köziratokról, a közlevéltárakról és a magánlevéltári anyag védelméről szóló 1995. évi LXVI. törvény (Levéltári törvény) szerint maradandó értékűnek minősülő iratait abból a célból, hogy a Társaság irattári anyagának maradandó értékű része épségben és használható állapotban a jövő nemzedékei számára is fennmaradjon. Az adattárolás ideje: a közlevéltár részére történő átadásig.</p>

<p>8.2. A személyes adatok címzettjei: a Társaság vezetője, iratkezelést, irattározást végző munkavállalója, a közlevéltár munkatársa.</p>

<p>&nbsp;</p>

<p><strong>9. Az érintett jogainak elősegítése</strong></p>

<p>9.1. A Társaság valamennyi adatkezelése során köteles biztosítani az érintett jogainak gyakorlását.</p>

<p>&nbsp;</p>

<p><strong>IV. FEJEZET</strong></p>

<p><strong>HONLAPON MEGADOTT SZEMÉLYES ADATOK KEZELÉSE</strong></p>

<p><strong>1.</strong> Az Adatkezelés célja:</p>

<ul>
	<li>
	<p>megrendelések teljesítése (név, szállítási cím);</p>
	</li>
	<li>
	<p>a szolgáltatás működésének ellenőrzése (név, telefonszám, e-mail cím);</p>
	</li>
	<li>
	<p>a visszaélések megakadályozása (név, telefonszám, e-mail cím);</p>
	</li>
	<li>
	<p>a Felhasználók azonosítása és egymástól való megkülönböztetése (név, telefonszám, szállítási cím, számlázási cím, e-mail cím, felhasználónév, jelszó);</p>
	</li>
	<li>
	<p>kapcsolatfelvétel (név, telefonszám, e-mail cím);</p>
	</li>
	<li>
	<p>statisztikák készítése (álnevesítéssel megrendelésenként);</p>
	</li>
	<li>
	<p>reklámüzenetek célzott küldése (név, e-mail cím);</p>
	</li>
	<li>
	<p>a Felhasználókkal (ügyfelekkel) fennálló jogviszonyhoz kapcsolódó jogok gyakorlása (név, számlázási cím, telefonszám, e-mail cím);</p>
	</li>
	<li>
	<p>kötelezettségek teljesítése (név, szállítási cím, számlázási cím, születési dátum, telefonszám, e-mail cím);</p>
	</li>
	<li>
	<p>a számla kiállítása (név, számlázási cím);</p>
	</li>
	<li>
	<p>a forgalom és a felhasználói szokások figyelemmel kísérése és rögzítése, ezáltal a Honlap Felhasználóinak személyre szabott hirdetések ajánlása (név, megrendelésadatok);</p>
	</li>
</ul>

<p><strong>2.</strong> A Felhasználók az adataik megadásával a Honlap használata során elektronikus formában a regisztráció / megrendelés / Felhasználói elektronikus információ kérés folyamatában tevőlegesen, a hírlevélre feliratkozás aláírásával / tickbox bepipálásával hozzájárulásukat adhatják ahhoz, hogy az Üzemeltető a megadott elérhetőségeken direkt marketing ajánlattal, elektronikus hirdetéssel (hírlevél, e-mail, SMS stb.) felkeresse őket. A hozzájárulás bármikor ingyenesen, korlátozástól mentesen és indokolás nélkül visszavonható, továbbá az elektronikus hirdetésben esetlegesen megjelölt módon szintén lehetőség van a hozzájárulás visszavonására. A hozzájárulás visszavonható továbbá az Üzemeltetőnek címzett és az Üzemeltető székhelyére postai úton eljuttatott nyilatkozattal is. Folyamatban lévő megrendelés esetén a jelen pontban foglalt (hírlevéllel kapcsolatos) adatkezelési hozzájárulás visszavonása a megrendelés teljesítését nem érinti. A GDPR 7. cikk (3) bekezdés, valamint 13. cikk (2) bekezdés c) pontja alapján a hozzájárulás visszavonása az azt megelőző adatkezelés jogszerűségét nem érinti.</p>

<p><strong>3.</strong> Minden olyan esetben, ha a szolgáltatott személyes adatokat Üzemeltető az eredeti adatfelvétel céljától eltérő célra kívánja felhasználni, erről a Felhasználót tájékoztatja, és ehhez előzetes, kifejezett hozzájárulását megszerzi, illetőleg lehetőséget biztosít számára, hogy a felhasználást megtiltsa.</p>

<p>&nbsp;</p>

<p><strong>V. FEJEZET</strong></p>

<p><strong>LÁTOGATÓI ADATKEZELÉS A TÁRSASÁG HONLAPJÁN - </strong><strong>TÁJÉKOZTATÁS SÜTIK (COOKIE) ALKALMAZÁSÁRÓL</strong></p>

<p>1. A honlapra látogatót a honlapon tájékoztatni kell a sütik alkalmazásáról, és ehhez – a technikailag elengedhetetlenül szükséges <strong>munkamenet (session) sütik kivételével – </strong>a hozzájárulását kell kérni.</p>

<p><strong>2. Általános tájékoztatás a sütikről </strong></p>

<p>2.1. A süti (angolul „cookie”) egy olyan adat, amit a meglátogatott weboldal küld a látogató böngészőjének (változónév-érték formában), hogy az eltárolja és később ugyanaz a weboldal be is tudja tölteni a tartalmát. A sütinek lehet érvényessége, érvényes lehet a böngésző bezárásáig, de korlátlan ideig is. A későbbiekben minden HTTP(S) kérésnél ezeket az adatokat is elküldi a böngésző a szervernek. Ezáltal a felhasználó gépén lévő adatokat módosítja.</p>

<p>2.2. A modern weboldalszolgáltatások természeténél fogva szükség van sütikre, melynek funkciója, hogy egy felhasználót megjelöljön (például, hogy belépett az oldalra) és annak megfelelően tudja a következőkben kezelni, akár későbbi visszatérésekor azonosítani. A veszélye abban rejlik, hogy erről a felhasználónak nem minden esetben van tudomása és alkalmas lehet arra, hogy felhasználót kövesse a weboldal üzemeltetője vagy más szolgáltató, akinek a tartalma be van építve az oldalban (pl. Facebook, Google Analytics), ezáltal profil jön létre róla, ebben az esetben pedig a süti tartalma személyes adatnak tekinthető.</p>

<p><strong>2.3. A sütik fajtái:</strong></p>

<p>2.3.1. Technikailag elengedhetetlenül szükséges munkamenet (session) sütik: amelyek nélkül az oldal egyszerűen nem működne funkcionálisan, ezek a felhasználó azonosításához, pl. annak kezeléséhez szükséges, hogy belépett-e, mit tett a kosárba stb. Ez jellemzően egy session-id letárolása, a többi adat a szerveren kerül tárolásra, ami így biztonságosabb. Van biztonsági vonatkozása, ha a session süti értéke nem jól van generálva, akkor session-hijacking támadás veszélye fennáll, ezért feltétlenül szükséges, hogy megfelelően legyenek ezek az értékek generálva. Más terminológiák session cookie-nak hívnak minden sütit, amik a böngészőből való kilépéskor törlődnek (egy session egy böngészőhasználat az elindítástól a kilépésig).</p>

<p>2.3.2. Használatot elősegítő sütik: így azokat a sütiket szoktál nevezni, amelyek megjegyzik a felhasználó választásait, például milyen formában szeretné a felhasználó az oldalt látni. Ezek a fajta sütik lényegében a sütiben tárolt beállítási adatokat jelentik.</p>

<p>2.3.3. Teljesítményt biztosító sütik: bár nem sok közük van a "teljesítmény"-hez, általában így nevezik azokat a sütiket, amelyek információkat gyűjtenek a felhasználónak a meglátogatott weboldalon belüli viselkedéséről, eltöltött idejéről, kattintásairól. Ezek jellemzően harmadik fél alkalmazásai (pl. Google Analytics, AdWords, vagy Yandex.ru sütik). Ezek a látogatóról profilalkotás készítésére alkalmasak.</p>

<p>A Google Analytics sütikről itt tájékozódhat:</p>

<p>https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage</p>

<p>A Google AdWords sütikről itt tájékozódhat:</p>

<p>https://support.google.com/adwords/answer/2407785?hl=hu</p>

<p>2.4. A sütik használatát elfogadni, azokat engedélyezni nem kötelező. Visszaállíthatja böngészője beállításait, hogy az utasítsa el az összes cookie-t, vagy hogy jelezze, ha a rendszer éppen egy cookie-t küld. A legtöbb böngésző ugyan alapértelmezettként automatikusan elfogadja a sütiket, de ezek általában megváltoztathatóak annak érdekében, hogy megakadályozható legyen az automatikus elfogadás.</p>

<p>A legnépszerűbb böngészők süti beállításairól az alábbi linkeken tájékozódhat</p>

<p>• Google Chrome: https://support.google.com/accounts/answer/61416?hl=hu</p>

<p>• Firefox: https://support.mozilla.org/hu/kb/sutik-engedelyezese-es-tiltasa-amit-weboldak-haszn</p>

<p>• Microsoft Internet Explorer 11: http://windows.microsoft.com/hu-hu/internet-explorer/delete-manage-cookies#ie=ie-11</p>

<p>• Microsoft Internet Explorer 10: http://windows.microsoft.com/hu-hu/internet-explorer/delete-manage-cookies#ie=ie-10-win-7</p>

<p>• Microsoft Internet Explorer 9: http://windows.microsoft.com/hu-hu/internet-explorer/delete-manage-cookies#ie=ie-9</p>

<p>• Microsoft Internet Explorer 8: http://windows.microsoft.com/hu-hu/internet-explorer/delete-manage-cookies#ie=ie-8</p>

<p>• Microsoft Edge: http://windows.microsoft.com/hu-hu/windows-10/edge-privacy-faq</p>

<p>• Safari: https://support.apple.com/hu-hu/HT201265</p>

<p>Mindezek mellett azonban felhívjuk a figyelmet arra, hogy előfordulhat, hogy bizonyos webhelyfunkciók vagy -szolgáltatások nem fognak megfelelően működni cookie-k nélkül.</p>

<p>&nbsp;</p>

<p><strong>3. Tájékoztatás a Társaság honlapján alkalmazott sütikről, illetve </strong><em><strong>a látogatás során létrejövő adatokról</strong></em></p>

<p>3.1. A látogatás során kezelt adatkör: Társaságunk honlapja a weboldal használata során a látogatóról, illetve az általa böngészésre használt eszközről az alábbi adatokat rögzítheti és kezelheti:<br />
• az látogató által használt IP cím,<br />
• a böngésző típusa,<br />
• a böngészésre használt eszköz operációs rendszerének jellemzői (beállított nyelv),<br />
• látogatás időpontja,<br />
• a meglátogatott (al)oldal, funkció vagy szolgáltatás.</p>

<p>• kattintás.</p>

<p><em>Ezeket az adatokat maximum 90 napig őrizzük meg és elsősorban biztonsági incidensek vizsgálatához használhatjuk.</em></p>

<p>&nbsp;</p>

<p><strong>3.2. A honlapon alkalmazott sütik</strong></p>

<p><strong>3.2.1. Technikailag elengedhetetlenül szükséges </strong><strong>munkamenet (session) sütik</strong></p>

<p>Az adatkezelés célja: a honlap megfelelő működésének biztosítása. Ezek a sütik ahhoz szükségesek, hogy a látogatók böngészhessék a weboldalt, zökkenőmentesen és teljes körűen használhassák annak funkcióit, a weboldalon keresztül elérhető szolgáltatásokat, így - többek között- különösen a látogató által az adott oldalakon végzett műveletek megjegyzését <em>vagy a bejelentkezett felhasználó azonosítását</em> egy látogatás során. Ezen sütik adatkezelésének időtartama kizárólag a látogató aktuális látogatására vonatkozik, a munkamenet végeztével, illetve a böngésző bezárásával a sütik e fajtája automatikusan törlődik a számítógépéről.</p>

<p>Ezen adatkezelés jogalapja az elektronikus kereskedelmi szolgáltatások, valamint az információs társadalmi szolgáltatások egyes kérdéseiről szóló 2001. CVIII. törvény (Elkertv.) 13/A. § (3) bekezdése, amely szerint a szolgáltató a szolgáltatás nyújtása céljából kezelheti azon személyes adatokat, amelyek a szolgáltatás nyújtásához technikailag elengedhetetlenül szükségesek. A szolgáltatónak az egyéb feltételek azonossága esetén úgy kell megválasztania és minden esetben oly módon kell üzemeltetnie az információs társadalommal összefüggő szolgáltatás nyújtása során alkalmazott eszközöket, hogy személyes adatok kezelésére csak akkor kerüljön sor, ha ez a szolgáltatás nyújtásához és az e törvényben meghatározott egyéb célok teljesüléséhez feltétlenül szükséges, azonban ebben az esetben is csak a szükséges mértékben és ideig.</p>

<p>&nbsp;</p>

<p><strong>3.2.1. Használatot elősegítő sütik:</strong></p>

<p>Ezek megjegyzik a felhasználó választásait, például milyen formában szeretné a felhasználó az oldalt látni. Ezek a fajta sütik lényegében a sütiben tárolt beállítási adatokat jelentik.</p>

<p>Az adatkezelés jogalapja a látogató hozzájárulása.</p>

<p><strong>Az adatkezelés célja: </strong>A szolgáltatás hatékonyságának növelése, felhasználói élmény növelése, a honlap használatának kényelmesebbé tétele.</p>

<p><em>Ez az adat jellemzően a felhasználó gépén van, a weboldal csak hozzáfér és felismer(het)i általa a látogatót. </em></p>

<p><strong>3.2.2. Teljesítményt biztosító sütik: </strong></p>

<p>Információkat gyűjtenek a felhasználónak a meglátogatott weboldalon belüli viselkedéséről, eltöltött idejéről, kattintásairól.</p>

<p>Az adatkezelés jogalapja: az érintett hozzájárulása.</p>

<p>Az adatkezelés célja: a honlap elemzése, reklámajánlatok küldése.</p>

<p>&nbsp;</p>

<p><strong>VI. FEJEZET</strong></p>

<p><strong>TÁJÉKOZTATÁS AZ ÉRINTETT SZEMÉLY JOGAIRÓL</strong></p>

<p><strong>I. Az érintett jogai röviden összefoglalva:</strong></p>

<p>1. Átlátható tájékoztatás, kommunikáció és az érintett joggyakorlásának elősegítése</p>

<p>2. Előzetes tájékozódáshoz való jog – ha a személyes adatokat az érintettől gyűjtik</p>

<p>3. Az érintett tájékoztatása és a rendelkezésére bocsátandó információk, ha a személyes adatokat az adatkezelő nem tőle szerezte meg</p>

<p>4. Az érintett hozzáférési joga</p>

<p>5. A helyesbítéshez való jog</p>

<p>6. A törléshez való jog („az elfeledtetéshez való jog”)</p>

<p>7. Az adatkezelés korlátozásához való jog</p>

<p>8. A személyes adatok helyesbítéséhez vagy törléséhez, illetve az adatkezelés korlátozásához kapcsolódó értesítési kötelezettség</p>

<p>9. Az adathordozhatósághoz való jog</p>

<p>10. A tiltakozáshoz való jog</p>

<p>11. Automatizált döntéshozatal egyedi ügyekben, beleértve a profilalkotást</p>

<p>12. Korlátozások</p>

<p>13. Az érintett tájékoztatása az adatvédelmi incidensről</p>

<p>14. A felügyeleti hatóságnál történő panasztételhez való jog (hatósági jogorvoslathoz való jog)</p>

<p>15. A felügyeleti hatósággal szembeni hatékony bírósági jogorvoslathoz való jog</p>

<p>16. Az adatkezelővel vagy az adatfeldolgozóval szembeni hatékony bírósági jogorvoslathoz való jog</p>

<p>&nbsp;</p>

<p><strong>II. Az érintett jogai részletesen: </strong></p>

<p><strong>1. Átlátható tájékoztatás, kommunikáció és az érintett joggyakorlásának elősegítése </strong></p>

<p><strong>1.1.</strong> Az adatkezelőnek az érintett részére a személyes adatok kezelésére vonatkozó valamennyi információt és minden egyes tájékoztatást tömör, átlátható, érthető és könnyen hozzáférhető formában, világosan és közérthetően megfogalmazva kell nyújtania, különösen a gyermekeknek címzett bármely információ esetében. Az információkat írásban vagy más módon – ideértve adott esetben az elektronikus utat is – kell megadni. Az érintett kérésére szóbeli tájékoztatás is adható, feltéve, hogy más módon igazolták az érintett személyazonosságát.</p>

<p><strong>1.2.</strong> Az adatkezelőnek elő kell segítenie az érintett jogainak a gyakorlását.</p>

<p><strong>1.3.</strong> Az adatkezelő indokolatlan késedelem nélkül, de mindenféleképpen a kérelem beérkezésétől számított egy hónapon belül tájékoztatja az érintettet a jogai gyakorlására irányuló kérelme nyomán hozott intézkedésekről. E határidő a Rendeletben írt feltételekkel további két hónappal meghosszabbítható. amelyről az érintettet tájékoztatni kell.</p>

<p><strong>1.4.</strong> Ha az adatkezelő nem tesz intézkedéseket az érintett kérelme nyomán, késedelem nélkül, de legkésőbb a kérelem beérkezésétől számított egy hónapon belül tájékoztatja az érintettet az intézkedés elmaradásának okairól, valamint arról, hogy az érintett panaszt nyújthat be valamely felügyeleti hatóságnál, és élhet bírósági jogorvoslati jogával.</p>

<p><strong>1.5.</strong> Az adatkezelő az információkat és az érintett jogairól szóló tájékoztatást és intézkedést díjmentesen biztosítja, azonban a Rendeletben írt esetekben díj számítható fel.</p>

<p>A részletes szabályok a Rendelet 12 cikke alatt találhatók.</p>

<p>&nbsp;</p>

<p><strong>2. Előzetes tájékozódáshoz való jog – ha </strong><strong>a személyes adatokat az érintettől gyűjtik</strong></p>

<p><strong>2.1.</strong> Az érintett jogosult arra, hogy az adatkezeléssel összefüggő tényekről és információkról az adatkezelés megkezdését megelőzően tájékoztatást kapjon. Ennek keretében az érintettet tájékoztatni kell:</p>

<p>a) az adatkezelő és képviselője kilétéről és elérhetőségeiről,</p>

<p>b) az adatvédelmi tisztviselő elérhetőségeiről (ha van ilyen),</p>

<p>c) a személyes adatok tervezett kezelésének céljáról, valamint az adatkezelés jogalapjáról,</p>

<p>d) jogos érdek érvényesítésén alapuló adatkezelés esetén, az adatkezelő vagy harmadik fél jogos érdekeiről,</p>

<p>e) a személyes adatok címzettjeiről – akikkel a személyes adatot közlik -, illetve a címzettek kategóriáiról, ha van ilyen;</p>

<p>e) adott esetben annak tényéről, hogy az adatkezelő harmadik országba vagy nemzetközi szervezet részére kívánja továbbítani a személyes adatokat.</p>

<p>&nbsp;</p>

<p><strong>2.2.</strong> A tisztességes és átlátható adatkezelés biztosítsa érdekében az adatkezelőnek az érintettet a következő kiegészítő információkról kell tájékoztatnia:</p>

<p>a) a személyes adatok tárolásának időtartamáról, vagy ha ez nem lehetséges, ezen időtartam meghatározásának szempontjairól;</p>

<p>b) az érintett azon jogáról, hogy kérelmezheti az adatkezelőtől a rá vonatkozó személyes adatokhoz való hozzáférést, azok helyesbítését, törlését vagy kezelésének korlátozását, és tiltakozhat az ilyen személyes adatok kezelése ellen, valamint az érintett adathordozhatósághoz való jogáról;</p>

<p>c) az érintett hozzájárulásán alapuló adatkezelés esetén arról, hogy a hozzájárulás bármely időpontban történő visszavonásához való jog, amely nem érinti a visszavonás előtt a hozzájárulás alapján végrehajtott adatkezelés jogszerűségét;</p>

<p>d) a felügyeleti hatósághoz címzett panasz benyújtásának jogáról;</p>

<p>e) arról, hogy a személyes adat szolgáltatása jogszabályon vagy szerződéses kötelezettségen alapul vagy szerződés kötésének előfeltétele-e, valamint, hogy az érintett köteles-e a személyes adatokat megadni, továbbá, hogy milyen lehetséges következményeikkel járhat az adatszolgáltatás elmaradása;</p>

<p>f) az automatizált döntéshozatal tényéről, ideértve a profilalkotást is, valamint legalább ezekben az esetekben az alkalmazott logikáról, és arra vonatkozóan érthető információkról, hogy az ilyen adatkezelés milyen jelentőséggel, és az érintettre nézve milyen várható következményekkel bír.</p>

<p>&nbsp;</p>

<p><strong>2.3.</strong> Ha az adatkezelő a személyes adatokon a gyűjtésük céljától eltérő célból további adatkezelést kíván végezni, a további adatkezelést megelőzően tájékoztatnia kell az érintettet erről az eltérő célról és minden releváns kiegészítő információról.</p>

<p>Az előzetes tájékozódáshoz való jog részletes szabályait a Rendelet 13. cikke tartalmazza.</p>

<p>&nbsp;</p>

<p><strong>3. Az érintett tájékoztatása és a rendelkezésére bocsátandó információk, ha a személyes adatokat az adatkezelő nem tőle szerezte meg</strong></p>

<p><strong>3.1.</strong> Ha az adatkezelő a személyes adatokat nem az érintettől szerezte meg, az érintettet az adatkezelőnek a személyes adatok megszerzésétől számított legkésőbb egy hónapon belül; ha a személyes adatokat az érintettel való kapcsolattartás céljára használják, legalább az érintettel való első kapcsolatfelvétel alkalmával; vagy ha várhatóan más címzettel is közlik az adatokat, legkésőbb a személyes adatok első alkalommal való közlésekor tájékoztatnia kell az előbbi 2. pontban írt tényekről és információkról, továbbá az érintett személyes adatok kategóriáiról, valamint a személyes adatok forrásáról és adott esetben arról, hogy az adatok nyilvánosan hozzáférhető forrásokból származnak-e.</p>

<p><strong>3.2.</strong> A további szabályokra az előbbi 2. pontban (Előzetes tájékozódáshoz való jog) írtak irányadók.</p>

<p>E tájékoztatás részletes szabályait a Rendelet 14. cikke tartalmazza.</p>

<p>&nbsp;</p>

<p><strong>4. Az érintett hozzáférési joga</strong></p>

<p><strong>4.1.</strong> Az érintett jogosult arra, hogy az adatkezelőtől visszajelzést kapjon arra vonatkozóan, hogy személyes adatainak kezelése folyamatban van-e, és ha ilyen adatkezelés folyamatban van, jogosult arra, hogy a személyes adatokhoz és az előbbi 2-3. pontban írt kapcsolódó információkhoz hozzáférést kapjon. (Rendelet 15. cikk).</p>

<p><strong>4.2.</strong> Ha személyes adatoknak harmadik országba vagy nemzetközi szervezet részére történő továbbítására kerül sor, az érintett jogosult arra, hogy tájékoztatást kapjon a továbbításra vonatkozóan a Rendelet 46. cikk szerinti megfelelő garanciákról.</p>

<p><strong>4.3.</strong> Az adatkezelőnek az adatkezelés tárgyát képező személyes adatok másolatát az érintett rendelkezésére kell bocsátania. Az érintett által kért további másolatokért az adatkezelő az adminisztratív költségeken alapuló, észszerű mértékű díjat számíthat fel.</p>

<p>Az érintett hozzáférési jogára vonatkozó részletes szabályokat a Rendelt 15. cikke tartalmazza.</p>

<p>&nbsp;</p>

<p><strong>5. A helyesbítéshez való jog</strong></p>

<p><strong>5.1.</strong> Az érintett jogosult arra, hogy kérésére az Adatkezelő indokolatlan késedelem nélkül helyesbítse a rá vonatkozó pontatlan személyes adatokat.</p>

<p><strong>5.2.</strong> Figyelembe véve az adatkezelés célját, az érintett jogosult arra, hogy kérje a hiányos személyes adatok – egyebek mellett kiegészítő nyilatkozat útján történő – kiegészítését is.</p>

<p>Ezen szabályokat a Rendelet 16. cikke tartalmazza.</p>

<p>&nbsp;</p>

<p><strong>6. A törléshez való jog („az elfeledtetéshez való jog”)</strong></p>

<p><strong>6.1.</strong> Az érintett jogosult arra, hogy kérésére az adatkezelő indokolatlan késedelem nélkül törölje a rá vonatkozó személyes adatokat, az adatkezelő pedig köteles arra, hogy az érintettre vonatkozó személyes adatokat indokolatlan késedelem nélkül törölje ha</p>

<p>a) a személyes adatokra már nincs szükség abból a célból, amelyből azokat gyűjtötték vagy más módon kezelték;</p>

<p>b) az érintett visszavonja az adatkezelés alapját képező hozzájárulását, és az adatkezelésnek nincs más jogalapja;</p>

<p>c) az érintett tiltakozik az adatkezelése ellen, és nincs elsőbbséget élvező jogszerű ok az adatkezelésre,</p>

<p>d) a személyes adatokat jogellenesen kezelték;</p>

<p>e) a személyes adatokat az adatkezelőre alkalmazandó uniós vagy tagállami jogban előírt jogi kötelezettség teljesítéséhez törölni kell;</p>

<p>f) a személyes adatok gyűjtésére közvetlenül gyermeknek kínált, információs társadalommal összefüggő szolgáltatások kínálásával kapcsolatosan került sor.</p>

<p><strong>6.2. </strong>A törléshez való jog nem érvényesíthető, ha az adatkezelés szükséges</p>

<p>a) a véleménynyilvánítás szabadságához és a tájékozódáshoz való jog gyakorlása céljából;</p>

<p>b) az adatkezelőre alkalmazandó uniós vagy tagállami jog szerinti kötelezettség teljesítése, illetve közérdekből vagy az adatkezelőre ruházott közhatalmi jogosítvány gyakorlása keretében végzett feladat végrehajtása céljából;</p>

<p>c) a népegészségügy területét érintő közérdek alapján;</p>

<p>d) a közérdekű archiválás céljából, tudományos és történelmi kutatási célból vagy statisztikai célból, amennyiben a törléshez való jog valószínűsíthetően lehetetlenné tenné vagy komolyan veszélyeztetné ezt az adatkezelést; vagy</p>

<p>e) jogi igények előterjesztéséhez, érvényesítéséhez, illetve védelméhez.</p>

<p>A törléshez való jogra vonatkozó részletes szabályokat a Rendelet 17. cikke tartalmazza.</p>

<p>&nbsp;</p>

<p><strong>7.</strong> <strong>Az adatkezelés korlátozásához való jog</strong></p>

<p><strong>7.1.</strong> Az adatkezelés korlátozása esetén az ilyen személyes adatokat a tárolás kivételével csak az érintett hozzájárulásával, vagy jogi igények előterjesztéséhez, érvényesítéséhez vagy védelméhez, vagy más természetes vagy jogi személy jogainak védelme érdekében, vagy az Unió, illetve valamely tagállam fontos közérdekéből lehet kezelni.</p>

<p><strong>7.2.</strong> Az érintett jogosult arra, hogy kérésére az Adatkezelő korlátozza az adatkezelést, ha az alábbiak valamelyike teljesül:</p>

<p>a) az érintett vitatja a személyes adatok pontosságát, ez esetben a korlátozás arra az időtartamra vonatkozik, amely lehetővé teszi, hogy az Adatkezelő ellenőrizze a személyes adatok pontosságát;</p>

<p>b) az adatkezelés jogellenes, és az érintett ellenzi az adatok törlését, és e helyett kéri azok felhasználásának korlátozását;</p>

<p>c) az Adatkezelőnek már nincs szüksége a személyes adatokra adatkezelés céljából, de az érintett igényli azokat jogi igények előterjesztéséhez, érvényesítéséhez vagy védelméhez; vagy</p>

<p>d) az érintett tiltakozott az adatkezelés ellen; ez esetben a korlátozás arra az időtartamra vonatkozik, amíg megállapításra nem kerül, hogy az adatkezelő jogos indokai elsőbbséget élveznek-e az érintett jogos indokaival szemben.</p>

<p><strong>7.3. </strong>Az adatkezelés korlátozásának feloldásáról az érintettet előzetesen tájékoztatni kell.</p>

<p>A vonatkozó szabályokat a Rendelet 18. cikke tartalmazza.</p>

<p>&nbsp;</p>

<p><strong>8. A személyes adatok helyesbítéséhez vagy törléséhez, illetve az adatkezelés korlátozásához kapcsolódó értesítési kötelezettség</strong></p>

<p>Az adatkezelő minden olyan címzettet tájékoztat valamennyi helyesbítésről, törlésről vagy adatkezelés-korlátozásról, akivel, illetve amellyel a személyes adatot közölték, kivéve, ha ez lehetetlennek bizonyul, vagy aránytalanul nagy erőfeszítést igényel. Az érintettet kérésére az adatkezelő tájékoztatja e címzettekről.</p>

<p>E szabályok a Rendelet 19. cikke alatt találhatók.</p>

<p>&nbsp;</p>

<p><strong>9. Az adathordozhatósághoz való jog</strong></p>

<p><strong>9.1.</strong> A Rendeletben írt feltételekkel az érintett jogosult arra, hogy a rá vonatkozó, általa egy adatkezelő rendelkezésére bocsátott személyes adatokat tagolt, széles körben használt, géppel olvasható formátumban megkapja, továbbá jogosult arra, hogy ezeket az adatokat egy másik adatkezelőnek továbbítsa anélkül, hogy ezt akadályozná az az adatkezelő, amelynek a személyes adatokat a rendelkezésére bocsátotta, ha</p>

<p>a) az adatkezelés hozzájáruláson vagy szerződésen alapul; és</p>

<p>b) az adatkezelés automatizált módon történik.</p>

<p><strong>9.2.</strong> Az érintett kérheti a személyes adatok adatkezelők közötti közvetlen továbbítását is.</p>

<p><strong>9.3</strong>. Az adathordozhatósághoz való jog gyakorlása nem sértheti a Rendelet 17. cikkét (A törléshez való jog („az elfeledtetéshez való jog”). Az adtahordozhatósághoz való jog nem alkalmazandó abban az esetben, ha az adatkezelés közérdekű vagy az adatkezelőre ruházott közhatalmi jogosítványai gyakorlásának keretében végzett feladat végrehajtásához szükséges. E jog nem érintheti hátrányosan mások jogait és szabadságait.</p>

<p>A részletes szabályokat a Rendelet 20. cikke tartalmazza.</p>

<p>&nbsp;</p>

<p><strong>10. A tiltakozáshoz való jog</strong></p>

<p><strong>10.1.</strong> Az érintett jogosult arra, hogy a saját helyzetével kapcsolatos okokból bármikor tiltakozzon személyes adatainak közérdeken, közfeladat végrehajtásán (6. cikk (1) e)), vagy jogos érdeken (6. cikk f)) alapuló kezelése ellen, ideértve az említett rendelkezéseken alapuló profilalkotást is. Ebben az esetben az adatkezelő a személyes adatokat nem kezelheti tovább, kivéve, ha az adatkezelő bizonyítja, hogy az adatkezelést olyan kényszerítő erejű jogos okok indokolják, amelyek elsőbbséget élveznek az érintett érdekeivel, jogaival és szabadságaival szemben, vagy amelyek jogi igények előterjesztéséhez, érvényesítéséhez vagy védelméhez kapcsolódnak.</p>

<p><strong>10.2.</strong> Ha a személyes adatok kezelése közvetlen üzletszerzés érdekében történik, az érintett jogosult arra, hogy bármikor tiltakozzon a rá vonatkozó személyes adatok e célból történő kezelése ellen, ideértve a profilalkotást is, amennyiben az a közvetlen üzletszerzéshez kapcsolódik. &nbsp;Ha az érintett tiltakozik a személyes adatok közvetlen üzletszerzés érdekében történő kezelése ellen, akkor a személyes adatok a továbbiakban e célból nem kezelhetők.</p>

<p><strong>10.3. </strong>Ezen jogokra legkésőbb az érintettel való első kapcsolatfelvétel során kifejezetten fel kell hívni annak figyelmét, és az erre vonatkozó tájékoztatást egyértelműen és minden más információtól elkülönítve kell megjeleníteni.</p>

<p><strong>10.4.</strong> Az érintett a tiltakozáshoz való jogot műszaki előírásokon alapuló automatizált eszközökkel is gyakorolhatja.</p>

<p><strong>10.5.</strong> Ha a személyes adatok kezelésére tudományos és történelmi kutatási célból vagy statisztikai célból kerül sor, az érintett jogosult arra, hogy a saját helyzetével kapcsolatos okokból tiltakozhasson a rá vonatkozó személyes adatok kezelése ellen, kivéve, ha az adatkezelésre közérdekű okból végzett feladat végrehajtása érdekében van szükség.</p>

<p>&nbsp;</p>

<p>A vonatkozó szabályokat a Rendelet cikke tartalmazza.</p>

<p>&nbsp;</p>

<p><strong>11. Automatizált döntéshozatal egyedi ügyekben, beleértve a profilalkotást</strong></p>

<p><strong>11.1.</strong> Az érintett jogosult arra, hogy ne terjedjen ki rá az olyan, kizárólag automatizált adatkezelésen – ideértve a profilalkotást is – alapuló döntés hatálya, amely rá nézve joghatással járna vagy őt hasonlóképpen jelentős mértékben érintené.</p>

<p><strong>11.2.</strong> Ez a jogosultság nem alkalmazandó abban az esetben, ha a döntés:</p>

<p>a) az érintett és az adatkezelő közötti szerződés megkötése vagy teljesítése érdekében szükséges;</p>

<p>b) meghozatalát az adatkezelőre alkalmazandó olyan uniós vagy tagállami jog teszi lehetővé, amely az érintett jogainak és szabadságainak, valamint jogos érdekeinek védelmét szolgáló megfelelő intézkedéseket is megállapít; vagy</p>

<p>c) az érintett kifejezett hozzájárulásán alapul.</p>

<p><strong>11.3. </strong>Az előbbi a) és c) pontjában említett esetekben az adatkezelő köteles megfelelő intézkedéseket tenni az érintett jogainak, szabadságainak és jogos érdekeinek védelme érdekében, ideértve az érintettnek legalább azt a jogát, hogy az adatkezelő részéről emberi beavatkozást kérjen, álláspontját kifejezze, és a döntéssel szemben kifogást nyújtson be.</p>

<p>&nbsp;</p>

<p>A további szabályokat a Rendelet 22. cikke tartalmazza.</p>

<p>&nbsp;</p>

<p><strong>12. Korlátozások</strong></p>

<p>Az adatkezelőre vagy adatfeldolgozóra alkalmazandó uniós vagy tagállami jog jogalkotási intézkedésekkel korlátozhatja jogok és kötelezettségek (Rendelet 12-22. cikk, 34. cikk, 5. cikk) hatályát, ha a korlátozás tiszteletben tartja az alapvető jogok és szabadságok lényeges tartalmát.</p>

<p>E korlátozás feltételeit a Rendelet 23. cikke tartalmazza.</p>

<p>&nbsp;</p>

<p><strong>13. Az érintett tájékoztatása az adatvédelmi incidensről</strong></p>

<p><strong>13.1.</strong> Ha az adatvédelmi incidens valószínűsíthetően magas kockázattal jár a természetes személyek jogaira és szabadságaira nézve, az adatkezelőnek indokolatlan késedelem nélkül tájékoztatnia kell az érintettet az adatvédelmi incidensről. E tájékoztatásban világosan és közérthetően ismertetni kell az adatvédelmi incidens jellegét, és közölni kell legalább a következőket:</p>

<p>a) az adatvédelmi tisztviselő vagy a további tájékoztatást nyújtó egyéb kapcsolattartó nevét és elérhetőségeit;</p>

<p>c) ismertetni kell az adatvédelmi incidensből eredő, valószínűsíthető következményeket;</p>

<p>d) ismertetni kell az adatkezelő által az adatvédelmi incidens orvoslására tett vagy tervezett intézkedéseket, beleértve adott esetben az adatvédelmi incidensből eredő esetleges hátrányos következmények enyhítését célzó intézkedéseket.</p>

<p><strong>13.2.</strong> Az érintettet nem kell az tájékoztatni, ha a következő feltételek bármelyike teljesül:</p>

<p>a) az adatkezelő megfelelő technikai és szervezési védelmi intézkedéseket hajtott végre, és ezeket az intézkedéseket az adatvédelmi incidens által érintett adatok tekintetében alkalmazták, különösen azokat az intézkedéseket – mint például a titkosítás alkalmazása –, amelyek a személyes adatokhoz való hozzáférésre fel nem jogosított személyek számára értelmezhetetlenné teszik az adatokat;</p>

<p>b) az adatkezelő az adatvédelmi incidenst követően olyan további intézkedéseket tett, amelyek biztosítják, hogy az érintett jogaira és szabadságaira jelentett, magas kockázat a továbbiakban valószínűsíthetően nem valósul meg;</p>

<p>c) a tájékoztatás aránytalan erőfeszítést tenne szükségessé. Ilyen esetekben az érintetteket nyilvánosan közzétett információk útján kell tájékoztatni, vagy olyan hasonló intézkedést kell hozni, amely biztosítja az érintettek hasonlóan hatékony tájékoztatását.</p>

<p>A további szabályokat a Rendelet 34. cikke tartalmazza.</p>

<p>&nbsp;</p>

<p><strong>14. A felügyeleti hatóságnál történő panasztételhez való jog (hatósági jogorvoslathoz való jog)</strong></p>

<p>Az érintett jogosult arra, hogy panaszt tegyen egy felügyeleti hatóságnál – különösen a szokásos tartózkodási helye, a munkahelye vagy a feltételezett jogsértés helye szerinti tagállamban –, ha az érintett megítélése szerint a rá vonatkozó személyes adatok kezelése megsérti a Rendeletet. &nbsp;Az a felügyeleti hatóság, amelyhez a panaszt benyújtották, köteles tájékoztatni az ügyfelet a panasszal kapcsolatos eljárási fejleményekről és annak eredményéről, ideértve azt is, hogy az ügyfél jogosult bírósági jogorvoslattal élni.</p>

<p>E szabályokat a Rendelet 77. cikke tartalmazza.</p>

<p>&nbsp;</p>

<p><strong>A felügyeleti hatóság elérhetősége: </strong></p>

<p><strong>Nemzeti Adatvédelmi és Információszabadság Hatóság </strong></p>

<p><strong>Székhely: 1125 Budapest Szilágyi Erzsébet fasor 22/c.</strong></p>

<p><strong>Postacím:1530 Budapest, Pf.: 5.</strong></p>

<p><strong>Telefonszám: +36 (1) 391-1400</strong></p>

<p><strong>Központi elektronikus levélcím: ugyfelszolgalat@naih.hu</strong></p>

<p>&nbsp;</p>

<p><strong>15. A felügyeleti hatósággal szembeni hatékony bírósági jogorvoslathoz való jog</strong></p>

<p><strong>15.1.</strong> Az egyéb közigazgatási vagy nem bírósági útra tartozó jogorvoslatok sérelme nélkül, minden természetes és jogi személy jogosult a hatékony bírósági jogorvoslatra a felügyeleti hatóság rá vonatkozó, jogilag kötelező erejű döntésével szemben.</p>

<p><strong>15.2.</strong> Az egyéb közigazgatási vagy nem bírósági útra tartozó jogorvoslatok sérelme nélkül, minden érintett jogosult a hatékony bírósági jogorvoslatra, ha az illetékes felügyeleti hatóság nem foglalkozik a panasszal, vagy három hónapon belül nem tájékoztatja az érintettet a benyújtott panasszal kapcsolatos eljárási fejleményekről vagy annak eredményéről.</p>

<p><strong>15.3.</strong> A felügyeleti hatósággal szembeni eljárást a felügyeleti hatóság székhelye szerinti tagállam bírósága előtt kell megindítani.</p>

<p><strong>15.4.</strong> Ha a felügyeleti hatóság olyan döntése ellen indítanak eljárást, amellyel kapcsolatban az egységességi mechanizmus keretében a Testület előzőleg véleményt bocsátott ki vagy döntést hozott, a felügyeleti hatóság köteles ezt a véleményt vagy döntést a bíróságnak megküldeni.</p>

<p>&nbsp;</p>

<p>E szabályokat a Rendelet 78. cikke tartalmazza.</p>

<p><strong>16. Az adatkezelővel vagy az adatfeldolgozóval szembeni hatékony bírósági jogorvoslathoz való jog</strong></p>

<p><strong>16.1.</strong> A rendelkezésre álló közigazgatási vagy nem bírósági útra tartozó jogorvoslatok – köztük a felügyeleti hatóságnál történő panasztételhez való jog – sérelme nélkül, minden érintett hatékony bírósági jogorvoslatra jogosult, ha megítélése szerint a személyes adatainak e rendeletnek nem megfelelő kezelése következtében megsértették az e rendelet szerinti jogait.</p>

<p><strong>16.2.</strong> Az adatkezelővel vagy az adatfeldolgozóval szembeni eljárást az adatkezelő vagy az adatfeldolgozó tevékenységi helye szerinti tagállam bírósága előtt kell megindítani. Az ilyen eljárás megindítható az érintett szokásos tartózkodási helye szerinti tagállam bírósága előtt is, kivéve, ha az adatkezelő vagy az adatfeldolgozó valamely tagállamnak a közhatalmi jogkörében eljáró közhatalmi szerve.</p>

<p>E szabályokat a Rendelet 79. cikke tartalmazza.</p>

<p>&nbsp;</p>

<p>Kelt, 2019.02.18.</p>

<p>&nbsp;</p>

<p><strong>Második Lehetőség Kereskedelmi Szociális Szövetkezet</strong></p>

<p><strong>Képviselő: Durszt Anikó Blanka vezető tisztségviselő</strong></p>
