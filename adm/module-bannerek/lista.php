<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
?>
<!--Modal-->
<div class="example-modal">
	<div id="rakerdez_torles" class="modal modal-danger">
	  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Törlés</h4>
			</div>
			<div class="modal-body">
				<p>Biztos törölni szeretnéd a(az) <span id="modal_torles_nev"></span> bannert?</p>
			</div>
			<div class="modal-footer">
				<input type="hidden" id="modal_torles_id" value="" />
				<button onClick="bannerTorles('rakerdez_torles')" type="button" class="btn btn-outline pull-left" data-dismiss="modal">Igen</button>
				<button onClick="megsem_banner('rakerdez_torles')" type="button" class="btn btn-outline">Mégsem</button>
			</div>
		</div>
	  </div>
	</div>
</div>

<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1 id="myModal">Bannerek</h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li class="active">Bannerek</li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title">Új bannerek létrehozásához húzd a képeket ebbe a mezőbe, vagy kattints bele. <?php print $conf_banner_meret; ?></h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<form id="kepfeltoltes" action="/upload-target" class="dropzone"></form>
					</div>
				</div>
			</div>
		</div>
		<div id="bannerek">
			<?php
				include $gyoker.'/adm/module-bannerek/bannerek.php';
			?>
		</div>
	</section>
</div>

