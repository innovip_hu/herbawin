<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		include '../config_adm.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	if(isset($_GET['command']) && $_GET['command'] =='uj_afa')
	{
		$insertcommand = "INSERT INTO ".$webjel."afa (afa) VALUES (".$_GET['afa'].")";
		$result = $pdo->prepare($insertcommand);
		$result->execute();
	}
	else if(isset($_GET['command']) && $_GET['command'] =='afa_modositas')
	{
		$updatecommand = "UPDATE ".$webjel."afa SET afa=".$_GET['afa']." WHERE id=".$_GET['id'];
		$result = $pdo->prepare($updatecommand);
		$result->execute();
	}
	else if(isset($_GET['command']) && $_GET['command'] =='afa_torles')
	{
		$deletecommand = "DELETE FROM ".$webjel."afa WHERE id =".$_GET['id'];
		$result = $pdo->prepare($deletecommand);
		$result->execute();
	}
?>
	<div class="form-group margbot40">
		<label>Új ÁFA</label>
		<div class="input-group">
			<span class="input-group-addon input_jelolo_kek"><i>%</i></span>
			<input type="text" class="form-control" id="afa_uj" placeholder="Új ÁFA (%)" value="">
			<span onClick="ujAfa()" class="input-group-addon input_gomb aktiv_gomb" data-toggle="tooltip" title="Mentés"><i class="fa fa-floppy-o"></i></span>
		</div>
	</div>
<?php	 
	$query = "SELECT * FROM ".$webjel."afa ORDER BY id ASC";
	foreach ($pdo->query($query) as $row)
	{
		$rownum = 0;
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE afa=".$row['id']);
		$res->execute();
		$rownum = $res->fetchColumn();
		?>
		<div class="form-group">
			<div class="input-group">
				<span class="input-group-addon input_jelolo_kek"><i>%</i></span>
				<input onKeyUp="afaMentesAktiv(<?php print $row['id']; ?>)" type="text" class="form-control" id="afa_<?php print $row['id']; ?>" placeholder="Szállítási költség" value="<?php print $row['afa']; ?>">
				<span onClick="afaMentes(<?php print $row['id']; ?>)" id="afa_mentes_gomb_<?php print $row['id']; ?>" class="input-group-addon input_gomb" data-toggle="tooltip" title="Mentés"><i class="fa fa-floppy-o"></i></span>
				<?php
					if($rownum == 0)
					{
						print '<span onClick="afaTorles('.$row['id'].')" class="input-group-addon input_gomb aktiv_gomb" data-toggle="tooltip" title="Törlés"><i class="fa fa-times"></i></span>';
					}
				?>
			</div>
		</div>
		<?php
	}
?>
