<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		include '../config_adm.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}

		if (isset($_POST['command']) && $_POST['command'] == 'arfolyam_szinkron_valtozas')
		{
			$pdo->exec("UPDATE ".$webjel."beallitasok SET eur_arfolyam=".$_POST['arfolyam']." WHERE id=1");
		}
	}

	$query = "SELECT * FROM ".$webjel."beallitasok WHERE id = 1";
	$res = $pdo->prepare($query);
	$res->execute();
	$row_beallitasok = $res -> fetch();	

?>

	<div class="form-group">
		<label>Euró</label>
		<div class="input-group">
			<span class="input-group-addon input_jelolo_kek"><i class="fa fa-euro"></i></span>
			<input type="text" class="form-control" id="arfolyam" value="<?=$row_beallitasok['eur_arfolyam']?>" >
			<span class="input-group-addon">Ft</span>
		</div>
	</div>