<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		include '../config_adm.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	if(isset($_GET['command']) && $_GET['command'] =='suly_modositas')
	{
		$updatecommand = "UPDATE ".$webjel."szall_suly SET ar=".$_GET['suly_ara']." WHERE id=".$_GET['id'];
		$result = $pdo->prepare($updatecommand);
		$result->execute();
	}

	$query = "SELECT * FROM ".$webjel."szall_suly ORDER BY max_suly ASC";
	foreach ($pdo->query($query) as $row)
	{
		?>
		<div class="form-group">
			<label>Maximum <?= $row['max_suly'] ?> kg</label>
			<div class="input-group">
				<span class="input-group-addon "><i>Ft</i></span>
				<input onKeyUp="sulyMentesAktiv(<?php print $row['id']; ?>)" class="form-control" id="suly_<?php print $row['id']; ?>"  value="<?= $row['ar'] ?>">
				<span onClick="sulyMentes(<?= $row['id'] ?>)" id="suly_mentes_gomb_<?= $row['id'] ?>" class="input-group-addon input_gomb" data-toggle="tooltip" title="Mentés"><i class="fa fa-floppy-o"></i></span>
			</div>
		</div>
		<?php
	}
?>
