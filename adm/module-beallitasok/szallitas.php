<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		include '../config_adm.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
	if(isset($_POST['ingyenes_szallitas']))
	{
		$pdo->exec("UPDATE ".$webjel."beallitasok SET szallitas_afa=".$_POST['szallitas_afa'].", ingyenes_szallitas=".$_POST['ingyenes_szallitas']." WHERE id=1");
		$query = "SELECT * FROM ".$webjel."kassza_szall_mod";
		foreach ($pdo->query($query) as $row)
		{
			$pdo->exec("UPDATE ".$webjel."kassza_szall_mod SET ar=".$_POST['ar_'.$row['id']].", ar_utanvet=".$_POST['ar_utanvet_'.$row['id']]." WHERE id=".$row['id']);
		}
	}
	
	$query = "SELECT * FROM ".$webjel."beallitasok WHERE id=1";
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
	$szallitas_afa = $row['szallitas_afa'];
?>
	<form id="szallitas_form">
	<div class="form-group">
		<label>Ingyenes szállítás <span style="font-size: 0.8em;">(Ha az érték 0, akkor nincs ingyenes szállítás)</span></label>
		<div class="input-group">
			<span class="input-group-addon input_jelolo_nsarga"><i class="fa fa-star"></i></span>
			<input type="text" class="form-control" name="ingyenes_szallitas" placeholder="Ingyenes szállítás" value="<?php print $row['ingyenes_szallitas']; ?>">
			<span class="input-group-addon">Ft</span>
		</div>
	</div>
	<?php
		$query = "SELECT * FROM ".$webjel."kassza_szall_mod ORDER BY sorrend ASC";
		foreach ($pdo->query($query) as $row)
		{
			$lathato_ez = '';
			if ($row['lathato'] == 0)
			{
				$lathato_ez = 'style="display: none;"';
			}			
			echo '<div class="row" '.$lathato_ez.'>
				<div class="col-md-6"><div class="form-group">
					<label>'.$row['nev'].'</label>
					<div class="input-group">
						<span class="input-group-addon input_jelolo_kek"><i class="fa fa-truck"></i></span>
						<input type="text" class="form-control" name="ar_'.$row['id'].'" value="'.$row['ar'].'">
						<span class="input-group-addon">Ft</span>
					</div>
				</div></div>
				<div class="col-md-6"><div class="form-group">
					<label>utánvét esetén</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-truck"></i></span>
						<input type="text" class="form-control" name="ar_utanvet_'.$row['id'].'" value="'.$row['ar_utanvet'].'">
						<span class="input-group-addon">Ft</span>
					</div>
				</div></div>
			</div>';
		}
	?>
	<div class="form-group">
		<label>Szállítási költség ÁFA</label>
		<div class="input-group">
			<span class="input-group-addon input_jelolo_kek"><i class="fa fa-eur"></i></span>
			<input type="text" class="form-control" name="szallitas_afa" placeholder="Szállítási költség ÁFA" value="<?= $szallitas_afa ?>">
			<span class="input-group-addon">%</span>
		</div>
	</div>
	</form>
