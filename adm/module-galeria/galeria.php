<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
// Oldalankénti szám
	if (isset($_GET['oldalszam']) && $_GET['oldalszam'] != '')
	{
		$oldalszam = $_GET['oldalszam'];
	}
	else
	{
		$oldalszam = 50; //ALAPÁLLAPOT
	}
// Rekordok száma
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."galeria ");
	$res->execute();
	$rownum = $res->fetchColumn();
// Kezdés meghatározása
	if (isset($_GET['kezd']) && $_GET['kezd'] != '' && $rownum > $oldalszam)
	{
		$kezd = $_GET['kezd'];
	}
	else
	{
		$kezd = 0;
	}
// Aktuális oldal
	$aktualis_oldal = ($kezd + $oldalszam) / $oldalszam;
// Utolsó oldal
	$utolso_oldal = ceil($rownum / $oldalszam);
// Sorrend
	if (isset($_GET['sorr_tip']))
	{
		$sorr_tip = $_GET['sorr_tip'];
	}
	else
	{
		$sorr_tip = 'id'; // Alap rendezési feltétel
	}
	if (isset($_GET['sorrend']))
	{
		$sorrend = $_GET['sorrend'];
	}
	else
	{
		$sorrend = 'ASC'; // Alap rendezési feltétel
	}
	
	// Új galéria
	/* if(isset($_GET['command']) && $_GET['command'] == 'uj_galeria')
	{
		// URL név meghatározása
		include $gyoker.'/adm/module/mod_urlnev.php';
		if ($nev_url == '')
		{
			$nev_url = rand(1,99999);
		}
		// Egyezőség vizsgálata
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."galeria WHERE nev_url = '".$nev_url."'");
		$res->execute();
		$rownum2 = $res->fetchColumn();
		if ($rownum2 > 0) // Ha van már ilyen nevű
		{
			$nev_url = $nev_url.'-'.rand(1,99999);
		}
		
		$insertcommand = "INSERT INTO ".$webjel."galeria (nev,nev_url) VALUES (:nev,:nev_url)";
		$result = $pdo->prepare($insertcommand);
		$result->execute(array(':nev'=>$_POST['nev'],
						  ':nev_url'=>$nev_url));
		$_GET['id'] = $pdo->lastInsertId(); // még a dropezone-t be kell rakni a js-be
	} */
	// Mentés
	if(isset($_GET['command']) && $_GET['command'] == 'mentes')
	{
		// URL név meghatározása
		include $gyoker.'/adm/module/mod_urlnev.php';
		if ($nev_url == '')
		{
			$nev_url = rand(1,99999);
		}
		// Egyezőség vizsgálata
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."galeria WHERE nev_url = '".$nev_url."'");
		$res->execute();
		$rownum2 = $res->fetchColumn();
		if ($rownum2 > 0) // Ha van már ilyen nevű
		{
			$query = "SELECT * FROM ".$webjel."galeria WHERE nev_url = '".$nev_url."'";
			$res = $pdo->prepare($query);
			$res->execute();
			$row = $res -> fetch();
			if($rownum2 > 1 || $row['id'] != $_GET['id']) // Ha nem saját maga
			{
				$nev_url = $nev_url.'-'.$_GET['id'];
			}
		}
		
		$updatecommand = "UPDATE ".$webjel."galeria SET nev=?, leiras=?, nev_url=? WHERE id=?";
		$result = $pdo->prepare($updatecommand);
		$result->execute(array($_POST['nev'],$_POST['leiras'],$nev_url,$_GET['id']));
	}
	// Adatok
	$query = "SELECT * FROM ".$webjel."galeria WHERE id=".$_GET['id'];
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
?>
<!--Modal-->
<div class="example-modal">
<div id="rakerdez_torles" class="modal modal-danger">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<h4 class="modal-title">Törlés</h4>
	  </div>
	  <div class="modal-body">
		<p>Biztos törölni szeretnéd a galériát?</p>
	  </div>
	  <div class="modal-footer">
		<button onClick="torol('<?php print $_GET['id']; ?>', '<?php print $_GET['fajl']; ?>')" type="button" class="btn btn-outline pull-left" data-dismiss="modal">Igen</button>
		<button onClick="megsem('rakerdez_torles')" type="button" class="btn btn-outline">Mégsem</button>
	  </div>
	</div>
  </div>
</div>
</div>
		  
<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1>Galéria<small><?php print $row['nev']; ?></small></h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li><a onClick="vissza('<?php print $_GET['fajl']; ?>')"><i class="fa fa-camera"></i> Galéria</a></li>
		<li class="active"><?php print $row['nev']; ?></li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Adatok</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Megnevezés</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
										<input type="text" class="form-control" id="nev" placeholder="Megnevezés" value="<?php print $row['nev']; ?>">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Leírás</label>
									<textarea class="form-control" id="leiras" rows="6" placeholder="Leírás"><?php print $row['leiras']; ?></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<button type="submit" onClick="mentesGaleria('<?php print $row['id']; ?>', '<?php print $_GET['fajl']; ?>')" class="btn btn-primary">Mentés</button>
						<button type="submit" onClick="rakerdez('rakerdez_torles')" class="btn btn-danger pull-right">Törlés</button>
					</div>
				</div>
			</div>
			<div class="col-md-12">
			<!-- Képek -->
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title">Képek feltöltése</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<form id="kepfeltoltes" action="/upload-target" class="dropzone"></form>
					</div>
					<div class="box-body" id="galeria_kepek_div">
						<?php
							include('galeria_kepek.php');
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<input type="hidden" name="oldalszam" id="oldalszam" value="<?php print $oldalszam;?>"/>
<!--Aktuális oldal - azaz honnan kezdődjön a lekérdezés-->
<input type="hidden" name="kezd" id="kezd" value="<?php print $kezd;?>"/>
<input type="hidden" name="kezd" id="oldalszam" value="<?php print $oldalszam;?>"/>
<!--Sorba rendezés-->
<input type="hidden" name="sorr_tip" id="sorr_tip" value="<?php print $sorr_tip;?>"/>
<input type="hidden" name="sorrend" id="sorrend" value="<?php print $sorrend;?>"/>
