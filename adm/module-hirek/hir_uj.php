<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
// Oldalankénti szám
	if (isset($_GET['oldalszam']) && $_GET['oldalszam'] != '')
	{
		$oldalszam = $_GET['oldalszam'];
	}
	else
	{
		$oldalszam = 50; //ALAPÁLLAPOT
	}
// Rekordok száma
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."users ");
	$res->execute();
	$rownum = $res->fetchColumn();
// Kezdés meghatározása
	if (isset($_GET['kezd']) && $_GET['kezd'] != '' && $rownum > $oldalszam)
	{
		$kezd = $_GET['kezd'];
	}
	else
	{
		$kezd = 0;
	}
// Aktuális oldal
	$aktualis_oldal = ($kezd + $oldalszam) / $oldalszam;
// Utolsó oldal
	$utolso_oldal = ceil($rownum / $oldalszam);
// Sorrend
	if (isset($_GET['sorr_tip']))
	{
		$sorr_tip = $_GET['sorr_tip'];
	}
	else
	{
		$sorr_tip = 'id'; // Alap rendezési feltétel
	}
	if (isset($_GET['sorrend']))
	{
		$sorrend = $_GET['sorrend'];
	}
	else
	{
		$sorrend = 'ASC'; // Alap rendezési feltétel
	}
	
?>
		  
<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1>Új hír<small></small></h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li><a onClick="vissza('<?php print $_GET['fajl']; ?>')"><i class="fa fa-newspaper-o"></i> Hírek</a></li>
		<li class="active">Új hír</li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<!-- Személyes adatok -->
				<div class="box box-warning">
					<div class="box-body">
						<form id="ujHirForm" action="module-hirek/mod_ujhir.php" method="post" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Cím</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
										<input type="text" class="form-control" id="cim" placeholder="Cím" value="">
									</div>
								</div>
								<div class="form-group">
									<label>Cím EN</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
										<input type="text" class="form-control" id="cim_en" placeholder="Cím EN" value="">
									</div>
								</div>
								<div class="form-group">
									<label>Cím DE</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
										<input type="text" class="form-control" id="cim_de" placeholder="Cím DE" value="">
									</div>
								</div>									
								<div class="form-group">
									<label>Megjelenés</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-calendar"></i></span>
										<input type="text" class="form-control datepicker" id="datum" placeholder="Megjelenés" value="<?php print date('Y-m-d'); ?>" >
									</div>
								</div>
								<div class="form-group">
									<label>Előzetes</label>
									<textarea class="form-control" id="elozetes" rows="3" placeholder="Előzetes"></textarea>
								</div>
								<div class="form-group">
									<label>Előzetes EN</label>
									<textarea class="form-control" id="elozetes_en" rows="3" placeholder="Előzetes EN"></textarea>
								</div>
								<div class="form-group">
									<label>Előzetes DE</label>
									<textarea class="form-control" id="elozetes_de" rows="3" placeholder="Előzetes DE"></textarea>
								</div>									
								<?php /*
								<div class="form-group">
									<label for="exampleInputFile">Kép feltöltése</label>
									<input type="file" id="exampleInputFile">
									<!--<p class="help-block">Example block-level help text here.</p>-->
									<div class="progress margtop10">
										<div class="bar"></div >
										<div class="percent">0%</div >
										<div id="status"></div>
									</div>
								</div>
								*/ ?>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Tartalom</label>
									<textarea id="editor1" name="editor1" rows="10" cols="80"></textarea>
								</div>
								<div class="form-group">
									<label>Tartalom EN</label>
									<textarea id="editor2" name="editor2" rows="10" cols="80">
									</textarea>
								</div>
								<div class="form-group">
									<label>Tartalom DE</label>
									<textarea id="editor3" name="editor3" rows="10" cols="80">
									</textarea>
								</div>									
							</div>
						</div>
						</form>
					</div>
					<div class="box-footer">
						<button type="submit" onClick="mentesUjHir('<?php print $_GET['fajl']; ?>')" class="btn btn-primary">Mentés</button>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<input type="hidden" name="oldalszam" id="oldalszam" value="<?php print $oldalszam;?>"/>
<!--Aktuális oldal - azaz honnan kezdődjön a lekérdezés-->
<input type="hidden" name="kezd" id="kezd" value="<?php print $kezd;?>"/>
<input type="hidden" name="kezd" id="oldalszam" value="<?php print $oldalszam;?>"/>
<!--Sorba rendezés-->
<input type="hidden" name="sorr_tip" id="sorr_tip" value="<?php print $sorr_tip;?>"/>
<input type="hidden" name="sorrend" id="sorrend" value="<?php print $sorrend;?>"/>
