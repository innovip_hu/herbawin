<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
// Oldalankénti szám
	if (isset($_GET['oldalszam']) && $_GET['oldalszam'] != '')
	{
		$oldalszam = $_GET['oldalszam'];
	}
	else
	{
		$oldalszam = 50; //ALAPÁLLAPOT
	}
// Rekordok száma
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."users ");
	$res->execute();
	$rownum = $res->fetchColumn();
// Kezdés meghatározása
	if (isset($_GET['kezd']) && $_GET['kezd'] != '' && $rownum > $oldalszam)
	{
		$kezd = $_GET['kezd'];
	}
	else
	{
		$kezd = 0;
	}
// Aktuális oldal
	$aktualis_oldal = ($kezd + $oldalszam) / $oldalszam;
// Utolsó oldal
	$utolso_oldal = ceil($rownum / $oldalszam);
// Sorrend
	if (isset($_GET['sorr_tip']))
	{
		$sorr_tip = $_GET['sorr_tip'];
	}
	else
	{
		$sorr_tip = 'id'; // Alap rendezési feltétel
	}
	if (isset($_GET['sorrend']))
	{
		$sorrend = $_GET['sorrend'];
	}
	else
	{
		$sorrend = 'ASC'; // Alap rendezési feltétel
	}
	
	// Új termék mentése
	if(isset($_POST['command']) && $_POST['command'] == 'uj_hir')
	{
		// URL név meghatározása
		include $gyoker.'/adm/module/mod_urlcim.php';
		if ($nev_url == '')
		{
			$nev_url = rand(1,99999);
		}
		// Egyezőség vizsgálata
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."hirek2 WHERE nev_url = '".$nev_url."'");
		$res->execute();
		$rownum2 = $res->fetchColumn();
		if ($rownum2 > 0) // Ha van már ilyen nevű
		{
			$query = "SELECT * FROM ".$webjel."hirek2 WHERE nev_url = '".$nev_url."'";
			$res = $pdo->prepare($query);
			$res->execute();
			$row = $res -> fetch();
			if($rownum2 > 1 || $row['id'] != $_GET['id']) // Ha nem saját maga
			{
				$nev_url = $nev_url.'-'.time().rand(1, 999);
			}
		}
		// Mentés
		$updatecommand = "UPDATE ".$webjel."hirek2 SET cim=?, cim_en=?, cim_de=?, datum=?, elozetes=?, elozetes_en=?, elozetes_de=?, tartalom=?, tartalom_en=?, tartalom_de=?, nev_url=? WHERE id=?";
		$result = $pdo->prepare($updatecommand);
		$result->execute(array($_POST['cim'],$_POST['cim_en'],$_POST['cim_de'],$_POST['datum'],$_POST['elozetes'],$_POST['elozetes_en'],$_POST['elozetes_de'],$_POST['tartalom'],$_POST['tartalom_en'],$_POST['tartalom_de'],$nev_url,$_GET['id']));
	}
	
	// Adatok mentése
	if(isset($_POST['command']) && $_POST['command'] == 'adatok_mentese')
	{
		// URL név meghatározása
		include $gyoker.'/adm/module/mod_urlcim.php';
		if ($nev_url == '')
		{
			$nev_url = rand(1,99999);
		}
		// Egyezőség vizsgálata
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."hirek2 WHERE nev_url = '".$nev_url."'");
		$res->execute();
		$rownum2 = $res->fetchColumn();
		if ($rownum2 > 0) // Ha van már ilyen nevű
		{
			$query = "SELECT * FROM ".$webjel."hirek2 WHERE nev_url = '".$nev_url."'";
			$res = $pdo->prepare($query);
			$res->execute();
			$row = $res -> fetch();
			if($rownum2 > 1 || $row['id'] != $_GET['id']) // Ha nem saját maga
			{
				$nev_url = $nev_url.'-'.$_GET['id'];
			}
		}
		// Mentés
		$updatecommand = "UPDATE ".$webjel."hirek2 SET cim=?, cim_en=?, cim_de=?, datum=?, elozetes=?, elozetes_en=?, elozetes_de=?, tartalom=?, tartalom_en=?, tartalom_de=?, nev_url=? WHERE id=?";
		$result = $pdo->prepare($updatecommand);
		$result->execute(array($_POST['cim'],$_POST['cim_en'],$_POST['cim_de'],$_POST['datum'],$_POST['elozetes'],$_POST['elozetes_en'],$_POST['elozetes_de'],$_POST['tartalom'],$_POST['tartalom_en'],$_POST['tartalom_de'],$nev_url,$_GET['id']));
	}
	
	// Adatok
	$query = "SELECT * FROM ".$webjel."hirek2 WHERE id=".$_GET['id'];
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
?>
<!--Modal-->
<div class="example-modal">
<div id="rakerdez_torles" class="modal modal-danger">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<h4 class="modal-title">Törlés</h4>
	  </div>
	  <div class="modal-body">
		<p>Biztos törölni szeretnéd a hírt?</p>
	  </div>
	  <div class="modal-footer">
		<button onClick="torol('<?php print $_GET['id']; ?>', '<?php print $_GET['fajl']; ?>')" type="button" class="btn btn-outline pull-left" data-dismiss="modal">Igen</button>
		<button onClick="megsem('rakerdez_torles')" type="button" class="btn btn-outline">Mégsem</button>
	  </div>
	</div>
  </div>
</div>
</div>
		  
<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1>Blog<small></small></h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li><a onClick="vissza('<?php print $_GET['fajl']; ?>')"><i class="fa fa-newspaper-o"></i>Blog</a></li>
		<li class="active"><?php print $row['cim']; ?></li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<!-- Személyes adatok -->
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title">Megjelenés: <?php print $row['datum']; ?></h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Cím</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
										<input type="text" class="form-control" id="cim" placeholder="Cím" value="<?php print $row['cim']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label>Cím EN</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
										<input type="text" class="form-control" id="cim_en" placeholder="Cím EN" value="<?php print $row['cim_en']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label>Cím DE</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
										<input type="text" class="form-control" id="cim_de" placeholder="Cím DE" value="<?php print $row['cim_de']; ?>">
									</div>
								</div>									
								<div class="form-group">
									<label>Oldal linkje</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
										<input type="text" class="form-control" id="link" placeholder="Cím" value="<?=$domain?>/blog/<?php print $row['nev_url']; ?>" readonly>
										<span class="input-group-addon">
											<a onclick="vagolapramasol('<?=$domain?>/blog/<?php print $row['nev_url']; ?>')" style="cursor: pointer;"><i class="fa fa-clipboard"></i></a>
										</span>
									</div>
								</div>								
								<div class="form-group">
									<label>Megjelenés</label>
									<div class="input-group">
										<span class="input-group-addon input_jelolo_kek"><i class="fa fa-calendar"></i></span>
										<input type="text" class="form-control datepicker" id="datum" placeholder="Megjelenés" value="<?php print $row['datum']; ?>" >
									</div>
								</div>
								<div class="form-group">
									<label>Előzetes</label>
									<textarea class="form-control" id="elozetes" rows="3" placeholder="Előzetes"><?php print $row['elozetes']; ?></textarea>
								</div>
								<div class="form-group">
									<label>Előzetes EN</label>
									<textarea class="form-control" id="elozetes_en" rows="3" placeholder="Előzetes EN"><?php print $row['elozetes_en']; ?></textarea>
								</div>
								<div class="form-group">
									<label>Előzetes DE</label>
									<textarea class="form-control" id="elozetes_de" rows="3" placeholder="Előzetes DE"><?php print $row['elozetes_de']; ?></textarea>
								</div>									
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Tartalom</label>
									<textarea id="editor1" name="editor1" rows="10" cols="80">
										<?php print $row['tartalom']; ?>
									</textarea>
								</div>
								<div class="form-group">
									<label>Tartalom EN</label>
									<textarea id="editor2" name="editor2" rows="10" cols="80">
										<?php print $row['tartalom_en']; ?>
									</textarea>
								</div>
								<div class="form-group">
									<label>Tartalom DE</label>
									<textarea id="editor3" name="editor3" rows="10" cols="80">
										<?php print $row['tartalom_de']; ?>
									</textarea>
								</div>									
							</div>
						</div>
					</div>
					<div class="box-footer">
						<button type="submit" onClick="mentes('<?php print $_GET['fajl']; ?>', <?php print $row['id']; ?>)" class="btn btn-primary">Mentés</button>
						<div class="box-tools pull-right">
							<button class="btn btn-danger" onclick="rakerdez('rakerdez_torles')" >Törlés</button>
						</div>
					</div>
				</div>
			</div>
			<!-- Képek -->
			<div class="col-md-6">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title">Képek feltöltése</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<form id="kepfeltoltes" action="/upload-target" class="dropzone"></form>
					</div>
					<div class="box-body" id="hir_kepek_div">
						<?php
							include('hir_kepek.php');
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<input type="hidden" name="oldalszam" id="oldalszam" value="<?php print $oldalszam;?>"/>
<!--Aktuális oldal - azaz honnan kezdődjön a lekérdezés-->
<input type="hidden" name="kezd" id="kezd" value="<?php print $kezd;?>"/>
<input type="hidden" name="kezd" id="oldalszam" value="<?php print $oldalszam;?>"/>
<!--Sorba rendezés-->
<input type="hidden" name="sorr_tip" id="sorr_tip" value="<?php print $sorr_tip;?>"/>
<input type="hidden" name="sorrend" id="sorrend" value="<?php print $sorrend;?>"/>
