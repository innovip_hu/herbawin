<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
	// Hír törlése
	if(isset($_GET['command']) && $_GET['command'] == 'hir_torles')
	{
		// Képek törlése
		$query = "SELECT * FROM ".$webjel."hir_kepek2 WHERE hir_id =".$_GET['id'];
		foreach ($pdo->query($query) as $row)
		{
			$dir = $gyoker."/images/termekek/";
			unlink($dir.$row['kep']);
		}
		// SQL
		$deletecommand = "DELETE FROM ".$webjel."hirek2 WHERE id =".$_GET['id'];
		$result = $pdo->prepare($deletecommand);
		$result->execute();

		//Hírkép törlés
		$deletecommand = "DELETE FROM ".$webjel."hir_kepek2 WHERE hir_id =".$_GET['id'];
		$result = $pdo->prepare($deletecommand);
		$result->execute();			
	}
	
// Oldalankénti szám
	if (isset($_GET['oldalszam']) && $_GET['oldalszam'] != '')
	{
		$oldalszam = $_GET['oldalszam'];
	}
	else
	{
		$oldalszam = 50; //ALAPÁLLAPOT
	}
// Rekordok száma
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."hirek2 ");
	$res->execute();
	$rownum = $res->fetchColumn();
// Kezdés meghatározása
	if (isset($_GET['kezd']) && $_GET['kezd'] != '' && $rownum > $oldalszam)
	{
		$kezd = $_GET['kezd'];
	}
	else
	{
		$kezd = 0;
	}
// Aktuális oldal
	$aktualis_oldal = ($kezd + $oldalszam) / $oldalszam;
// Utolsó oldal
	$utolso_oldal = ceil($rownum / $oldalszam);
// Sorrend
	if (isset($_GET['sorr_tip']))
	{
		$sorr_tip = $_GET['sorr_tip'];
	}
	else
	{
		$sorr_tip = 'id'; // Alap rendezési feltétel
	}
	if (isset($_GET['sorrend']))
	{
		$sorrend = $_GET['sorrend'];
	}
	else
	{
		$sorrend = 'DESC'; // Alap rendezési feltétel
	}
?>
<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1 id="myModal">Blog</h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li class="active">Blog</li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-body no-padding with-border">
						<div class="mailbox-controls">
							<span class="menu_gomb"><a onClick="ujHir('module-hirek2/lista.php')"><i class="fa fa-plus"></i> Új bejegyzés</span></a>
							<div class="pull-right">
								<?php
									if($oldalszam > $rownum) $oldalszam_mod = $rownum;
									else $oldalszam_mod = $oldalszam;
									$vege = $kezd+$oldalszam_mod;
									if($vege > $rownum) $vege = $rownum;
									print ($kezd+1).'-'.$vege.'/'.$rownum;
								?>
								<div class="btn-group">
								<?php
									if ($kezd != 0) // Lapozás vissza (balra)
									{
										?>
										<button onclick="lapozasVissza('module-hirek2/lista.php')" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
										<?php
									}
									if (($utolso_oldal / $aktualis_oldal) != 1) // Lapozás előre (jobbra)
									{
										?>
										<button onclick="lapozasTovabb('module-hirek2/lista.php')" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
										<?php
									}
								?>
								</div>
							</div>
						</div>
						<div class="table-responsive">
							<table class="table table-hover table-striped margbot0">
								<tbody>
									<tr>
										<th>Dátum</th>
										<th>Cím</th>
									</tr>
									<?php
									$query = "SELECT * FROM ".$webjel."hirek2 ORDER BY datum DESC LIMIT ".$kezd.",".$oldalszam;
									foreach ($pdo->query($query) as $row)
									{
										?>
										<tr class="kattintos_sor" onClick="Belepes('module-hirek2/hir.php', 'module-hirek2/lista.php', <?php print $row['id']; ?>)">
										<?php
										print '<td>'.$row['datum'].'</td>
											<td>'.$row['cim'].'</td>
										</tr>';
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer no-padding">
						<div class="mailbox-controls">
							<div class="btn-group" style="height:30px">
								&nbsp;
							</div>
							<div class="pull-right">
								<?php
									print ($kezd+1).'-'.$vege.'/'.$rownum;
								?>
								<div class="btn-group">
								<?php
									if ($kezd != 0) // Lapozás vissza (balra)
									{
										?>
										<button onclick="lapozasVissza('module-hirek2/lista.php')" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
										<?php
									}
									if (($utolso_oldal / $aktualis_oldal) != 1) // Lapozás előre (jobbra)
									{
										?>
										<button onclick="lapozasTovabb('module-hirek2/lista.php')" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
										<?php
									}
								?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<input type="hidden" name="oldalszam" id="oldalszam" value="<?php print $oldalszam;?>"/>
<!--Aktuális oldal - azaz honnan kezdődjön a lekérdezés-->
<input type="hidden" name="kezd" id="kezd" value="<?php print $kezd;?>"/>
<!--Sorba rendezés-->
<input type="hidden" name="sorr_tip" id="sorr_tip" value="<?php print $sorr_tip;?>"/>
<input type="hidden" name="sorrend" id="sorrend" value="<?php print $sorrend;?>"/>
