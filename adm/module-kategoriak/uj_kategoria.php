<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
?>
		  
<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1>Új kategória<small>(<?php print $_GET['nev']; ?>)</small></h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li><a href="kategoriak.php"><i class="fa fa-folder-open"></i> Kategóriák</a></li>
		<li class="active">Új kategória</li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-6">
				<!-- Személyes adatok -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Adatok</h3>
					</div>
					<div class="box-body">
						<form id="uj_kategoria_form" action="" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label>Megnevezés</label>
								<div class="input-group">
									<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
									<input type="text" class="form-control" name="nev" id="nev" placeholder="Megnevezés" value="">
								</div>
							</div>
							<div class="form-group">
								<label for="kep">Kép feltöltése</label>
								<input type="file" name="kep" id="kep">
								<!--<p class="help-block">Example block-level help text here.</p>-->
							</div>
							<div class="form-group">
								<label>Leírás</label>
								<textarea class="form-control" name="leiras" id="leiras" rows="3" placeholder="Leírás"></textarea>
							</div>
							<div class="form-group">
								<label>SEO title</label>
								<div class="input-group">
									<span class="input-group-addon input_jelolo_kek"><i class=""><i class="fa fa-link"></i></i></span>
									<input type="text" class="form-control" name="seo_title" id="seo_title" placeholder="SEO title" value="">
								</div>
							</div>
							<div class="form-group">
								<label>SEO description</label>
								<div class="input-group">
									<span class="input-group-addon input_jelolo_kek"><i class=""><i class="fa fa-link"></i></i></span>
									<input type="text" class="form-control" name="seo_description" id="seo_description" placeholder="SEO description" value="">
								</div>
							</div>
							<div class="row margtop10">
								<div class="col-sm-6">
									<div class="checkbox ">
										<label>
											<input type="checkbox" class="minimal" name="lathato" id="lathato" value="ok" checked/> Látható
										</label>
									</div>
								</div>
							</div>
							<input type="hidden" name="csop_id" value="<?php print $_GET['csop_id']; ?>" />
							<input type="hidden" name="command" value="uj_kategoria_mentese" />
						</form>
					</div>
					<div class="box-footer">
						<!--<button type="submit" onClick="mentesUjKategoria('<?php print $_GET['csop_id']; ?>')" class="btn btn-primary">Mentés</button>-->
						<button type="submit" onClick="mentesUjKategoria2('<?php print $_GET['csop_id']; ?>')" class="btn btn-primary">Mentés</button>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
