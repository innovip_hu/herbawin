<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
?>
<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1 id="myModal">Kupon statisztikák</h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li class="active">Kupon statisztikák</li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-8">
				<div class="box box-primary">
					<div class="box-header with-border">
						&nbsp;
					</div>
					<div class="table-responsive">
						<table class="table table-hover margbot0">
							<tbody>
								<tr>
									<th>Kód</th>
									<th>Kedvezmény tipus</th>
									<th style="text-align:center;">Mérték / Termék ID</th>
									<th style="text-align:right;">Vásárlások</th>
									<th style="text-align:right;">Összeg</th>
								</tr>
								<?php
									$query = "SELECT *, ".$webjel."kuponok.kod AS kod, ".$webjel."kuponok.id AS id 
										FROM ".$webjel."kuponok 
										INNER JOIN ".$webjel."rendeles 
										ON ".$webjel."kuponok.id = ".$webjel."rendeles.kupon_id 
										GROUP BY ".$webjel."kuponok.id 
										ORDER BY vege DESC";
									$feher_hatter = 0;
									$a = 0;
									foreach ($pdo->query($query) as $row)
									{
										if($a == 0) { $hatter = ' style="background-color:#F9F9F9;"'; } else { $hatter = ''; }
										print '<tr class="lenyito_sor" attr_rendeles_id="'.$row['id'].'" '.$hatter.'>';
											print '<td class="tablasor" >'.$row['kod'].'</td>
											<td>'.$row['kupon_fajta'];
												if($row['min_kosar_ertek'] > 0)
												{
													print '<br/>(min. érték: '.$row['min_kosar_ertek'].')';
												}
											print '</td>';
											print '<td style="text-align:center;">';
												if ($row['kupon_fajta'] == 'Ajándék termék')
												{
													print 'ID: '.$row['termek_id'];
												}
												else if ($row['kupon_fajta'] == 'Kedvezmény')
												{
													print $row['kedvezmeny'];
													if ($row['kedv_tipus'] == 'szazalek')
													{
														print '%';
													}
													else if ($row['kedv_tipus'] == 'osszeg')
													{
														print 'Ft';
													}
												}
											print '</td>';
											$query_2 = "SELECT *, ".$webjel."rendeles.id as id
														, ".$webjel."rendeles.rendeles_id as rendeles_id
														, SUM(IF(".$webjel."rendeles_tetelek.term_akcios_ar > 0, ".$webjel."rendeles_tetelek.term_akcios_ar * ".$webjel."rendeles_tetelek.term_db, ".$webjel."rendeles_tetelek.term_ar * ".$webjel."rendeles_tetelek.term_db)) as rendelt_osszeg 
														, SUM(".$webjel."rendeles_tetelek.term_db) as rendelt_db 
											FROM ".$webjel."rendeles 
											LEFT JOIN ".$webjel."rendeles_tetelek 
											ON ".$webjel."rendeles.id = ".$webjel."rendeles_tetelek.rendeles_id 
											WHERE kupon_id=".$row['id']."
											GROUP BY ".$webjel."rendeles.id";
											$vasarlas = 0;
											$vasarlas_db = 0;
											$rendelesek = '';
											foreach ($pdo->query($query_2) as $row_2)
											{
												$vasarlas += $row_2['rendelt_osszeg'];
												$vasarlas_db++;
												// Rendelések
												if ($row['kupon_fajta'] == 'Kedvezmény') { $row_2['rendelt_db']--; }
												$rendelesek .= '<tr class="lenyilo_sor" attr_rendeles_id="'.$row['id'].'" '.$hatter.'>
													<td></td>
													<td>'.$row_2['vasarlo_nev'].'</td>
													<td>Rendelésszám: '.$row_2['id'].'</td>
													<td style="text-align:right;">'.$row_2['rendelt_db'].' termék</td>
													<td style="text-align:right;">'.number_format($row_2['rendelt_osszeg'], 0, ',', ' ').' Ft</td>
												</tr>';
											}
											print '<td style="text-align:right;">'.$vasarlas_db.' db</td>';
											print '<td style="text-align:right;">'.$vasarlas.' Ft</td>';
										print '</tr>';
										
										echo $rendelesek;
										if($a == 1) { $a = 0; } else { $a = 1; }
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

