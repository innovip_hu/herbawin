<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	// Kupon törlése
	if(isset($_GET['torlendo_kupon_id']))
	{
		$deletecommand = "DELETE FROM ".$webjel."kuponok WHERE id =".$_GET['torlendo_kupon_id'];
		$result = $pdo->prepare($deletecommand);
		$result->execute();
	}
	if (isset($_POST['command']) && $_POST['command'] == "uj_kupon")
	{
		if($_POST['egyszeri'] == 'true')
		{
			$egyszeri = 1;
		}
		else
		{
			$egyszeri = 0;
		}
		if ($_POST['kupon_fajta'] == 'Kedvezmény')
		{
			$kedv_tipus = $_POST['kedv_tipus'];
		}
		else
		{
			$kedv_tipus = '';
		}
		$insertcommand = "INSERT INTO ".$webjel."kuponok (kod, indul, vege, kupon_fajta, termek_id, kedvezmeny, egyszeri, min_kosar_ertek, kedv_tipus) VALUES ('".$_POST['kod']."', '".$_POST['indul']."', '".$_POST['vege']."', '".$_POST['kupon_fajta']."', '".$_POST['termek_id']."', '".$_POST['kedvezmeny']."','".$egyszeri."', '".$_POST['min_kosar_ertek']."','".$kedv_tipus."')";
		$result = $pdo->prepare($insertcommand);
		$result->execute();
		
	}
?>
<!--Modal-->
<div class="example-modal">
	<div id="rakerdez_torles" class="modal modal-danger">
	  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Törlés</h4>
			</div>
			<div class="modal-body">
				<p>Biztos törölni szeretnéd a(az) <span id="modal_torles_nev"></span> kupont?</p>
			</div>
			<div class="modal-footer">
				<input type="hidden" id="modal_torles_id" value="" />
				<button onClick="kuponTorles('rakerdez_torles')" type="button" class="btn btn-outline pull-left" data-dismiss="modal">Igen</button>
				<button onClick="megsem('rakerdez_torles')" type="button" class="btn btn-outline">Mégsem</button>
			</div>
		</div>
	  </div>
	</div>
</div>

<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1 id="myModal">Kuponok</h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li class="active">Kuponok</li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<span class="menu_gomb"><a onClick="ujKuponFrom()"><i class="fa fa-plus"></i> Új kupon</span></a>
					</div>
					<div id="uj_kupon" class="padtop10 padbot10" style="display:none;">
						<div class="col-md-12">
							<div class="form-group">
								<label>Kupon típusa</label>
								<div class="input-group">
									<span class="input-group-addon input_jelolo_nsarga"><i class="fa fa-tags"></i></span>
									<select onChange="kupon()" class="form-control" id="kupon_fajta">
										<option value="Ingyenes szállítás">Ingyenes szállítás</option>
										<option value="Ajándék termék">Ajándék termék</option>
										<option value="Kedvezmény" selected="">Kedvezmény</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label>Kód</label>
										<?php
											$length = 16;
											$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
											$charactersLength = strlen($characters);
											$kod = '';
											for ($i = 0; $i < $length; $i++) {
												$kod .= $characters[rand(0, $charactersLength - 1)];
											}
										?>
										<div class="input-group">
											<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
											<input type="text" class="form-control" id="kod" placeholder="Kód" value="<?php print $kod; ?>">
										</div>
									</div>
									<div class="form-group">
										<label>Minimum kosár érték</label>
										<div class="input-group">
											<span class="input-group-addon input_jelolo_kek"><i class="fa fa-shopping-cart"></i></span>
											<input type="text" class="form-control" id="min_kosar_ertek" placeholder="(nem kötelező)" value="">
											<span class="input-group-addon">Ft</span>
										</div>
									</div>
									<div class="form-group">
										<label>Akció kezdete</label>
										<div class="input-group">
											<span class="input-group-addon input_jelolo_kek"><i class="fa fa-calendar"></i></span>
											<input type="text" class="form-control datepicker" id="indul" placeholder="Akció kezdete" value="<?php print date('Y-m-d') ?>">
										</div>
									</div>
									<div class="form-group">
										<label>Akció vége</label>
										<div class="input-group">
											<span class="input-group-addon input_jelolo_kek"><i class="fa fa-calendar"></i></span>
											<input type="text" class="form-control datepicker" id="vege" placeholder="Akció vége" value="<?php print date('Y-m-d') ?>">
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group" id="termek_id_div" style="display:none;">
										<label>Termék ID</label>
										<div class="input-group">
											<span class="input-group-addon input_jelolo_kek"><i class="fa fa-star"></i></span>
											<input type="text" class="form-control" id="termek_id" placeholder="Termék" value="">
											<span class="input-group-addon">ID</span>
										</div>
									</div>
									<div class="form-group" id="kedvezmeny_div" style="display:none;">
										<label>Kedvezmény</label>
										<div class="input-group">
											<span class="input-group-addon input_jelolo_kek"><i class="fa fa-star"></i></span>
											<input type="text" class="form-control" id="kedvezmeny" placeholder="Kedvezmény" value="">
											<span id="kedv_tipus_jel" class="input-group-addon">%</span>
										</div>
									</div>
									<div class="form-group" id="kedv_tipus_div" style="display:none;">
										<label>Kedvezmény típusa</label>
										<div class="input-group">
											<span class="input-group-addon input_jelolo_kek"><i class="fa fa-star-o"></i></span>
											<select onChange="kedvTipus()" class="form-control" id="kedv_tipus">
												<option value="szazalek">Százalék</option>
												<option value="osszeg">Összeg</option>
											</select>
										</div>
									</div>
									<div class="checkbox margtop40">
										<label>
											<input type="checkbox" class="minimal" id="egyszeri" /> Egyszer használatos
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<button type="submit" onClick="ujKuponMentes()" class="btn btn-primary">Mentés</button>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-hover table-striped margbot0">
							<tbody>
								<tr>
									<th>Kód</th>
									<th>Kedvezmény tipus</th>
									<th style="text-align:center;">Mérték / Termék ID</th>
									<th style="text-align:center;">Egyszeri</th>
									<th style="text-align:center;">Vége</th>
									<th style="width:30px;"></th>
								</tr>
								<?php
									$query = "SELECT * FROM ".$webjel."kuponok ORDER BY vege DESC";
									$feher_hatter = 0;
									foreach ($pdo->query($query) as $row)
									{
										print '<tr>';
											print '<td class="tablasor" >'.$row['kod'].'</td>
											<td>'.$row['kupon_fajta'];
												if($row['min_kosar_ertek'] > 0)
												{
													print '<br/>(min. érték: '.$row['min_kosar_ertek'].')';
												}
											print '</td>';
											print '<td style="text-align:center;">';
												if ($row['kupon_fajta'] == 'Ajándék termék')
												{
													// $query_csop = "SELECT * FROM ".$webjel."termekek where id=".$row['termek_id'];
													// $res = $pdo->prepare($query_csop);
													// $res->execute();
													// $row_csop = $res -> fetch();
													// $link = 'termek_kezelo.php?csop_id='.$row_csop['csop_id'].'&id='.$row['termek_id'];
													// print '<a href="'.$link.'">'.$row['termek_id'].'</a>';
													print 'ID: '.$row['termek_id'];
												}
												else if ($row['kupon_fajta'] == 'Kedvezmény')
												{
													print $row['kedvezmeny'];
													if ($row['kedv_tipus'] == 'szazalek')
													{
														print '%';
													}
													else if ($row['kedv_tipus'] == 'osszeg')
													{
														print 'Ft';
													}
												}
											print '</td>';
											if($row['egyszeri'] == 0)
											{
												print '<td  style="text-align:center;">nem</td>';
											}
											else
											{
												if($row['rendeles_id'] == 0)
												{
													print '<td  style="text-align:center;" style="color:#67b735;">igen</td>';
												}
												else
												{
													// print '<td  style="text-align:center;"><a style="color:red;" href="rendelesek.php?rendeles_id='.$row['rendeles_id'].'&command=rendelesek_uj">rendelés: '.$row['rendeles_id'].'</a></td>';
													$query_rend = "SELECT * FROM ".$webjel."rendeles WHERE rendeles_id=".$row['rendeles_id'];
													$res = $pdo->prepare($query_rend);
													$res->execute();
													$row_rend = $res -> fetch();
													print '<td  style="text-align:center;"><a style="color:red;" href="rendelesek.php?id='.$row_rend['id'].'&fajl=module-rendelesek/lista.php">rendelés: '.$row['rendeles_id'].'</a></td>';
												}
											}
											print '<td  style="text-align:center;">'.$row['vege'].'</td>';
											print '<td  style="text-align:center;">';
												?>
												<img onClick="rakerdez_kupon('rakerdez_torles', <?php print $row['id']; ?>, '<?php print $row['kod']; ?>')" src="images/ikon_torles_2.png" data-toggle="tooltip" style="cursor:pointer;" title="Kupon törlése" />
												<?php
											print '</td>';
										print '</tr>';
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

