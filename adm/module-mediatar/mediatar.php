<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
	// Mentés
	/* if(isset($_GET['command']) && $_GET['command'] == 'mentes')
	{
		// URL név meghatározása
		include $gyoker.'/adm/module/mod_urlnev.php';
		if ($nev_url == '')
		{
			$nev_url = rand(1,99999);
		}
		// Egyezőség vizsgálata
		$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."galeria WHERE nev_url = '".$nev_url."'");
		$res->execute();
		$rownum2 = $res->fetchColumn();
		if ($rownum2 > 0) // Ha van már ilyen nevű
		{
			$query = "SELECT * FROM ".$webjel."galeria WHERE nev_url = '".$nev_url."'";
			$res = $pdo->prepare($query);
			$res->execute();
			$row = $res -> fetch();
			if($rownum2 > 1 || $row['id'] != $_GET['id']) // Ha nem saját maga
			{
				$nev_url = $nev_url.'-'.$_GET['id'];
			}
		}
		
		$updatecommand = "UPDATE ".$webjel."galeria SET nev=?, leiras=?, nev_url=? WHERE id=?";
		$result = $pdo->prepare($updatecommand);
		$result->execute(array($_POST['nev'],$_POST['leiras'],$nev_url,$_GET['id']));
	} */
?>
		  
<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1>Médiatár<small></small></h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li class="active">Médiatár</li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
			<!-- Képek -->
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title">Képek feltöltése</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<div class="row"><div class="col-md-12"><form id="kepfeltoltes" action="/upload-target" class="dropzone"></form></div></div>
					</div>
					<div class="box-body" id="galeria_kepek_div"><div class="row">
						<?php
							include('mediatar_kepek.php');
						?>
					</div></div>
				</div>
			</div>
		</div>
	</section>
</div>
