<?php
	if (isset($_GET['script']))
	{
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
	// Kép törlés
	if(isset($_GET['nev']))
	{
		$dir = $gyoker."/images/mediatar/";
		unlink($dir.$_GET['nev']);
	}
	 
	$dir = $gyoker."/images/mediatar/";
	if ($dh = opendir($dir)) {
		$images = array();
		$json = array();

		while (($file = readdir($dh)) !== false) {
			if (!is_dir($dir.$file)) {
				$images[] = $file;
				$json[] = $elem = '{ "image": "'.$domain.'/images/mediatar/'.$file.'", "thumb": "'.$domain.'/images/mediatar/'.$file.'", "folder": "Mediatar" }';
			}
		}

		closedir($dh);
		// print_r($images);
	}
	
	// mediatar.json újraírása
	
	$file = fopen($gyoker."/adm/mediatar.json","w");
	fwrite($file, '[');
	$a = 0;
	foreach ($json as $row)
	{
		if($a>0) { fwrite($file, ','); }
		$a++;
		// $elem = '{ "image": "'.$domain.'/images/mediatar/'.$row.'", "thumb": "'.$domain.'/images/mediatar/'.$row.'", "folder": "Mediatar" }';
		// fputcsv($file,$row);
		// fputcsv($file,explode(',',$row));
		fwrite($file, $row);
	}
	fwrite($file, ']');
	fclose($file);
	
	
	
	$rownum = count($images); // Hány kép van a mappában?
	
	$kep_1_oszlopban = floor($rownum / 4);
	$maradek = $rownum - ($kep_1_oszlopban * 4);
// print $rownum;
	
	$oszlop_1_ig = $kep_1_oszlopban - 1;
	if($maradek > 0) { $oszlop_1_ig++; }
	$oszlop_2_tol = $oszlop_1_ig + 1;
	$oszlop_2_ig = $oszlop_2_tol + $kep_1_oszlopban - 1;
	if($maradek > 1) { $oszlop_2_ig++; }
	$oszlop_3_tol = $oszlop_2_ig + 1;
	$oszlop_3_ig = $oszlop_3_tol + $kep_1_oszlopban - 1;
	if($maradek > 2) { $oszlop_3_ig++; }
	$oszlop_4_tol = $oszlop_3_ig + 1;
	$oszlop_4_ig = $oszlop_4_tol + $kep_1_oszlopban - 1;
	
	/* $oszlop_1_ig = $kep_1_oszlopban;
	if($maradek > 0) { $oszlop_1_ig++; }

	$oszlop_2_tol = $oszlop_1_ig;
	$oszlop_2_ig = $kep_1_oszlopban;
	if($maradek > 1) { $oszlop_2_ig++; }

	$oszlop_3_tol = $kep_1_oszlopban * 2;
	if($maradek > 0) { $oszlop_3_tol = $kep_1_oszlopban + $oszlop_2_ig + 1; }
	$oszlop_3_ig = $oszlop_3_tol + $kep_1_oszlopban; */
	
	// 1. oszlop
	print '<div class="col-md-3 col-sm-6">';
		// print 'ig:'.$oszlop_1_ig;
		for ($i = 0; $i <= $oszlop_1_ig; $i++) 
		{
			print '<div class="kepek_termek_adatlap"><img src="../images/mediatar/'.$images[$i].'" />';
			?><img onClick="termekKepTorles('<?php print $images[$i]; ?>')" src="images/ikon_torles.png" class="kepek_termek_adatlap_torles" data-toggle="tooltip" title="Törlés" /> <?php
			print '</div>';
		}
	print '</div>';
	// 2. oszlop
	print '<div class="col-md-3 col-sm-6">';
		// print 'tol:'.$oszlop_2_tol.' ig:'.$oszlop_2_ig;
		for ($i = $oszlop_2_tol; $i <= $oszlop_2_ig; $i++) 
		{
			print '<div class="kepek_termek_adatlap"><img src="../images/mediatar/'.$images[$i].'" />';
			?><img onClick="termekKepTorles('<?php print $images[$i]; ?>')" src="images/ikon_torles.png" class="kepek_termek_adatlap_torles" data-toggle="tooltip" title="Törlés" /> <?php
			print '</div>';
		}
	print '</div>';
	// 3. oszlop
	print '<div class="col-md-3 col-sm-6">';
		// print 'tol:'.$oszlop_3_tol.' ig:'.$oszlop_3_ig;
		for ($i = $oszlop_3_tol; $i <= $oszlop_3_ig; $i++) 
		{
			print '<div class="kepek_termek_adatlap"><img src="../images/mediatar/'.$images[$i].'" />';
			?><img onClick="termekKepTorles('<?php print $images[$i]; ?>')" src="images/ikon_torles.png" class="kepek_termek_adatlap_torles" data-toggle="tooltip" title="Törlés" /> <?php
			print '</div>';
		}
	print '</div>';
	// 4. oszlop
	print '<div class="col-md-3 col-sm-6">';
		// print 'tol:'.$oszlop_4_tol.' ig:'.$oszlop_4_ig;
		for ($i = $oszlop_4_tol; $i <= $oszlop_4_ig; $i++) 
		{
			print '<div class="kepek_termek_adatlap"><img src="../images/mediatar/'.$images[$i].'" />';
			?><img onClick="termekKepTorles('<?php print $images[$i]; ?>')" src="images/ikon_torles.png" class="kepek_termek_adatlap_torles" data-toggle="tooltip" title="Törlés" /> <?php
			print '</div>';
		}
	print '</div>';
?>
