<?php
	session_start();
	ob_start();
	include '../../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	if($_POST['szuro'] == 'true') { $szuro = 1; } else { $szuro = 0; }
	if($_POST['valaszthato'] == 'true') { $valaszthato = 1; } else { $valaszthato = 0; }
	if($_POST['plecsni'] == 'true') { $plecsni = 1; } else { $plecsni = 0; }
	
	$updatecommand = "UPDATE ".$webjel."termek_uj_parameterek SET nev=?, nev_en=?, nev_de=?, szuro=".$szuro.", szuro_sorrend=".$_POST['szuro_sorrend'].", valaszthato=".$valaszthato.", plecsni=".$plecsni.", mertekegyseg=? WHERE id=".$_POST['id'];
	$result = $pdo->prepare($updatecommand);
	$result->execute(array($_POST['nev'],$_POST['nev_en'],$_POST['nev_de'],$_POST['mertekegyseg']));
	// print_r($_POST);
	
	// echo $updatecommand;
?>

