<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	// Törlés
	if(isset($_POST['torles_id']))
	{
		// Paraméter
		$pdo->exec("DELETE FROM ".$webjel."termek_uj_parameterek WHERE id =".$_POST['torles_id']);
		// Értékek
		$pdo->exec("DELETE FROM ".$webjel."termek_uj_parameter_ertekek WHERE parameter_id =".$_POST['torles_id']);
	}
	// Ikon törlés
	else if(isset($_POST['torles_ikon_id']))
	{
		$pdo->exec("UPDATE ".$webjel."termek_uj_parameterek SET ikon='' WHERE id=".$_POST['torles_ikon_id']);
	}
	// Ikon feltöltése
	else if (isset($_POST['ikon_param_id'])) {
		$ds = DIRECTORY_SEPARATOR;
		$dir = $gyoker.'/images/termekek/';
		$tempFile = $_FILES['file']['tmp_name'];
		$targetPath = $dir . $ds;
		$file = $_FILES['file']['name'];
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		$fn = 'param-ikon-'.$_POST['ikon_param_id'].'.'.$ext;
		$targetFile =  $targetPath. $fn; 
		move_uploaded_file($tempFile,$targetFile);
		
		// Adatbázidba töltés
		$pdo->exec("UPDATE ".$webjel."termek_uj_parameterek SET ikon='".$fn."' WHERE id=".$_POST['ikon_param_id']);
	}
?>
<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1 id="myModal">Termék paraméterek</h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li class="active">Termék paraméterek</li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<span class="menu_gomb">Új paramétert a termékeknél lehet létrehozni.</a>
					</div>
					<!--LISTA-->
					<div class="table-responsive">
						<table class="table table-hover table-bordered table-striped margbot0">
							<tbody>
								<tr>
									<th>Megnevezés</th>
									<th>Megnevezés EN</th>
									<th>Megnevezés DE</th>
									<th>Mértékegység</th>
									<th>Szűrőbe</th>
									<th>Szűrő sorrend</th>
									<th>Választható</th>
									<th>Ikon</th>
									<th>Plecsni</th>
									<th style="width: 86px;"></th>
									<th></th>
								</tr>
								<?php
									$query = "SELECT *
									FROM ".$webjel."termek_uj_parameterek ORDER BY nev ASC";
									foreach ($pdo->query($query) as $row)
									{
										echo '<tr>';
											echo '<td><input attr_termid="'.$row['id'].'" class="form-control sgomb_ok" type="text" id="nev_'.$row['id'].'" value="'.$row['nev'].'"/></td>
											<td><input attr_termid="'.$row['id'].'" class="form-control sgomb_ok" type="text" id="nev_en_'.$row['id'].'" value="'.$row['nev_en'].'"/></td>
											<td><input attr_termid="'.$row['id'].'" class="form-control sgomb_ok" type="text" id="nev_de_'.$row['id'].'" value="'.$row['nev_de'].'"/></td>
											<td><input attr_termid="'.$row['id'].'" class="form-control sgomb_ok" type="text" id="me_'.$row['id'].'" value="'.$row['mertekegyseg'].'"/></td>
											<td align="center" class="td_vkozep">';
												if($row['szuro'] == 1) { $checked = 'checked'; } else { $checked = ''; }
												echo '<input attr_termid="'.$row['id'].'" class="checkbox sgomb_cb" type="checkbox" value="1" id="szuro_cbox_'.$row['id'].'" '.$checked.'/>';
											echo '</td>
											<td><input attr_termid="'.$row['id'].'" style="width: 90px;" class="form-control sgomb_ok" type="text" id="szuro_sorrend_'.$row['id'].'" value="'.$row['szuro_sorrend'].'"/></td>
											<td align="center" class="td_vkozep">';
												if($row['valaszthato'] == 1) { $checked = 'checked'; } else { $checked = ''; }
												echo '<input attr_termid="'.$row['id'].'" class="checkbox sgomb_cb" type="checkbox" value="1" id="valaszthato_cbox_'.$row['id'].'" '.$checked.'/>';
											echo '</td>
											<td class="td_vkozep">';
												if($row['ikon'] == '')
												{
													echo '<a onClick="ikonFeltoltes('.$row['id'].')" class="fa fa-cloud-upload upload_ikon"></a>';
												}
												else
												{
													echo '<a onClick="ikonTorles('.$row['id'].')" data-toggle="tooltip" title="Ikon törlése"><img src="'.$domain.'/images/termekek/'.$row['ikon'].'" style="width:26px;cursor:pointer;"/></a>';
												}
											echo '</td>';
											echo '<td align="center" class="td_vkozep">';
												if($row['plecsni'] == 1) { $checked = 'checked'; } else { $checked = ''; }
												echo '<input attr_termid="'.$row['id'].'" class="checkbox sgomb_cb" type="checkbox" value="1" id="plecsni_cbox_'.$row['id'].'" '.$checked.'/>';
											echo '</td>';											
											echo '<td>
												<a style="display:none" onclick="mentesParameter('.$row['id'].')" class="btn btn-primary" id="mod-mentes'.$row['id'].'">Mentés</a>
											</td>';
											print '<td  style="text-align:center;">';
												?>
												<img onClick="parameterTorles(<?php print $row['id']; ?>, '<?php print $row['nev']; ?>')" src="images/ikon_torles_2.png" data-toggle="tooltip" style="cursor:pointer;" title="Paraméter törlése" />
												<?php
											print '</td>';
										print '</tr>';
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

