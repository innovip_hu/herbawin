<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	if(isset($_GET['torlendo_parameter_id']))
	{
		$pdo->exec("DELETE FROM ".$webjel."termek_parameterek WHERE id =".$_GET['torlendo_parameter_id']);
		$pdo->exec("DELETE FROM ".$webjel."termek_parameter_ertekek WHERE termek_parameter_id =".$_GET['torlendo_parameter_id']);
		$pdo->exec("DELETE FROM ".$webjel."termek_termek_parameter_ertekek WHERE termek_parameter_id =".$_GET['torlendo_parameter_id']);
	}
	else if(isset($_GET['torlendo_parameter_ertek_id']))
	{
		$pdo->exec("DELETE FROM ".$webjel."termek_parameter_ertekek WHERE id =".$_GET['torlendo_parameter_ertek_id']);
		$pdo->exec("DELETE FROM ".$webjel."termek_termek_parameter_ertekek WHERE termek_parameter_id =".$_GET['parameter_id']." AND ertek ='".$_GET['torlendo_parameter_ertek_id']."'");
	}
	else if (isset($_POST['command']) && $_POST['command'] == "uj_parameter")
	{
		$insertcommand = "INSERT INTO ".$webjel."termek_parameterek (nev, tipus, mertekegyseg) VALUES ('".$_POST['nev']."', '".$_POST['tipus']."', '".$_POST['mertekegyseg']."')";
		$result = $pdo->prepare($insertcommand);
		$result->execute();
		$last_id = $pdo->lastInsertId();
		if (isset($_POST['elso_ertek']) && $_POST['elso_ertek'] != '')
		{
			$insertcommand = "INSERT INTO ".$webjel."termek_parameter_ertekek (termek_parameter_id, nev) VALUES ('".$last_id."', '".$_POST['elso_ertek']."')";
			$result = $pdo->prepare($insertcommand);
			$result->execute();
		}
	}
	else if (isset($_POST['command']) && $_POST['command'] == "uj_parameter_ertek")
	{
		$insertcommand = "INSERT INTO ".$webjel."termek_parameter_ertekek (termek_parameter_id, nev) VALUES ('".$_POST['parameter_id']."', '".$_POST['nev']."')";
		$result = $pdo->prepare($insertcommand);
		$result->execute();
	}
?>
<!--Modal-->
<div class="example-modal">
	<div id="rakerdez_torles" class="modal modal-danger">
	  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Törlés</h4>
			</div>
			<div class="modal-body">
				<p>Biztos törölni szeretnéd a(az) "<span id="modal_torles_nev"></span>" paramétert?</p>
			</div>
			<div class="modal-footer">
				<input type="hidden" id="modal_torles_id" value="" />
				<button onClick="kuponTorles('rakerdez_torles')" type="button" class="btn btn-outline pull-left" data-dismiss="modal">Igen</button>
				<button onClick="megsem('rakerdez_torles')" type="button" class="btn btn-outline">Mégsem</button>
			</div>
		</div>
	  </div>
	</div>
</div>
<div class="example-modal">
	<div id="rakerdez_torles_ertek" class="modal modal-danger">
	  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Törlés</h4>
			</div>
			<div class="modal-body">
				<p>Biztos törölni szeretnéd a(az) "<span id="modal_torles_nev_ertek"></span>" paramétert?</p>
			</div>
			<div class="modal-footer">
				<input type="hidden" id="modal_torles_id_ertek" value="" />
				<input type="hidden" id="modal_torles_id_ertek_ertek" value="" />
				<button onClick="parameterErtekTorles('rakerdez_torles_ertek')" type="button" class="btn btn-outline pull-left" data-dismiss="modal">Igen</button>
				<button onClick="megsem('rakerdez_torles_ertek')" type="button" class="btn btn-outline">Mégsem</button>
			</div>
		</div>
	  </div>
	</div>
</div>

<div class="content-wrapper">
	<section class="content-header">
	  <h1 id="myModal">Termék paraméterek</h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li class="active">Termék paraméterek</li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-6">
				<div class="box box-primary">
					<div class="box-header with-border">
						<span class="menu_gomb"><a onClick="ujKuponFrom()"><i class="fa fa-plus"></i> Új paraméter</span></a>
						<span class="menu_gomb"><a onClick="ujParameterErtekForm()"><i class="fa fa-plus"></i> Új érték</span></a>
					</div>
					<!--Új paraméter-->
					<div id="uj_kupon" class="padtop10 padbot10" style="display:none;">
						<div class="col-md-12">
							<div class="form-group">
								<label>Típus</label>
								<div class="input-group">
									<span class="input-group-addon input_jelolo_nsarga"><i class="fa fa-tags"></i></span>
									<select onChange="parameterTipusai()" class="form-control" id="tipus">
										<option value="egyedi">Egyedi</option>
										<option value="igen_nem">Igen/nem</option>
										<option value="lenyilo-egy_valaszthato">Lenyíló egy lehetőséggel</option>
										<option value="lenyilo-tobb_valaszthato">Lenyíló több lehetőséggel</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-12">
									<div class="form-group">
										<label>Megnevezés</label>
										<div class="input-group">
											<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
											<input type="text" class="form-control" id="nev" placeholder="Megnevezés" value="">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-12">
									<div class="form-group" id="mertekegyseg_div">
										<label>Mértékegység</label>
										<div class="input-group">
											<span class="input-group-addon input_jelolo_kek"><i class="fa fa-beer"></i></span>
											<input type="text" class="form-control" id="mertekegyseg" placeholder="Mértékegység" value="">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-12">
									<div class="form-group" id="elso_ertek_div" style="display:none;">
										<label>Első érték</label>
										<div class="input-group">
											<span class="input-group-addon input_jelolo_kek"><i class="fa fa-list-ol"></i></span>
											<input type="text" class="form-control" id="elso_ertek" placeholder="Első érték" value="">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<button type="submit" onClick="ujParameterMentes()" class="btn btn-primary">Mentés</button>
						</div>
					</div>
					<!--Új érték-->
					<div id="uj_parameter_ertek_div" class="padtop10 padbot10" style="display:none;">
						<div class="col-md-12">
							<div class="form-group">
								<label>Paraméter</label>
								<div class="input-group">
									<span class="input-group-addon input_jelolo_nsarga"><i class="fa fa-tags"></i></span>
									<select class="form-control" id="ertek_parametere">
										<?php
										$query = "SELECT * FROM ".$webjel."termek_parameterek WHERE tipus='lenyilo-egy_valaszthato' OR tipus='lenyilo-tobb_valaszthato' ORDER BY nev ASC";
										foreach ($pdo->query($query) as $row)
										{
											print '<option value="'.$row['id'].'">'.$row['nev'].'</option>';
										}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-12">
									<div class="form-group">
										<label>Megnevezés</label>
										<div class="input-group">
											<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>
											<input type="text" class="form-control" id="ertek_nev" placeholder="Megnevezés" value="">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<button type="submit" onClick="ujParameterErtekMentes()" class="btn btn-primary">Mentés</button>
						</div>
					</div>
					<!--LISTA-->
					<div class="table-responsive">
						<table class="table table-hover table-striped margbot0">
							<tbody>
								<tr>
									<th>Megnevezés</th>
									<th>Tipus</th>
									<th>Mértékegység</th>
									<th></th>
								</tr>
								<?php
									$query = "SELECT *, tipus as tipus_eredeti 
											,(CASE 
												WHEN tipus = 'egyedi' THEN 'Egyedi' 
												WHEN tipus = 'igen_nem' THEN 'Igen/nem' 
												WHEN tipus = 'lenyilo-egy_valaszthato' THEN 'Lenyíló egy lehetőséggel' 
												WHEN tipus = 'lenyilo-tobb_valaszthato' THEN 'Lenyíló több lehetőséggel' 
											END) AS tipus 
									FROM ".$webjel."termek_parameterek ORDER BY nev ASC";
									$feher_hatter = 0;
									foreach ($pdo->query($query) as $row)
									{
										print '<tr>';
											print '<td>'.$row['nev'].'</td>
											<td>'.$row['tipus'].'</td>
											<td>'.$row['mertekegyseg'].'</td>';
											print '<td  style="text-align:center;">';
												?>
												<img onClick="rakerdez_kupon('rakerdez_torles', <?php print $row['id']; ?>, '<?php print $row['nev']; ?>')" src="images/ikon_torles_2.png" data-toggle="tooltip" style="cursor:pointer;" title="Paraméter törlése" />
												<?php
											print '</td>';
										print '</tr>';
										// Választós paraméterek
										if($row['tipus_eredeti'] == 'lenyilo-egy_valaszthato' || $row['tipus_eredeti'] == 'lenyilo-tobb_valaszthato')
										{
											$query_ertekek = "SELECT * FROM ".$webjel."termek_parameter_ertekek WHERE termek_parameter_id=".$row['id']." ORDER BY nev ASC";
											foreach ($pdo->query($query_ertekek) as $row_ertekek)
											{
												print '<tr>';
													print '<td colspan="2" style="padding-left:50px; color: #3C8DBC;">'.$row_ertekek['nev'].'</td>';
													print '<td>';
														?>
														<img onClick="rakerdez_param_ertek('rakerdez_torles_ertek', <?php print $row['id']; ?>, '<?php print $row_ertekek['nev']; ?>', <?php print $row_ertekek['id']; ?>)" src="images/ikon_torles_2.png" data-toggle="tooltip" style="cursor:pointer;" title="Érték törlése" />
														<?php
													print '</td><td></td>';
												print '</tr>';
											}
										}
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

