<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	// popup törlése
	if(isset($_GET['torlendo_banner_id']))
	{
		$query = "SELECT * FROM ".$webjel."popup WHERE id=".$_GET['torlendo_banner_id'];
		$res = $pdo->prepare($query);
		$res->execute();
		$row = $res -> fetch();
		// popup képe
		$dir = $gyoker."/images/termekek/";
		unlink($dir.$row['kep']);
		// popup
		$deletecommand = "DELETE FROM ".$webjel."popup WHERE id =".$_GET['torlendo_banner_id'];
		$result = $pdo->prepare($deletecommand);
		$result->execute();
	}
	
	$query = "SELECT * FROM ".$webjel."popup ORDER BY id DESC";
	$i = 1;
	$van = 0; 
	foreach ($pdo->query($query) as $row)
	{
		$van = 1; 
		if($i == 1) { print '<div class="row">'; }
		print '<div class="col-md-4">
			<div class="box box-primary banner">
				<div class="box-body">
					<img src="'.$domain.'/images/termekek/'.$row['kep'].'" />';
					?>
					<img onClick="rakerdez_banner('rakerdez_torles', <?php print $row['id']; ?>, '<?php print $row['nev']; ?>')" src="images/ikon_torles.png" class="banner_torles" data-toggle="tooltip" title="Pop-up törlése" />
					<?php
					print '<div class="form-group">
						<label>Megnevezés</label>
						<div class="input-group">
							<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>';
							?>
							<input onKeyUp="mentesNezet(<?php print $row['id']; ?>, 'nev_')" type="text" class="form-control" id="nev_<?php print $row['id']; ?>" placeholder="Megnevezés" value="<?php print $row['nev']; ?>">
							<?php
						print '</div>
					</div>
					<div class="form-group">
						<label>Link</label>
						<div class="input-group">
							<span class="input-group-addon input_jelolo_kek"><i class="fa fa-link"></i></span>';
							?>
							<input onKeyUp="mentesNezet(<?php print $row['id']; ?>, 'link_')" type="text" class="form-control" id="link_<?php print $row['id']; ?>" placeholder="Link" value="<?php print $row['link']; ?>">
							<?php
						print '</div>
					</div>
					<div class="form-group">
						<label>Megjelenés kezdete</label>
						<div class="input-group">
							<span class="input-group-addon input_jelolo_kek"><i class="fa fa-calendar"></i></span>';
							?>
							<input onClick="mentesNezet(<?php print $row['id']; ?>, 'datum_tol_')" type="text" class="form-control datepicker" id="datum_tol_<?php print $row['id']; ?>" value="<?php print $row['datum_tol']; ?>">
							<?php
						print '</div>
					</div>
					<div class="form-group">
						<label>Megjelenés vége</label>
						<div class="input-group">
							<span class="input-group-addon input_jelolo_kek"><i class="fa fa-calendar"></i></span>';
							?>
							<input onClick="mentesNezet(<?php print $row['id']; ?>, 'datum_ig_')" type="text" class="form-control datepicker" id="datum_ig_<?php print $row['id']; ?>" value="<?php print $row['datum_ig']; ?>">
							<?php
						print '</div>
					</div>
					<div class="box-footer" id="banner_footer_'.$row['id'].'">
						<button type="submit" onClick="bannerMentes('.$row['id'].')" class="btn btn-primary pull-right">Mentés</button>
					</div>
				</div>
			</div>
		</div>';
		$i++;
		if($i == 5) { print '</div>'; $i = 1; }
	}
	if($i > 1 && $van == 1) { print '</div>'; }
?>

