<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
// Oldalankénti szám
	if (isset($_GET['oldalszam']) && $_GET['oldalszam'] != '')
	{
		$oldalszam = $_GET['oldalszam'];
	}
	else
	{
		$oldalszam = 200; //ALAPÁLLAPOT
	}
// Rekordok száma
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."users ");
	$res->execute();
	$rownum = $res->fetchColumn();
// Kezdés meghatározása
	if (isset($_GET['kezd']) && $_GET['kezd'] != '' && $rownum > $oldalszam)
	{
		$kezd = $_GET['kezd'];
	}
	else
	{
		$kezd = 0;
	}
// Aktuális oldal
	$aktualis_oldal = ($kezd + $oldalszam) / $oldalszam;
// Utolsó oldal
	$utolso_oldal = ceil($rownum / $oldalszam);
// Sorrend
	if (isset($_GET['sorr_tip']))
	{
		$sorr_tip = $_GET['sorr_tip'];
	}
	else
	{
		$sorr_tip = 'id'; // Alap rendezési feltétel
	}
	if (isset($_GET['sorrend']))
	{
		$sorrend = $_GET['sorrend'];
	}
	else
	{
		$sorrend = 'ASC'; // Alap rendezési feltétel
	}
	
	// Adatok
	$query = "SELECT * FROM ".$webjel."rendeles WHERE id=".$_GET['id'];
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
?>
<!--Modal-->
<div class="example-modal">
<div id="rakerdez_torles" class="modal modal-danger">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
		<h4 class="modal-title">Törlés</h4>
	  </div>
	  <div class="modal-body">
		<p>Biztos törölni szeretnéd a rendelést?</p>
	  </div>
	  <div class="modal-footer">
		<button onClick="torol('<?php print $_GET['id']; ?>', '<?php print $_GET['fajl']; ?>')" type="button" class="btn btn-outline pull-left" data-dismiss="modal">Igen</button>
		<button onClick="megsem('rakerdez_torles')" type="button" class="btn btn-outline">Mégsem</button>
	  </div>
	</div>
  </div>
</div>
</div>
		  
<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1>Rendelések<small>Azonosító: <?php print $row['id']; ?></small></h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li><a onClick="vissza('<?php print $_GET['fajl']; ?>')"><i class="fa fa-shopping-cart"></i> Rendelések</a></li>
		<li class="active"><?php print $row['id']; ?></li>
	  </ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-6">
				<!-- Személyes adatok -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Rendelés adatok - <?php print $row['id']; ?></h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<strong><i class="fa fa-calendar margin-r-5"></i>  Vásárlás időpontja</strong>
						<p class="text-muted"><?php print $row['datum'].' '.$row['ido']; ?></p>

						<hr>

						<strong><i class="fa fa-eur margin-r-5"></i> Fizetés</strong>
						<p class="text-muted">
							<?php
								print $row['fiz_mod'];
								if($row['fiz_mod'] == 'Bankkártyás fizetés') // Bankkártyás fizetés
								{
									if($row['fizetve_otp'] == 1) // Fizetve
									{
										print ' - <span style="color: #00a65a;">Kifizetve</span>';
									}
									else
									{
										print ' - <span style="color: #dd4b39;">Megszakítva</span>';
									}
								}
							?>
						</p>

						<hr>

						<strong><i class="fa fa-truck margin-r-5"></i> Szállítás</strong>
						<p class="text-muted"><?php print $row['szall_mod']; ?></p>

						<hr>

						<strong><i class="fa fa-sticky-note-o margin-r-5"></i> Megjegyzés</strong>
						<p class="text-muted"><?php print nl2br($row['megjegyzes']); ?></p>
					</div>

				</div>
				<!-- Gombok -->
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">Vezérlőpult</h3>
					</div>
					<div class="box-body">
						<?php
							if ($row['teljesitve'] == 0) //ha még nincs feldolgozva
							{
								if ($row['szallitva'] == 0) //ha még nincs kiszállítva
								{
									?><a onClick="szallitva('<?php print $_GET['id']; ?>', '<?php print $_GET['fajl']; ?>')" class="btn btn-app"><i class="fa fa-truck"></i> Szállítás</a><?php
								}
								?>
									<a onClick="lezar('<?php print $_GET['id']; ?>', '<?php print $_GET['fajl']; ?>')" class="btn btn-app"><i class="fa fa-lock"></i> Lezár</a>
									<a onclick="rakerdez('rakerdez_torles')" class="btn btn-app"><i class="fa fa-times"></i> Töröl</a>
								<?php
							}
							else
							{
								?>
									<a onClick="felnyit('<?php print $_GET['id']; ?>', '<?php print $_GET['fajl']; ?>')" class="btn btn-app"><i class="fa fa-unlock-alt"></i> Felnyit</a>
								<?php
							}
						?>
						<form style="display:none;" id="nyomtat_form" method="POST" action="<?php print $domain; ?>/adm/pdf/rendeles.php" target="_blank">
							<input type="hidden" name="rendeles_id" value="<?php print $row['id']; ?>" />
						</form>
						<a onClick="document.getElementById('nyomtat_form').submit();" class="btn btn-app"><i class="fa fa-print"></i> Nyomtatás</a>
					</div>
				</div>
				<!-- Vásárolt tételek -->
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Vásárolt tételek</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body no-padding">
						<table class="table table-striped">
							<tbody>
								<tr>
									<th>Terméknév</th>
									<th>Egységár</th>
									<th>Mennyiség</th>
									<th style="text-align:right;">Összesen</th>
								</tr>
								<?php
									$query_tetelek = "SELECT * FROM ".$webjel."rendeles_tetelek WHERE rendeles_id=".$_GET['id'];
									$db = 0;
									$ar = 0;
									foreach ($pdo->query($query_tetelek) as $row_tetelek)
									{
										$query_term = 'SELECT * FROM '.$webjel.'termekek WHERE id='.$row_tetelek['term_id'];
										$res = $pdo->prepare($query_term);
										$res->execute();
										$row_tetelek_term  = $res -> fetch();
										$db = $db + 1;
										
										echo '<td>';
											if(isset($row_tetelek_term['id']) && $row_tetelek_term['id'] > 0)
											{
												echo '<a href="'.$domain.'/adm/termekek.php?csop_id='.$row_tetelek_term['csop_id'].'&kezd=0&id='.$row_tetelek_term['id'].'" target="_blank" style="color: #126495; text-decoration: none">';
											}
												echo '<b>'.$row_tetelek['term_nev'].'</b>';
											if(isset($row_tetelek_term['id']) && $row_tetelek_term['id'] > 0)
											{
												echo '</a>';
											}
											// Jellemzők
											$query_jell = "SELECT * FROM ".$webjel."rendeles_tetelek_termek_parameterek WHERE rendeles_tetel_id = ".$row_tetelek['id']." ORDER BY termek_parameter_nev ASC";
											// $elso = 1;
											$elso = 0;
											foreach ($pdo->query($query_jell) as $row_tetelek_jell)
											{
												if($elso == 1) { print ''; $elso = 0; } else { print '<br/>'; }
												print $row_tetelek_jell['termek_parameter_nev'].': '.$row_tetelek_jell['termek_parameter_ertek_nev'];
											}						
											print '</td>';
										if($row_tetelek['term_akcios_ar'] == 0) //ha nem akciós
										{
														print '<td>'.number_format($row_tetelek['term_ar'], 0, ',', ' ').' Ft</td>
														<td>'.$row_tetelek['term_db'].'</td>
														<td style="text-align:right;">'.number_format($row_tetelek['term_ar'] * $row_tetelek['term_db'], 0, ',', ' ').' Ft</td>';
											$ar = $ar + ($row_tetelek['term_ar'] * $row_tetelek['term_db']);
										}
										else //ha akciós
										{
														print '<td>'.number_format($row_tetelek['term_akcios_ar'], 0, ',', ' ').' Ft</td>
														<td>'.$row_tetelek['term_db'].'</td>
														<td style="text-align:right;">'.number_format($row_tetelek['term_akcios_ar'] * $row_tetelek['term_db'], 0, ',', ' ').' Ft</td>';
											$ar = $ar + ($row_tetelek['term_akcios_ar'] * $row_tetelek['term_db']);
										}
										print '</tr>';
									}
									if($row['kupon_id'] > 0)
									{
										$query_kupon = "SELECT * FROM ".$webjel."kuponok WHERE id=".$row['kupon_id'];
										$res = $pdo->prepare($query_kupon);
										$res->execute();
										$row_kupon = $res -> fetch();
										if($row_kupon['kupon_fajta'] == 'Ajándék termék')
										{
											$query_kupon_term = "SELECT * FROM ".$webjel."termekek WHERE id=".$row_kupon['termek_id'];
											$res = $pdo->prepare($query_kupon_term);
											$res->execute();
											$row_kupon_term = $res -> fetch();
											print '<tr><td>'.$row_kupon_term['nev'].'</td><td>(kuponból)</td><td>1</td><td style="text-align:right;">0 Ft</td></tr>';
										}
									}
									
									print '<tr>';
									print '<th colspan="2"></th><th style="text-align:right;">Összesen</th><th style="text-align:right;">'.number_format($ar, 0, ',', ' ').' Ft</th></tr>';
									print '<tr>';
									print '<td colspan="3" style="text-align:right;">Szállítás</td><td style="text-align:right;">'.number_format($row['szallitasi_dij'], 0, ',', ' ').' Ft</td></tr>';
									print '<tr>';
									print '<th colspan="3" style="text-align:right;">Fizetendő</th><th style="text-align:right;">'.number_format($ar + $row['szallitasi_dij'], 0, ',', ' ').' Ft</th></tr>';
								?>
							<tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<!-- Vásárló adatai -->
				<?php
					if ($row['noreg'] == 0)//regisztrált vevő
					{
						$query_user = "SELECT * FROM ".$webjel."users where id=".$row['user_id'];
					}
					else //noreg vevő
					{
						$query_user = "SELECT * FROM ".$webjel."users_noreg where id=".$row['user_id'];
					}
					$res = $pdo->prepare($query_user);
					$res->execute();
					$row_user  = $res -> fetch();
				?>
				<div class="box box-danger">
					<div class="box-header with-border">
						<h3 class="box-title">Vásárló adatai</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<strong><i class="fa fa-user margin-r-5"></i> Név</strong>
						<p class="text-muted"><?php print $row_user['vezeteknev']; ?></p>

						<?php if ($row['hirlevel'] == 1): ?>
						<hr>

						<strong><i class="fa fa-envelope margin-r-5"></i> Hírlevél feliratkozás</strong>
						<p class="text-muted">Igen</p>
						
						<?php else: ?>
						<hr>

						<strong><i class="fa fa-envelope margin-r-5"></i> Hírlevél feliratkozás</strong>
						<p class="text-muted">Nem</p>

						<?php endif ?>

						<hr>

						<strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>
						<p class="text-muted"><?php print $row_user['email']; ?></p>

						<hr>

						<strong><i class="fa fa-phone margin-r-5"></i> Telefon</strong>
						<p class="text-muted"><?php print $row_user['telefon']; ?></p>

						<hr>

						<?php
							if($row['postapont'] != '')
							{
								?>
									<strong><i class="fa fa-eur margin-r-5"></i> Számlázási cím</strong>
									<p class="text-muted"><?php print $row_user['cim_irszam'].' '.$row_user['cim_varos'].', '.$row_user['cim_utca'].' '.$row_user['cim_hszam']; ?></p>

									<hr>

									<strong><i class="fa fa-map-marker margin-r-5"></i> Postapont</strong>
									<p class="text-muted"><?php print $row['postapont']; ?></p>
								<?php
							}
							else if($row['glscspont'] != '')
							{
								?>
									<strong><i class="fa fa-eur margin-r-5"></i> Számlázási cím</strong>
									<p class="text-muted"><?php print $row_user['cim_irszam'].' '.$row_user['cim_varos'].', '.$row_user['cim_utca'].' '.$row_user['cim_hszam']; ?></p>

									<hr>

									<strong><i class="fa fa-map-marker margin-r-5"></i> GLS CsomagPont</strong>
									<p class="text-muted"><?php print $row['glscspont']; ?></p>
								<?php
							}
							else
							{
								if($row_user['cim_szall_varos'] == '')
								{
									?>
										<strong><i class="fa fa-eur margin-r-5"></i> Számlázási-, és szállítási cím</strong>
										<p class="text-muted"><?php print $row_user['cim_irszam'].' '.$row_user['cim_varos'].', '.$row_user['cim_utca'].' '.$row_user['cim_hszam']; ?></p>
									<?php
								}
								else
								{
									$cim_szall_nev = '';
									if($row_user['cim_szall_nev'] != '') { $cim_szall_nev = ', '.$row_user['cim_szall_nev']; }
									?>
										<strong><i class="fa fa-eur margin-r-5"></i> Számlázási cím</strong>
										<p class="text-muted"><?php print $row_user['cim_irszam'].' '.$row_user['cim_varos'].', '.$row_user['cim_utca'].' '.$row_user['cim_hszam']; ?></p>
										
										<hr>

										<strong><i class="fa fa-truck margin-r-5"></i> Szállítási cím</strong>
										<p class="text-muted"><?php print $row_user['cim_szall_irszam'].' '.$row_user['cim_szall_varos'].', '.$row_user['cim_szall_utca'].' '.$row_user['cim_szall_hszam'].$cim_szall_nev; ?></p>
									<?php
								}
							}
						?>
					</div>
				</div>
				<!-- Saját megjegyzés -->
				<div class="box box-lila">
					<div class="box-header with-border">
						<h3 class="box-title">Saját megjegyzés</h3>
						<div class="box-tools pull-right">
							<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group margbot0">
									<textarea class="form-control" id="sajat_megjegyzes" rows="3" placeholder="Saját megjegyzés" style="height: 110px;"><?php print $row['sajat_megjegyzes']; ?></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<button type="submit" onClick="sajatMegjegyzesMentese(<?php print $row['id']; ?>)" class="btn btn-primary">Mentés</button>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<input type="hidden" name="oldalszam" id="oldalszam" value="<?php print $oldalszam;?>"/>
<!--Aktuális oldal - azaz honnan kezdődjön a lekérdezés-->
<input type="hidden" name="kezd" id="kezd" value="<?php print $kezd;?>"/>
<input type="hidden" name="kezd" id="oldalszam" value="<?php print $oldalszam;?>"/>
<!--Sorba rendezés-->
<input type="hidden" name="sorr_tip" id="sorr_tip" value="<?php print $sorr_tip;?>"/>
<input type="hidden" name="sorrend" id="sorrend" value="<?php print $sorrend;?>"/>
