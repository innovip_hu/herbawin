<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
	// Lezárás
	if(isset($_POST['sajat_megjegyzes']))
	{
		$updatecommand = "UPDATE ".$webjel."rendeles SET sajat_megjegyzes=? WHERE id=?";
		$result = $pdo->prepare($updatecommand);
		$result->execute(array($_POST['sajat_megjegyzes'],$_GET['id']));
	}
?>