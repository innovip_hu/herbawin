<?php
	session_start();
	ob_start();
	include '../../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	$updatecommand = "UPDATE ".$webjel."slider SET nev=?, nev_en=?, nev_de=?, link=?, szoveg=?, szoveg_en=?, szoveg_de=?, sorrend=? WHERE id=?";
	$result = $pdo->prepare($updatecommand);
	$result->execute(array($_POST['nev'],$_POST['nev_en'],$_POST['nev_de'],$_POST['link'],$_POST['szoveg'],$_POST['szoveg_en'],$_POST['szoveg_de'],$_POST['sorrend'],$_POST['id']));

?>

