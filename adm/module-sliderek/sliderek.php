<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	// Slider törlése
	if(isset($_GET['torlendo_slider_id']))
	{
		$query = "SELECT * FROM ".$webjel."slider WHERE id=".$_GET['torlendo_slider_id'];
		$res = $pdo->prepare($query);
		$res->execute();
		$row = $res -> fetch();
		// slider képe
		$dir = $gyoker."/images/termekek/";
		unlink($dir.$row['kep']);
		// slider
		$deletecommand = "DELETE FROM ".$webjel."slider WHERE id =".$_GET['torlendo_slider_id'];
		$result = $pdo->prepare($deletecommand);
		$result->execute();
	}
	
	$query = "SELECT * FROM ".$webjel."slider ORDER BY sorrend ASC";
	$i = 1;
	$van = 0; 
	foreach ($pdo->query($query) as $row)
	{
		$van = 1; 
		if($i == 1) { print '<div class="row">'; }
		print '<div class="col-md-6">
			<div class="box box-primary slider">
				<div class="box-body">
					<img src="'.$domain.'/images/termekek/'.$row['kep'].'" />';
					?>
					<img onClick="rakerdez_slider('rakerdez_torles', <?php print $row['id']; ?>, '<?php print $row['nev']; ?>')" src="images/ikon_torles.png" class="slider_torles" data-toggle="tooltip" title="Slider törlése" />
					<?php
					print '<div class="form-group">
						<label>Megnevezés</label>
						<div class="input-group">
							<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>';
							?>
							<input onKeyUp="mentesNezet(<?php print $row['id']; ?>, 'nev_')" type="text" class="form-control" id="nev_<?php print $row['id']; ?>" placeholder="Megnevezés" value="<?php print htmlentities($row['nev']); ?>">
							<?php
						print '</div>
					</div>';
					print '<div class="form-group">
						<label>Megnevezés EN</label>
						<div class="input-group">
							<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>';
							?>
							<input onKeyUp="mentesNezet(<?php print $row['id']; ?>, 'neven_')" type="text" class="form-control" id="neven_<?php print $row['id']; ?>" placeholder="Megnevezés EN" value="<?php print htmlentities($row['nev_en']); ?>">
							<?php
						print '</div>
					</div>';
					print '<div class="form-group">
						<label>Megnevezés DE</label>
						<div class="input-group">
							<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>';
							?>
							<input onKeyUp="mentesNezet(<?php print $row['id']; ?>, 'nevde_')" type="text" class="form-control" id="nevde_<?php print $row['id']; ?>" placeholder="Megnevezés DE" value="<?php print htmlentities($row['nev_de']); ?>">
							<?php
						print '</div>
					</div>';										
					print '<div class="form-group">
						<label>Sorrend</label>
						<div class="input-group">
							<span class="input-group-addon input_jelolo_kek"><i class="fa fa-sort-numeric-asc"></i></span>';
							?>
							<input onKeyUp="mentesNezet(<?php print $row['id']; ?>, 'sorrend_')" type="text" class="form-control" id="sorrend_<?php print $row['id']; ?>" placeholder="Sorrend" value="<?php print $row['sorrend']; ?>">
							<?php
						print '</div>
					</div>';
					print '<div class="form-group">
						<label>Link</label>
						<div class="input-group">
							<span class="input-group-addon input_jelolo_kek"><i class="fa fa-link"></i></span>';
							?>
							<input onKeyUp="mentesNezet(<?php print $row['id']; ?>, 'link_')" type="text" class="form-control" id="link_<?php print $row['id']; ?>" placeholder="Link" value="<?php print $row['link']; ?>">
							<?php
						print '</div>
					</div>
					<div class="form-group">
						<label>Szöveg</label>
						<div class="input-group">
							<span class="input-group-addon input_jelolo_kek"><i class="fa fa-sticky-note-o"></i></span>';
							?>
							<input onKeyUp="mentesNezet(<?php print $row['id']; ?>, 'szoveg_')" type="text" class="form-control" id="szoveg_<?php print $row['id']; ?>" placeholder="Szöveg" value="<?php print htmlentities($row['szoveg']); ?>">
							<?php
						print '</div>
					</div>	
					<div class="form-group">
						<label>Szöveg EN</label>
						<div class="input-group">
							<span class="input-group-addon input_jelolo_kek"><i class="fa fa-sticky-note-o"></i></span>';
							?>
							<input onKeyUp="mentesNezet(<?php print $row['id']; ?>, 'szovegen_')" type="text" class="form-control" id="szovegen_<?php print $row['id']; ?>" placeholder="Szöveg EN" value="<?php print htmlentities($row['szoveg_en']); ?>">
							<?php
						print '</div>
					</div>										
					<div class="form-group">
						<label>Szöveg DE</label>
						<div class="input-group">
							<span class="input-group-addon input_jelolo_kek"><i class="fa fa-sticky-note-o"></i></span>';
							?>
							<input onKeyUp="mentesNezet(<?php print $row['id']; ?>, 'szovegde_')" type="text" class="form-control" id="szovegde_<?php print $row['id']; ?>" placeholder="Szöveg DE" value="<?php print htmlentities($row['szoveg_de']); ?>">
							<?php
						print '</div>
					</div>';
					print '
					<div class="box-footer" id="slider_footer_'.$row['id'].'">
						<button type="submit" onClick="sliderMentes('.$row['id'].')" class="btn btn-primary pull-right">Mentés</button>
					</div>
				</div>
			</div>
		</div>';
		$i++;
		if($i == 3) { print '</div>'; $i = 1; }
	}
	if($i > 1 && $van == 1) { print '</div>'; }
?>

