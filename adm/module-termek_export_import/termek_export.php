<?php
	include '../../config.php';
	include '../../adm/config_adm.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	
// Excel
	//Include PHPExcel
	require_once($gyoker.'/adm/PHPExcel.php');

	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();

	// Set document properties
	$objPHPExcel->getProperties()->setCreator("Innovip.hu Kft.")
								 ->setLastModifiedBy("Innovip.hu Kft.")
								 ->setTitle("Office 2007 XLSX Termekek")
								 ->setSubject("Office 2007 XLSX Termekek")
								 ->setDescription("Webshop termekek")
								 ->setKeywords("termekek")
								 ->setCategory("export");

	// Fejléc
	$i = 0;
	foreach ($conf_mezok as $elem) {
		// $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $elem);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($i, 1, $elem);
		$i++;
	}
	
	// Paraméterek
	$parameterek = array();
	$query_param = "SELECT * FROM ".$webjel."termek_uj_parameterek WHERE valaszthato=0 ORDER BY nev ASC";
	foreach ($pdo->query($query_param) as $row_param)
	{
		$_POST['nev'] = 'a'.$row_param['id'].'_'.$row_param['nev']; // átalakítás a változók miatt
		include $gyoker.'/adm/module/mod_url_tomb.php';
		$parameterek[] = $nev_url;
		$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($i, 1, $nev_url);
		$oszlop = PHPExcel_Cell::stringFromColumnIndex($i);
		$i++;
	}
	
	// var_dump($parameterek);
	
	// Fejléc kék
	$objPHPExcel->getActiveSheet()->getStyle('A1:'.$oszlop.'1')->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('rgb' => '2C82C9')
							),
		  'borders' => array(
								'bottom'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							),
		  'font' => array(
								// 'bold'	=> true,
								'color'	=> array('rgb' => 'FFFFFF')
							)
		 )
	);
	
	// Termékek
	if($_POST['csop_id'] > 0)
	{
		$query = "SELECT * FROM ".$webjel."termekek WHERE csop_id=".$_POST['csop_id']." ORDER BY nev ASC";
	}
	else
	{
		$query = "SELECT * FROM ".$webjel."termekek ORDER BY nev ASC";
	}
	$j = 2;
	foreach ($pdo->query($query) as $row)
	{
		// termek tábla mezői
		$i = 0;
		foreach ($conf_mezok as $elem) {
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($i, $j, $row[$elem]);
			$i++;
		}
		// paraméterek tábla mezői
		foreach ($parameterek as $elem) {
			$param_id = ltrim(strstr($elem,"_",true), 'a');  // ID kinyerése
			$query_param = "SELECT * FROM ".$webjel."termek_uj_parameter_ertekek WHERE termek_id=".$row['id']." AND parameter_id=".$param_id;
			$res = $pdo->prepare($query_param);
			$res->execute();
			$row_param = $res -> fetch();
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($i, $j, $row_param['ertek']);
			$i++;
		}
		$j++;
	}
	// ID-ék piros hátteret kapnak
	$objPHPExcel->getActiveSheet()->getStyle('A2:A'.($j-1))->applyFromArray(
	array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('rgb' => 'ffc7ce')
							),
		  'font' => array(
								'color'	=> array('rgb' => 'cf2d06')
							)
		 )
	);
	
	// Rename worksheet
	$objPHPExcel->getActiveSheet()->setTitle('Termékek');

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);

	// Save Excel 2007 file
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('termekek.xlsx');
	
?>  