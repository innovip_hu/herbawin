<?php
	include '../../config.php';
	include '../../adm/config_adm.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	// Paraméterek
	$parameterek = array();
	$query_param = "SELECT * FROM ".$webjel."termek_uj_parameterek WHERE valaszthato=0 ORDER BY nev ASC";
	foreach ($pdo->query($query_param) as $row_param)
	{
		$_POST['nev'] = 'a'.$row_param['id'].'_'.$row_param['nev']; // átalakítás a változók miatt
		include $gyoker.'/adm/module/mod_url_tomb.php';
		$parameterek[] = $nev_url;
	}
	
	var_dump($parameterek);
	
	if($_POST['csop_id'] > 0)
	{
		$query = "SELECT * FROM ".$webjel."termekek WHERE csop_id=".$_POST['csop_id']." ORDER BY nev ASC";
	}
	else
	{
		$query = "SELECT * FROM ".$webjel."termekek ORDER BY nev ASC";
	}
	
// XML
	$xml = new DOMDocument('1.0', 'UTF-8');
	
	$root = $xml->createElement("products");
	
	$xml->appendChild($root);
	$root->setAttributeNS('http://www.w3.org/2000/xmlns/' ,'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');

	
	foreach ($pdo->query($query) as $row)
	{
		// termek tábla mezői
		foreach ($conf_mezok as $elem) {
			$a = $elem;
			$b = $elem.'Text';
			
			$$a = $xml->createElement($elem);
			$$b = $xml->createTextNode($row[$elem]);
			$$a->appendChild($$b);
		}
		// paraméterek tábla mezői
		foreach ($parameterek as $elem) {
			$param_id = ltrim(strstr($elem,"_",true), 'a');  // ID kinyerése
			$query_param = "SELECT * FROM ".$webjel."termek_uj_parameter_ertekek WHERE termek_id=".$row['id']." AND parameter_id=".$param_id;
			$res = $pdo->prepare($query_param);
			$res->execute();
			$row_param = $res -> fetch();
			
			$a = $elem;
			$b = $elem.'Text';
			
			$$a = $xml->createElement($elem);
			$$b = $xml->createTextNode($row_param['ertek']);
			$$a->appendChild($$b);
		}

		$product = $xml->createElement("product");
		
		// termek tábla mezői
		foreach ($conf_mezok as $elem) {
			$a = $elem;
			$product->appendChild($$a);
		}
		// paraméterek tábla mezői
		foreach ($parameterek as $elem) {
			$a = $elem;
			$product->appendChild($$a);
		}
		
		$root->appendChild($product);
	}

	$xml->save("../xml_termekek.xml");
?>  