<?php
	include '../../config.php';
	include '../../adm/config_adm.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
// Paraméterek
	$parameterek = array();
	$query_param = "SELECT * FROM ".$webjel."termek_uj_parameterek WHERE valaszthato=0 ORDER BY nev ASC";
	foreach ($pdo->query($query_param) as $row_param)
	{
		$_POST['nev'] = 'a'.$row_param['id'].'_'.$row_param['nev']; // átalakítás a változók miatt
		include $gyoker.'/adm/module/mod_url_tomb.php';
		$parameterek[] = $nev_url;
	}

// Ellenőrzés	
	$hiba = 0;
	$hibaszam = 0;
	$hibauzenet = '';
	$path = $_FILES['file']['name'];
	$ext = pathinfo($path, PATHINFO_EXTENSION);
	if($ext != 'xlsx')
	{
		$hiba = 1;
		$hibaszam++;
		$hibauzenet .= '<li class="list-group-item text-right">A feltöltött fájl nem xlsx.</li>';
	}
		
// Feldolgozás
	$i = 0;
	$j = 0;
	if ($_FILES['file']['size'] > 0 && $hiba == 0)
	{
		// Fájl mentése
		$dir = $gyoker.'/adm/_log_xml_feltoltesek/';
		$kiterj = '.'.pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
		$uj_nev = date('Y-m-d-H-i-s');
		move_uploaded_file($_FILES['file']['tmp_name'], $dir.$uj_nev.$kiterj);

		require_once($gyoker.'/adm/PHPExcel/IOFactory.php');

		$inputFileName = $dir.$uj_nev.$kiterj;
		$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);

		$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
		// var_dump($sheetData);
		
		$elem_nevek = array();
		$a = 0;		
		// Excel sorok
		foreach ($sheetData as $sor) {
			// Excel mezők
			if($a == 0) // első sor
			{
				foreach ($sor as $mezo) {
					$elem_nevek[] = $mezo;
				}
			}
			else
			{
				// var_dump($sor);
				$keys = array_keys($sor);
				// echo $sor[$keys[0]];
				
				
				// Módosítás
				if($sor[$keys[0]] > 0) 
				{
					$valtozok = array();
					$sql_elemek = '';
					$k = 0;
					$csop_id_hiba = 0;
					$id = 0;
					// Termék
					foreach ($conf_mezok as $elem) { // mezőkön (oszlopokon) megy végig
						$ertek = $sor[$keys[$k]];
						if($elem == 'id') // ID
						{
							$id = $ertek;
						}
						// ELL, csop_id
						if($elem == 'csop_id')
						{
							if($ertek > 0 && $ertek < 999999999999999) {  } // OK
							else
							{
								$csop_id_hiba = 1;
								$hibaszam++;
								$hibauzenet .= '<li class="list-group-item text-right">csop_id mező nincs kitöltve ('.$id.').</li>';
							}
						}
						// Dátumok
						if($elem == 'akcio_tol' || $elem == 'akcio_ig')
						{
							if(stripos($ertek, ".") !== false)
							{
								$ertek = str_replace(".", "-", $ertek);
							}
							else if (stripos($ertek, "-") == false && stripos($ertek, ".") == false)
							{
								$ertek = PHPExcel_Style_NumberFormat::toFormattedString($ertek,PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY ) ;
								$datum_tomb = explode("/",$ertek);
								$ertek = $datum_tomb[2].'-'.$datum_tomb[1].'-'.$datum_tomb[0];
							}
						}
						$valtozok[] = $ertek;
						if($k > 0) { $sql_elemek .= ', '; }
						$sql_elemek .= $elem.'=? ';
						$k++;
					}
					if($csop_id_hiba == 0)
					{
						$updatecommand = "UPDATE ".$webjel."termekek SET ".$sql_elemek ." WHERE id=".$id;
						$result = $pdo->prepare($updatecommand);
						$result->execute($valtozok);
						$i++;
						// echo $updatecommand.'<br/>';
						// var_dump($valtozok);
					}
				}
				// Új termék
				else
				{
					$valtozok = array();
					$sql_elemek_1 = '';
					$sql_elemek_2 = '';
					$k = 0;
					foreach ($conf_mezok as $elem) {
						$ertek = $sor[$keys[$k]];
						if($ertek == null) { $ertek = ''; }
						
						// Név ellenőrzése
						if($elem == 'nev')
						{
							if($ertek == '') { $mehet = 0; } else { $mehet = 1; }
						}
						// Dátumok
						if($ertek != '')
						{
							if($elem == 'akcio_tol' || $elem == 'akcio_ig')
							{
								if(stripos($ertek, ".") !== false)
								{
									$ertek = str_replace(".", "-", $ertek);
								}
								else if (stripos($ertek, "-") == false && stripos($ertek, ".") == false)
								{
									$ertek = PHPExcel_Style_NumberFormat::toFormattedString($ertek,PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY ) ;
									$datum_tomb = explode("/",$ertek);
									$ertek = $datum_tomb[2].'-'.$datum_tomb[1].'-'.$datum_tomb[0];
								}
							}
						}

						$valtozok[':'.$elem] = $ertek;
						if($k > 0) { $sql_elemek_1 .= ', '; $sql_elemek_2 .= ','; }
						$sql_elemek_1 .= $elem;
						$sql_elemek_2 .= ':'.$elem;
						$k++;
					}
					if($mehet == 1)
					{
						$insertcommand = "INSERT INTO ".$webjel."termekek (".$sql_elemek_1.") VALUES (".$sql_elemek_2.")";
						$result = $pdo->prepare($insertcommand);
						$result->execute($valtozok);
						// echo $insertcommand.'<br/>';
						// var_dump($valtozok);
						$j++;
					}
					$id = $pdo->lastInsertId(); // paraméterek miatt kell id
				}
				// Paraméterek
				$k = count($elem_nevek) - count($conf_mezok) ;
				// foreach ($parameterek as $elem) {
				for ($m=$k; $m<count($elem_nevek); $m++)
				{
					// echo $m;
					// $param_id = ltrim(strstr($elem,"_",true), 'a');  // ID kinyerése
					$param_id = ltrim(strstr($elem_nevek[$m],"_",true), 'a');  // ID kinyerése
					$query_param = "SELECT * FROM ".$webjel."termek_uj_parameter_ertekek WHERE termek_id=".$id." AND parameter_id=".$param_id;
					$res = $pdo->prepare($query_param);
					$res->execute();
					$row_param = $res -> fetch();
					
					// echo $elem.'<br/>';
					$param_id = ltrim(strstr($elem_nevek[$m],"_",true), 'a');  // ID kinyerése
					if($param_id > 0 && $param_id < 9999999999) // Ha paraméter
					{
						// régi paraméter törlése
						$pdo->exec("DELETE FROM ".$webjel."termek_uj_parameter_ertekek WHERE termek_id=".$id." AND parameter_id =".$param_id);
						// Új felvitele
						$ertek = $sor[$keys[$m]];
						if($ertek != '' || $ertek != null)
						{
							$insertcommand = "INSERT INTO ".$webjel."termek_uj_parameter_ertekek (termek_id, parameter_id, ertek) 
											VALUES (".$id.",".$param_id.", :ertek)";
							$result = $pdo->prepare($insertcommand);
							$result->execute(array(':ertek'=>$ertek));
							// echo '<br/>'.$id.'-'.$elem_nevek[$m].' : '.$ertek.'<br/>';
						}
					}
					$k++;
				}
			}
			$a = 1;
			// echo '<br/>';
		}
		
		// var_dump($elem_nevek);
		
		
		
		
		/* 
		// XML megnyitása
		$xml=simplexml_load_file($file);
		foreach ($xml -> product as $row) { // termék sorokon megy végig
			$id = $row -> id;
			// Módosítás
			if($id > 0) 
			{
				$valtozok = array();
				$sql_elemek = '';
				$k = 0;
				$csop_id_hiba = 0;
				$id = 0;
				// Termék
				foreach ($conf_mezok as $elem) { // mezőkön (oszlopokon) megy végig
					$ertek = $row -> $elem;
					if($elem == 'id')
					{
						$id = $ertek;
					}
					// ELL, csop_id
					if($k > 0 && $elem == 'csop_id')
					{
						if($ertek > 0 && $ertek < 999999999999999) {  } // OK
						else
						{
							$csop_id_hiba = 1;
							$hibaszam++;
							$hibauzenet .= '<li class="list-group-item text-right">csop_id mező nincs kitöltve ('.$id.').</li>';
						}
					}
					// Dátumok
					if($elem == 'akcio_tol' || $elem == 'akcio_ig')
					{
						if(stripos($ertek, ".") !== false)
						{
							$ertek = str_replace(".", "-", $ertek);
						}
						else if (stripos($ertek, "-") == false && stripos($ertek, ".") == false)
						{
							$UNIX_DATE = ($ertek - 25569) * 86400;
							$ertek = gmdate("Y-m-d", $UNIX_DATE);
						}
					}
					$valtozok[] = $ertek;
					if($k > 0) { $sql_elemek .= ', '; }
					$sql_elemek .= $elem.'=? ';
					$k++;
				}
				if($csop_id_hiba == 0)
				{
					$updatecommand = "UPDATE ".$webjel."termekek SET ".$sql_elemek ." WHERE id=".$id;
					$result = $pdo->prepare($updatecommand);
					$result->execute($valtozok);
					$i++;
				}
			}
			// Új termék
			else
			{
				$valtozok = array();
				$sql_elemek_1 = '';
				$sql_elemek_2 = '';
				$k = 0;
				foreach ($conf_mezok as $elem) {
					$ertek = $row -> $elem;
					
					// Név ellenőrzése
					if($elem == 'nev')
					{
						if($ertek == '') { $mehet = 0; } else { $mehet = 1; }
					}
					// Dátumok
					if($elem == 'akcio_tol' || $elem == 'akcio_ig')
					{
						if(stripos($ertek, ".") !== false)
						{
							$ertek = str_replace(".", "-", $ertek);
						}
						else if (stripos($ertek, "-") == false && stripos($ertek, ".") == false)
						{
							$UNIX_DATE = ($ertek - 25569) * 86400;
							$ertek = gmdate("Y-m-d", $UNIX_DATE);
						}
					}

					$valtozok[':'.$elem] = $ertek;
					if($k > 0) { $sql_elemek_1 .= ', '; $sql_elemek_2 .= ','; }
					$sql_elemek_1 .= $elem;
					$sql_elemek_2 .= ':'.$elem;
					$k++;
				}
				if($mehet == 1)
				{
					$insertcommand = "INSERT INTO ".$webjel."termekek (".$sql_elemek_1.") VALUES (".$sql_elemek_2.")";
					$result = $pdo->prepare($insertcommand);
					$result->execute($valtozok);
					$j++;
				}
				$id = $pdo->lastInsertId(); // paraméterek miatt kell id
			}
			// Paraméterek
			foreach ($parameterek as $elem) {
				$param_id = ltrim(strstr($elem,"_",true), 'a');  // ID kinyerése
				$query_param = "SELECT * FROM ".$webjel."termek_uj_parameter_ertekek WHERE termek_id=".$row['id']." AND parameter_id=".$param_id;
				$res = $pdo->prepare($query_param);
				$res->execute();
				$row_param = $res -> fetch();
				
				// echo $elem.'<br/>';
				$param_id = ltrim(strstr($elem,"_",true), 'a');  // ID kinyerése
				if($param_id > 0 && $param_id < 9999999999) // Ha paraméter
				{
					// régi paraméter törlése
					$pdo->exec("DELETE FROM ".$webjel."termek_uj_parameter_ertekek WHERE termek_id=".$id." AND parameter_id =".$param_id);
					// Új felvitele
					$ertek = $row -> $elem;
					if($ertek != '')
					{
						$insertcommand = "INSERT INTO ".$webjel."termek_uj_parameter_ertekek (termek_id, parameter_id, ertek) 
										VALUES (".$id.",".$param_id.", :ertek)";
						$result = $pdo->prepare($insertcommand);
						$result->execute(array(':ertek'=>$ertek));
					}
				}
			}
		}
		
		// Új termékek miatt URL név generálás
		$query = 'SELECT * FROM '.$webjel.'termekek WHERE nev_url=""';
		foreach ($pdo->query($query) as $row)
		{
			// URL név meghatározása
			$nev_url = $row['nev'];
			$patterns = array();
			$patterns[0] = '/á/';
			$patterns[1] = '/é/';
			$patterns[2] = '/í/';
			$patterns[3] = '/ó/';
			$patterns[4] = '/ö/';
			$patterns[5] = '/ő/';
			$patterns[6] = '/ú/';
			$patterns[7] = '/ü/';
			$patterns[8] = '/ű/';
			$patterns[9] = '/Á/';
			$patterns[10] = '/É/';
			$patterns[11] = '/Í/';
			$patterns[12] = '/Ó/';
			$patterns[13] = '/Ö/';
			$patterns[14] = '/Ő/';
			$patterns[15] = '/Ú/';
			$patterns[16] = '/Ü/';
			$patterns[17] = '/Ű/';
			$patterns[18] = '/ /';
			$replacements = array();
			$replacements[0] = 'a';
			$replacements[1] = 'e';
			$replacements[2] = 'i';
			$replacements[3] = 'o';
			$replacements[4] = 'o';
			$replacements[5] = 'o';
			$replacements[6] = 'u';
			$replacements[7] = 'u';
			$replacements[8] = 'u';
			$replacements[9] = '/A/';
			$replacements[10] = '/E/';
			$replacements[11] = '/I/';
			$replacements[12] = '/O/';
			$replacements[13] = '/O/';
			$replacements[14] = '/O/';
			$replacements[15] = '/U/';
			$replacements[16] = '/U/';
			$replacements[17] = '/U/';
			$replacements[18] = '-';
			$nev_url = preg_replace($patterns, $replacements, $nev_url);
			$nev_url = preg_replace('/[^a-zA-Z0-9_-]/', '', $nev_url);
			if ($nev_url == '')
			{
				$nev_url = $row['id'];
			}
			// Egyezőség vizsgálata
			$res = $pdo->prepare('SELECT COUNT(*) FROM '.$webjel.'termekek WHERE nev_url = "'.$nev_url.'"');
			$res->execute();
			$rownum = $res->fetchColumn();
			if ($rownum > 0) // Ha van már ilyen nevű
			{
				$nev_url = $nev_url.'_'.$row['id'];
			}
			$updatecommand = "UPDATE ".$webjel."termekek SET nev_url = '".$nev_url."' WHERE id = ".$row["id"] ;
			$result = $pdo->prepare($updatecommand);
			$result->execute();
		} */
	}
	echo '<ul class="list-group list-group-striped margbot0">
		<li class="list-group-item">Feltöltött fájl <span class="pull-right">'.$_FILES['file']['name'].'</span></li>
		<li class="list-group-item">Módosult termék <span class="pull-right badge bg-blue">'.$i.'</span></li>
		<li class="list-group-item">Új termék <span class="pull-right badge bg-green">'.$j.'</span></li>
		<li class="list-group-item">Hibák <span class="pull-right badge bg-red">'.$hibaszam.'</span></li>
		'.$hibauzenet.'
	</ul>';
	
?>  