	<script src="scripts/termekek.js"></script>
<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
?>
<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1 id="myModal">Termékek<small>Kereső</small></h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li class="active">Termékek</li>
	  </ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-body no-padding with-border">
						<div class="table-responsive">
							<table class="table table-hover table-striped margbot0">
								<tbody>
									<tr>
										<th>ID</th>
										<th>Megnevezés</th>
										<!--<th></th>kép-->
										<th>ÁFA</th>
										<?php
										if($config_keszlet_kezeles == 'I')
										{
											print '<th style="text-align:right;">Készlet</th>';
										}
										?>
										<th style="text-align:right;">Akció</th>
										<th style="text-align:right;">Ár</th>
									</tr>
									<?php
									$query = "SELECT *, ".$webjel."termekek.id as id, ".$webjel."afa.afa as afa
									FROM ".$webjel."termekek 
									INNER JOIN ".$webjel."afa 
									ON ".$webjel."termekek.afa = ".$webjel."afa.id 
									WHERE ".$webjel."termekek.nev LIKE '%".$_GET['tabla_kereso']."%' OR ".$webjel."termekek.leiras LIKE '%".$_GET['tabla_kereso']."%' OR ".$webjel."termekek.rovid_leiras LIKE '%".$_GET['tabla_kereso']."%' OR ".$webjel."termekek.ar LIKE '%".$_GET['tabla_kereso']."%' OR ".$webjel."termekek.akciosar LIKE '%".$_GET['tabla_kereso']."%' 
									ORDER BY ".$webjel."termekek.nev ASC";
									foreach ($pdo->query($query) as $row)
									{
										if($row['lathato'] == 0) { $nem_lathato_sor = 'nem_lathato_sor';} else { $nem_lathato_sor = ''; }
										?>
										<tr class="kattintos_sor <?php print $nem_lathato_sor; ?>" onClick="window.open('termekek.php?csop_id=<?= $row['csop_id'] ?>&kezd=0&id=<?= $row['id'] ?>' , '_self');">
										<?php
										if($row['akciosar'] > 0)
										{
											if($row['akcio_ig'] >= date("Y-m-d") && $row['akcio_tol'] <= date("Y-m-d"))
											{
												$akciosar = $row['akciosar'];
											}
											else
											{
												$akciosar = 0;
											}
										}
										else { $akciosar = $row['akciosar']; }
										print '<td>'.$row['id'].'</td>
											<td>'.$row['nev'].'</td>';
										// print '<td>'.$row['kep'].'</td>';
											print '<td>'.$row['afa'].' %</td>';
											if($config_keszlet_kezeles == 'I')
											{
												print '<td style="text-align:right;">'.$row['raktaron'].'</td>';
											}
											print '<td style="text-align:right;">'.number_format($akciosar, 0, ',', ' ').' Ft</td>
											<td style="text-align:right;">'.number_format($row['ar'], 0, ',', ' ').' Ft</td>
										</tr>';
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer no-padding">
						<div class="mailbox-controls">
							<div class="btn-group" style="height:30px">
								&nbsp;
							</div>
							<div class="pull-right">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
