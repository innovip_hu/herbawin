<?php
	include '../../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	

	$html = '<select onChange="parameterOpcioBeir()" class="form-control" id="parmeter_opciok_select">
		<option value=""> - Válassz opciót - </option>';
		$query_parameter = "SELECT * FROM ".$webjel."termek_uj_parameter_ertekek 
			WHERE parameter_id = ".$_POST['parmeter']." 
			GROUP BY ertek 
			ORDER BY ertek ASC";
		$a = 0;
		foreach ($pdo->query($query_parameter) as $row_param)
		{
			$a = 1;
			$html .= '<option value="'.$row_param['ertek'].'" >'.$row_param['ertek'].'</option>';
		}
	$html .= '</select>';
	
	if($a == 1) { echo $html; }
?>
