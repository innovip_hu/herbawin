<?php
	include '../../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	$_POST['nev'] = str_replace('"', '&quot;', $_POST['nev']);
	// URL név meghatározása
	include $gyoker.'/adm/module/mod_urlnev.php';
	if ($nev_url == '')
	{
		$nev_url = rand(1,99999);
	}
	// Egyezőség vizsgálata
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE nev_url = '".$nev_url."'");
	$res->execute();
	$rownum2 = $res->fetchColumn();
	if ($rownum2 > 0) // Ha van már ilyen nevű
	{
		$query = "SELECT * FROM ".$webjel."termekek WHERE nev_url = '".$nev_url."'";
		$res = $pdo->prepare($query);
		$res->execute();
		$row = $res -> fetch();
		if($rownum2 > 1 || $row['id'] != $_GET['id']) // Ha nem saját maga
		{
			$nev_url = $nev_url.'-'.$_GET['id'];
		}
	}
	// Mentés
	if($_POST['lathato'] == 'true') { $lathato = 1; } else { $lathato = 0; }
	if($_POST['kiemelt'] == 'true') { $kiemelt = 1; } else { $kiemelt = 0; }
	$updatecommand = "UPDATE ".$webjel."termekek SET nev=?, nev_en=?, nev_de=?, ar=?, suly=?, afa=?, akciosar=?, akcio_tol=?, akcio_ig=?, rovid_leiras=?, rovid_leiras_en=?, rovid_leiras_de=?, lathato=?, kiemelt=?, leiras=?, leiras_en=?, leiras_de=?, csop_id=?, nev_url=?, seo_title=?, seo_description=?, raktaron=? WHERE id=?";
	$result = $pdo->prepare($updatecommand);
	$result->execute(array($_POST['nev'], $_POST['nev_en'], $_POST['nev_de'], $_POST['ar'], $_POST['suly'], $_POST['afa'], $_POST['akciosar'], $_POST['akcio_tol'], $_POST['akcio_ig'], $_POST['rovid_leiras'], $_POST['rovid_leiras_en'], $_POST['rovid_leiras_de'], $lathato, $kiemelt, $_POST['leiras'], $_POST['leiras_en'], $_POST['leiras_de'], $_POST['csop_id'], $nev_url, $_POST['seo_title'], $_POST['seo_description'], $_POST['raktaron'], $_GET['id']));
	
	// Termék csoportokhoz rendelés
	$_termek_csoportok = array();
	if (!isset($_GET['id'])) {
		$_termek_csoportok = array();
	}
	else {
		$termek_termek_csoportok = $pdo->query("SELECT * FROM `".$webjel."termek_termek_csoportok` WHERE termek_id=".$_GET['id']."")->fetchAll();
		$_termek_csoportok = array_map(function($termek_termek_csoport) {return $termek_termek_csoport['termek_csoport_id'];}, $termek_termek_csoportok);
	}
	
	if (isset($_POST['termek_csoportok'])) {
		$_termek_csoportok = !empty($_POST['termek_csoportok']) ? explode(',', $_POST['termek_csoportok']) : array();
	}

	function termek_termek_csoportok_Insert($termek_id) {
		global $_termek_csoportok, $pdo, $webjel;
		foreach ($_termek_csoportok as $termek_csoport_id) {
			$insert = $pdo->prepare("INSERT INTO ".$webjel."termek_termek_csoportok (termek_id, termek_csoport_id) values (".$termek_id.", ".$termek_csoport_id.")");
			$insert->execute();
		}
	}
	function termek_termek_csoportok_Delete($termek_id) {
		global $pdo, $webjel;
		$pdo->exec("DELETE FROM ".$webjel."termek_termek_csoportok WHERE termek_id=".$termek_id."");
	}

	if (isset($_POST['termek_csoportok'])) {
		termek_termek_csoportok_Delete($_GET['id']);
		termek_termek_csoportok_Insert($_GET['id']);
	}
?>