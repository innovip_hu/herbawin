<?php
	include '../../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
		// Klón létrehozása
		$insertcommand = "INSERT INTO ".$webjel."termekek
		(csop_id, nev, nev_url, vonalkod, ar, afa, kep, leiras, lathato, akciosar, akcio_tol, akcio_ig, sorrend, raktaron, kod, yt_video, rovid_leiras, kiemelt, gyarto, garancia, cikkszam, seo_title, seo_description)
		SELECT csop_id, nev, nev_url, vonalkod, ar, afa, kep, leiras, lathato, akciosar, akcio_tol, akcio_ig, sorrend, raktaron, kod, yt_video, rovid_leiras, kiemelt, gyarto, garancia, cikkszam, seo_title, seo_description
		FROM ".$webjel."termekek
		WHERE id = ".$_POST['id'];
		$result = $pdo->prepare($insertcommand);
		$result->execute();
		$uj_id = $pdo->lastInsertId();
		// Név és url név átírása
		$updatecommand = "UPDATE ".$webjel."termekek SET nev=CONCAT(nev, ' (klón)'), nev_url=CONCAT(nev_url, '_".$uj_id."') WHERE id=".$uj_id;
		$result = $pdo->prepare($updatecommand);
		$result->execute();
		// Paraméterek
		$query = "SELECT * FROM ".$webjel."termek_termek_parameter_ertekek WHERE termek_id=".$_POST['id'];
		foreach ($pdo->query($query) as $row)
		{
			$insertcommand = "INSERT INTO ".$webjel."termek_termek_parameter_ertekek (termek_id, termek_parameter_id, ertek, felar) VALUES (:termek_id, :termek_parameter_id, :ertek, :felar)";
			$result = $pdo->prepare($insertcommand);
			$result->execute(array(':termek_id'=>$uj_id,
							  ':termek_parameter_id'=>$row['termek_parameter_id'],
							  ':ertek'=>$row['ertek'],
							  ':felar'=>$row['felar']));
		}
		// Képek
		$query = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$_POST['id'];
		foreach ($pdo->query($query) as $row)
		{
			$dir = $gyoker.'/images/termekek/';
			$regi_kep = $row['kep'];
			$regi_kep_thumb = $row['thumb'];
			$uj_kep = $uj_id.'_'.$row['kep'];
			$uj_kep_thumb = $uj_id.'_'.$row['thumb'];
			$insertcommand = "INSERT INTO ".$webjel."termek_kepek (termek_id, kep, thumb, alap) VALUES (:termek_id, :kep, :thumb, :alap)";
			$result = $pdo->prepare($insertcommand);
			$result->execute(array(':termek_id'=>$uj_id,
							  ':kep'=>$uj_kep,
							  ':thumb'=>$uj_kep_thumb,
							  ':alap'=>$row['alap']));
			copy($dir.$regi_kep, $dir.$uj_kep);
			copy($dir.$regi_kep_thumb, $dir.$uj_kep_thumb);
		}
						  
		echo $uj_id;
?>