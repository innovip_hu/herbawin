<?php
	include '../../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	$_POST['nev'] = str_replace('"', '&quot;', $_POST['nev']);
	// URL név meghatározása
	include $gyoker.'/adm/module/mod_urlnev.php';
	if ($nev_url == '')
	{
		$nev_url = rand(1,99999);
	}
	// Egyezőség vizsgálata
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE nev_url = '".$nev_url."'");
	$res->execute();
	$rownum2 = $res->fetchColumn();
	if ($rownum2 > 0) // Ha van már ilyen nevű
	{
		$query = "SELECT * FROM ".$webjel."termekek WHERE nev_url = '".$nev_url."'";
		$res = $pdo->prepare($query);
		$res->execute();
		$row = $res -> fetch();
		if($rownum2 > 1 || $row['id'] != $_GET['id']) // Ha nem saját maga
		{
			$nev_url = $nev_url.'-'.time().rand(1, 999);
		}
	}
	// Mentés
	if($_POST['lathato'] == 'true') { $lathato = 1; } else { $lathato = 0; }
	if($_POST['kiemelt'] == 'true') { $kiemelt = 1; } else { $kiemelt = 0; }
	
	$insertcommand = "INSERT INTO ".$webjel."termekek (nev, ar, afa, akciosar, akcio_tol, akcio_ig, rovid_leiras, lathato, kiemelt, leiras, nev_url, seo_title, seo_description, raktaron, csop_id) VALUES (:nev, :ar, :afa, :akciosar, :akcio_tol, :akcio_ig, :rovid_leiras, :lathato, :kiemelt, :leiras, :nev_url, :seo_title, :seo_description, :raktaron, :csop_id)";
	$result = $pdo->prepare($insertcommand);
	$result->execute(array(':nev'=>$_POST['nev'],
						':ar'=>$_POST['ar'],
						':afa'=>$_POST['afa'],
						':akciosar'=>$_POST['akciosar'],
						':akcio_tol'=>$_POST['akcio_tol'],
						':akcio_ig'=>$_POST['akcio_ig'],
						':rovid_leiras'=>$_POST['rovid_leiras'],
						':lathato'=>$lathato,
						':kiemelt'=>$kiemelt,
						':leiras'=>$_POST['leiras'],
						':nev_url'=>$nev_url,
						':seo_title'=>$_POST['seo_title'],
						':seo_description'=>$_POST['seo_description'],
						':csop_id'=>$_POST['csop_id'],
						':raktaron'=>$_POST['raktaron']));
	
	print $pdo->lastInsertId();
?>