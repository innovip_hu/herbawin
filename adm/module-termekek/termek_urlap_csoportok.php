<?php
	if (isset($_GET['script2']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}

	$_termek_csoportok = array();
	if (!isset($_GET['id'])) {
		$_termek_csoportok = array();
	}
	else {
		$termek_termek_csoportok = $pdo->query("SELECT * FROM `".$webjel."termek_termek_csoportok` WHERE termek_id=".$_GET['id']."")->fetchAll();
		$_termek_csoportok = array_map(function($termek_termek_csoport) {return $termek_termek_csoport['termek_csoport_id'];}, $termek_termek_csoportok);
	}
?>
<link rel="stylesheet" href="css/multiple-select.css">
<select name="termek_csoportok[]" id="termek_csoportok" multiple="multiple" class="muliselectes csoportok">
	<?php $termek_csoportok = $pdo->query("SELECT * FROM `".$webjel."term_csoportok` WHERE csop_id=0 ORDER BY sorrend")->fetchAll(); ?>
	<?php foreach ($termek_csoportok as $termek_csoport): ?>
	<?php $termek_csoportok1 = $pdo->query("SELECT * FROM `".$webjel."term_csoportok` WHERE csop_id={$termek_csoport['id']} ORDER BY sorrend")->fetchAll(); ?>
	<option <?php if (!empty($termek_csoportok1)): ?>disabled="disabled"<?php endif; ?> value="<?php echo $termek_csoport['id']; ?>" <?php if (in_array($termek_csoport['id'], $_termek_csoportok)): ?>selected="selected"<?php endif; ?>><?php echo $termek_csoport['nev']; ?></option>
		<?php foreach ($termek_csoportok1 as $termek_csoport1): ?>
		<?php $termek_csoportok2 = $pdo->query("SELECT * FROM `".$webjel."term_csoportok` WHERE csop_id={$termek_csoport1['id']} ORDER BY sorrend")->fetchAll(); ?>
		<option <?php if (!empty($termek_csoportok2)): ?>disabled="disabled"<?php endif; ?> style="padding-left: 20px;" value="<?php echo $termek_csoport1['id']; ?>" <?php if (in_array($termek_csoport1['id'], $_termek_csoportok)): ?>selected="selected"<?php endif; ?>><?php echo $termek_csoport1['nev']; ?></option>
			<?php foreach ($termek_csoportok2 as $termek_csoport2): ?>
			<option style="padding-left: 40px;" value="<?php echo $termek_csoport2['id']; ?>" <?php if (in_array($termek_csoport2['id'], $_termek_csoportok)): ?>selected="selected"<?php endif; ?>><?php echo $termek_csoport2['nev']; ?></option>
			<?php endforeach; ?>
		<?php endforeach; ?>
	<?php endforeach; ?>
</select>