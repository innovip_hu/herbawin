<?php
	include '../../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	if($_POST['akcio_merteke'] >= 0 && $_POST['akcio_merteke'] < 100)
	{
		$updatecommand = "UPDATE ".$webjel."termekek SET akciosar=ROUND(ar -(ar * ".$_POST['akcio_merteke']." / 100)), akcio_tol='".$_POST['akcio_tol']."', akcio_ig='".$_POST['akcio_ig']."' WHERE csop_id=".$_POST['csop_id'];
		$result = $pdo->prepare($updatecommand);
		$result->execute();
	}
?>