<?php
	// Adatok
	$query = "SELECT * FROM ".$webjel."term_csoportok WHERE id=".$_GET['csop_id'];
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
	$kat_nev = $row['nev'];
?>
		  
<div class="content-wrapper bg_admin">
	<section class="content-header">
	  <h1>Tömeges akció <small>(<?php print $kat_nev; ?>)</small></h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li><a href="?csop_id=<?= $_GET['csop_id'] ?>"><i class="fa fa-folder-open"></i> <?php print $kat_nev; ?></a></li>
		<li class="active">Tömeges akció</li>
	  </ol>
	</section>

	<section class="content">
		<!-- Termék adatok -->
		<div class="row">
			<div class="col-md-6">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title"><?php print $kat_nev; ?></h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Akció mértéke (ennyivel csökken az eredeti ár)</label>
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-star-o"></i></span>
										<input type="text" class="form-control" id="akcio_merteke" placeholder="Akció mértéke" value="" >
										<span class="input-group-addon">%</span>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label>Akció kezdete</label>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												<input type="text" class="form-control datepicker" id="akcio_tol" placeholder="Akció kezdete" value="" >
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>Akció vége</label>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												<input type="text" class="form-control datepicker" id="akcio_ig" placeholder="Akció vége" value="" >
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="box-footer">
						<button type="submit" onClick="mentesUjTomegesAkcio('<?php print $_GET['csop_id']; ?>')" class="btn btn-primary">Mentés</button>
						<button class="btn btn-danger pull-right" onclick="tomegesAkcioMegszuntetese('<?php print $_GET['csop_id']; ?>')">Összes akció megszűntetése</button>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
