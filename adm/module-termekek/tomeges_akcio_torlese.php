<?php
	include '../../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	// Tömeges akció megszűntetése
	if(isset($_POST['csop_id']) && $_POST['csop_id'] > 0)
	{
		$updatecommand = "UPDATE ".$webjel."termekek SET akciosar=0, akcio_tol='0000-00-00', akcio_ig='0000-00-00' WHERE csop_id=".$_POST['csop_id'];
		$result = $pdo->prepare($updatecommand);
		$result->execute();
	}
?>