<?php
	header("Access-Control-Allow-Origin: *"); 
	header('Content-type: application/json');
	
	include '../../config.php';
	include '../../adm/config_adm.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	$ds = DIRECTORY_SEPARATOR;
	$dir = $gyoker.'/images/termekek/';
	
	// Termék neve
	$query = "SELECT * FROM ".$webjel."termekek WHERE id=".$_GET['id'];
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
	$urlnev = $row['nev_url'];
	
	// Kép másolása mappába
	if (!empty($_FILES)) {
		$tempFile = $_FILES['file']['tmp_name'];
		// $kep_eredeti_szelesseg = imagesx($tempFile);
		// $kep_eredeti_magassag = imagesy($tempFile);
		$targetPath = $dir . $ds;
		$file = $_FILES['file']['name'];
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		// $fn = 'img-'.time().rand(0,999999999999).'.'.$ext;
		$fn = $urlnev.'-'.$_GET['id'].time().rand(0,999999).'.'.$ext;
		$targetFile =  $targetPath. $fn; 
		move_uploaded_file($tempFile,$targetFile);
		
		// Bélyegkép készítése 2
		require_once($gyoker.'/adm/simpleImage_class3.php');
		$image = new SimpleImage();
		$image->load($targetPath. $fn);
		if($conf_csak_szelesseg == 1)
		{
			$image->resizeToWidth($conf_thumb_max_szeklesseg); // csak szélesség szerint
		}
		else
		{
			$image->resize_and_cut($conf_thumb_max_szeklesseg,$conf_thumb_max_magassag);
		}
		$thumb = 'thumb-'.$fn;
		$image->save($targetPath.$thumb);

		
		// Adatbázidba töltés
		$insertcommand = "INSERT INTO ".$webjel."termek_kepek (termek_id,kep, thumb) VALUES (:termek_id,:kep, :thumb)";
		$result = $pdo->prepare($insertcommand);
		$result->execute(array(':termek_id'=>$_GET['id'],
						  ':kep'=>$fn,
						  ':thumb'=>$thumb));
	}
?>  