	<script src="scripts/termekek.js"></script>
<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
// Oldalankénti szám
	if (isset($_GET['oldalszam']) && $_GET['oldalszam'] != '')
	{
		$oldalszam = $_GET['oldalszam'];
	}
	else
	{
		$oldalszam = 50; //ALAPÁLLAPOT
	}
// Rekordok száma
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE WHERE ".$webjel."termekek.nev LIKE '%".$_GET['tabla_kereso']."%' OR ".$webjel."termekek.leiras LIKE '%".$_GET['tabla_kereso']."%' OR ".$webjel."termekek.rovid_leiras LIKE '%".$_GET['tabla_kereso']."%' OR ".$webjel."termekek.ar LIKE '%".$_GET['tabla_kereso']."%' OR ".$webjel."termekek.akciosar LIKE '%".$_GET['tabla_kereso']."%'");
	$res->execute();
	$rownum = $res->fetchColumn();
// Kezdés meghatározása
	if (isset($_GET['kezd']) && $_GET['kezd'] != '' && $rownum > $oldalszam)
	{
		$kezd = $_GET['kezd'];
	}
	else
	{
		$kezd = 0;
	}
// Aktuális oldal
	$aktualis_oldal = ($kezd + $oldalszam) / $oldalszam;
// Utolsó oldal
	$utolso_oldal = ceil($rownum / $oldalszam);
// Sorrend
	if (isset($_GET['sorr_tip']))
	{
		$sorr_tip = $_GET['sorr_tip'];
	}
	else
	{
		$sorr_tip = 'id'; // Alap rendezési feltétel
	}
	if (isset($_GET['sorrend']))
	{
		$sorrend = $_GET['sorrend'];
	}
	else
	{
		$sorrend = 'DESC'; // Alap rendezési feltétel
	}
?>
<div class="content-wrapper">
	<section class="content-header">
	  <h1 id="myModal">Termékek<small>Kereső</small></h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li class="active">Termékek</li>
	  </ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-body no-padding with-border">
						<div class="table-responsive">
							<table class="table table-hover table-striped margbot0">
								<tbody>
									<tr>
										<th>ID</th>
										<th>Megnevezés</th>
										<!--<th></th>kép-->
										<th>ÁFA</th>
										<?php
										if($config_keszlet_kezeles == 'I')
										{
											print '<th style="text-align:right;">Készlet</th>';
										}
										?>
										<th style="text-align:right;">Akció</th>
										<th style="text-align:right;">Ár</th>
									</tr>
									<?php
									$query = "SELECT *, ".$webjel."termekek.id as id, ".$webjel."afa.afa as afa
									FROM ".$webjel."termekek 
									INNER JOIN ".$webjel."afa 
									ON ".$webjel."termekek.afa = ".$webjel."afa.id 
									WHERE ".$webjel."termekek.nev LIKE '%".$_GET['tabla_kereso']."%' OR ".$webjel."termekek.leiras LIKE '%".$_GET['tabla_kereso']."%' OR ".$webjel."termekek.rovid_leiras LIKE '%".$_GET['tabla_kereso']."%' OR ".$webjel."termekek.ar LIKE '%".$_GET['tabla_kereso']."%' OR ".$webjel."termekek.akciosar LIKE '%".$_GET['tabla_kereso']."%' 
									ORDER BY ".$webjel."termekek.nev ASC LIMIT ".$kezd.",".$oldalszam;
									foreach ($pdo->query($query) as $row)
									{
										if($row['lathato'] == 0) { $nem_lathato_sor = 'nem_lathato_sor';} else { $nem_lathato_sor = ''; }
										?>
										<tr class="kattintos_sor <?php print $nem_lathato_sor; ?>" onClick="BelepesTermekbe('module-termekek/termek.php', 'module-termekek/lista.php', <?php print $row['id']; ?>, 'kereso')">
										<?php
										if($row['akciosar'] > 0)
										{
											if($row['akcio_ig'] >= date("Y-m-d") && $row['akcio_tol'] <= date("Y-m-d"))
											{
												$akciosar = $row['akciosar'];
											}
											else
											{
												$akciosar = 0;
											}
										}
										else { $akciosar = $row['akciosar']; }
										print '<td>'.$row['id'].'</td>
											<td>'.$row['nev'].'</td>';
										// print '<td>'.$row['kep'].'</td>';
											print '<td>'.$row['afa'].' %</td>';
											if($config_keszlet_kezeles == 'I')
											{
												print '<td style="text-align:right;">'.$row['raktaron'].'</td>';
											}
											print '<td style="text-align:right;">'.number_format($akciosar, 0, ',', ' ').' Ft</td>
											<td style="text-align:right;">'.number_format($row['ar'], 0, ',', ' ').' Ft</td>
										</tr>';
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer no-padding">
						<div class="mailbox-controls">
							<div class="btn-group" style="height:30px">
								&nbsp;
							</div>
							<div class="pull-right">
								<?php
									if($rownum > 0)
									{
										print ($kezd+1).'-'.$vege.'/'.$rownum;
									}
								?>
								<div class="btn-group">
								<?php
									if ($kezd != 0) // Lapozás vissza (balra)
									{
										?>
										<button onclick="lapozasVisszaTermekek('module-termekek/lista.php', <?php print $_GET['csop_id']; ?>)" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
										<?php
									}
									if (($utolso_oldal / $aktualis_oldal) != 1 && $rownum>0) // Lapozás előre (jobbra)
									{
										?>
										<button onclick="lapozasTovabbTermekek('module-termekek/lista.php', <?php print $_GET['csop_id']; ?>)" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
										<?php
									}
								?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<input type="hidden" name="oldalszam" id="oldalszam" value="<?php print $oldalszam;?>"/>
<!--Aktuális oldal - azaz honnan kezdődjön a lekérdezés-->
<input type="hidden" name="kezd" id="kezd" value="<?php print $kezd;?>"/>
<!--Sorba rendezés-->
<input type="hidden" name="sorr_tip" id="sorr_tip" value="<?php print $sorr_tip;?>"/>
<input type="hidden" name="sorrend" id="sorrend" value="<?php print $sorrend;?>"/>
