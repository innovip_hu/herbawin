<?php
	include '../../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	$insertcommand = "INSERT INTO ".$webjel."termekek (csop_id) VALUES (".$_GET['csop_id'].")";
	$result = $pdo->prepare($insertcommand);
	$result->execute();
	
	print $pdo->lastInsertId();
?>