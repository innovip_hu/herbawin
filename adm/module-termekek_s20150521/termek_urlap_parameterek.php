<?php
	if (isset($_GET['script2']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
	// Törlés
	if(isset($_GET['command']) && $_GET['command'] == 'parameter_torles')
	{
		$pdo->exec("DELETE FROM ".$webjel."termek_termek_parameter_ertekek WHERE termek_parameter_id =".$_GET['parameter_id']." AND termek_id=".$_GET['id']);
	}
	// Törlés
	else if(isset($_GET['command']) && $_GET['command'] == 'parameter_ertek_torlesi')
	{
		$pdo->exec("DELETE FROM ".$webjel."termek_termek_parameter_ertekek WHERE id =".$_GET['parameter_ertek_id']);
	}
	// Módosítás (igen_nem)
	else if(isset($_GET['command']) && $_GET['command'] == 'parameter_mod_igen_nem')
	{
		if($_GET['ertek'] == 1) { $ertek=0; } else { $ertek=1; } 
		$pdo->exec("UPDATE ".$webjel."termek_termek_parameter_ertekek SET ertek=".$ertek." WHERE termek_parameter_id =".$_GET['parameter_id']." AND termek_id=".$_GET['id']);
	}
	// Új több választós paraméter
	else if(isset($_GET['command']) && $_GET['command'] == 'uj_tobb_valasztos_parameter_ertek')
	{
		$pdo->exec("INSERT INTO ".$webjel."termek_termek_parameter_ertekek (termek_id,termek_parameter_id, ertek) VALUES (".$_GET['id'].",".$_GET['parameter_id'].",".$_GET['ertek'].")");
	}
	// Új paraméter hozzáadása
	else if(isset($_GET['command']) && $_GET['command'] == 'uj_parameter')
	{
		$query_uj_param = "SELECT * FROM ".$webjel."termek_parameterek WHERE id=".$_GET['parameter_id'];
		$res = $pdo->prepare($query_uj_param);
		$res->execute();
		$row_uj_param = $res -> fetch();
		if($row_uj_param['tipus'] == 'egyedi')
		{
			$pdo->exec("INSERT INTO ".$webjel."termek_termek_parameter_ertekek (termek_id,termek_parameter_id) VALUES (".$_GET['id'].",".$_GET['parameter_id'].")");
		}
		else if($row_uj_param['tipus'] == 'igen_nem')
		{
			$pdo->exec("INSERT INTO ".$webjel."termek_termek_parameter_ertekek (termek_id, termek_parameter_id, ertek) VALUES (".$_GET['id'].", ".$_GET['parameter_id'].", 1)");
		}
		else if($row_uj_param['tipus'] == 'lenyilo-egy_valaszthato')
		{
			$query_uj_param_ertek = "SELECT * FROM ".$webjel."termek_parameter_ertekek WHERE termek_parameter_id=".$_GET['parameter_id']." ORDER BY nev ASC LIMIT 1";
			$res = $pdo->prepare($query_uj_param_ertek);
			$res->execute();
			$row_uj_param_ertek = $res -> fetch();
			
			$pdo->exec("INSERT INTO ".$webjel."termek_termek_parameter_ertekek (termek_id, termek_parameter_id, ertek) VALUES (".$_GET['id'].", ".$_GET['parameter_id'].", '".$row_uj_param_ertek['id']."')");
		}
		else if($row_uj_param['tipus'] == 'lenyilo-tobb_valaszthato')
		{
			$query_uj_param_ertek = "SELECT * FROM ".$webjel."termek_parameter_ertekek WHERE termek_parameter_id=".$_GET['parameter_id']." ORDER BY nev ASC LIMIT 1";
			$res = $pdo->prepare($query_uj_param_ertek);
			$res->execute();
			$row_uj_param_ertek = $res -> fetch();
			
			$pdo->exec("INSERT INTO ".$webjel."termek_termek_parameter_ertekek (termek_id, termek_parameter_id, ertek) VALUES (".$_GET['id'].", ".$_GET['parameter_id'].", '".$row_uj_param_ertek['id']."')");
		}
	}
	
	// Új paraméter létrehozása select
	print '<div class="form-group">
		<div class="input-group">
			<span class="input-group-addon input_jelolo_nsarga"><i class="fa fa-plus"></i></span>
			<select class="form-control" id="parameter_uj_parameter" attr_termek_id="'.$_GET['id'].'">
				<option value=""> - Új paraméter hozzáadása - </option>';
				$query_param_valaszto = "SELECT * FROM ".$webjel."termek_parameterek e 
					WHERE   NOT EXISTS
					(
					SELECT  null 
					FROM    ".$webjel."termek_termek_parameter_ertekek d
					WHERE   d.termek_parameter_id = e.id AND d.termek_id = ".$_GET['id']." 
					)
					ORDER BY nev ASC";
				foreach ($pdo->query($query_param_valaszto) as $row_param_valaszto)
				{
					print '<option value="'.$row_param_valaszto['id'].'" '.$selected.'>'.$row_param_valaszto['nev'].'</option>';
				}
			print '</select>
		</div>
	</div>';
			
	$query_param = "SELECT * FROM ".$webjel."termek_parameterek
		INNER JOIN ".$webjel."termek_termek_parameter_ertekek 
		ON ".$webjel."termek_parameterek.id = ".$webjel."termek_termek_parameter_ertekek.termek_parameter_id 
		WHERE termek_id = ".$_GET['id']."
		GROUP BY ".$webjel."termek_termek_parameter_ertekek.termek_parameter_id 
		ORDER BY ".$webjel."termek_parameterek.nev ASC";
	foreach ($pdo->query($query_param) as $row_param)
	{
		if($row_param['tipus'] == 'egyedi')
		{
			print '<div class="form-group">
				<label>'.$row_param['nev'];
					if($row_param['mertekegyseg'] != '')
					{
						print ' ('.$row_param['mertekegyseg'].')';
					}
				print '</label>
				<div class="input-group">
					<span class="input-group-addon parameter_mentes_gomb" id="parameter_span_gomb_'.$row_param['termek_parameter_id'].'" attr_parameter_id="'.$row_param['termek_parameter_id'].'" attr_termek_id="'.$_GET['id'].'">
						<i class="fa" style="width:12px;">&nbsp;</i>
					</span>
					<input type="text" class="form-control parameter_input_egyedi" attr_parameter_id="'.$row_param['termek_parameter_id'].'" id="parameter_'.$row_param['termek_parameter_id'].'" value="'.$row_param['ertek'].'">
					<span class="input-group-addon input_jelolo_piros input_jelolo_gomb parameter_torles_gomb" attr_parameter_id="'.$row_param['termek_parameter_id'].'" attr_termek_id="'.$_GET['id'].'"><i class="fa fa-times"></i></span>
				</div>
			</div>';
		}
		else if($row_param['tipus'] == 'lenyilo-egy_valaszthato')
		{
			print '<div class="form-group">
				<label>'.$row_param['nev'].'</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa" style="width:12px;">&nbsp;</i></span>
					<select class="form-control parameter_select" id="parameter_'.$row_param['termek_parameter_id'].'" attr_parameter_id="'.$row_param['termek_parameter_id'].'" attr_termek_id="'.$_GET['id'].'">';
						$query_param_valaszto = "SELECT * FROM ".$webjel."termek_parameter_ertekek WHERE termek_parameter_id=".$row_param['termek_parameter_id']." ORDER BY nev ASC";
						foreach ($pdo->query($query_param_valaszto) as $row_param_valaszto)
						{
							$selected = '';
							if($row_param_valaszto['id'] == $row_param['ertek']) { $selected = ' selected'; }
							print '<option value="'.$row_param_valaszto['id'].'" '.$selected.'>'.$row_param_valaszto['nev'].'</option>';
						}
					print '</select>
					<span class="input-group-addon input_jelolo_piros input_jelolo_gomb parameter_torles_gomb" attr_parameter_id="'.$row_param['termek_parameter_id'].'" attr_termek_id="'.$_GET['id'].'"><i class="fa fa-times"></i></span>
				</div>
			</div>';
		}
		else if($row_param['tipus'] == 'igen_nem')
		{
			$checked = '';
			if($row_param['ertek'] == 1) { $checked = ' checked'; }
			print '<div class="form-group">
				<label>'.$row_param['nev'].'</label>
				<div class="checkbox" style="display:inline; margin-left:10px;">
					<input type="checkbox" class="minimal parameter_igen_nem_gomb" id="parameter_'.$row_param['termek_parameter_id'].'" attr_parameter_id="'.$row_param['termek_parameter_id'].'" attr_termek_id="'.$_GET['id'].'" attr_ertek="'.$row_param['ertek'].'" '.$checked.'>
				</div>
				<span class="input-group-addon input_jelolo_piros input_jelolo_gomb parameter_torles_gomb" attr_parameter_id="'.$row_param['termek_parameter_id'].'" attr_termek_id="'.$_GET['id'].'" style="width: auto; display: inline; float: right;"><i class="fa fa-times"></i></span>
			</div>';
		}
		else if($row_param['tipus'] == 'lenyilo-tobb_valaszthato')
		{
			print '<div class="form-group">
				<label>'.$row_param['nev'].'</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-plus"></i></span>
					<select class="form-control parameter_select_tobb_valasztos" id="parameter_'.$row_param['termek_parameter_id'].'" attr_parameter_id="'.$row_param['termek_parameter_id'].'" attr_termek_id="'.$_GET['id'].'">
						<option value=""> - Új érték hozzáadása - </option>';
						$query_param_valaszto = "SELECT * FROM ".$webjel."termek_parameter_ertekek e 
						WHERE termek_parameter_id=".$row_param['termek_parameter_id']." 
							AND NOT EXISTS
							(
							SELECT  null 
							FROM    ".$webjel."termek_termek_parameter_ertekek d
							WHERE   d.ertek = e.id AND d.termek_id = ".$_GET['id']." AND d.termek_parameter_id = ".$row_param['termek_parameter_id']." 
							)
						ORDER BY nev ASC";
						foreach ($pdo->query($query_param_valaszto) as $row_param_valaszto)
						{
							print '<option value="'.$row_param_valaszto['id'].'">'.$row_param_valaszto['nev'].'</option>';
						}
					print '</select>
					<span class="input-group-addon input_jelolo_piros input_jelolo_gomb parameter_torles_gomb" attr_parameter_id="'.$row_param['termek_parameter_id'].'" attr_termek_id="'.$_GET['id'].'"><i class="fa fa-times"></i></span>
				</div>';
				// Értekek
				print '<div class="table-responsive" style="border: 1px solid #d2d6de; border-top: 0;">
					<table class="table table-hover table-striped margbot0">
						<tbody>
							<tr>
								<th>Paraméter</th>
								<th class="text-right">Felár</th>
								<th></th>
							</tr>';
							$query_param_ertek = "SELECT *, ".$webjel."termek_termek_parameter_ertekek.id as id
								FROM ".$webjel."termek_parameterek
								INNER JOIN ".$webjel."termek_termek_parameter_ertekek 
								ON ".$webjel."termek_parameterek.id = ".$webjel."termek_termek_parameter_ertekek.termek_parameter_id 
								INNER JOIN ".$webjel."termek_parameter_ertekek 
								ON ".$webjel."termek_termek_parameter_ertekek.ertek = ".$webjel."termek_parameter_ertekek.id 
								WHERE ".$webjel."termek_termek_parameter_ertekek.termek_id = ".$_GET['id']." AND ".$webjel."termek_termek_parameter_ertekek.termek_parameter_id = ".$row_param['termek_parameter_id']." 
								ORDER BY ".$webjel."termek_parameterek.nev ASC";
							foreach ($pdo->query($query_param_ertek) as $row_param_ertek)
							{
								print '<tr>
									<td>'.$row_param_ertek['nev'].'</td>
									<td class="text-right">
										<a id="param_felar_gomb_'.$row_param_ertek['id'].'" attr_param_ertek_id="'.$row_param_ertek['id'].'" class="btn fa fa-floppy-o param_felar_gomb" style="padding: 2px 4px; display:none;" ></a>
										<input type="text" id="param_felar_input_'.$row_param_ertek['id'].'" value="'.$row_param_ertek['felar'].'" style="padding: 0 6px; width: 80px; text-align: right;" class="param_felar_input" attr_param_ertek_id="'.$row_param_ertek['id'].'" />
									</td>
									<td style="width: 30px;"><a class="btn fa fa-times param_ertek_torles_gomb" style="padding: 2px 4px;" attr_param_ertek_id="'.$row_param_ertek['id'].'" attr_termek_id="'.$_GET['id'].'"></a></td>
								</tr>';
							}
						print '</tbody>
					</table>
				</div>';
			print '</div>';
		}
		
	}
?>
