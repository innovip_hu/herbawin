<?php
	if (isset($_GET['script2']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	// Paraméterek (Andor)
	$termek_parameterek = $pdo->query(""
			. "SELECT tp.* FROM "
			. "`".$webjel."termek_parameterek` tp "
			. "JOIN `".$webjel."termek_csoport_termek_parameterek_adm` tcstpa ON tp.id=tcstpa.termek_parameter_id AND tcstpa.termek_csoport_id=".(isset($_GET['csop_id']) ? $_GET['csop_id'] : 0)." ". "")->fetchAll();
	if ($termek_parameterek === array()) {
		$termek_parameterek = $pdo->query("SELECT * FROM `".$webjel."termek_parameterek`")->fetchAll();
	}
	if ($termek_parameterek !== array()) {
		$_termek_parameter = array();
		$_termek_parameter_ertek_felar = array();
		foreach ($termek_parameterek as $termek_parameter) {
			if (!isset($_GET['id'])) {
				if ($termek_parameter['tipus'] != TERMEK_PARAMETER_TIPUS_LENYILO_TOBB_VALASZTHATO) {
					$_termek_parameter[$termek_parameter['id']] = '';
				}
				else {
					$_termek_parameter[$termek_parameter['id']] = array();
					$termek_parameter_ertekek = $pdo->query("SELECT * FROM `".$webjel."termek_parameter_ertekek` WHERE `termek_parameter_id` = ".$termek_parameter['id']."")->fetchAll();
					foreach ($termek_parameter_ertekek as $termek_parameter_ertek) {
						$_termek_parameter_ertek_felar[$termek_parameter['id']][$termek_parameter_ertek['id']] = '';
					}
				}
			}
			else {
				if ($termek_parameter['tipus'] != TERMEK_PARAMETER_TIPUS_LENYILO_TOBB_VALASZTHATO) {
					$termek_termek_parameter_ertek = $pdo->query("SELECT * FROM `".$webjel."termek_termek_parameter_ertekek` WHERE termek_id=".$_GET['id']." AND termek_parameter_id=".$termek_parameter['id']."")->fetch();
					$_termek_parameter[$termek_parameter['id']] = $termek_termek_parameter_ertek ? $termek_termek_parameter_ertek['ertek'] : '';
				}
				else {
					$termek_termek_parameter_ertekek = $pdo->query("SELECT * FROM `".$webjel."termek_termek_parameter_ertekek` WHERE termek_id=".$_GET['id']." AND termek_parameter_id=".$termek_parameter['id']."")->fetchAll();
					$_termek_parameter[$termek_parameter['id']] = array_map(function($termek_termek_parameter_ertek) {return $termek_termek_parameter_ertek['ertek'];}, $termek_termek_parameter_ertekek);
					$termek_parameter_ertekek = $pdo->query("SELECT * FROM `".$webjel."termek_parameter_ertekek` tpe LEFT JOIN `".$webjel."termek_termek_parameter_ertekek` ttpe ON tpe.id=ttpe.ertek AND ttpe.termek_id=".$_GET['id']." WHERE tpe.termek_parameter_id=".$termek_parameter['id']."")->fetchAll();
					foreach ($termek_parameter_ertekek as $termek_parameter_ertek) {
						$_termek_parameter_ertek_felar[$termek_parameter['id']][$termek_parameter_ertek['id']] = $termek_parameter_ertek['felar'] ? $termek_parameter_ertek['felar'] : '';
					}
				}
			}
			
			if (isset($_POST['termek_parameter'][$termek_parameter['id']])) {
				$_termek_parameter[$termek_parameter['id']] = $_POST['termek_parameter'][$termek_parameter['id']];
			}
			elseif (isset($_POST['termek_parameter']) AND $termek_parameter['tipus'] == TERMEK_PARAMETER_TIPUS_LENYILO_TOBB_VALASZTHATO) {
				$_termek_parameter[$termek_parameter['id']] = array();
			}
			if (isset($_POST['termek_parameter_ertek_felar'][$termek_parameter['id']])) {
				$_termek_parameter_ertek_felar[$termek_parameter['id']] = $_POST['termek_parameter_ertek_felar'][$termek_parameter['id']];
			}
		}
	}
		
	if (isset($_GET['termek_id'])) {
		function termek_termek_parameter_ertekek_Insert($termek_id) {
			global $_termek_parameter, $_termek_parameter_ertek_felar, $pdo, $webjel;
			foreach ($_termek_parameter as $termek_parameter_id => $ertek) {
				if (!is_array($ertek) AND $ertek !== "") {
					$insert = $pdo->prepare("INSERT INTO ".$webjel."termek_termek_parameter_ertekek (termek_id, termek_parameter_id, ertek) values (".$termek_id.", ".$termek_parameter_id.", '".$ertek."')");
					$insert->execute();
				}
				elseif (is_array($ertek)) {
					foreach ($ertek as $_ertek) {
						$insert = $pdo->prepare("INSERT INTO ".$webjel."termek_termek_parameter_ertekek ("
								. "termek_id, "
								. "termek_parameter_id, "
								. "ertek, "
								. "felar"
								. ") values ("
								. "".$termek_id.", "
								. "".$termek_parameter_id.", "
								. "'".$_ertek."', "
								. "".(isset($_termek_parameter_ertek_felar[$termek_parameter_id][$_ertek]) ? "'".$_termek_parameter_ertek_felar[$termek_parameter_id][$_ertek]."'" : "''")
								. ")");
						$insert->execute();
					}
				}
			}
		}
		function termek_termek_parameter_ertekek_Delete($termek_id) {
			global $pdo, $webjel;
			$pdo->exec("DELETE FROM ".$webjel."termek_termek_parameter_ertekek WHERE termek_id=".$termek_id."");
		}

		termek_termek_parameter_ertekek_Delete($_GET['termek_id']);
		termek_termek_parameter_ertekek_Insert($_GET['termek_id']);
	}
	
?>
<link rel="stylesheet" href="css/multiple-select.css">
<form id="parameterek_form">
<?php foreach ($termek_parameterek as $termek_parameter): ?>
<div class="col-md-6">
	<div class="form-group">
		<label><?php echo $termek_parameter['nev']; ?>:</label>
		<div class="input-group">
			<?php
			if ($termek_parameter['tipus'] != TERMEK_PARAMETER_TIPUS_LENYILO_TOBB_VALASZTHATO)
			{ 
				// print '<span class="input-group-addon input_jelolo_kek"><i class="fa fa-angle-right"></i></span>';
			}
			?>
			
			<?php if ($termek_parameter['tipus'] == TERMEK_PARAMETER_TIPUS_EGYEDI): ?>
			<input type="text" name="termek_parameter[<?php echo $termek_parameter['id']; ?>]" value="<?php echo $_termek_parameter[$termek_parameter['id']]; ?>" class="form-control"/>
			
			<?php elseif ($termek_parameter['tipus'] == TERMEK_PARAMETER_TIPUS_IGEN_NEM): ?>
			<select class="form-control" name="termek_parameter[<?php echo $termek_parameter['id']; ?>]">
				<option value=""></option>
				<option value="1" <?php if ($_termek_parameter[$termek_parameter['id']] === '1'): ?>selected="selected"<?php endif; ?>>Igen</option>
				<option value="0" <?php if ($_termek_parameter[$termek_parameter['id']] === '0'): ?>selected="selected"<?php endif; ?>>Nem</option>
			</select>
			
			<?php elseif ($termek_parameter['tipus'] == TERMEK_PARAMETER_TIPUS_LENYILO_EGY_VALASZTHATO): ?>
			<select class="form-control" name="termek_parameter[<?php echo $termek_parameter['id']; ?>]">
				<option value=""></option>
				<?php $termek_parameter_ertekek = $pdo->query("SELECT * FROM `".$webjel."termek_parameter_ertekek` WHERE `termek_parameter_id` = ".$termek_parameter['id']."")->fetchAll(); ?>
				<?php foreach ($termek_parameter_ertekek as $termek_parameter_ertek): ?>
				<option value="<?php echo $termek_parameter_ertek['id']; ?>" <?php if ($_termek_parameter[$termek_parameter['id']] === $termek_parameter_ertek['id']): ?>selected="selected"<?php endif; ?>><?php echo $termek_parameter_ertek['nev']; ?></option>
				<?php endforeach; ?>
			</select>
			
			<?php elseif ($termek_parameter['tipus'] == TERMEK_PARAMETER_TIPUS_LENYILO_TOBB_VALASZTHATO): ?>
			<select name="termek_parameter[<?php echo $termek_parameter['id']; ?>][]" id="termek_parameter_<?php echo $termek_parameter['id']; ?>" multiple="multiple" class="muliselectes jellemzo">
				<?php $termek_parameter_ertekek = $pdo->query("SELECT * FROM `".$webjel."termek_parameter_ertekek` WHERE `termek_parameter_id` = ".$termek_parameter['id']."")->fetchAll(); ?>
				<?php foreach ($termek_parameter_ertekek as $termek_parameter_ertek): ?>
				<option value="<?php echo $termek_parameter_ertek['id']; ?>" <?php if (in_array($termek_parameter_ertek['id'], $_termek_parameter[$termek_parameter['id']])): ?>selected="selected"<?php endif; ?>><?php echo $termek_parameter_ertek['nev']; ?></option>
				<?php endforeach; ?>
			</select>
			<?php foreach ($termek_parameter_ertekek as $termek_parameter_ertek): ?>
			<div class="felar_div" id="termek_parameter_ertek_<?php echo $termek_parameter_ertek['id']; ?>" style="<?php if (!in_array($termek_parameter_ertek['id'], $_termek_parameter[$termek_parameter['id']])): ?>display: none;<?php endif; ?>">
				<?php echo $termek_parameter_ertek['nev']; ?>
				<input type="text" name="termek_parameter_ertek_felar[<?php echo $termek_parameter['id']; ?>][<?php echo $termek_parameter_ertek['id']; ?>]" value="<?php echo $_termek_parameter_ertek_felar[$termek_parameter['id']][$termek_parameter_ertek['id']]; ?>"  class=""/>
				Ft felár
			</div>
			<?php endforeach; ?>
			<script>
				/* $('#termek_parameter_<?php echo $termek_parameter['id']; ?>').multipleSelect({
					onClick : function(view) {
						if (view.checked) {
							$('#termek_parameter_ertek_'+view.value).show();
						}
						else {
							$('#termek_parameter_ertek_'+view.value).hide();
							$('#termek_parameter_ertek_'+view.value+' input').val('');
						}
					}
				}); */
			</script>
			
			<?php endif; ?>
			
			<?php if (!empty($termek_parameter['mertekegyseg'])): ?>
			<?php echo '<span class="input-group-addon">'.$termek_parameter['mertekegyseg'].'</span>'; ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php endforeach; ?>
</form>