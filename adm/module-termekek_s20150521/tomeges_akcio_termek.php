<link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		include '../config_adm.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
// Oldalankénti szám
	if (isset($_GET['oldalszam']) && $_GET['oldalszam'] != '')
	{
		$oldalszam = $_GET['oldalszam'];
	}
	else
	{
		$oldalszam = 50; //ALAPÁLLAPOT
	}
// Rekordok száma
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE csop_id=".$_GET['csop_id']);
	$res->execute();
	$rownum = $res->fetchColumn();
// Kezdés meghatározása
	if (isset($_GET['kezd']) && $_GET['kezd'] != '' && $rownum > $oldalszam)
	{
		$kezd = $_GET['kezd'];
	}
	else
	{
		$kezd = 0;
	}
// Aktuális oldal
	$aktualis_oldal = ($kezd + $oldalszam) / $oldalszam;
// Utolsó oldal
	$utolso_oldal = ceil($rownum / $oldalszam);
// Sorrend
	if (isset($_GET['sorr_tip']))
	{
		$sorr_tip = $_GET['sorr_tip'];
	}
	else
	{
		$sorr_tip = 'vezeteknev'; // Alap rendezési feltétel
	}
	if (isset($_GET['sorrend']))
	{
		$sorrend = $_GET['sorrend'];
	}
	else
	{
		$sorrend = 'ASC'; // Alap rendezési feltétel
	}
	// Adatok
	$query = "SELECT * FROM ".$webjel."term_csoportok WHERE id=".$_GET['csop_id'];
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
	$kat_nev = $row['nev'];
?>
		  
<div class="content-wrapper">
	<section class="content-header">
	  <h1>Tömeges akció <small>(<?php print $kat_nev; ?>)</small></h1>
	  <ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
		<li><a onClick="visszaTermekek('<?php print $_GET['fajl']; ?>', '<?php print $_GET['csop_id']; ?>', '0')"><i class="fa fa-folder-open"></i> <?php print $kat_nev; ?></a></li>
		<li class="active">Tömeges akció</li>
	  </ol>
	</section>

	<section class="content">
		<!-- Termék adatok -->
		<div class="row">
			<div class="col-md-6">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title"><?php print $kat_nev; ?></h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Akció mértéke (ennyivel csökken az eredeti ár)</label>
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-star-o"></i></span>
										<input type="text" class="form-control" id="akcio_merteke" placeholder="Akció mértéke" value="" >
										<span class="input-group-addon">%</span>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label>Akció kezdete</label>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												<input type="text" class="form-control datepicker" id="akcio_tol" placeholder="Akció kezdete" value="" >
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>Akció vége</label>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												<input type="text" class="form-control datepicker" id="akcio_ig" placeholder="Akció vége" value="" >
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="box-footer">
						<button type="submit" onClick="mentesUjTomegesAkcio('<?php print $_GET['fajl']; ?>', '<?php print $_GET['csop_id']; ?>')" class="btn btn-primary">Mentés</button>
						<button class="btn btn-danger pull-right" onclick="tomegesAkcioMegszuntetese('<?php print $_GET['fajl']; ?>', '<?php print $_GET['csop_id']; ?>')">Összes akció megszűntetése</button>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<input type="hidden" name="oldalszam" id="oldalszam" value="<?php print $oldalszam;?>"/>
<!--Aktuális oldal - azaz honnan kezdődjön a lekérdezés-->
<input type="hidden" name="kezd" id="kezd" value="<?php print $kezd;?>"/>
<input type="hidden" name="kezd" id="oldalszam" value="<?php print $oldalszam;?>"/>
<!--Sorba rendezés-->
<input type="hidden" name="sorr_tip" id="sorr_tip" value="<?php print $sorr_tip;?>"/>
<input type="hidden" name="sorrend" id="sorrend" value="<?php print $sorrend;?>"/>
