<?php
	session_start();
	ob_start();
	include '../../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	$upload = FALSE;
	if (!empty($_FILES)) {
		$tempFile = $_FILES['video']['tmp_name'];
		$targetPath = $gyoker.'/images/video_slider/';
		$file = $_FILES['video']['name'];
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		$fn = 'video_slider-'.time().rand(0,999999999999).'.'.$ext;
		$targetFile =  $targetPath.$fn;
		$upload = move_uploaded_file($tempFile,$targetFile);
	}
	
	$updatecommand = "UPDATE ".$webjel."video_slider SET nev=?, link=?, szoveg=?, sorrend=?".($upload ? ", video=?" : "")." WHERE id=?";
	$result = $pdo->prepare($updatecommand);
	$input_parameters = array($_POST['nev'],$_POST['link'],$_POST['szoveg'],$_POST['sorrend']);
	if ($upload) {
		$input_parameters = array_merge($input_parameters, array($fn));
	}
	$input_parameters = array_merge($input_parameters, array($_POST['id']));
	$result->execute($input_parameters);

?>

