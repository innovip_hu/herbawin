<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		include '../config_adm.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	// video Slider törlése
	if(isset($_GET['torlendo_slider_id']))
	{
		$query = "SELECT * FROM ".$webjel."video_slider WHERE id=".$_GET['torlendo_slider_id'];
		$res = $pdo->prepare($query);
		$res->execute();
		$row = $res -> fetch();
		// video slider képe
		$dir = $gyoker."/images/video_slider/";
		unlink($dir.$row['kep']);
		unlink($dir.$row['video']);
		// video slider
		$deletecommand = "DELETE FROM ".$webjel."video_slider WHERE id =".$_GET['torlendo_slider_id'];
		$result = $pdo->prepare($deletecommand);
		$result->execute();
	}
	
	$query = "SELECT * FROM ".$webjel."video_slider ORDER BY sorrend ASC";
	$i = 1;
	$van = 0; 
	foreach ($pdo->query($query) as $row)
	{
		$van = 1; 
		if($i == 1) { print '<div class="row">'; }
		print '<div class="col-md-6">
			<div class="box box-primary video_slider">
				<div class="box-body">
					<img src="'.$domain.'/images/video_slider/'.$row['kep'].'" style="width: 100%;" />';
					?>
					<img onClick="rakerdez_video_slider('rakerdez_torles', <?php print $row['id']; ?>, '<?php print $row['nev']; ?>')" src="images/ikon_torles.png" class="video_slider_torles" data-toggle="tooltip" title="Video Slider törlése" />
					<?php
					print '<div class="form-group">
						<label>Video</label>
						<div class="input-group">
							<span class="input-group-addon input_jelolo_kek"><i class="fa fa-file-video-o"></i></span>';
							?>
							<?php if (empty($row['video'])): ?>
							<input onKeyUp="mentesNezet(<?php print $row['id']; ?>, 'video_')" type="file" class="form-control" id="video_<?php print $row['id']; ?>" <?php /* ?>placeholder="Video" value="<?php print $row['video']; ?>"<?php */ ?>>
							<?php else: ?>
							<input type="text" class="form-control" value="<?php print $row['video']; ?>" disabled="disabled">
							<?php endif; ?>
							<?php
						print '</div>
					</div>';
					print '<div class="form-group" style="'.(in_array('nev', $conf_video_slider_kikapcsolt_mezok) ? 'display:none;':'').'">
						<label>Megnevezés</label>
						<div class="input-group">
							<span class="input-group-addon input_jelolo_kek"><i class="fa fa-tag"></i></span>';
							?>
							<input onKeyUp="mentesNezet(<?php print $row['id']; ?>, 'nev_')" type="text" class="form-control" id="nev_<?php print $row['id']; ?>" placeholder="Megnevezés" value="<?php print $row['nev']; ?>">
							<?php
						print '</div>
					</div>';
					print '<div class="form-group" style="'.(in_array('sorrend', $conf_video_slider_kikapcsolt_mezok) ? 'display:none;':'').'">
						<label>Sorrend</label>
						<div class="input-group">
							<span class="input-group-addon input_jelolo_kek"><i class="fa fa-sort-numeric-asc"></i></span>';
							?>
							<input onKeyUp="mentesNezet(<?php print $row['id']; ?>, 'sorrend_')" type="text" class="form-control" id="sorrend_<?php print $row['id']; ?>" placeholder="Sorrend" value="<?php print $row['sorrend']; ?>">
							<?php
						print '</div>
					</div>';
					print '<div class="form-group" style="'.(in_array('link', $conf_video_slider_kikapcsolt_mezok) ? 'display:none;':'').'">
						<label>Link</label>
						<div class="input-group">
							<span class="input-group-addon input_jelolo_kek"><i class="fa fa-link"></i></span>';
							?>
							<input onKeyUp="mentesNezet(<?php print $row['id']; ?>, 'link_')" type="text" class="form-control" id="link_<?php print $row['id']; ?>" placeholder="Link" value="<?php print $row['link']; ?>">
							<?php
						print '</div>
					</div>';
					print '<div class="form-group" style="'.(in_array('szoveg', $conf_video_slider_kikapcsolt_mezok) ? 'display:none;':'').'">
						<label>Szöveg</label>
						<div class="input-group">
							<span class="input-group-addon input_jelolo_kek"><i class="fa fa-sticky-note-o"></i></span>';
							?>
							<input onKeyUp="mentesNezet(<?php print $row['id']; ?>, 'szoveg_')" type="text" class="form-control" id="szoveg_<?php print $row['id']; ?>" placeholder="Szöveg" value="<?php print $row['szoveg']; ?>">
							<?php
						print '</div>
					</div>';
					print '<div class="box-footer" id="video_slider_footer_'.$row['id'].'">
						<button type="submit" onClick="video_sliderMentes('.$row['id'].')" class="btn btn-primary pull-right" id="video_slider_mentes_gomb_'.$row['id'].'">Mentés</button>
						<img src="images/preloader.gif" class="pull-right" id="video_slider_preloader_'.$row['id'].'" style="display:none;"/>
					</div>
				</div>
			</div>
		</div>';
		$i++;
		if($i == 3) { print '</div>'; $i = 1; }
	}
	if($i > 1 && $van == 1) { print '</div>'; }
?>

