<?php
	if (isset($_GET['script']))
	{
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
	if(isset($_GET['command']) && $_GET['command'] == 'link_mentes')
	{
		$insertcommand = "INSERT INTO ".$webjel."yt_galeria_linkek (galeria_id,link) VALUES (:galeria_id,:link)";
		$result = $pdo->prepare($insertcommand);
		$result->execute(array(':galeria_id'=>$_GET['id'],
						  ':link'=>$_POST['uj_link']));
	}
	else if(isset($_GET['torlendo_link_id']))
	{
		// SQL
		$deletecommand = "DELETE FROM ".$webjel."yt_galeria_linkek WHERE id =".$_GET['torlendo_link_id'];
		$result = $pdo->prepare($deletecommand);
		$result->execute();
	}
	else if(isset($_GET['alap_kep_id']))
	{
		// Összes alap le
		$updatecommand = "UPDATE ".$webjel."yt_galeria_linkek SET alap=0 WHERE galeria_id=".$_GET['id'];
		$result = $pdo->prepare($updatecommand);
		$result->execute();
		// Alap
		$updatecommand = "UPDATE ".$webjel."yt_galeria_linkek SET alap=1 WHERE id=".$_GET['alap_kep_id'];
		$result = $pdo->prepare($updatecommand);
		$result->execute();
	}
	
	$query_kepek = "SELECT * FROM ".$webjel."yt_galeria_linkek WHERE galeria_id=".$_GET['id']." ORDER BY alap DESC, id ASC ";
	foreach ($pdo->query($query_kepek) as $row_kepek)
	{
		$video = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe width=\"100%\" height=\"210\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>",$row_kepek['link']);
		print '<div class="col-md-4 col-sm-4">';
			print '<div class="kepek_termek_adatlap">
				'.$video.'
				<img onClick="termekKepTorles('.$_GET['id'].', '.$row_kepek['id'].')" src="images/ikon_torles.png" class="kepek_termek_adatlap_torles" data-toggle="tooltip" title="Törlés" />';
				if($row_kepek['alap'] == 1) // Alap
				{
					print '<img src="images/ikon_alap_on.png" class="kepek_termek_adatlap_alap" style="cursor:default;"/>';
				}
				else
				{
					print '<img onClick="galeriaKepAlap('.$_GET['id'].', '.$row_kepek['id'].')" src="images/ikon_alap_off.png" class="kepek_termek_adatlap_alap" data-toggle="tooltip" title="Alap"/>';
				}
			print '</div>';
		print '</div>';
	}
?>
