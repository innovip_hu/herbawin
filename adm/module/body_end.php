    <!-- Saját script -->
    <script src="scripts/admin.js"></script>
    <!-- Hover message -->
	<script type="text/javascript" src="scripts/hovermessage.js"></script>
    <!-- Multiselect jellemzőkhöz -->
	<script src="scripts/jquery.multiple.select.js"></script>
    <!-- Confirm -->
	<script src="scripts/jquery-confirm.min.js"></script>
	<script>
		function termekKeresoAC() {
			if(document.getElementById('termek_kereso_input_AC').value.length >= 2)
			{
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
					}
				  }
				xmlhttp.open("GET","module-termekek/lista_AC.php?script=ok&tabla_kereso="+encodeURIComponent(document.getElementById("termek_kereso_input_AC").value),true);
				xmlhttp.send();
			}
		};
		// Aktív menük lenyitása
		$( document ).ready(function() {
			$(".sidebar-menu ul:has(.menu_term_kat_aktiv)").css("display", "block"); // Kinyitjuk az aktív menüt.
		});	
	</script>