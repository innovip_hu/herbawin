﻿<?php
	session_start();
	ob_start();
	
	include '../config.php';
	
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	$oldal = 'parameterek';
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Termék paraméterek | Admin</title>
		<?php
			include 'module/head.php';
		?>
		<script src="scripts/parameterek.js"></script>
	</head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
		<?php
			include 'module/header.php';
			include 'module/menu.php';
		?>
		<div id="munkaablak">
			<?php
				include 'module-parameterek/lista.php';
			?>
		</div>
		<?php
			include 'module/footer.php';
		?>
    </div>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Slimscroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- Datepicker -->
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <!-- CK Editor -->
    <script src="plugins/ckeditor/ckeditor.js"></script>
    <!-- Dropzone -->
	<script src="scripts/dropzone.js"></script>
    <!-- Page Script -->
    <script>
      $(function () {
		  
        //iCheck for checkbox and radio inputs
        $('.checkbox input[type="checkbox"]').iCheck({
          checkboxClass: 'icheckbox_flat-blue',
          radioClass: 'iradio_flat-blue'
        });
		
		// Datepicker
		$('.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			startDate: false,
			isRTL: false,
			autoclose:true,
			todayBtn: true,
			todayHighlight: true,
		});

        //Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function () {
          var clicks = $(this).data('clicks');
          if (clicks) {
            //Uncheck all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
            $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
          } else {
            //Check all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("check");
            $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
          }
          $(this).data("clicks", !clicks);
        });

        //Handle starring for glyphicon and font awesome
        $(".mailbox-star").click(function (e) {
          e.preventDefault();
          //detect type
          var $this = $(this).find("a > i");
          var glyph = $this.hasClass("glyphicon");
          var fa = $this.hasClass("fa");

          //Switch states
          if (glyph) {
            $this.toggleClass("glyphicon-star");
            $this.toggleClass("glyphicon-star-empty");
          }

          if (fa) {
            $this.toggleClass("fa-star");
            $this.toggleClass("fa-star-o");
          }
        });
      });
    </script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
	<?php
		include 'module/body_end.php';
	?>
  </body>
</html>
