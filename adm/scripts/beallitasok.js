	// Szállítás mentése
	function mentesSzallitas() {
		$.post('module-beallitasok/szallitas.php?script=ok&', $('#szallitas_form').serialize(),function(response,status){ 
			$('#szallitas_div').html(response);
			// Hover message
			$('<span>Szállítási adatok módosítva.</span>').hovermessage({
				autoclose : 3000,
				position : 'top-right',
			});
		});
		/* $.get('module-beallitasok/szallitas.php?script=ok&' + $('#szallitas_form').serialize(),function(response,status){ 
			$('#szallitas_div').html(response);
			// Hover message
			$('<span>Szállítási adatok módosítva.</span>').hovermessage({
				autoclose : 3000,
				position : 'top-right',
			});
		}); */
		
	};
	// Új ÁFA mentése
	function ujAfa() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("afa_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Új ÁFA elmentve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-beallitasok/afa.php?script=ok&command=uj_afa&afa="+document.getElementById("afa_uj").value,true);
		xmlhttp.send();
	};
	// ÁFA módosításának aktiválása
	function afaMentesAktiv(id) {
		$("#afa_mentes_gomb_"+id).addClass("aktiv_gomb");
		$("#afa_"+id).css("color","#DD4B39");
	};
	// ÁFA módosítása
	function afaMentes(id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("afa_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>ÁFA módosítva.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-beallitasok/afa.php?script=ok&command=afa_modositas&id="+id+"&afa="+document.getElementById("afa_"+id).value,true);
		xmlhttp.send();
	};
	// ÁFA módosítása
	function afaTorles(id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("afa_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>ÁFA törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-beallitasok/afa.php?script=ok&command=afa_torles&id="+id,true);
		xmlhttp.send();
	};

// Szállítási súly ár módosítása
	function sulyMentes(id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("suly_alapu_szallitas_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Adatok elmentve módosítva.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-beallitasok/suly_alapu_szallitas.php?script=ok&command=suly_modositas&id="+id+"&suly_ara="+document.getElementById("suly_"+id).value,true);
		xmlhttp.send();
	};
// Szállítási súly ár módosításának aktiválása
	function sulyMentesAktiv(id) {
		$("#suly_mentes_gomb_"+id).addClass("aktiv_gomb");
		$("#suly_"+id).css("color","#DD4B39");
	};

	function arfolyam_mentes() {
		$.post('module-beallitasok/arfolyam.php?script=ok', {
			command : 'arfolyam_szinkron_valtozas',
			arfolyam : $('#arfolyam').val(),
			arfolyam_usd : $('#arfolyam_usd').val()
		} ,function(response,status){ 
			$('#arfolyam_div').html(response);
			$('.datepicker').datepicker({
				format: 'yyyy-mm-dd',
				startDate: false,
				isRTL: false,
				autoclose:true,
				todayBtn: true,
				todayHighlight: true,
			}); 			
			$('<span>Árfolyam változások mentve.</span>').hovermessage({
				autoclose : 3000,
				position : 'top-right',
			});
		});
	}	