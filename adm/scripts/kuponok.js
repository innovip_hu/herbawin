	// Modal nyitás
	function rakerdez_kupon(modal_id, id, nev) {
		document.getElementById("modal_torles_id").value=id;
		document.getElementById("modal_torles_nev").innerHTML=nev;
		document.getElementById(modal_id).style.display = 'block';
	};
	function megsem(modal_id) {
		document.getElementById("modal_torles_id").value=0;
		document.getElementById("modal_torles_nev").innerHTML='';
		document.getElementById(modal_id).style.display = 'none';
	};
	// Kupon törlése
	function kuponTorles(modal_id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById(modal_id).style.display = 'none';
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				//iCheck for checkbox and radio inputs
				$('.checkbox input[type="checkbox"]').iCheck({
				  checkboxClass: 'icheckbox_flat-blue',
				  radioClass: 'iradio_flat-blue'
				});
				// Datepicker
				$('.datepicker').datepicker({
					format: 'yyyy-mm-dd',
					startDate: false,
					isRTL: false,
					autoclose:true,
					todayBtn: true,
					todayHighlight: true,
				});
				// Hover message
				$('<span>Kupon törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-kuponok/lista.php?script=ok&torlendo_kupon_id="+document.getElementById("modal_torles_id").value,true);
		xmlhttp.send();
	};
	// Új kupon lenyitása
	function ujKuponFrom() {
		$( "#uj_kupon" ).animate({
			height: [ "toggle", "swing" ]
		}, 300);
	};
	// Kupon típusai
	function kupon() {
		if (document.getElementById('kupon_fajta').value == 'Ajándék termék') {
			$("#termek_id_div").slideDown(300);
			$("#kedvezmeny_div").slideUp(300);
			$("#kedv_tipus_div").slideUp(300);
			document.getElementById('kedvezmeny').value = '';
		}
		else if (document.getElementById('kupon_fajta').value == 'Kedvezmény') {
			$("#termek_id_div").slideUp(300);
			$("#kedvezmeny_div").slideDown(300);
			$("#kedv_tipus_div").slideDown(300);
			document.getElementById('termek_id').value = '';
		}
		else if (document.getElementById('kupon_fajta').value == 'Ingyenes szállítás') {
			$("#termek_id_div").slideUp(300);
			$("#kedvezmeny_div").slideUp(300);
			$("#kedv_tipus_div").slideUp(300);
			document.getElementById('termek_id').value = '';
			document.getElementById('kedvezmeny').value = '';
		}
	};
	// Kedvezmény típusai
	function kedvTipus() {
		if (document.getElementById('kedv_tipus').value == 'szazalek') {
			document.getElementById('kedv_tipus_jel').innerHTML = '%';
		}
		else if (document.getElementById('kedv_tipus').value == 'osszeg') {
			document.getElementById('kedv_tipus_jel').innerHTML = 'Ft';
		}
	};
	// Kupon típusai
	function ujKuponMentes() {
		$( "#uj_kupon" ).slideUp( 300, function() {
			// Összecsukás után mentés
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
					//iCheck for checkbox and radio inputs
					$('.checkbox input[type="checkbox"]').iCheck({
					  checkboxClass: 'icheckbox_flat-blue',
					  radioClass: 'iradio_flat-blue'
					});
					// Datepicker
					$('.datepicker').datepicker({
						format: 'yyyy-mm-dd',
						startDate: false,
						isRTL: false,
						autoclose:true,
						todayBtn: true,
						todayHighlight: true,
					});
					// Hover message
					$('<span>Új kupon létrehozva.</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("POST","module-kuponok/lista.php?script=ok",true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("command=uj_kupon"
				+"&kod="+encodeURIComponent(document.getElementById("kod").value)
				+"&indul="+encodeURIComponent(document.getElementById("indul").value)
				+"&vege="+encodeURIComponent(document.getElementById("vege").value)
				+"&kupon_fajta="+encodeURIComponent(document.getElementById("kupon_fajta").value)
				+"&termek_id="+encodeURIComponent(document.getElementById("termek_id").value)
				+"&kedvezmeny="+encodeURIComponent(document.getElementById("kedvezmeny").value)
				+"&egyszeri="+document.getElementById("egyszeri").checked
				+"&min_kosar_ertek="+encodeURIComponent(document.getElementById("min_kosar_ertek").value)
				+"&kedv_tipus="+encodeURIComponent(document.getElementById("kedv_tipus").value)
				);
		});
	};

	