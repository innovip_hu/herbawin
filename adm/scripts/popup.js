	// Mentés megjelenítése
	function mentesNezet(id, input) {
		document.getElementById("banner_footer_"+id).style.display = 'block';
		document.getElementById(input+id).style.color = '#DD4B39';
	}
	// Mentés
	function bannerMentes(id) {
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("banner_footer_"+id).style.display = 'none';
				document.getElementById('nev_'+id).style.color = '#555';
				document.getElementById('link_'+id).style.color = '#555';
				document.getElementById('datum_tol_'+id).style.color = '#555';
				document.getElementById('datum_ig_'+id).style.color = '#555';
				// Hover message
				$('<span>Adatok elmentve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		}
 		xmlhttp.open("POST","module-popup/popup_mentes.php",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("id="+id
			+"&nev="+encodeURIComponent(document.getElementById("nev_"+id).value)
			+"&link="+encodeURIComponent(document.getElementById("link_"+id).value)
			+"&datum_tol="+encodeURIComponent(document.getElementById("datum_tol_"+id).value)
			+"&datum_ig="+encodeURIComponent(document.getElementById("datum_ig_"+id).value)
			);
	}
	// Modal nyitás
	function rakerdez_banner(modal_id, id, nev) {
		document.getElementById("modal_torles_id").value=id;
		document.getElementById("modal_torles_nev").innerHTML=nev;
		document.getElementById(modal_id).style.display = 'block';
	};
	function megsem_banner(modal_id) {
		document.getElementById("modal_torles_id").value=0;
		document.getElementById("modal_torles_nev").innerHTML='';
		document.getElementById(modal_id).style.display = 'none';
	};
	// Banner törlése
	function bannerTorles(modal_id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById(modal_id).style.display = 'none';
				document.getElementById("popupok").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Popup törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-popup/popupok.php?script=ok&torlendo_banner_id="+document.getElementById("modal_torles_id").value,true);
		xmlhttp.send();
	};

	