	// lapozás Tovább gomb
	function lapozasTovabb(fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+(Number(document.getElementById("kezd").value)+Number(document.getElementById("oldalszam").value)),true);
		xmlhttp.send();
	};
	// lapozás Vissza gomb
	function lapozasVissza(fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+(Number(document.getElementById("kezd").value)-Number(document.getElementById("oldalszam").value)),true);
		xmlhttp.send();
	};
	
 	// Belépés
	function Belepes(belep_fajl,fajl,id)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				$('html,body').scrollTop(0);
			}
		  }
		xmlhttp.open("GET",belep_fajl+"?script=ok&oldalszam="+document.getElementById("oldalszam").value+"&sorr_tip="+document.getElementById("sorr_tip").value+"&sorrend="+document.getElementById("sorrend").value+"&kezd="+document.getElementById("kezd").value+"&id="+id+"&fajl="+fajl,true);
		xmlhttp.send();
	};

	// Visszalépés a listába
	function vissza(fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok&oldalszam="+encodeURIComponent(document.getElementById("oldalszam").value)+"&sorr_tip="+encodeURIComponent(document.getElementById("sorr_tip").value)+"&sorrend="+encodeURIComponent(document.getElementById("sorrend").value)+"&kezd="+encodeURIComponent(document.getElementById("kezd").value),true);
		xmlhttp.send();
	};

	// Rendelés lezárása
	function lezar(id, fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Rendelés lezárva.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET",fajl+"?lezar_id="+id+"&script=ok&oldalszam="+encodeURIComponent(document.getElementById("oldalszam").value)+"&sorr_tip="+encodeURIComponent(document.getElementById("sorr_tip").value)+"&sorrend="+encodeURIComponent(document.getElementById("sorrend").value)+"&kezd="+encodeURIComponent(document.getElementById("kezd").value),true);
		xmlhttp.send();
	};

	// Szállítva státusz
	function szallitva(id, fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Szállítás alatt státusz beállítva.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET",fajl+"?szallitva_id="+id+"&script=ok&oldalszam="+encodeURIComponent(document.getElementById("oldalszam").value)+"&sorr_tip="+encodeURIComponent(document.getElementById("sorr_tip").value)+"&sorrend="+encodeURIComponent(document.getElementById("sorrend").value)+"&kezd="+encodeURIComponent(document.getElementById("kezd").value),true);
		xmlhttp.send();
	};

	// Rendelés felnyitása
	function felnyit(id, fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Rendelés felnyitva.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET",fajl+"?felnyit_id="+id+"&script=ok&oldalszam="+encodeURIComponent(document.getElementById("oldalszam").value)+"&sorr_tip="+encodeURIComponent(document.getElementById("sorr_tip").value)+"&sorrend="+encodeURIComponent(document.getElementById("sorrend").value)+"&kezd="+encodeURIComponent(document.getElementById("kezd").value),true);
		xmlhttp.send();
	};

	// Rendelés törlése
	function torol(id, fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Rendelés törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET",fajl+"?torol_id="+id+"&script=ok&oldalszam="+encodeURIComponent(document.getElementById("oldalszam").value)+"&sorr_tip="+encodeURIComponent(document.getElementById("sorr_tip").value)+"&sorrend="+encodeURIComponent(document.getElementById("sorrend").value)+"&kezd="+encodeURIComponent(document.getElementById("kezd").value),true);
		xmlhttp.send();
	};

	// Modal nyitás
	function rakerdez(modal_id) {
		document.getElementById(modal_id).style.display = 'block';
	}
	function megsem(modal_id) {
		document.getElementById(modal_id).style.display = 'none';
	}

	// AutoComplete keresés
	function tablaAutoComplete()
	{
		var keresett_szoveg = document.getElementById("tabla_kereso").value;
		if(document.getElementById('tabla_kereso').value.length >= 2)
		{
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
					document.getElementById("tabla_kereso").value = keresett_szoveg;
					document.getElementById("tabla_kereso").focus();
				}
			  }
			xmlhttp.open("GET","module-rendelesek/lista_AC.php?script=ok&tabla_kereso="+encodeURIComponent(document.getElementById("tabla_kereso").value),true);
			xmlhttp.send();
		}
	};
	// Autocomplete törlés
	function autoCompleteTorles(fajl)
	{
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("munkaablak").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET",fajl+"?script=ok",true);
		xmlhttp.send();
	};
	// Saját megjegyzés
	function sajatMegjegyzesMentese(id) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
 		xmlhttp.open("POST","module-rendelesek/sajat_megjegyzes_mentese.php?script=ok&id="+id,true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("sajat_megjegyzes="+encodeURIComponent(document.getElementById("sajat_megjegyzes").value));
		
		// Hover message
		$('<span>Megjegyzés elmentve.</span>').hovermessage({
			autoclose : 3000,
			position : 'top-right',
		});
	}
	
	