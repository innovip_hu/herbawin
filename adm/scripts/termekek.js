// Nézet váltás
	function nezetValtas(csop_id, nezet)
	{
		// Cookie beállítása
		var expires;
		var days = 365;
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		expires = "; expires=" + date.toGMTString();
		document.cookie = "admin_termek_lista_nezet="+nezet + expires + "; path=/";
		
		window.open("termekek.php?csop_id="+csop_id , "_self");
	};
// Új termék
	function mentesUjTermek(csop_id) {
		var leiras = encodeURIComponent(CKEDITOR.instances.editor1.getData());
		
		// ÚJ termék létrehozása ID miatt
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				var id = parseFloat(xmlhttp.responseText); // Az új termék ID-je
				window.open("termekek.php?csop_id="+csop_id+"&id="+id , "_self");
			}
		  }
		// xmlhttp.open("GET","module-termekek/termek_letrehozas.php?csop_id="+csop_id,true);
		// xmlhttp.send();
		xmlhttp.open("POST","module-termekek/termek_letrehozas.php?csop_id="+csop_id,true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("command=uj_termek&nev="+encodeURIComponent(document.getElementById("nev").value)
				+"&ar="+encodeURIComponent(document.getElementById("ar").value)
				+"&afa="+encodeURIComponent(document.getElementById("afa").value)
				+"&akciosar="+encodeURIComponent(document.getElementById("akciosar").value)
				+"&akcio_tol="+encodeURIComponent(document.getElementById("akcio_tol").value)
				+"&akcio_ig="+encodeURIComponent(document.getElementById("akcio_ig").value)
				+"&rovid_leiras="+encodeURIComponent(document.getElementById("rovid_leiras").value)
				+"&seo_title="+encodeURIComponent(document.getElementById("seo_title").value)
				+"&seo_description="+encodeURIComponent(document.getElementById("seo_description").value)
				+"&lathato="+encodeURIComponent(document.getElementById("lathato").checked)
				+"&raktaron="+encodeURIComponent(document.getElementById("raktaron").value)
				+"&kiemelt="+encodeURIComponent(document.getElementById("kiemelt").checked)
				+"&leiras="+leiras
				+"&csop_id="+csop_id
		);
	};
// Tömeges akció megszűntetése
	function tomegesAkcioMegszuntetese(csop_id) {
		$.confirm({
			icon: 'fa fa-warning',
			title: 'Akciók törlése',
			theme: 'supervan', // 'material', 'bootstrap', 'light', 'dark'
			content: 'Biztos törölni szeretnéd az összes akciót?',
			buttons: {
				igen: {
					text: 'Igen',
					action: function(){
						$.post('module-termekek/tomeges_akcio_torlese.php',{
								csop_id : csop_id
								},function(response,status){ // Required Callback Function
									window.open("termekek.php?csop_id="+csop_id , "_self");
							});
					}
				},
				nem: {
					text: 'Nem',
					action: function(){
						// bezár
					}
				}
			}
		});
	};
// Új tömeges akció
	function mentesUjTomegesAkcio(csop_id) {
		if($('#akcio_merteke').val() == '' || $('#akcio_tol').val() == '' || $('#akcio_ig').val() == '') {
			if($('#akcio_merteke').val() == '') { var uzenet = 'Add meg az akció mértékét!'; }
			else if($('#akcio_tol').val() == '') { var uzenet = 'Add meg az akció kezdetét!'; }
			else if($('#akcio_ig').val() == '') { var uzenet = 'Add meg az akció végét!'; }
			$.confirm({
				icon: 'fa fa-warning',
				title: 'Üres mező',
				type: 'red',
				theme: 'light', // 'material', 'bootstrap', 'light', 'dark'
				content: uzenet,
				buttons: {
					ok: {
						action: function(){
							// $('#akcio_merteke').focus()
						}
					}
				}
			});
		}
		else {
			$.post('module-termekek/tomeges_akcio_letrehozas.php',{
					command : 'uj_tomeges_akcio'
					, akcio_merteke : $('#akcio_merteke').val()
					, akcio_tol : $('#akcio_tol').val()
					, akcio_ig : $('#akcio_ig').val()
					, csop_id : csop_id
					},function(response,status){ // Required Callback Function
						window.open("termekek.php?csop_id="+csop_id , "_self");
				});
		}
	};
// Termék akciózás a listában
	function mentesListaAr(id){
		var ar = $('#mod-ar'+id).val();
		var akcios = $('#mod-akcios-ar'+id).val();
		var akciotol = $('#mod-akcio-tol'+id).val();
		var akcioig = $('#mod-akcio-ig'+id).val();

		// console.log(akciotol);

		$.post('module-termekek/termek_akcio_listaban.php',{
			term_id : id,
			modAkcios : akcios,
			modAr : ar,
			akciotol : akciotol,
			akcioig : akcioig

		},function(response,status){ // Required Callback Function
			$('<span>Adatok elmentve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			$('#mod-mentes'+id).css('display', 'none');
			if(akcios > 0)
			{
				$('#mod-akcios-ar'+id).addClass("akcios_term_input");
			}
			else
			{
				$('#mod-akcios-ar'+id).removeClass("akcios_term_input");
			}
		});
	}
// Listában save gomb megjelenés
	$( document ).ready(function() {
		$('.sgomb_ok').change(function(){
			var id = $(this).attr('attr_termid');
			$('#mod-mentes'+id).css('display', 'block');
		});
		$('.sgomb_ok').keyup(function(){
			var id = $(this).attr('attr_termid');
			$('#mod-mentes'+id).css('display', 'block');
		});
	});
// Alapadatok mentése
	function mentesTermek(id) {
		var leiras = encodeURIComponent(CKEDITOR.instances.editor1.getData());
		var leiras_en = encodeURIComponent(CKEDITOR.instances.editor2.getData());
		var leiras_de = encodeURIComponent(CKEDITOR.instances.editor3.getData());
		
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				// Hover message
				$('<span>Adatok elmentve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("POST","module-termekek/termek_adatok_mentese.php?id="+id,true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("command=adaok_mentese&nev="+encodeURIComponent(document.getElementById("nev").value)
				+"&suly="+encodeURIComponent(document.getElementById("suly").value)
				+"&nev_en="+encodeURIComponent(document.getElementById("nev_en").value)
				+"&nev_de="+encodeURIComponent(document.getElementById("nev_de").value)
				+"&ar="+encodeURIComponent(document.getElementById("ar").value)
				+"&afa="+encodeURIComponent(document.getElementById("afa").value)
				+"&akciosar="+encodeURIComponent(document.getElementById("akciosar").value)
				+"&akcio_tol="+encodeURIComponent(document.getElementById("akcio_tol").value)
				+"&akcio_ig="+encodeURIComponent(document.getElementById("akcio_ig").value)
				+"&rovid_leiras="+encodeURIComponent(document.getElementById("rovid_leiras").value)
				+"&rovid_leiras_en="+encodeURIComponent(document.getElementById("rovid_leiras_en").value)
				+"&rovid_leiras_de="+encodeURIComponent(document.getElementById("rovid_leiras_de").value)
				+"&seo_title="+encodeURIComponent(document.getElementById("seo_title").value)
				+"&seo_description="+encodeURIComponent(document.getElementById("seo_description").value)
				+"&lathato="+encodeURIComponent(document.getElementById("lathato").checked)
				+"&kiemelt="+encodeURIComponent(document.getElementById("kiemelt").checked)
				+"&leiras="+leiras
				+"&leiras_en="+leiras_en
				+"&leiras_de="+leiras_de
				+"&csop_id="+encodeURIComponent(document.getElementById("csop_id").value)
				+"&raktaron="+encodeURIComponent(document.getElementById("raktaron").value)
				+"&termek_csoportok="+encodeURIComponent(getSelectValues(document.getElementById("termek_csoportok")))
				);
	}
	// Több kategóriához rendelés a mentésnél
	function getSelectValues(select) {
		var result = [];
		var options = select && select.options;
		var opt;
		for (var i=0, iLen=options.length; i<iLen; i++) {
			opt = options[i];
			if (opt.selected) {
				result.push(opt.value || opt.text);
			}
		}
		return result;
	}
// Képek törlése
	function termekKepTorles(id, kep_id) {
		var leiras = encodeURIComponent(CKEDITOR.instances.editor1.getData());
		
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("termek_kepek_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Kép törölve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-termekek/termek_kepek.php?script=ok&id="+id+"&torlendo_kep_id="+kep_id,true);
		xmlhttp.send();
	};
// Képek alapértelmezése
	function termekKepAlap(id, kep_id) {
		var leiras = encodeURIComponent(CKEDITOR.instances.editor1.getData());
		
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("termek_kepek_div").innerHTML=xmlhttp.responseText;
				// Hover message
				$('<span>Alapértelmezés beállítva</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			}
		  }
		xmlhttp.open("GET","module-termekek/termek_kepek.php?script=ok&id="+id+"&alap_kep_id="+kep_id,true);
		xmlhttp.send();
	};
// Termék törlése
	function torolTermek(csop_id, id)
	{
		$.confirm({
			icon: 'fa fa-warning',
			title: 'Termék törlése',
			theme: 'supervan', // 'material', 'bootstrap', 'light', 'dark'
			content: 'Biztos törölni szeretnéd a terméket?',
			buttons: {
				igen: {
					text: 'Igen',
					action: function(){
						$.post('module-termekek/termek_torlese.php?id='+id,{
								csop_id : csop_id
								},function(response,status){ // Required Callback Function
									window.open("termekek.php?csop_id="+csop_id , "_self");
							});
					}
				},
				nem: {
					text: 'Nem',
					action: function(){
						// bezár
					}
				}
			}
		});
	};
// Klónozás
	function klonozas(csop_id, id, kezd) {
		// ÚJ termék létrehozása ID miatt
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				var id = parseFloat(xmlhttp.responseText); // Az új termék ID-je
				window.open("termekek.php?csop_id="+csop_id+"&id="+id+"&kezd="+kezd , "_self");
			}
		  }
		// xmlhttp.open("GET","module-termekek/termek_letrehozas.php?csop_id="+csop_id,true);
		// xmlhttp.send();
		xmlhttp.open("POST","module-termekek/termek_klon_letrehozas.php?csop_id="+csop_id,true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("command=uj_klon&id="+id);
	};
// Paraméterek
	function parameter_mentes_egyedi () {
		// Törlés
		$('.parameter_torles_gomb').click(function() {
			// alert( "Paraméter: "+$(this).attr('attr_parameter_id') );
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("parameterek").innerHTML=xmlhttp.responseText;
					// iCheck for checkbox and radio inputs
					var scrpt = document.createElement('script');
					scrpt.src='plugins/iCheck/icheck.min.js';
					document.head.appendChild(scrpt);
					$('.checkbox input[type="checkbox"]').iCheck({
					  checkboxClass: 'icheckbox_flat-blue',
					  radioClass: 'iradio_flat-blue'
					});
					// Paraméter újra hívása
					parameter_mentes_egyedi ();
					// Hover message
					$('<span>Paraméter törölve!</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("GET","module-termekek/termek_urlap_parameterek.php?script2=ok&command=parameter_torles&id="+$(this).attr('attr_termek_id')+"&parameter_id="+$(this).attr('attr_parameter_id'),true);
			xmlhttp.send();
		});
		// Módósítás előkészítése (egyedi paraméter)
		$('.parameter_input_egyedi').keyup(function() {
			$("#parameter_span_gomb_"+$(this).attr('attr_parameter_id')).addClass("input_jelolo_piros");
			$("#parameter_span_gomb_"+$(this).attr('attr_parameter_id')).addClass("input_jelolo_gomb");
			$("#parameter_span_gomb_"+$(this).attr('attr_parameter_id')).children("i").addClass("fa-floppy-o");
		});
		// Módósítás (egyedi paraméter)
		$('.parameter_mentes_gomb').click(function() {
			var ertek = $('#parameter_'+$(this).attr('attr_parameter_id')).val();
			var id = $(this).attr('attr_parameter_id');
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					$("#parameter_span_gomb_"+id).removeClass("input_jelolo_piros");
					$("#parameter_span_gomb_"+id).removeClass("input_jelolo_gomb");
					$("#parameter_span_gomb_"+id).children("i").removeClass("fa-floppy-o");
					// Hover message
					$('<span>Paraméter módosítva!</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("POST","module-termekek/parameter_mentes.php?script2=ok&command=parameter_mod_egyedi&id="+$(this).attr('attr_termek_id')+"&parameter_id="+$(this).attr('attr_parameter_id'),true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("ertek="+encodeURIComponent(ertek));
		});
		// Módósítás (lenyilo-egy_valaszthato)
		$('.parameter_select').change(function() {
			var ertek = $(this).val();
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					// Hover message
					$('<span>Paraméter módosítva!</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("GET","module-termekek/parameter_mentes.php?script2=ok&command=parameter_mod_lenyilo_egy_valaszthato&id="+$(this).attr('attr_termek_id')+"&parameter_id="+$(this).attr('attr_parameter_id')+"&ertek="+ertek,true);
			xmlhttp.send();
		});
		// Módósítás (igen-nem paraméter)
		$('.parameter_igen_nem_gomb').on('ifClicked', function(event){
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("parameterek").innerHTML=xmlhttp.responseText;
					// iCheck for checkbox and radio inputs
					var scrpt = document.createElement('script');
					scrpt.src='plugins/iCheck/icheck.min.js';
					document.head.appendChild(scrpt);
					$('.checkbox input[type="checkbox"]').iCheck({
					  checkboxClass: 'icheckbox_flat-blue',
					  radioClass: 'iradio_flat-blue'
					});
					// Paraméter újra hívása
					parameter_mentes_egyedi ();
					// Hover message
					$('<span>Paraméter módosítva!</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("GET","module-termekek/termek_urlap_parameterek.php?script2=ok&command=parameter_mod_igen_nem&id="+$(this).attr('attr_termek_id')+"&parameter_id="+$(this).attr('attr_parameter_id')+"&ertek="+$(this).attr('attr_ertek'),true);
			xmlhttp.send();
		});
		// Új paraméter
		$('#parameter_uj_parameter').change(function() {
			var parameter_id = $(this).val();
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("parameterek").innerHTML=xmlhttp.responseText;
					// iCheck for checkbox and radio inputs
					var scrpt = document.createElement('script');
					scrpt.src='plugins/iCheck/icheck.min.js';
					document.head.appendChild(scrpt);
					$('.checkbox input[type="checkbox"]').iCheck({
					  checkboxClass: 'icheckbox_flat-blue',
					  radioClass: 'iradio_flat-blue'
					});
					// Paraméter újra hívása
					parameter_mentes_egyedi ();
					// Hover message
					$('<span>Új paraméter hozzáadva!</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("GET","module-termekek/termek_urlap_parameterek.php?script2=ok&command=uj_parameter&id="+$(this).attr('attr_termek_id')+"&parameter_id="+parameter_id,true);
			xmlhttp.send();
		});
		// Feláras paraméter árának mentésének előkészítése
		$('.param_felar_input').keyup(function() {
			var id = $(this).attr('attr_param_ertek_id');
			$("#param_felar_gomb_"+id).css('display', 'inline');
		});
		// Feláras paraméter árának mentése
		$('.param_felar_gomb').click(function() {
			var felar = $('#param_felar_input_'+$(this).attr('attr_param_ertek_id')).val();
			var id = $(this).attr('attr_param_ertek_id');
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					$("#param_felar_gomb_"+id).css('display', 'none');
					// Hover message
					$('<span>Felár módosítva!</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("GET","module-termekek/parameter_mentes.php?script2=ok&command=parameter_mod_felar&id="+id+"&felar="+felar,true);
			xmlhttp.send();
		});
		// Feláras paraméter érték törlése
		$('.param_ertek_torles_gomb').click(function() {
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("parameterek").innerHTML=xmlhttp.responseText;
					// iCheck for checkbox and radio inputs
					var scrpt = document.createElement('script');
					scrpt.src='plugins/iCheck/icheck.min.js';
					document.head.appendChild(scrpt);
					$('.checkbox input[type="checkbox"]').iCheck({
					  checkboxClass: 'icheckbox_flat-blue',
					  radioClass: 'iradio_flat-blue'
					});
					// Paraméter újra hívása
					parameter_mentes_egyedi ();
					// Hover message
					$('<span>Érték törölve!</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("GET","module-termekek/termek_urlap_parameterek.php?script2=ok&command=parameter_ertek_torlesi&id="+$(this).attr('attr_termek_id')+"&parameter_ertek_id="+$(this).attr('attr_param_ertek_id'),true);
			xmlhttp.send();
		});
		// Új több választós paraméter
		$('.parameter_select_tobb_valasztos').change(function() {
			var ertek_id = $(this).val();
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("parameterek").innerHTML=xmlhttp.responseText;
					// iCheck for checkbox and radio inputs
					var scrpt = document.createElement('script');
					scrpt.src='plugins/iCheck/icheck.min.js';
					document.head.appendChild(scrpt);
					$('.checkbox input[type="checkbox"]').iCheck({
					  checkboxClass: 'icheckbox_flat-blue',
					  radioClass: 'iradio_flat-blue'
					});
					// Paraméter újra hívása
					parameter_mentes_egyedi ();
					// Hover message
					$('<span>Új paraméter hozzáadva!</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
				}
			  }
			xmlhttp.open("GET","module-termekek/termek_urlap_parameterek.php?script2=ok&command=uj_tobb_valasztos_parameter_ertek&id="+$(this).attr('attr_termek_id')+"&parameter_id="+$(this).attr('attr_parameter_id')+"&ertek="+ertek_id,true);
			xmlhttp.send();
		});
	};
	$( document ).ready(function() {
		parameter_mentes_egyedi ();
	});

// Új paraméter hozzáadása és mentése
	function uj_parameter_hozzaadasa_mentese(id) {
		if($('#parmeter_hozzaadas').val() == 0 && $('#uj_parameter').val() == '')
		{
			$.confirm({
				icon: 'fa fa-warning',
				title: 'Üres mező',
				type: 'red',
				theme: 'light', // 'material', 'bootstrap', 'light', 'dark'
				content: 'Válassz egy paramétert, vagy add meg az újnak a nevét!',
				buttons: {
					ok: {
						action: function(){
							// $('#akcio_merteke').focus()
						}
					}
				}
			});
		}
		else if($('#uj_parameter_ertek').val() == '')
		{
			$.confirm({
				icon: 'fa fa-warning',
				title: 'Üres mező',
				type: 'red',
				theme: 'light', // 'material', 'bootstrap', 'light', 'dark'
				content: 'Add meg a paraméter értékét!',
				buttons: {
					ok: {
						action: function(){
							// $('#akcio_merteke').focus()
						}
					}
				}
			});
		}
		else
		{
			$.post('module-termekek/termek_uj_parameterek.php?script2=ok&id='+id,{
					parmeter_hozzaadas : $('#parmeter_hozzaadas').val(),
					uj_parameter : $('#uj_parameter').val(),
					uj_parameter_ertek : $('#uj_parameter_ertek').val(),
					uj_parameter_ertek_en : $('#uj_parameter_ertek_en').val(),
					uj_parameter_ertek_de : $('#uj_parameter_ertek_de').val(),
					uj_parameter_me : $('#uj_parameter_me').val(),
					command : 'uj_parameter'
				},function(response,status){ // Required Callback Function
					$('#parameterek_uj_div').html(response);
					$('<span>Új paraméter hozzáadva!</span>').hovermessage({
						autoclose : 3000,
						position : 'top-right',
					});
			});
		}
	};
// Paraméter érték törlése
	function param_ertek_torles2(id, param_id) {
		$.confirm({
			icon: 'fa fa-warning',
			title: 'Paraméter törlése',
			theme: 'supervan', 
			content: 'Biztos törölni szeretnéd a paramétert?',
			buttons: {
				igen: {
					text: 'Igen',
					action: function(){
						$.post('module-termekek/termek_uj_parameterek.php?script2=ok&id='+id,{
								param_id : param_id,
								command : 'parameter_ertek_torlese'
							},function(response,status){ 
								$('#parameterek_uj_div').html(response);
								$('<span>Paraméter érték törölve!</span>').hovermessage({
									autoclose : 3000,
									position : 'top-right',
								});
						});
					}
				},
				nem: {
					text: 'Nem',
					action: function(){
						// bezár
					}
				}
			}
		});
	};
// Paraméter opciók selectbe generálása
	function parameterOpcioSelect() {
		$.post('module-termekek/parameter_ertek_opciok.php',{
				parmeter : $('#parmeter_hozzaadas').val()
			},function(response,status){ 
				$('#parameter_ertek_opcio').html(response);
		});
	};
// Paraméter opció beírása
	function parameterOpcioBeir() {
		$('#uj_parameter_ertek').val($('#parmeter_opciok_select').val());
	};
// Feláras paraméter árának mentésének előkészítése
	function param_felar_mentes_elok(id) {
		$('.param_felar_input').keyup(function() {
			$("#param_felar_gomb_"+id).css('display', 'inline');
		});
	};
// Feláras paraméter árának mentése
	function param_felar_mentes(id) {
		$.post('module-termekek/param_felar_mentes.php',{
			id : id,
			felar : $('#param_felar_input_'+id).val()
		},function(response,status){
			$('<span>Adatok elmentve.</span>').hovermessage({
					autoclose : 3000,
					position : 'top-right',
				});
			$('#param_felar_gomb_'+id).css('display', 'none');
		});
	};
	
	