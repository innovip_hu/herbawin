<?php
class SimpleImage {
     var $image;
     var $image_type;

     function load($filename) {
        $image_info = getimagesize($filename);
        $this->image_type = $image_info[2];
        if( $this->image_type == IMAGETYPE_JPEG ) {
            $this->image = imagecreatefromjpeg($filename);
        } elseif( $this->image_type == IMAGETYPE_GIF ) {
            $this->image = imagecreatefromgif($filename);
        } elseif( $this->image_type == IMAGETYPE_PNG ) {
            $this->image = imagecreatefrompng($filename);
        }
    }
    /* function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
        if( $image_type == IMAGETYPE_JPEG ) {
            imagejpeg($this->image,$filename,$compression);
        } elseif( $image_type == IMAGETYPE_GIF ) {
            imagegif($this->image,$filename);
        } elseif( $image_type == IMAGETYPE_PNG ) {
            imagepng($this->image,$filename);
        }
        if( $permissions != null) {
            chmod($filename,$permissions);
        }
    } */
	function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {

	   // do this or they'll all go to jpeg
	   $image_type=$this->image_type;

	  if( $image_type == IMAGETYPE_JPEG ) {
		 imagejpeg($this->image,$filename,$compression);
	  } elseif( $image_type == IMAGETYPE_GIF ) {
		 imagegif($this->image,$filename);  
	  } elseif( $image_type == IMAGETYPE_PNG ) {
		// need this for transparent png to work          
		imagealphablending($this->image, false);
		imagesavealpha($this->image,true);
		imagepng($this->image,$filename);
	  }   
	  if( $permissions != null) {
		 chmod($filename,$permissions);
	  }
	}  
  
    function output($image_type=IMAGETYPE_JPEG) {
        if( $image_type == IMAGETYPE_JPEG ) {
            imagejpeg($this->image);
        } elseif( $image_type == IMAGETYPE_GIF ) {
            imagegif($this->image);
        } elseif( $image_type == IMAGETYPE_PNG ) {
            imageAlphaBlending($this->image, true);
            imageSaveAlpha($this->image, true);
            imagepng($this->image);
        }   
    }

    function getWidth() {
        return imagesx($this->image);
    }
    function getHeight() {
        return imagesy($this->image);
    }
    function resizeToHeight($height) {
        $ratio = $height / $this->getHeight();
        $width = $this->getWidth() * $ratio;
        $this->resize($width,$height);
    }
    function resizeToWidth($width) {
        $ratio = $width / $this->getWidth();
        $height = $this->getheight() * $ratio;
        $this->resize($width,$height);
    }
    function scale($scale) {
        $width = $this->getWidth() * $scale/100;
        $height = $this->getheight() * $scale/100;
        $this->resize($width,$height);
    }
    /* function resize($width,$height) {

        // ADDED CODE IS HERE - NOT SURE WHY IT DOESN'T WORK FOR PNG

        // Setup new image
        $new_image = imagecreatetruecolor($width, $height);
        // These parameters are required for handling PNG files.
        imagealphablending($new_image, false);
        imagesavealpha($new_image,true);
        $transparent = imagecolorallocatealpha($new_image, 255, 255, 255, 127);
        imagefilledrectangle($new_image, 0, 0, $width, $height, $transparent);
        // Resize image
        imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
		
        $this->image = $new_image;
    } */
	function resize($width,$height,$forcesize='n') {
	  /* optional. if file is smaller, do not resize. */
	  if ($forcesize == 'n') {
		  if ($width > $this->getWidth() && $height > $this->getHeight()){
			  $width = $this->getWidth();
			  $height = $this->getHeight();
		  }
	  }

	  $new_image = imagecreatetruecolor($width, $height);
	  /* Check if this image is PNG or GIF, then set if Transparent*/  
	  if(($this->image_type == IMAGETYPE_GIF) || ($this->image_type==IMAGETYPE_PNG)){
		  imagealphablending($new_image, false);
		  imagesavealpha($new_image,true);
		  $transparent = imagecolorallocatealpha($new_image, 255, 255, 255, 127);
		  imagefilledrectangle($new_image, 0, 0, $width, $height, $transparent);
	  }
	  imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());

	  $this->image = $new_image;   
	}
	function resize_and_cut($width,$height) {
	  // optional. if file is smaller, do not resize.
	  if ($forcesize == 'n') {
		  if ($width > $this->getWidth() && $height > $this->getHeight()){
			  $width = $this->getWidth();
			  $height = $this->getHeight();
		  }
	  }

	  $new_image = imagecreatetruecolor($width, $height); // új kép mérete
	  $height_regi = $height;
	  $width_regi = $width;
	  
	  // vizsgálat
	  $szel_kulomb = $this->getWidth() - $width;
	  $mag_kulomb = $this->getHeight() - $height;
	  
	  if($mag_kulomb > $szel_kulomb) // álló
	  {
		$ratio = $width / $this->getWidth();
		$height = $this->getHeight() * $ratio;
		$kep_igazitas_x = 0;
		$kep_igazitas_y = 0+($height_regi-$height)/2;
	  }
	  else // fekvő
	  {
		$ratio = $height / $this->getHeight();
		$width = $this->getWidth() * $ratio;
		$kep_igazitas_x = 0+($width_regi-$width)/2;
		$kep_igazitas_y = 0;
	  }
		
	  // Check if this image is PNG or GIF, then set if Transparent 
	  if(($this->image_type == IMAGETYPE_GIF) || ($this->image_type==IMAGETYPE_PNG)){
		  imagealphablending($new_image, false);
		  imagesavealpha($new_image,true);
		  $transparent = imagecolorallocatealpha($new_image, 255, 255, 255, 127);
		  imagefilledrectangle($new_image, 0, 0, $width, $height, $transparent);
	  }
	  imagecopyresampled($new_image, $this->image, $kep_igazitas_x, $kep_igazitas_y, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());

	  $this->image = $new_image;   
	}
}
?>