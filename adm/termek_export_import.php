<?php
	session_start();
	ob_start();
	
	include '../config.php';
	
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	$oldal = 'termek_export_import';
	if(isset($_GET['megoszto']))
	{
		include 'xml_arak_megosztasa.php';
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Termék export/import | Admin</title>
		<?php
			include 'module/head.php';
		?>
	</head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
		<?php
			include 'module/header.php';
			include 'module/menu.php';
		?>
		
		<div id="munkaablak">
			<div class="content-wrapper bg_admin">
				<section class="content-header">
				  <h1 id="myModal">Termék export/import</h1>
				  <ol class="breadcrumb">
					<li><a href="index.php"><i class="fa fa-home"></i> Nyitóoldal</a></li>
					<li class="active">Termék export/import</li>
				  </ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-6">
							<div class="box box-primary">
								<div class="box-header with-border">
									<h3 class="box-title">Exportálás</h3>
									<div class="box-tools pull-right">
										<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="box-body">
									<div class="box-body">
										<form id="export_form">
										<div class="form-group">
											<label>Kategória</label>
											<div class="input-group">
												<span class="input-group-addon input_jelolo_kek"><i class="fa fa-folder-open"></i></span>
												<select class="form-control" name="csop_id" id="csop_id">
													<option value="0"> - Összes - </option>
													<?php
														$query2 = "SELECT * FROM ".$webjel."term_csoportok ORDER BY nev ASC";
														foreach ($pdo->query($query2) as $row2)
														{
															$rownum = 0;
															$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE csop_id=".$row2['id']);
															$res->execute();
															$rownum = $res->fetchColumn();
															if($rownum == 0) // nincs alkategóriája
															{
																if($row2['csop_id'] > 0)
																{
																	$query_szul = "SELECT * FROM ".$webjel."term_csoportok WHERE id=".$row2['csop_id'];
																	$res = $pdo->prepare($query_szul);
																	$res->execute();
																	$row_szul = $res -> fetch();
																	$nev = $row2['nev'].' ('.$row_szul['nev'].')';
																}
																else
																{
																	$nev = $row2['nev'];
																}
																echo '<option value="'.$row2['id'].'">'.$nev.'</option>';
															}
														}
													?>
												 </select>
											</div>
										</div>
										</form>
										<p>A letöltött Excel fájl szerkeszthető Microsoft Office-szal, és Libre Office-szal is</p>
									</div>
								</div>
								<div class="box-footer">
									<button type="submit" onClick="termekExport()" class="btn btn-primary">Export</button>
									<a type="submit" href="module-termek_export_import/termekek.xlsx" id="letolt_gomb" style="display:none;" class="btn btn-success" download>Fájl letöltése</a>
								</div>
								<div id="export_overlay" class="overlay" style="display:none;">
									<i class="fa fa-refresh fa-spin"></i>
								</div>
							</div>
							<div class="box box-success">
								<div class="box-header with-border">
									<h3 class="box-title">Importálás</h3>
									<div class="box-tools pull-right">
										<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="box-body">
									<form id="fajlfeltoltes" action="/upload-target" class="dropzone"></form>
								</div>
								<div class="box-footer no-padding" id="feltoltot_fajl_div"></div>
								<div id="import_overlay" class="overlay" style="display:none;">
									<i class="fa fa-refresh fa-spin"></i>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="box box-warning">
								<div class="box-header with-border">
									<h3 class="box-title">Kategóriák</h3>
									<div class="box-tools pull-right">
										<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="table-responsive">
									<table class="table table-hover table-bordered table-striped margbot0">
										<tbody>
											<tr>
												<tr><th>Név</th><th>id</th></tr>
											</tr>
											<?php
												$query2 = "SELECT * FROM ".$webjel."term_csoportok ORDER BY nev ASC";
												foreach ($pdo->query($query2) as $row2)
												{
													$rownum = 0;
													$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE csop_id=".$row2['id']);
													$res->execute();
													$rownum = $res->fetchColumn();
													if($rownum == 0) // nincs alkategóriája
													{
														if($row2['csop_id'] > 0)
														{
															$query_szul = "SELECT * FROM ".$webjel."term_csoportok WHERE id=".$row2['csop_id'];
															$res = $pdo->prepare($query_szul);
															$res->execute();
															$row_szul = $res -> fetch();
															$nev = $row2['nev'].' ('.$row_szul['nev'].')';
														}
														else
														{
															$nev = $row2['nev'];
														}
														echo '<tr><td>'.$nev.'</td><td>'.$row2['id'].'</td></tr>';
													}
												}
											?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="box box-warning">
								<div class="box-header with-border">
									<h3 class="box-title">Mezőnevek és formátumok</h3>
									<div class="box-tools pull-right">
										<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div class="table-responsive">
									<table class="table table-hover table-bordered table-striped margbot0">
										<tbody>
											<tr>
												<tr><th>Mezőnév</th><th>Név</th><th>Formátum</th></tr>
											</tr>
											<tr><td>id</td><td>Egyedi termék azonosító</td><td>Tilos módosítani!</td></tr>
											<tr><td>csop_id</td><td>Kategória azonosító</td><td>szám</td></tr>
											<tr><td>nev</td><td>Név</td><td>szöveg</td></tr>
											<tr><td>leiras</td><td>Leírás</td><td>szöveg</td></tr>
											<tr><td>rovid_leiras</td><td>Rövid leírás</td><td>szöveg</td></tr>
											<tr><td>ar</td><td>Ár</td><td>szám</td></tr>
											<tr><td>akciosar</td><td>Akciós ár</td><td>szám</td></tr>
											<tr><td>akcio_tol</td><td>Akció kezdete</td><td>dátum, formátuma éééé-hh-nn</td></tr>
											<tr><td>akcio_ig</td><td>Akció vége</td><td>dátum, formátuma éééé-hh-nn</td></tr>
											<tr><td>lathato</td><td>Látható-e a termék</td><td>0 = nem, 1 = igen</td></tr>
											<tr><td>kiemelt</td><td>Kiemelt-e a termék</td><td>0 = nem, 1 = igen</td></tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
		<?php
			include 'module/footer.php';
		?>
    </div>

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Slimscroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- Datepicker -->
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <!-- CK Editor -->
    <script src="plugins/ckeditor/ckeditor.js"></script>
    <!-- Dropzone -->
	<script src="scripts/dropzone.js"></script>
    <!-- Page Script -->
	<script src="scripts/termek_export_import.js"></script>
    <script>
      $(function () {
		// Dropzone
		$("#fajlfeltoltes").dropzone({
			sending: function(){
				$('#feltoltot_fajl_div').html('');
			},
			dictDefaultMessage: "Húzd ide a feltöltendő fájlt, vagy kattints a mezőbe",
			autoProcessQueue: true,
			// acceptedFiles: "application/xml",
			url: 'module-termek_export_import/termek_import.php',
			// maxFiles: 1, // Number of files at a time
			maxFilesize: 20, //in MB
			/* maxfilesexceeded: function(file)
			{
				$.confirm({
					icon: 'fa fa-warning',
					title: 'Csak egyesével',
					type: 'red',
					theme: 'light', // 'material', 'bootstrap', 'light', 'dark'
					content: 'Egyszerre csak 1 fájlt tölthetsz fel!',
					buttons: {
						ok: {
							action: function(){
								// $('#akcio_merteke').focus()
							}
						}
					}
				});
			}, */
			init: function () {
				this.on("complete", function (file) {
					setTimeout( function() {
						$('.dz-complete').remove();
					}, 1000); // feltöltés után az ikon törlése
				});
			},
			success: function(file, response){
				var valasz = response;
				// alert(response);
				$('#import_overlay').css('display', 'block');
				setTimeout(function(){ 
					$.post('module-termek_export_import/termek_export.php',{
							csop_id : $('#csop_id').val()
						},function(response,status){ 
							$('#import_overlay').css('display', 'none');
							$('#feltoltot_fajl_div').html(valasz);
							$('<span>Adatok feldolgozva.</span>').hovermessage({
									autoclose : 3000,
									position : 'top-right',
								});
					});
				}, 1000);
			}
		});
        //Enable check and uncheck all functionality
        $(".checkbox-toggle").click(function () {
          var clicks = $(this).data('clicks');
          if (clicks) {
            //Uncheck all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
            $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
          } else {
            //Check all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("check");
            $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
          }
          $(this).data("clicks", !clicks);
        });

        //Handle starring for glyphicon and font awesome
        $(".mailbox-star").click(function (e) {
          e.preventDefault();
          //detect type
          var $this = $(this).find("a > i");
          var glyph = $this.hasClass("glyphicon");
          var fa = $this.hasClass("fa");

          //Switch states
          if (glyph) {
            $this.toggleClass("glyphicon-star");
            $this.toggleClass("glyphicon-star-empty");
          }

          if (fa) {
            $this.toggleClass("fa-star");
            $this.toggleClass("fa-star-o");
          }
        });
      });
    </script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
	<?php
		include 'module/body_end.php';
	?>
  </body>
</html>
