<p><strong>ÁLTALÁNOS SZERZŐDÉSI FELTÉTELEK (ÁSZF)</strong><br />
hatályos ettől a naptól: 2019-02-18</p>

<p><br />
&nbsp;</p>

<p><strong>Preambulum</strong></p>

<p>Üdvözöljük honlapunkon! Köszönjük, hogy vásárlása során bennünket tisztel meg bizalmával!</p>

<p>Kérjük, hogy megrendelése véglegesítése előtt figyelmesen olvassa el a jelen dokumentumot, mert megrendelése véglegesítésével Ön elfogadja a jelen ÁSZF tartalmát!</p>

<p>Ha a jelen Általános Szerződési Feltételekkel, a honlap használatával, az egyes termékekkel, a vásárlás menetével kapcsolatban kérdése merült fel, vagy amennyiben egyedi igényét szeretné velünk megbeszélni, úgy kérjük, vegye fel munkatársunkkal a kapcsolatot a megadott elérhetőségeken!</p>

<p><strong>Impresszum: a Szolgáltató (Eladó, Vállalkozás) adatai</strong></p>

<p>Név: Második Lehetőség Kereskedelmi Szociális Szövetkezet</p>

<p>Székhely: 6725 Szeged, Alföldi utca 45. 1/2</p>

<p>Levelezési cím: 6722 Szeged, Szentháromság utca 50.</p>

<p>Nyilvántartásba vevő hatóság: Szegedi Törvényszék Cégbírósága</p>

<p>Cégjegyzékszám: 06-02-000686</p>

<p>Adószám: 24375821-2-06</p>

<p>Képviselő: Durszt Anikó Blanka</p>

<p>Telefonszám: 06-70/618-9013</p>

<p>E-mail: herbawin.webshop@gmail.com</p>

<p>Honlap: https://herbawin.hu</p>

<p>Bankszámlaszám:</p>

<p>Tárhelyszolgáltató adatai:</p>

<p>Név: Innovip.hu Kft.</p>

<p>Székhely: 6723 Szeged, Gát utca 7. B. lház. 3. em. 7.</p>

<p>Elérhetőség: kiss.gergely@innovip.hu</p>

<p><strong>Fogalmak</strong></p>

<p><em><strong>Felek:</strong></em> Eladó és Vevő együttesen</p>

<p><em><strong>Fogyasztó</strong></em><em>:</em> a szakmája, önálló foglalkozása vagy üzleti tevékenysége körén kívül eljáró természetes személy</p>

<p><em><strong>Fogyasztói szerződés:</strong></em> olyan szerződés, melynek egyik alanya fogyasztónak minősül</p>

<p><em><strong>Honlap</strong></em>: https://herbawin.hu weboldal, amely a szerződés megkötésére szolgáló weboldal</p>

<p><em><strong>Szerződés:</strong></em> Eladó és Vevő között a Honlap és elektronikus levelezés igénybevételével létrejövő adásvételi szerződés</p>

<p><em><strong>Távollévők közötti kommunikációt lehetővé tevő eszköz:</strong></em> olyan eszköz, amely alkalmas a felek távollétében – szerződés megkötése érdekében – szerződési nyilatkozat megtételére. Ilyen eszköz különösen a címzett vagy a címzés nélküli nyomtatvány, a szabványlevél, a sajtótermékben közzétett hirdetés megrendelőlappal, a katalógus, a telefon, a telefax és az internetes hozzáférést biztosító eszköz.</p>

<p><em><strong>Távollévők között kötött szerződés:</strong></em> olyan fogyasztói szerződés, amelyet a szerződés szerinti termék szervezett távértékesítési rendszer keretében a felek egyidejű fizikai jelenléte nélkül úgy kötnek meg, hogy a szerződés megkötése érdekében a szerződő felek kizárólag távollévők közötti kommunikációt lehetővé tévő eszközt alkalmaznak</p>

<p><em><strong>Termék:</strong></em> a Honlap kínálatában szereplő, a Honlapon értékesítésre szánt minden birtokba vehető forgalomképes ingó dolog, mely a Szerződés tárgyát képezi</p>

<p><em><strong>Vállalkozás:</strong></em> a szakmája, önálló foglalkozása vagy üzleti tevékenysége körében eljáró személy</p>

<p><em><strong>Vevő/Ön:</strong></em> a Honlapon keresztül vételi ajánlatot tevő szerződést kötő személy</p>

<p><em><strong>Jótállás:</strong></em> A fogyasztó és a vállalkozás között kötött szerződések esetén (a továbbiakban: fogyasztói szerződés) a Polgári Törvénykönyv szerinti,</p>

<p>1. a) a szerződés teljesítéséért vállalt jótállás, amelyet a vállalkozás a szerződés megfelelő teljesítéséért a jogszabályi kötelezettségén túlmenően vagy annak hiányában önként vállal, valamint</p>

<p>2. b) a jogszabályon alapuló kötelező jótállás</p>

<p><strong>Vonatkozó jogszabályok</strong></p>

<p>A Szerződésre a magyar jog előírásai az irányadóak, és különösen az alábbi jogszabályok vonatkoznak:</p>

<p>1997. évi CLV. törvény a fogyasztóvédelemről</p>

<p>2001. évi CVIII. törvény az elektronikus kereskedelmi szolgáltatások, valamint az információs társadalommal összefüggő szolgáltatások egyes kérdéseiről</p>

<p>2013. évi V. törvény a Polgári Törvénykönyvről</p>

<p>151/2003. (IX.22.) kormányrendelet a tartós fogyasztási cikkekre vonatkozó kötelező jótállásról</p>

<p>45/2014. (II.26.) kormányrendelet a fogyasztó és a vállalkozás közötti szerződések részletes szabályairól</p>

<p>19/2014. (IV.29.) NGM rendelet a fogyasztó és vállalkozás közötti szerződés keretében eladott dolgokra vonatkozó szavatossági és jótállási igények intézésének eljárási szabályairól</p>

<p>1997. évi LXXVI. törvény a szerzői jogról</p>

<p>2011. évi CXX. törvény az információs önrendelkezési jogról és az információszabadságról</p>

<p>AZ EURÓPAI PARLAMENT ÉS A TANÁCS (EU) 2018/302 RENDELETE (2018. február 28.) a belső piacon belül a vevő állampolgársága, lakóhelye vagy letelepedési helye alapján történő indokolatlan területi alapú tartalomkorlátozással és a megkülönböztetés egyéb formáival szembeni fellépésről, valamint a 2006/2004/EK és az (EU) 2017/2394 rendelet, továbbá a 2009/22/EK irányelv módosításáról</p>

<p>AZ EURÓPAI PARLAMENT ÉS A TANÁCS (EU) 2016/679 RENDELETE (2016. április 27.) a természetes személyeknek a személyes adatok kezelése tekintetében történő védelméről és az ilyen adatok szabad áramlásáról, valamint a 95/46/EK rendelet hatályon kívül helyezéséről (általános adatvédelmi rendelet)</p>

<p><strong>Az ÁSZF hatálya, elfogadása</strong></p>

<p>A közöttünk létrejövő szerződés tartalmát – a vonatkozó kötelező érvényű jogszabályok rendelkezései mellett – a jelen Általános Szerződési Feltételek (a továbbiakban: ÁSZF) határozzák meg. Ennek megfelelően tartalmazza a jelen ÁSZF az Önt és bennünket illető jogokat és kötelezettségeket, a szerződés létrejöttének feltételeit, a teljesítési határidőket, a szállítási és fizetési feltételeket, a felelősségi szabályokat, valamint az elállási jog gyakorlásának feltételeit.</p>

<p>A Honlap használatához szükséges azon technikai tájékoztatást, melyet jelen ÁSZF nem tartalmaz, a Honlapon elérhető egyéb tájékoztatások nyújtják.</p>

<p>Ön a megrendelése véglegesítése előtt köteles megismerni a jelen ÁSZF rendelkezéseit. A webáruházunkon keresztül történő vásárlással Ön elfogadja a jelen ÁSZF rendelkezéseit, és az ÁSZF maradéktalanul az Ön és az Eladó között létrejövő szerződés részét képezi.</p>

<p><strong>A szerződés nyelve, a szerződés formája</strong></p>

<p>A jelen ÁSZF hatálya alá tartozó szerződések nyelve a magyar nyelv.</p>

<p>A jelen ÁSZF hatálya alá tartozó szerződések nem minősülnek írásba foglalt szerződéseknek, azokat az Eladó nem iktatja.</p>

<p><strong>Árak</strong></p>

<p>Az árak forintban értendők, tartalmazzák a 27%-os ÁFÁ-t. Az árak tájékoztató jellegűek, az árváltozás jogát fenntartjuk.</p>

<p><strong>Panaszügyintézés és jogérvényesítési lehetőségek</strong></p>

<p>A fogyasztó a termékkel vagy az Eladó tevékenységével kapcsolatos fogyasztói kifogásait az alábbi elérhetőségeken terjesztheti elő:</p>

<p>Telefon: 06-70/618-9013</p>

<p>Internet cím: https://herbawin.hu</p>

<p>E-mail: herbawin.webshop@gmail.com</p>

<p>A fogyasztó szóban vagy írásban közölheti a vállalkozással a panaszát, amely a vállalkozásnak, illetve a vállalkozás érdekében vagy javára eljáró személynek az áru fogyasztók részére történő forgalmazásával, illetve értékesítésével közvetlen kapcsolatban álló magatartására, tevékenységére vagy mulasztására vonatkozik.</p>

<p>A szóbeli panaszt a vállalkozás köteles azonnal megvizsgálni, és szükség szerint orvosolni. Ha a fogyasztó a panasz kezelésével nem ért egyet, vagy a panasz azonnali kivizsgálása nem lehetséges, a vállalkozás a panaszról és az azzal kapcsolatos álláspontjáról haladéktalanul köteles jegyzőkönyvet felvenni, és annak egy másolati példányát a fogyasztónak átadni. Telefonon vagy egyéb elektronikus hírközlési szolgáltatás felhasználásával közölt szóbeli panasz esetén a fogyasztónak legkésőbb 30 napon belül - az írásbeli panaszra adott válaszra vonatkozó előírásoknak megfelelően - az érdemi válasszal egyidejűleg megküldeni. Egyebekben pedig az írásbeli panaszra vonatkozóan az alábbiak szerint köteles eljárni. Az írásbeli panaszt a vállalkozás - ha az Európai Unió közvetlenül alkalmazandó jogi aktusa eltérően nem rendelkezik - a beérkezését követően harminc napon belül köteles írásban érdemben megválaszolni és intézkedni annak közlése iránt. Ennél rövidebb határidőt jogszabály, hosszabb határidőt törvény állapíthat meg. A panaszt elutasító álláspontját a vállalkozás indokolni köteles. A telefonon vagy elektronikus hírközlési szolgáltatás felhasználásával közölt szóbeli panaszt a vállalkozás köteles egyedi azonosítószámmal ellátni.</p>

<p>A panaszról felvett jegyzőkönyvnek tartalmaznia kell az alábbiakat:</p>

<p>1. a fogyasztó neve, lakcíme,</p>

<p>2. a panasz előterjesztésének helye, ideje, módja,</p>

<p>3. a fogyasztó panaszának részletes leírása, a fogyasztó által bemutatott iratok, dokumentumok és egyéb bizonyítékok jegyzéke,</p>

<p>4. a vállalkozás nyilatkozata a fogyasztó panaszával kapcsolatos álláspontjáról, amennyiben a panasz azonnali kivizsgálása lehetséges,</p>

<p>5. a jegyzőkönyvet felvevő személy és - telefonon vagy egyéb elektronikus hírközlési szolgáltatás felhasználásával közölt szóbeli panasz kivételével - a fogyasztó aláírása,</p>

<p>6. a jegyzőkönyv felvételének helye, ideje,</p>

<p>7. telefonon vagy egyéb elektronikus hírközlési szolgáltatás felhasználásával közölt szóbeli panasz esetén a panasz egyedi azonosítószáma.</p>

<p>A vállalkozás a panaszról felvett jegyzőkönyvet és a válasz másolati példányát öt évig köteles megőrizni, és azt az ellenőrző hatóságoknak kérésükre bemutatni.</p>

<p>A panasz elutasítása esetén a vállalkozás köteles a fogyasztót írásban tájékoztatni arról, hogy panaszával - annak jellege szerint - mely hatóság vagy békéltető testület eljárását kezdeményezheti. A tájékoztatásnak tartalmaznia kell továbbá az illetékes hatóság, illetve a fogyasztó lakóhelye vagy tartózkodási helye szerinti békéltető testület székhelyét, telefonos és internetes elérhetőségét, valamint levelezési címét. A tájékoztatásnak arra is ki kell terjednie, hogy a vállalkozás a fogyasztói jogvita rendezése érdekében igénybe veszi-e a békéltető testületi eljárást.</p>

<p>Amennyiben az Eladó és a fogyasztó között esetlegesen fennálló fogyasztói jogvita a tárgyalások során nem rendeződik, az alábbi jogérvényesítési lehetőségek állnak nyitva a fogyasztó számára:</p>

<p>Panasztétel a fogyasztóvédelmi hatóságoknál. Amennyiben a fogyasztó fogyasztói jogainak megsértését észleli, jogosult panasszal fordulni a lakóhelye szerint illetékes fogyasztóvédelmi hatósághoz. A panasz elbírálását követően a hatóság dönt a fogyasztóvédelmi eljárás lefolytatásáról. A fogyasztóvédelmi elsőfokú hatósági feladatokat a fogyasztó lakóhelye szerint illetékes járási hivatalok látják el, ezek listája itt található: http://jarasinfo.gov.hu/</p>

<p>Bírósági eljárás. Ügyfél jogosult a fogyasztói jogvitából származó követelésének bíróság előtti érvényesítésére polgári eljárás keretében a Polgári Törvénykönyvről szóló 2013. évi V. törvény, valamint a Polgári Perrendtartásról szóló 2016. évi CXXX. törvény rendelkezései szerint. Tájékoztatjuk, hogy Ön velünk szemben fogyasztói panasszal élhet. Amennyiben az Ön fogyasztói panaszát elutasítjuk, úgy Ön jogosult az Ön lakóhelye vagy tartózkodási helye szerint illetékes Békéltető Testülethez is fordulni: a békéltető testület eljárása megindításának feltétele, hogy a fogyasztó az érintett vállalkozással közvetlenül megkísérelje a vitás ügy rendezését. Az eljárásra - a fogyasztó erre irányuló kérelme alapján - az illetékes testület helyett a fogyasztó kérelmében megjelölt békéltető testület illetékes.</p>

<p>A vállalkozást a békéltető testületi eljárásban együttműködési kötelezettség terheli. Ennek keretében fennáll a vállalkozásoknak a békéltető testület felhívására történő válaszirat megküldési kötelezettsége, továbbá kötelezettségként kerül rögzítésre a békéltető testület előtti megjelenési kötelezettség („meghallgatáson egyezség létrehozatalára feljogosított személy részvételének biztosítása”). Amennyiben a vállalkozás székhelye vagy telephelye nem a területileg illetékes békéltető testületet működtető kamara szerinti megyébe van bejegyezve, a vállalkozás együttműködési kötelezettsége a fogyasztó igényének megfelelő írásbeli egyezségkötés lehetőségének felajánlására terjed ki.</p>

<p>A fenti együttműködési kötelezettség megszegése esetén a fogyasztóvédelmi hatóság rendelkezik hatáskörrel, amely alapján a jogszabályváltozás következtében a vállalkozások jogsértő magatartása esetén kötelező bírságkiszabás alkalmazandó, bírságtól való eltekintésre nincs lehetőség. A fogyasztóvédelemről szóló törvény mellett módosításra került a kis- és középvállalkozásokról szóló törvény vonatkozó rendelkezése is, így a kis- és középvállalkozások esetén sem mellőzhető majd a bírság kiszabása.</p>

<p>A bírság mértéke kis- és középvállalkozások esetén 15 ezer forinttól 500 ezer forintig terjedhet, míg a számviteli törvény hatálya alá tartozó, 100 millió forintot meghaladó éves nettó árbevétellel rendelkező, nem kis- és középvállalkozás esetén 15 ezer forinttól, a vállalkozás éves nettó árbevételének 5%-áig, de legfeljebb 500 millió forintig terjedhet. A kötelező bírság bevezetésével a jogalkotó a békéltető testületekkel való együttműködés nyomatékosítását, illetve a vállalkozásoknak a békéltető testületi eljárásban való aktív részvételének biztosítását célozza.</p>

<p>A békéltető testület hatáskörébe tartozik a fogyasztói jogvita bírósági eljáráson kívüli rendezése. A békéltető testület feladata, hogy megkísérelje a fogyasztói jogvita rendezése céljából egyezség létrehozását a felek között, ennek eredménytelensége esetén az ügyben döntést hoz a fogyasztói jogok egyszerű, gyors, hatékony és költségkímélő érvényesítésének biztosítása érdekében. A békéltető testület a fogyasztó vagy a vállalkozás kérésére tanácsot ad a fogyasztót megillető jogokkal és a fogyasztót terhelő kötelezettségekkel kapcsolatban.</p>

<p>A békéltető testület eljárása a fogyasztó kérelmére indul. A kérelmet a békéltető testület elnökéhez kell írásban benyújtani: az írásbeliség követelményének levél, távirat, távgépíró vagy telefax útján, továbbá bármely egyéb olyan eszközzel is eleget lehet tenni, amely a címzett számára lehetővé teszi a neki címzett adatoknak az adat céljának megfelelő ideig történő tartós tárolását, és a tárolt adatok változatlan formában és tartalommal történő megjelenítését.</p>

<p>A kérelemnek tartalmaznia kell</p>

<p>a. a fogyasztó nevét, lakóhelyét vagy tartózkodási helyét,</p>

<p>b. a fogyasztói jogvitával érintett vállalkozás nevét, székhelyét vagy érintett telephelyét,</p>

<p>c. ha a fogyasztó az illetékességet az illetékes békéltető testület helyett kérelmezett testület megjelölését,</p>

<p>d. a fogyasztó álláspontjának rövid leírását, az azt alátámasztó tényeket és azok bizonyítékait,</p>

<p>e. a fogyasztó nyilatkozatát arról, hogy a fogyasztó az érintett vállalkozással közvetlenül megkísérelte a vitás ügy rendezését</p>

<p>f. a fogyasztó nyilatkozatát arra nézve, hogy az ügyben más békéltető testület eljárását nem kezdeményezte, közvetítői eljárás nem indult, keresetlevél beadására, illetve fizetési meghagyás kibocsátása iránti kérelemelőterjesztésére nem került sor</p>

<p>g. a testület döntésére irányuló indítványt,</p>

<p>h. a fogyasztó aláírását.</p>

<p>A kérelemhez csatolni kell azt az okiratot, illetve annak másolatát (kivonatát), amelynek tartalmára a fogyasztó bizonyítékként hivatkozik, így különösen a vállalkozás írásbeli nyilatkozatát a panasz elutasításáról, ennek hiányában a fogyasztó rendelkezésére álló egyéb írásos bizonyítékot az előírt egyeztetés megkísérléséről. Ha a fogyasztó meghatalmazott útján jár el, a kérelemhez csatolni kell a meghatalmazást.</p>

<p>A Békéltető Testületekről bővebb információ itt érhető el: http://www.bekeltetes.hu</p>

<p>A területileg illetékes Békéltető Testületekről bővebb információ itt érhető el:</p>

<p>http://www.bekeltetes.hu/index.php?id=testuletek</p>

<p><em><strong>Az egyes területileg illetékes Békéltető Testületek elérhetőségei:</strong></em></p>

<p>Baranya Megyei Békéltető Testület</p>

<p>Cím: 7625 Pécs, Majorossy I. u. 36.</p>

<p>Telefonszám: 06-72-507-154</p>

<p>Fax: 06-72-507-152</p>

<p>E-mail: abeck@pbkik.hu; <a href="mailto:mbonyar@pbkik.hu">mbonyar@pbkik.hu</a></p>

<p><br />
&nbsp;</p>

<p>Bács-Kiskun Megyei Békéltető Testület</p>

<p>Cím: 6000 Kecskemét, Árpád krt. 4.</p>

<p>Telefonszám: 06-76-501-500; 06-76-501-525, 06-76-501-523</p>

<p>Fax: 06-76-501-538</p>

<p>E-mail: bekeltetes@bacsbekeltetes.hu; mariann.matyus@bkmkik.hu</p>

<p>Honlap: <a href="http://www.bacsbekeltetes.hu/">www.bacsbekeltetes.hu</a></p>

<p><br />
&nbsp;</p>

<p>Békés Megyei Békéltető Testület</p>

<p>Cím: 5600 Békéscsaba, Penza ltp. 5.</p>

<p>Telefonszám: 06-66-324-976</p>

<p>Fax: 06-66-324-976</p>

<p>E-mail: eva.toth@bmkik.hu</p>

<p>Borsod-Abaúj-Zemplén Megyei Békéltető Testület</p>

<p>Cím: 3525 Miskolc, Szentpáli u. 1.</p>

<p>Telefonszám:06-46-501-091;06-46-501-870</p>

<p>Fax: 06-46-501-099</p>

<p>E-mail: <a href="mailto:kalna.zsuzsa@bokik.hu">kalna.zsuzsa@bokik.hu</a></p>

<p><br />
&nbsp;</p>

<p>Budapesti Békéltető Testület</p>

<p>Cím: 1016 Budapest, Krisztina krt. 99. III. em. 310.</p>

<p>Telefonszám: 06-1-488-2131</p>

<p>Fax: 06-1-488-2186</p>

<p>E-mail: <a href="mailto:bekelteto.testulet@bkik.hu">bekelteto.testulet@bkik.hu</a></p>

<p><br />
&nbsp;</p>

<p>Csongrád Megyei Békéltető Testület</p>

<p>Cím: 6721 Szeged, Párizsi krt. 8-12.</p>

<p>Telefonszám: 06-62-554-250/118</p>

<p>Fax: 06-62-426-149</p>

<p>E-mail: <a href="mailto:bekelteto.testulet@csmkik.hu">bekelteto.testulet@csmkik.hu</a></p>

<p><br />
&nbsp;</p>

<p>Fejér Megyei Békéltető Testület</p>

<p>Cím: 8000 Székesfehérvár, Hosszúsétatér 4-6.</p>

<p>Telefonszám:06-22-510-310</p>

<p>Fax: 06-22-510-312</p>

<p>E-mail: <a href="mailto:fmkik@fmkik.hu">fmkik@fmkik.hu</a></p>

<p><br />
&nbsp;</p>

<p>Győr-Moson-Sopron Megyei Békéltető Testület</p>

<p>Cím: 9021 Győr, Szent István út 10/a.</p>

<p>Telefonszám: 06-96-520-217</p>

<p>Fax: 06-96-520-218</p>

<p>E-mail: <a href="mailto:bekeltetotestulet@gymskik.hu">bekeltetotestulet@gymskik.hu</a></p>

<p><br />
&nbsp;</p>

<p>Hajdú-Bihar Megyei Békéltető Testület</p>

<p>Cím: 4025 Debrecen, Vörösmarty u. 13-15.</p>

<p>Telefonszám: 06-52-500-710</p>

<p>Fax: 06-52-500-720</p>

<p>E-mail: <a href="mailto:korosi.vanda@hbkik.hu">korosi.vanda@hbkik.hu</a></p>

<p><br />
&nbsp;</p>

<p>Heves Megyei Békéltető Testület</p>

<p>Cím: 3300 Eger, Faiskola út 15.</p>

<p>Telefonszám: 06-36-429-612</p>

<p>Fax: 06-36-323-615</p>

<p>E-mail: <a href="mailto:hkik@hkik.hu">hkik@hkik.hu</a></p>

<p><br />
&nbsp;</p>

<p>Jász-Nagykun-Szolnok Megyei Békéltető Testület</p>

<p>Cím: 5000 Szolnok, Verseghy park 8. III. emelet 305-306.</p>

<p>Telefonszám: 06-56-510-621, 06-20-373-2570</p>

<p>Fax: 06-56-510-628</p>

<p>E-mail: <a href="mailto:bekeltetotestulet@jnszmkik.hu">bekeltetotestulet@jnszmkik.hu</a></p>

<p><br />
&nbsp;</p>

<p>Komárom-Esztergom Megyei Békéltető Testület</p>

<p>Cím: 2800 Tatabánya, Fő tér 36.</p>

<p>Telefonszám: 06-34-513-027</p>

<p>Fax: 06-34-316-259</p>

<p>E-mail: <a href="mailto:szilvi@kemkik.hu">szilvi@kemkik.hu</a></p>

<p><br />
&nbsp;</p>

<p>Nógrád Megyei Békéltető Testület</p>

<p>Cím: 3100 Salgótarján, Alkotmány út 9/A.</p>

<p>Telefonszám: 06-32-520-860</p>

<p>Fax: 06-32-520-862</p>

<p>E-mail: <a href="mailto:nkik@nkik.hu">nkik@nkik.hu</a></p>

<p><br />
&nbsp;</p>

<p>Pest Megyei Békéltető Testület</p>

<p>Cím: 1119 Budapest, Etele út 59-61. II. emelet 240.</p>

<p>Levelezési cím: 1364 Budapest, Pf.: 81</p>

<p>Telefonszám: 06-1-269-0703</p>

<p>Fax: 06-1-474-7921</p>

<p>E-mail: <a href="mailto:pmbekelteto@pmkik.hu">pmbekelteto@pmkik.hu</a></p>

<p><br />
&nbsp;</p>

<p>Somogy Megyei Békéltető Testület</p>

<p>Cím: 7400 Kaposvár, Anna u.6.</p>

<p>Telefonszám: 06-82-501-026</p>

<p>Fax: 06-82-501-046</p>

<p>E-mail: <a href="mailto:skik@skik.hu">skik@skik.hu</a></p>

<p><br />
&nbsp;</p>

<p>Szabolcs-Szatmár-Bereg Megyei Békéltető Testület</p>

<p>Cím: 4400 Nyíregyháza, Széchenyi u. 2.</p>

<p>Telefonszám: 06-42-311-544</p>

<p>Fax: 06-42-311-750</p>

<p>E-mail: <a href="mailto:bekelteto@szabkam.hu">bekelteto@szabkam.hu</a></p>

<p><br />
&nbsp;</p>

<p>Tolna Megyei Békéltető Testület</p>

<p>Cím: 7100 Szekszárd, Arany J. u. 23-25. III. emelet</p>

<p>Telefonszám: 06-74-411-661</p>

<p>Fax: 06-74-411-456</p>

<p>E-mail: <a href="mailto:kamara@tmkik.hu">kamara@tmkik.hu</a></p>

<p><br />
&nbsp;</p>

<p>Vas Megyei Békéltető Testület</p>

<p>Cím: 9700 Szombathely, Honvéd tér 2.</p>

<p>Telefonszám: 06-94-312-356</p>

<p>Fax: 06-94-316-936</p>

<p>E-mail: <a href="mailto:vmkik@vmkik.hu">vmkik@vmkik.hu</a></p>

<p><br />
&nbsp;</p>

<p>Veszprém Megyei Békéltető Testület</p>

<p>Cím: 8200 Veszprém, Radnóti tér 1. földszint 116.</p>

<p>Telefonszám: 06-88-429-008</p>

<p>Fax: 06-88-412-150</p>

<p>E-mail: <a href="mailto:bekelteto@veszpremikamara.hu">bekelteto@veszpremikamara.hu</a></p>

<p><br />
&nbsp;</p>

<p>Zala Megyei Békéltető Testület</p>

<p>Cím: 8900 Zalaegerszeg, Petőfi u. 24.</p>

<p>Telefonszám: 06-92-550-513</p>

<p>Fax: 06-92-550-525</p>

<p>E-mail: <a href="mailto:zmbekelteto@zmkik.hu">zmbekelteto@zmkik.hu</a></p>

<p><br />
&nbsp;</p>

<p><strong>Online vitarendezési platform</strong></p>

<p>Az Európai Bizottság létrehozott egy honlapot, amelybe a fogyasztók beregisztrálhatnak, így ezen keresztül lehetőségük nyílik arra, hogy online vásárláshoz kapcsolódó jogvitáikat ezen keresztül rendezzék egy kérelem kitöltésével, elkerülve a bírósági eljárást. Így a fogyasztók tudják érvényesíteni jogaikat anélkül, hogy például a távolság meggátolná őket ebben.</p>

<p>Ha Ön panaszt kíván tenni egy, az interneten vásárolt termékkel kapcsolatban, és nem akar feltétlenül bírósághoz fordulni, igénybe veheti az online vitarendezés eszközét.</p>

<p>A portálon Ön és a kereskedő, akivel szemben panasszal élt, közösen kiválaszthatják a panasz kezelésével megbízni kívánt vitarendezési testületet.</p>

<p>Az online vitarendezési platform itt érhető el:</p>

<p>https://webgate.ec.europa.eu/odr/main/?event=main.home.show&amp;lng=HU</p>

<p><strong>Szerzői jogok</strong></p>

<p>A szerzői jogról szóló 1999. évi LXXVI. törvény (továbbiakban: Szjt.) 1. § (1) bekezdése értelmében a weboldal szerzői műnek minősül, így annak minden része szerzői jogi védelem alatt áll. Az Szjt. 16. § (1) bekezdése alapján tilos a weboldalon található grafikai és szoftveres megoldások, számítógépi programalkotások engedély nélküli felhasználása, illetve bármely olyan alkalmazás használata, amellyel a weboldal, vagy annak bármely része módosítható. A weboldalról és annak adatbázisából bármilyen anyagot átvenni a jogtulajdonos írásos hozzájárulása esetén is csak a weboldalra való hivatkozással, forrás feltüntetésével lehet. A jogtulajdonos: Második Lehetőség Kereskedelmi Szociális Szövetkezet</p>

<p><strong>Részleges érvénytelenség, magatartási kódex</strong></p>

<p>Ha az ÁSZF valamely pontja jogilag hiányos vagy hatálytalan, attól még a szerződés további pontjai érvényben maradnak és a hatálytalan vagy hibás rész helyett a vonatkozó jogszabályok rendelkezései alkalmazandóak.</p>

<p>Az Eladónak nincs a fogyasztókkal szembeni tisztességtelen kereskedelmi gyakorlat tilalmáról szóló törvény szerinti magatartási kódexe.</p>

<p><strong>A digitális adattartalom működése, műszaki védelmi intézkedések</strong></p>

<p>A weboldalon megjelenő adatokat szolgáltató szerverek elérhetősége évi 99,9% feletti. Rendszeresen mentés készül a teljes adattartalomról, így probléma esetén az eredeti adattartalom visszaállítható. A weboldalon megjelenő adatokat MSSQL és MySQL adatbázisban tároljuk. Az érzékeny adatok megfelelő erősségű titkosítással vannak tárolva, kódolásukhoz processzorba épített hardveres támogatást használunk.</p>

<p><strong>A termékek lényeges tulajdonságaira vonatkozó tájékoztatás</strong></p>

<p>A honlapon a megvásárolható termékek lényeges tulajdonságairól az egyes termékeknél szereplő leírásokban adunk tájékoztatást. A termékoldalon található adatok tájékoztató jellegűek! A képek esetenként illusztrációk, a színek nem mindig felelnek meg a valóságnak!</p>

<p><strong>Az adatbeviteli hibák javítása - Felelősség a megadott adatok valóságáért</strong></p>

<p>Önnek a megrendelés során a megrendelés véglegesítése előtt folyamatosan lehetősége van az Ön által bevitt adatok módosítására (a böngészőben a vissza gombra kattintva az előző oldal nyílik meg, így akkor is javíthatóak a bevitt adatok, ha Ön már a következő oldalra lépett). Felhívjuk a figyelmét, hogy az Ön felelőssége, hogy az Ön által megadott adatok pontosan kerüljenek bevitelre, hiszen az Ön által megadott adatok alapján kerül számlázásra, illetve szállításra a termék. Ön a megrendelésével tudomásul veszi, hogy az Eladó jogosult az Ön hibás adatbeviteléből, a pontatlanul megadott adatokból eredő minden kárát és költségét Önre hárítani. Az Eladó a pontatlan adatbevitel alapján történő teljesítésért felelősségét kizárja.</p>

<p>Felhívjuk a figyelmét arra, hogy a rosszul megadott e-mail cím vagy a postafiókhoz tartozó tárhely telítettsége a visszaigazolás kézbesítésének hiányát eredményezheti és meggátolhatja a szerződés létrejöttét.</p>

<p><strong>Eljárás hibás ár esetén</strong></p>

<p>Eladó nem vállal felelősséget a gondossága ellenére és/vagy az informatikai rendszer hibájából eredően nyilvánvalóan hibásan feltüntetett árért.</p>

<p>Nyilvánvalóan hibásan feltüntetett árnak minősül különösen:</p>

<ul>
	<li>
	<p>0 Ft-os ár,</p>
	</li>
	<li>
	<p>kedvezménnyel csökkentett, de a kedvezményt tévesen feltüntető ár (pl.: 1000 Ft-os termék esetén a 20 %-os kedvezmény feltüntetése mellett 500 Ft-ért kínált termék),</p>
	</li>
	<li>
	<p>az olyan esetek, amikor nyilvánvaló, hogy a Termék nem az igazolható piaci áron került feltüntetésre és az Eladó nem kedvezménnyel értékesíti a (pl.: nyilvánvalóan elírt ár: 10 000 Ft helyett 1000 Ft-ért feltüntetett ár).</p>
	</li>
</ul>

<p>Igazolt piaci árnak az árukereső oldalak szerinti átlagár tekintendő.</p>

<p>Hibás ár feltüntetése esetén Eladó felajánlja a termék valós áron történő megvásárlásának lehetőségét, mely információ birtokában a Vásárló eldöntheti, hogy megrendeli valós áron a terméket vagy minden hátrányos jogkövetkezmény nélkül lemondja a megrendelést.</p>

<p><strong>Telefonos megrendelés</strong></p>

<p>A Szolgáltató telefonon is fogad megrendeléseket H-P 08:00-16:00 óráig. Az ügyintéző kizárólag a megrendelést veszi fel, kérdésekre válaszolni nem tud. A telefonos megrendelést a +36 70/618-9013 telefonszámon tudja leadni a Vásárló. Ha a Vásárlónak panasza vagy egyéb kérdése lenne, akkor az Impresszumban megjelölt telefonszámon vagy e-mail címen tudja felvenni a Szolgáltatóval a kapcsolatot.</p>

<p><br />
&nbsp;</p>

<p><strong>A honlap használata</strong></p>

<p>A honlap termékbemutatási, és online megrendelési lehetőséget biztosít a Felhasználók számára. A honlapon a Felhasználó menüpontok segítségével böngészhet. A termékeink kategóriarendszerbe sorolva találhatók meg.</p>

<p>Az Aktualitások menüpontban találhatók érdekességek, hírek, termékekkel kapcsolatos adatok. A kategória nevére kattintva a benne szereplő termékek listája látható. A termék listáról a részletes termék oldal a termék nevére klikkelve érhető el, itt tájékozódhat a megrendelni kívánt termék részletes jellemzőiről, áráról.</p>

<p>A honlapon lehetőség van kulcsszó alapján terméket keresni. A keresési feltételeknek megfelelő termék találatok a kategóriákhoz hasonlóan listaszerűen jelennek meg.</p>

<p>A választott termék a kosárba gomb segítségével helyezhető a kosárba, a gomb felett a szükséges darabszám beállítható. A Felhasználó a kosár tartalmát a Kosár menüpont segítségével ellenőrizheti. Itt módosíthatja azt, hogy a kosárba tett termékből milyen mennyiséget kíván rendelni, illetve törölheti az adott tételt. A Kosár ürítése gomb segítségével lehetőség van a kosár teljes kiürítésére is.</p>

<p>A Felhasználó a Vásárlás folytatása gombra klikkelve folytathatja a vásárlási folyamatot. Második lépésként lehetőség van belépésre, regisztrációra, valamint regisztráció nélküli vásárlásra. A regisztráció, és a regisztráció nélküli vásárlás esetén az alábbi adatokat kell a Felhasználónak megadnia: e-mail cím, név, telefonszám, számlázási cím, valamint ha ettől eltérő a szállítási cím. Regisztrációhoz az előbbi adatokon felül egy jelszó megadása is szükséges. A sikeres regisztrációról a Felhasználó e-mailben, és a honlapon tájékozódhat. A Felhasználó a regisztrációjának törlését e-mailben kérheti a Szolgáltatótól.</p>

<p>A hozzáférési adatok titokban tartásáért a Felhasználó felelős. A Felhasználó felelős az adatai frissítéséért, valamint köteles a Szolgáltatónak bejelenteni, ha tudomására jutott, hogy az adataival harmadik fél visszaélt. Elfelejtett jelszó esetén, a honlapon új jelszó kérhető a regisztrált e-mail címre. Amennyiben a Felhasználó korábban regisztrált a honlapon, a megrendelési folyamat az e-mail címének és jelszavának megadásával folytatható.</p>

<p>A megrendelés következő lépéseként a Felhasználónak ki kell választania a számára megfelelő fizetési és szállítási módot. A Felhasználó a „Profil” oldal segítségével ellenőrizheti minden korábban megadott adatát, és a megrendelni kívánt termékeket, azok mennyiségét.</p>

<p>Adatbeviteli hibák esetén a profil oldal segítségével javíthat a megadott adatokon.</p>

<p>Ha mindent megfelelőnek talál, akkor a Rendelés leadása gomb segítségével véglegesítheti rendelését. Erről a honlapon, illetve e-mailben kap megerősítést. Amennyiben a megrendelés rögzítése után (pl.: a visszaigazoló e-mailben) észlel hibás adatot, azt haladéktalanul, de legfeljebb 24 órán belül köteles jelezni azt a Szolgáltató felé. Megrendelési szándéktól függetlenül a Felhasználó a belépést a Belépés menüpont segítségével végezheti el. Belépés után a Profil menüpont alatt tudja módosítani a regisztráció során megadott adatait, valamint a leadott megrendelésének adatait, és állapotát követheti nyomon.</p>

<p><br />
&nbsp;</p>

<p><strong>A rendelés véglegesítése </strong></p>

<p>Amennyiben Ön meggyőződött arról, hogy a kosár tartalma megfelel az Ön által megrendelni kívánt termékeknek, valamint az Ön adatai helyesen szerepelnek, úgy a „Rendelés leadása” gombra kattintva zárhatja le megrendelését. A honlapon közölt információk nem minősülnek az Eladó részéről szerződés megkötésére vonatkozó ajánlatnak. A jelen ÁSZF hatálya alá tartozó megrendelések esetén Ön minősül ajánlattevőnek, és a szerződés az Ön által a honlapon keresztül tett ajánlatnak az Eladó által történő elfogadásával jön létre a jelen ÁSZF rendelkezései szerint</p>

<p>Ön a „Rendelés leadása!” gomb megnyomásával kifejezetten tudomásul veszi, hogy ajánlatát megtettnek kell tekinteni, és nyilatkozata – az Eladó jelen ÁSZF szerinti visszaigazolása esetén - fizetési kötelezettséget von maga után.</p>

<p><strong>Rendelés feldolgozása</strong></p>

<p>A rendelések feldolgozása két lépcsőben történik. Önnek bármilyen időpontban lehetősége van a megrendelés leadására. A rendelésről először egy automata visszajelzést kap, ami csak annak tényét rögzíti, hogy az Ön megrendelése a honlapon keresztül megérkezett. Amennyiben Ön azt észleli, hogy az automata visszaigazoló e-mail értesítés az Ön adatait tévesen tartalmazza (pl. név, szállítási cím, telefonszám, stb.), úgy Ön köteles ennek tényét – a helyes adatok megadásával egyidejűleg – velünk e-mail (herbawin.webshop@gmail.com) útján haladéktalanul közölni. Amennyiben Ön a megrendelésétől számított 24 órán belül nem kapja meg az automata visszaigazoló e-mailt, úgy kérjük, vegye fel velünk a kapcsolatot, mert elképzelhető, hogy megrendelése technikai okok miatt nem érkezett meg rendszerünkbe.</p>

<p><strong>Fizetési módok</strong></p>

<p><em><strong>Bankkártyás fizetés</strong></em></p>

<p>Webáruházunkban a SimplePay - a teljesen hazai fejlesztésű, bankoktól független – bankkártya elfogadó rendszerét használjuk.</p>

<p><em><strong>Utánvétel</strong></em></p>

<p>Amennyiben a rendelés értékét a csomag kézhezvételekor kívánja kiegyenlíteni, akkor válassza az "Utánvétel" fizetési módot. Utánvétes fizetésnek minősül a GLS átvevőponton történő bankkártyás fizetés is. Az utánvétes fizetési módnak költség vonzata van. E fizetési mód választása esetén - a rendelés végösszegétől függetlenül – a honlapon feltüntetett aktuális utánvételi díjat vagyunk kénytelenek felszámítani.</p>

<p><strong>Átvételi módok, átvételi díjak</strong></p>

<p><em><strong>GLS futárszolgálat</strong></em></p>

<p>Rendelését a GLS futárszolgálat szállítja ki. A kiszállítás napjáról e-mailben tájékoztatjuk.</p>

<p><strong>Teljesítési határidő</strong></p>

<p>A megrendelésre vonatkozóan az általános teljesítési határidő a rendelés visszaigazolásától számított legfeljebb 30 nap. Ez a szállítási határidő tájékoztató jellegű, az ettől történő eltérést minden esetben e-mail útján jelezzük.</p>

<p><strong>Jogfenntartás, tulajdonjogi kikötés</strong></p>

<p>Amennyiben Ön korábban már úgy rendelt terméket, hogy azt a kiszállítás során nem vette át (ide nem értve azt az esetet, amikor elállási jogával élt), vagy a Termék nem kereste jelzéssel érkezett vissza az eladóhoz, az Eladó a megrendelés teljesítését a vételár és a szállítási költségek előre történő megfizetéséhez köti. Eladó visszatarthatja a Termék átadását, ameddig nem győződik meg arról, hogy a Termék árának kifizetése sikeresen megtörtént az elektronikus fizetési megoldás használatával.</p>

<p><strong>Külföldre történő értékesítés</strong></p>

<p>Eladó nem különbözteti meg a Honlap használatával Magyarország területén és az azon kívül az Európai Unió területén belüli vásárókat. A jelen ÁSZF eltérő rendelkezése hiányában az Eladó Magyarország területén biztosítja a megrendelt termékek kiszállítását/átvételét.</p>

<p>A Magyarországon kívüli vásárlásra is jelen ÁSZF rendelkezései az irányadóak azzal, hogy a vonatkozó rendelet előírásai alapján jelen pont értelmezésében vevőnek az a fogyasztó minősül, aki valamely tagállam állampolgára, vagy valamely tagállamban lakóhellyel rendelkezik, vagy az a vállalkozás, amely valamely tagállamban letelepedési hellyel rendelkezik, és az Európai Unión belül kizárólag végfelhasználás céljából vásárol árut, vagy ilyen szándékkal jár el. Fogyasztónak az a természetes személy minősül, aki olyan célból jár el, amely kívül esik kereskedelmi, ipari, kézműipari vagy szakmai tevékenysége körén.</p>

<p>A kommunikáció és a vásárlás nyelve elsősorban a magyar nyelv, Eladó nem köteles a vásárló tagállami szerinti nyelven kommunikálni Vásárlóval. Eladó nem köteles megfeleljen a Vásárló tagállama szerinti nemzeti jogban az érintett Termékkel kapcsolatban meghatározott szerződésen kívüli követelményeknek, például címkézési vagy ágazatspecifikus követelményeknek, vagy hogy tájékoztassa a Vásárlót ezekről a követelményekről.</p>

<p>Eladó eltérő rendelkezése hiányában Magyarországi ÁFÁ-t alkalmazza minden Termék esetében.</p>

<p>Vásárló a jelen ÁSZF szerint élhet jogérvényesítési lehetőségeivel.</p>

<p>Elektronikus fizetési megoldás alkalmazása esetén a fizetés az Eladó által meghatározott pénznemben valósul meg. Eladó visszatarthatja a Termék átadását ameddig nem győződik meg arról, hogy a Termék árának és a szállítási díjnak kifizetése sikeresen és maradéktalanul megtörtént az elektronikus fizetési megoldás használatával (ideértve azt az esetet is, amikor az átutalással fizetett termék esetén Vásárló a tagállama szerinti pénznemben utalja el a vételárat (szállítási díjat) és az átváltás, valamint a banki jutalékok, költségek miatt Eladó nem kapja meg teljes mértékben a vételár összegét). Amennyiben a Termék ára nem került teljes mértékben kifizetésre, az Eladó a vételár kiegészítésére hívhatja fel a Vásárlót.</p>

<p>Eladó a Termék átadása érdekében a magyar vásárlókat megillető átadási lehetőségeket biztosítja a nem magyarországi vásárlóknak is. Amennyiben Vásárló az ÁSZF szerint kérheti a Termék szállítását Magyarország területére, vagy bármely más Európai Uniós tagállam területére, ezt kérheti a nem magyarországi vásárló is bármely az ÁSZF-ben megjelölt szállítási módon.</p>

<p>Egyebekben a Vásárló kérheti, hogy a Termék szállítását saját költségén oldhassa meg külföldre. Magyar vásárlót ez a jog nem illet meg.</p>

<p>Eladó a szállítási díj megfizetését követően teljesíti a megrendelést, amennyiben Vásárló a szállítási díjat nem fizeti meg Eladó számára, vagy a saját szállítást nem oldja meg az előre egyezetetett időpontig, Eladó a szerződést felmondja és az előre megfizetett vételárat visszafizeti Vásárló számára.</p>

<p><strong>Fogyasztói tájékoztató a 45/2014. (II. 26.) Korm. rendelet alapján</strong></p>

<p><em><strong>Tájékoztató a fogyasztó vevőt megillető elállási jogról</strong></em></p>

<p>Fogyasztónak a Ptk. 8:1. § 1. bekezdés 3. pontja szerint csak a szakmája, önálló foglalkozása vagy üzleti tevékenysége körén kívül eljáró természetes személy minősül, így jogi személyek nem élhetnek az indokolás nélküli elállási joggal!</p>

<p>A fogyasztót a 45/2014. (II. 26.) Korm. rendelet 20. § szerint megilleti az indokolás nélküli elállás joga. A fogyasztó az elállási jogát</p>

<p>a) termék adásvételére irányuló szerződés esetén</p>

<p>aa) a terméknek,</p>

<p>ab) több termék adásvételekor, ha az egyes termékek szolgáltatása eltérő időpontban történik, az utoljára szolgáltatott terméknek, a fogyasztó vagy az általa megjelölt, a fuvarozótól eltérő harmadik személy általi átvételének napjától számított határidőn belül gyakorolhatja, mely határidő 14 nap.</p>

<p>A jelen pontban foglaltak nem érintik a fogyasztó azon jogát, hogy az e pontban meghatározott elállási jogát a szerződés megkötésének napja és a termék átvételének napja közötti időszakban is gyakorolja.</p>

<p>Ha a szerződés megkötésére a fogyasztó tett ajánlatot, a fogyasztót a szerződés megkötése előtt megilleti az ajánlat visszavonásának joga, ami a szerződés megkötésére kiterjedő ajánlati kötöttséget megszünteti.</p>

<p><em><strong>Elállási nyilatkozat, a fogyasztót megillető elállási vagy felmondási jog gyakorlása</strong></em></p>

<p>A fogyasztó a 45/2014. (II. 26.) Korm. rendelet 20. §-ban biztosított jogát az erre vonatkozó egyértelmű nyilatkozat útján, vagy a honlapról is letölthető nyilatkozat-minta felhasználásával gyakorolhatja.</p>

<p><em><strong>A fogyasztó elállási nyilatkozatának érvényessége</strong></em></p>

<p>Az elállási jogot határidőben érvényesítettnek kell tekinteni, ha a fogyasztó nyilatkozatát határidőn belül elküldi. A határidő 14 nap.</p>

<p>A fogyasztót terheli annak bizonyítása, hogy az elállás jogát e rendelkezéssel összhangban gyakorolta.</p>

<p>Az Eladó a fogyasztó elállási nyilatkozatát annak megérkezését követően köteles elektronikus adathordozón visszaigazolni.</p>

<p><strong>Az Eladó kötelezettségei a fogyasztó elállása esetén</strong></p>

<p><em><strong>Az Eladó visszatérítési kötelezettsége</strong></em></p>

<p>Ha a fogyasztó a 45/2014. (II. 26.) Korm. rendelet 22. §-nak megfelelően eláll a szerződéstől, az Eladó legkésőbb az elállásról való tudomásszerzésétől számított tizennégy napon belül visszatéríti a fogyasztó által ellenszolgáltatásként megfizetett teljes összeget, ideértve a teljesítéssel összefüggésben felmerült költségeket, így a szállítási díjat is. Felhívjuk a figyelmét, hogy ez a rendelkezés nem vonatkozik a legkevésbé költséges szokásos fuvarozási módtól eltérő fuvarozási mód választásával okozott többletköltségekre.</p>

<p><em><strong>Az Eladó visszatérítési kötelezettségének módja</strong></em></p>

<p>A 45/2014. (II. 26.) Korm. rendelet 22. §-nak megfelelő elállás vagy felmondás esetén az Eladó a fogyasztónak visszajáró összeget a fogyasztó által igénybe vett fizetési móddal megegyező módon téríti vissza. A fogyasztókifejezett beleegyezése alapján az Eladó a visszatérítésre más fizetési módot is alkalmazhat, de a fogyasztót ebből adódóan semmilyen többletdíj nem terhelheti. A Fogyasztó által hibásan és/vagy pontatlanul megadott bankszámlaszám vagy postai cím következtében történő késedelem miatt az Eladó-t felelősség nem terheli.</p>

<p><em><strong>Többletköltségek</strong></em></p>

<p>Ha a fogyasztó kifejezetten a legkevésbé költséges szokásos fuvarozási módtól eltérő fuvarozási módot választ, az Eladó nem köteles visszatéríteni az ebből eredő többletköltségeket. Ilyen esetben a feltüntetett általános szállítási díjtételek erejéig áll fenn visszatérítési kötelezettségünk.</p>

<p><em><strong>Visszatartási jog</strong></em></p>

<p>Az Eladó mindaddig visszatarthatja a fogyasztónak visszajáró összeget, amíg a fogyasztó a terméket vissza nem szolgáltatta, vagy kétséget kizáróan nem igazolta, hogy azt visszaküldte; a kettő közül a korábbi időpontot kell figyelembe venni. Utánvéttel vagy portósan feladott küldeményeket nem áll módunkban elfogadni.</p>

<p><strong>A fogyasztó kötelezettségei elállása vagy felmondása esetén</strong></p>

<p><em><strong>A termék visszaszolgáltatása</strong></em></p>

<p>Ha a fogyasztó a 45/2014. (II. 26.) Korm. rendelet 22. §-nak megfelelően eláll a szerződéstől, köteles a terméket haladéktalanul, de legkésőbb az elállás közlésétől számított tizennégy napon belül visszaküldeni, illetve az Eladónak vagy az Eladó által a termék átvételére meghatalmazott személynek átadni. A visszaküldés határidőben teljesítettnek minősül, ha a fogyasztó a terméket a határidő lejárta előtt elküldi.</p>

<p><em><strong>A termék visszaszolgáltatásával kapcsolatos költségek viselése</strong></em></p>

<p>A fogyasztó viseli a termék visszaküldésének költségét. A terméket az Eladó címére kell visszaküldeni.</p>

<p>A fogyasztó által arányosan fizetendő összeget a szerződésben megállapított ellenszolgáltatás adóval növelt teljes összege alapján kell megállapítani. Ha a fogyasztó bizonyítja, hogy az ily módon megállapított teljes összeg túlzottan magas, az arányos összeget a szerződés megszűnésének időpontjáig teljesített szolgáltatások piaci értéke alapján kell kiszámítani. Kérjük vegye figyelembe, hogy utánvéttel vagy portósan visszaküldött terméket nem áll módunkban átvenni.</p>

<p><em><strong>Fogyasztó felelőssége az értékcsökkenésért</strong></em></p>

<p>A fogyasztó a termék jellegének, tulajdonságainak és működésének megállapításához szükséges használatot meghaladó használatból eredő értékcsökkenésért felel.</p>

<p><em><strong>1. Kellékszavatosság</strong></em></p>

<p>Ön, mint Fogyasztó a Második Lehetőség Kereskedelmi Szociális Szövetkezettel szemben kellékszavatossági igényt érvényesíthet a Polgári Törvénykönyv szabályai szerint, ha hibás terméket kap. A Fogyasztó köteles a hibát annak felfedezése után haladéktalanul, de nem később, mint a hiba felfedezésétől számított kettő hónapon belül közölni. A vevő fogyasztói szerződés esetén az átvétel időpontjától számított 2 éven belül érvényesítheti szavatossági igényeit, azokért a termékhibákért, amelyek a termék átadása időpontjában már léteztek. Két éves elévülési határidőn túl a vásárló kellékszavatossági nem tudja érvényesíteni. A Fogyasztó a kellékszavatossági jog érvényesítése esetén kérhet kijavítást vagy kicserélést, kivéve, ha az ezek közül a Fogyasztó által választott igény teljesítése lehetetlen vagy a vállalkozás számára más igénye teljesítéséhez képest aránytalan többletköltséggel járna. Ha a kijavítást vagy a kicserélést nem kérte, illetve nem kérhette, úgy igényelheti az ellenszolgáltatás arányos leszállítását vagy a hibát a vállalkozás költségére Ön is kijavíthatja, illetve mással kijavíttathatja vagy – végső esetben – a szerződéstől is elállhat. Választott kellékszavatossági jogáról a Fogyasztó egy másikra is áttérhet, ebben az esetben viszont a Fogyasztó köteles az áttérés költségét viselni kivéve, ha az indokolt volt, vagy arra a vállalkozás adott okot. A teljesítéstől számított hat hónapon belül a kellékszavatossági igénye érvényesítésének a hiba közlésén túl nincs egyéb feltétele amennyiben a Fogyasztó igazolja, hogy a terméket, illetve a szolgáltatást a Második Lehetőség Kereskedelmi Szociális Szövetkezet nyújtotta. A teljesítéstől számított hat hónap eltelte után azonban már Fogyasztó köteles bizonyítani, hogy a Fogyasztó által felismert hiba már a teljesítés időpontjában is megvolt.</p>

<p><em><strong>2. Termékszavatosság</strong></em></p>

<p>A termék hibája esetén a Fogyasztó eldöntheti, hogy a korábban ismertetett Kellékszavatossági jogot vagy a Termékszavatossági igényt érvényesíthet. Termékszavatossági igényként a Fogyasztó kizárólag a hibás termék kijavítását vagy kicserélését kérheti. A termék abban az esetben tekinthető hibásnak, ha az nem felel meg a forgalomba hozatalakor hatályos minőségi követelményeknek vagy pedig, ha nem rendelkezik a gyártó által adott leírásban szereplő tulajdonságokkal. Termékszavatossági igényét a Fogyasztó a termék gyártó általi forgalomba hozatalától számított két éven belül érvényesítheti. E határidő elteltével a Fogyasztó e jogosultságát elveszti. Termékszavatossági igényét a Fogyasztó kizárólag a termék gyártójával vagy forgalmazójával szemben gyakorolhatja. A termék hibáját termékszavatossági igény érvényesítése esetén a Fogyasztónak kell bizonyítania. A gyártó (forgalmazó) kizárólag akkor mentesül termékszavatossági kötelezettsége alól, ha bizonyítani tudja, hogy</p>

<ul>
	<li>
	<p>a terméket nem üzleti tevékenysége körében gyártotta, illetve hozta forgalomba, vagy</p>
	</li>
	<li>
	<p>a hiba a tudomány és a technika állása szerint a forgalomba hozatal időpontjában nem volt felismerhető vagy</p>
	</li>
	<li>
	<p>a termék hibája jogszabály vagy kötelező hatósági előírás alkalmazásából ered.</p>
	</li>
</ul>

<p>A gyártónak (forgalmazónak) a mentesüléshez elegendő egy okot bizonyítania. Felhívom figyelmét, hogy ugyanazon hiba miatt kellékszavatossági és termékszavatossági igényt egyszerre, egymással párhuzamosan nem érvényesíthet. Termékszavatossági igényének eredményes érvényesítése esetén azonban a kicserélt termékre, illetve kijavított részre vonatkozó kellékszavatossági igényét a gyártóval szemben érvényesítheti. Jótállás Hibás teljesítés esetén a Második Lehetőség Kereskedelmi Szocális Szövetkezet a 151/2003. (IX. 22.) Korm. rendelet alapján az illetve a rendelet mellékletében felsorolt tartós fogyasztási cikkekre (10 000 Ft feletti értékű termékekre) a kötelező 1 év jótállást vállal. A Második Lehetőség Kereskedelmi Szociális Szövetkezet jótállási kötelezettsége alól csak abban az esetben mentesül, ha bizonyítja, hogy a hiba oka a teljesítés után keletkezett. Felhívom a figyelmét, hogy ugyanazon hiba miatt kellékszavatossági és jótállási igényt, illetve termékszavatossági és jótállási igényt egyszerre, egymással párhuzamosan nem érvényesíthet, egyébként viszont a Fogyasztót a jótállásból fakadó jogok az Kellékszavatosság és a Termékszavatosság ismertetésénél meghatározott jogosultságoktól függetlenül megilletik. Amennyiben a jótállás kérése helytálló, a vásárló kérheti a termék cseréjét, vagy javítását. Amennyiben nem áll módunkban a termék cseréje készlethiány miatt, a javítás lehetősége áll fenn. Lehetőség van az árleszállítás igénylésére illetve a vételár visszatérítésére abban az esetben, ha sem a termék cseréjére sem a javítására nincs lehetőség. Amennyiben a Fogyasztó a termék meghibásodása miatt a termék átadásától számított három munkanapon belül érvényesíti csereigényét a Második Lehetőség Kereskedelmi Szociális Szövetkezet köteles a terméket kicserélni, feltéve, hogy a meghibásodás a rendeltetésszerű használatot akadályozza.</p>

<p>A megvásárolt termékekre vonatkozó szavatossági vagy jótállási igényét jelezheti személyesen vagy levélben az alábbi elérhetőségek egyikén:</p>

<p>Telefon: 06-70/618-9013</p>

<p>E-mail: herbawin.webshop@gmail.com</p>

<p><em><strong>3. A fogyasztót megillető elállási és felmondási jog alóli kivételek</strong></em></p>

<p>Webáruházunk kínálatában olyan termékek is találhatók, melyek esetében a fogyasztó felbontás után nem gyakorolhatja az elállási jogot (vitaminok, étrend-kiegészítők, élelmiszerek stb.), ezzel kapcsolatban érdeklődjön ügyfélszolgálatunkon.</p>

<p>Tájékoztatásként a 45/2014 (II.26.) Kormányrendelet 29. paragrafusa alapján a fogyasztó nem gyakorolhatja az elállás jogát az alábbi esetekben:</p>

<p>&nbsp;</p>

<p>d) romlandó vagy minőségét rövid ideig megőrző termék tekintetében,</p>

<p>e) olyan zárt csomagolású termék tekintetében, amely egészségvédelmi vagy higiéniai okokból az átadást követő felbontása után nem küldhető vissza.</p>

<p><strong>Az ÁSZF-ben nem szabályozott kérdésekben a Polgári Törvénykönyv 2013. évi V. törvénye érvényes.</strong></p>

<p>&nbsp;</p>

<p><strong>Nyilatkozat-minta elálláshoz</strong></p>

<p>Címzett: Második Lehetőség Kereskedelmi Szociális Szövetkezet</p>

<p>(cím: 6722 Szeged, Szentháromság utca 50., e-mail: herbawin.webshop@gmail.com)</p>

<p><br />
&nbsp;</p>

<p>Alulírott/ak kijelentem/kijelentjük, hogy gyakorlom/gyakoroljuk elállási/felmondási jogomat/jogunkat az alábbi termék/ek adásvételére vagy az alábbi szolgáltatás nyújtására irányuló szerződés tekintetében:</p>

<p><br />
&nbsp;</p>

<p>………………………………………………………………………………………………………………………………………………………………………………</p>

<p><br />
&nbsp;</p>

<p>Szerződéskötés időpontja /átvétel időpontja: …………………………………………………………</p>

<p><br />
&nbsp;</p>

<p>A fogyasztó(k) neve: …………………………………………………………………………………</p>

<p><br />
&nbsp;</p>

<p>A fogyasztó(k) címe: …………………………………………………………………………………</p>

<p><br />
&nbsp;</p>

<p>A vételárat az alábbi bankszámlaszámra kérem visszautalni (abban az esetben töltse ki, ha szeretné, hogy a vételárat banki átutalással fizessük vissza):</p>

<p>……………………………………………………………………………………………………………………………………………………………………</p>

<p><br />
&nbsp;</p>

<p>A fogyasztó(k) aláírása: (kizárólag papíron tett nyilatkozat esetén)</p>

<p><br />
&nbsp;</p>

<p>…………………………………………………………………………………</p>

<p><br />
&nbsp;</p>

<p>Kelt</p>
