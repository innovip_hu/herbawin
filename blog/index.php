<?php 
	session_start();
	ob_start();
?>
<!DOCTYPE html>
<html class="wide smoothscroll wow-animation" lang="hu">
  <head>
  	<?php
  		$oldal = "blog";
  		include '../config.php';
      $title = $sz_blog_1;
      $description = $sz_blog_2;
  		include $gyoker.'/module/mod_head.php';
  	?>

      <title><?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">  	
  </head>
  <body>
  	<?php include $gyoker.'/module/mod_body_start.php'; ?>
    <!-- Page-->
    <div class="page">
      <!-- Page preloader-->

		<?php include $gyoker.'/module/mod_header.php'; ?>

      <section class="section section-bredcrumbs bg-image-breadcrumbs-1">
        <div class="shell">
          <div class="range range-center">
            <div class="cell-sm-10 cell-xl-8">
              <div class="breadcrumb-wrapper"><img src="<?=$domain?>/images/image-icon-1-49x43.png" alt="" width="49" height="43"/>
                <?php
                  if (isset($_GET['nev_url']) && $_GET['nev_url'] != '') //  belépve
                  {
                    $res = $pdo->prepare("SELECT * FROM ".$webjel."hirek2 where nev_url='".$_GET['nev_url']."'");
                    $res->execute();
                    $row  = $res -> fetch();
                    $time_input = $row['datum'];
                    $date = DateTime::createFromFormat( 'Y-m-d', $time_input);
                    $ujdatum = $date->format( 'Y.m.d.');
                    
                    print '<h2>'.$row['cim'.$_SESSION['lang_sql']].'</h2>';
                    print '<ol class="breadcrumbs-custom">
                      <li><a href="'.$domain.'/">'.$sz_blog_3.'</a></li>
                      <li><a href="'.$domain.'/blog/">'.$sz_blog_4.'</a></li>
                      <li>'.$row['cim'.$_SESSION['lang_sql']].'</li>
                    </ol>';
                  }
                  else // Hírek lista
                  {
                    ?>
                    <h2><?=$sz_blog_4?></h2>
                    <ol class="breadcrumbs-custom">
                      <li><a href="<?=$domain?>"><?=$sz_blog_3?></a></li>
                      <li><?=$sz_blog_4?>
                      </li>
                    </ol>
                    <a href="<?=$domain?>/blog/rss/" target="_blank"><img src="<?=$domain?>/images/rss.svg" style="max-width: 20px;"></a>
                    <?php
                  }
                ?>                
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Company overview-->
      <section class="section section-lg bg-white">
        <div class="shell">
          <div class="range range-35 range-center text-left meret_hosszu">
            <?php include $gyoker.'/webshop/hirek2.php'; ?>
          </div>
        </div>
      </section>
  

	<?php include $gyoker.'/module/mod_footer.php'; ?>

    </div>
	<?php include $gyoker.'/module/mod_body_end.php'; ?>

    <script>
        $(document).ready(function(){ 

            $('.meret_hosszu').each(function(){  

                var highestBox = 0;
                $('.unit__body', this).each(function(){

                    if($(this).height() > highestBox) 
                    highestBox = $(this).height(); 
                });  

                $('.unit__body',this).height(highestBox);

            });  

            $('.meret_hosszu').each(function(){  

                var highestBox = 0;
                $('.hover-heading', this).each(function(){

                    if($(this).height() > highestBox) 
                    highestBox = $(this).height(); 
                });  

                $('.hover-heading',this).height(highestBox);

            });            
        });

    </script>  
  </body>
</html>