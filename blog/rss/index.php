<?php 
  header('Content-type: text/xml; charset=utf-8');
  date_default_timezone_set('Europe/Budapest');

  include '../../config.php';

  $dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
  try
  {
    $pdo = new PDO(
    $dsn, $dbuser, $dbpass,
    Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
    );
  }
  catch (PDOException $e)
  {
    die("Nem lehet kapcsolódni az adatbázishoz!");
  }

  $xml = new SimpleXMLElement('<rss version="2.0"></rss>'); 
   
  
  $xml->addChild('channel'); 
  $xml->channel->addChild('title', 'Herbawin | Legfrissebb blog híreink'); 
  $xml->channel->addChild('link', $domain.'/blog'); 
  $xml->channel->addChild('description', 'Kövesse weboldalunkat és legfrissebb blog bejegyzéseinket, legyen naprakész információja aktuális témáinkkal kapcsolatban!');
  $xml->channel->addChild('language', 'hu'); 
  $xml->channel->addChild('webMaster', 'bioiroda2@gmail.com');
  $xml->channel->addChild('generator', 'PHP SimpleXML');
  $xml->channel->addChild('docs', $domain.'/blog/rss/');
  $xml->channel->addChild('pubDate', date(DATE_RSS)); 

  
  $datum = date("Y-m-d");
  $query = "SELECT * FROM `".$webjel."hirek2` Where `datum` <= '".$datum."' ORDER BY `datum` DESC, `id` DESC";
  foreach ($pdo->query($query) as $key => $row) { 
       
      $item = $xml->channel->addChild('item'); 
      $item->addChild('title', $row['cim']); 
      $item->addChild('link', $domain.'/blog/'.$row['nev_url']);
      $item->addChild('description', $row['elozetes']); 
      $item->addChild('pubDate', date(DATE_RSS, strtotime($row['datum']))); 
  } 
   
  echo $xml->asXML();


?>

