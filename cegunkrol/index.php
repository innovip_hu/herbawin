<?php 
	session_start();
	ob_start();
?>
<!DOCTYPE html>
<html class="wide smoothscroll wow-animation" lang="hu">
  <head>
  	<?php
  		$oldal = "cegunkrol";
  		include '../config.php';
      $title = $sz_cegunkrol_1;
      $description = $sz_cegunkrol_2;
  		include $gyoker.'/module/mod_head.php';
  	?>

      <title><?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">  	
  </head>
  <body>
  	<?php include $gyoker.'/module/mod_body_start.php'; ?>
    <!-- Page-->
    <div class="page">
      <!-- Page preloader-->

		<?php include $gyoker.'/module/mod_header.php'; ?>

      <!-- Company overview-->
      <section class="section section-lg section-lg-bottom-40 bg-white">
        <div class="shell text-center">
          <h1 class="heading-decorative"><span><img src="<?=$domain?>/images/typography-image-1-83x72.png" alt="" width="83" height="72"/><?=$sz_cegunkrol_3?></span></h1>
          <p class="subtitle"><?=$sz_cegunkrol_4?></p>
          <div class="range range-30 range-md-50 number-counter">
            <div class="cell-sm-12 text-left">
              <p class="big"><?=$sz_cegunkrol_5?></p>
              <p class="big"><?=$sz_cegunkrol_6?></p>
              <p class="big"><?=$sz_cegunkrol_7?></p>              
              <p class="big"><?=$sz_cegunkrol_8?></p>
              <p class="big"><?=$sz_cegunkrol_9?></p>
              <p class="big"><?=$sz_cegunkrol_10?>:</p>
              <p class="big"><?=$sz_cegunkrol_11?></p>
              <p class="big"><?=$sz_cegunkrol_12?></p>
            </div>
          </div>
        </div>
      </section>
      <!-- Why choose us-->
      <section class="section section-lg section-lg-top-40">
        <div class="shell">
          <img src="<?=$domain?>/images/cegunkrol.jpg" class="img-responsive" alt="<?=$sz_cegunkrol_13?>">
        </div>
      </section>    

	<?php include $gyoker.'/module/mod_footer.php'; ?>

    </div>
	<?php include $gyoker.'/module/mod_body_end.php'; ?>
  </body>
</html>