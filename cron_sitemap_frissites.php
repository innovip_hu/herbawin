<?php
	include 'config.php';

	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsol�dni az adatb�zishoz!");
	}

	$datum = date("Y-m-d");

	$xml = new DOMDocument('1.0', 'UTF-8');
	$root = $xml->createElement("urlset");
	$xml->appendChild($root);
	$root->setAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
	$root->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
	$root->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
	
	// Főoldal
		$loc = $xml->createElement("loc");
		$locText = $xml->createTextNode($domain);
		$loc->appendChild($locText);

		$changefreq = $xml->createElement("changefreq");
		$changefreqText = $xml->createTextNode("daily");
		$changefreq->appendChild($changefreqText);

		$priority = $xml->createElement("priority");
		$priorityText = $xml->createTextNode("1.00");
		$priority->appendChild($priorityText);
		
		$url = $xml->createElement("url");
		$url->appendChild($loc);
		$url->appendChild($changefreq);
		$url->appendChild($priority);
		
		$root->appendChild($url);
	
	// Aloldalak
	foreach ($conf_aloldalak as $elem)
	{
		$loc = $xml->createElement("loc");
		$locText = $xml->createTextNode($domain."/".$elem."/");
		$loc->appendChild($locText);

		$changefreq = $xml->createElement("changefreq");
		$changefreqText = $xml->createTextNode("daily");
		$changefreq->appendChild($changefreqText);

		$priority = $xml->createElement("priority");
		$priorityText = $xml->createTextNode("0.85");
		$priority->appendChild($priorityText);
		
		$url = $xml->createElement("url");
		$url->appendChild($loc);
		$url->appendChild($changefreq);
		$url->appendChild($priority);
		
		$root->appendChild($url);
	}
	
	// Kategóriák generálása
	$query = "SELECT * FROM ".$webjel."term_csoportok WHERE lathato=1";
	foreach ($pdo->query($query) as $row)
	{
		$loc = $xml->createElement("loc");
		$locText = $xml->createTextNode($domain."/termekek/".$row['nev_url']."/");
		$loc->appendChild($locText);

		$changefreq = $xml->createElement("changefreq");
		$changefreqText = $xml->createTextNode("daily");
		$changefreq->appendChild($changefreqText);

		$priority = $xml->createElement("priority");
		$priorityText = $xml->createTextNode("0.80");
		$priority->appendChild($priorityText);
		
		$url = $xml->createElement("url");
		$url->appendChild($loc);
		$url->appendChild($changefreq);
		$url->appendChild($priority);
		
		$root->appendChild($url);
	}
	
	// Termékek generálása
	$query = "SELECT *, ".$webjel."afa.afa as afa_szaz 
					, ".$webjel."termekek.id as id 
					, ".$webjel."termekek.leiras as leiras 
					, ".$webjel."termekek.nev as nev 
					, ".$webjel."termekek.kep as kep 
					, ".$webjel."termekek.nev_url as nev_url 
					, ".$webjel."term_csoportok.nev as csop_nev 
					, ".$webjel."term_csoportok.csop_id as csop_id 
					, ".$webjel."term_csoportok.nev_url as kat_nev_url 
	FROM ".$webjel."termekek
	INNER JOIN ".$webjel."afa 
	ON ".$webjel."termekek.afa=".$webjel."afa.id 
	INNER JOIN ".$webjel."term_csoportok 
	ON ".$webjel."termekek.csop_id=".$webjel."term_csoportok.id 
	WHERE ".$webjel."termekek.lathato=1";
	foreach ($pdo->query($query) as $row)
	{
		$loc = $xml->createElement("loc");
		$locText = $xml->createTextNode($domain."/termekek/".$row['kat_nev_url']."/".$row['nev_url']);
		$loc->appendChild($locText);

		$changefreq = $xml->createElement("changefreq");
		$changefreqText = $xml->createTextNode("daily");
		$changefreq->appendChild($changefreqText);

		$priority = $xml->createElement("priority");
		$priorityText = $xml->createTextNode("0.75");
		$priority->appendChild($priorityText);
		
		$url = $xml->createElement("url");
		$url->appendChild($loc);
		$url->appendChild($changefreq);
		$url->appendChild($priority);
		
		$root->appendChild($url);
	}

	$xml->save("sitemap.xml");


?>