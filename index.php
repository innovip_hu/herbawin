<?php 
	session_start();
	ob_start();
?>
<!DOCTYPE html>
<html class="wide smoothscroll wow-animation" lang="hu">
  <head>
  	<?php
  		$oldal = "fooldal";
  		include 'config.php';
  		include $gyoker.'/module/mod_head.php';
  	?>

	<title><?php print $title; ?></title>
    <meta name="description" content="<?php print $description; ?>">  	
  </head>
  <body>
  	<?php include $gyoker.'/module/mod_body_start.php'; ?>
    <!-- Page-->
    <div class="page">
      <!-- Page preloader-->

		<?php include $gyoker.'/module/mod_header.php'; ?>

      <section class="section">
        <div class="swiper-custom-container">
          <!-- Swiper-->
          <div class="swiper-container swiper-slider" data-loop="true" data-autoplay="5000" data-simulate-touch="false">
            <div class="swiper-wrapper">
            <?php
              $query = "SELECT * FROM ".$webjel."slider ORDER BY sorrend ASC";
              foreach ($pdo->query($query) as $row)
              { ?>

                <div class="swiper-slide" data-slide-bg="images/termekek/<?=$row['kep']?>">
                  <div class="swiper-slide-caption">
                    <div class="shell">
                      <div class="range range-center">
                        <div class="cell-sm-9 cell-lg-8">
                          <div class="jumbotron-custom">
                            <div class="light-box"><img src="images/image-icon-1-49x43.png" alt="<?=$row['nev'.$_SESSION['lang_sql']]?>" width="49" height="43"/>
                              <h2 class="title"><?=$row['nev'.$_SESSION['lang_sql']]?></h2>
                              <div class="exeption big"><?=$row['szoveg'.$_SESSION['lang_sql']]?></div>
                            </div>
                            <?php if ($row['link'] != ''): ?>
                                <div class="primary-box"><a class="button button-white" href="<?=$row['link']?>"><?=$sz_menu_2?></a></div>
                            <?php endif ?>
                          </div>
                        </div>
                      </div>
                    </div>                  
                  </div>
                </div>
            <?php
              }
            ?>                           
            </div>
            <!-- Swiper Pagination-->
            <div class="swiper-pagination"></div>
            <!-- Swiper Navigation-->
            <div class="swiper-button swiper-button-prev">
            </div>
            <div class="swiper-button swiper-button-next">
            </div>
          </div>
        </div>
      </section>

      <section class="section section-lg bg-white text-center">
        <div class="shell">
          <h1 class="heading-decorative"><span><img src="images/typography-image-1-83x72.png" alt="<?=$sz_fo_1?>" width="83" height="72"/><?=$sz_fo_1?></span></h1>
          <div class="range range-35 range-center text-left">
          <?php 
              $datum = date("Y-m-d");
              $query = "SELECT * FROM `".$webjel."hirek` Where `datum` <= '".$datum."' ORDER BY `datum` DESC, `id` DESC LIMIT 3";
              foreach ($pdo->query($query) as $row)
              {            

                  $query_kep = "SELECT * FROM ".$webjel."hir_kepek WHERE hir_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
                  $res = $pdo->prepare($query_kep);
                  $res->execute();
                  $row_kep = $res -> fetch();
                  $alap_kep = $row_kep['kep'];

                ?>

                <div class="cell-md-4 cell-sm-6 cell-xs-12">
                  <div class="post-classic"><a href="<?=$domain?>/aktualitasok/<?=$row['nev_url']?>"><div class="post-boxed-image-wrap"><img src="<?=$domain?>/images/termekek/<?=$alap_kep?>" alt="<?=$row['cim']?>" class="post-boxed-image"/></div></a>
                    <div class="caption">
                      <div class="unit unit-vertical unit-sm-horizontal unit-sm--inverse unit-spacing-sm">
                        <div class="unit__body">
                          <h6><a class="hover-heading" href="<?=$domain?>/aktualitasok/<?=$row['nev_url']?>"><?=$row['cim'.$_SESSION['lang_sql']]?></a></h6>
                          <p><span><?=$row['elozetes'.$_SESSION['lang_sql']]?></span><a class="link" href="<?=$domain?>/aktualitasok/<?=$row['nev_url']?>"><?=$sz_fo_2?></a></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

          <?php
              }
          ?>
          </div>
        </div>
      </section>        

      <?php /*      
      <!-- Company overview-->
      <section class="section section-lg bg-white">
        <div class="shell text-center">
          <h5 class="heading-1 heading-decorative"><span><img src="images/typography-image-1-83x72.png" alt="Gyümölcsös különlegességek" width="83" height="72"/>Gyümölcsös különlegességek</span></h5>
          <p class="subtitle">Fűben, fában orvosság.</p>
          <div class="range range-20 range-around">
            <div class="cell-sm-12 text-left">
              <p class="big">A kézműves élelmiszerek egyedisége és különlegesen gazdag ízvilága az előállításuk során tanúsított gondosságnak és szeretetnek köszönhető. Minden termék és termékcsoport mellett ott egy arc, aki büszkén vállalja a munkája eredményét.</p>
              <p class="big">Kézműves, biogyümölcsből és zöldségből készült lekvár- és méz választékunkban mindenki megtalálja a saját szája ízének való finomságot. Legyen az egy ínycsiklandozó paprikalekvár vagy egy csepp, különleges selyemfű méz.</p>
              <p class="big">Termékeink tartósítószertől mentesek, továbbá nem tartalmaznak mesterséges anyagokat, vagy színezékeket, teljesen „E” mentesek. Készítményeink alapanyagait igazolt és folyamatosan felügyelt ökogazdaságokban állítják elő, ahol a törvényi előírásoknak megfelelően semmilyen mérgező vegyszert, káros műtrágyát nem használnak. A termésnövelést és a tápanyagtartalmat kizárólag szerves adalékokkal biztosítják.</p>
            </div>
            <div class="cell-sm-3 text-gray">
              <div class="blurb-center">
                <h2 class="index-counter"><img src="images/ecology.svg" style="max-width: 60px;" alt="Csak természetesen"></h2>
                <h4 class="blurb-title">Csak természetesen!</h4>
                <p class="blurb-content">Tartósítószerektől és mesterséges anyagoktól mentes termékek.</p>
              </div>
            </div>
            <div class="cell-sm-3 text-gray">
              <div class="blurb-center">
                <h2 class="index-counter"><img src="images/guarantee.svg" style="max-width: 60px;" alt="Ellenőrzött termékek"></h2>
                <h4 class="blurb-title">Ellenőrzött termékek</h4>
                <p class="blurb-content">Készítményeink alapanyagait igazolt és folyamatosan felügyelt ökogazdaságokban állítják elő.</p>
              </div>
            </div>
            <div class="cell-sm-3 text-gray">
              <div class="blurb-center">
                <h2 class="index-counter"><img src="images/heart.svg" style="max-width: 60px;" alt="Szívvel, lélekkel"></h2>
                <h4 class="blurb-title">Szívvel, lélekkel</h4>
                <p class="blurb-content">A gondosságnak köszönhető egyediség és különleges ízvilág.</p>
              </div>
            </div>
          </div>
          <div class="button-custom-wrapper"><a class="button button-primary" href="<?=$domain?>/cegunkrol">Ismerjen meg minket!</a></div>
        </div>
      </section>

      <section class="section section-lg bg-white">
        <div class="shell text-center">
          <h5 class="heading-1 heading-decorative"><span><img src="images/typography-image-2-58x60.png" alt="Kiemelt termékeink" width="58" height="60"/>Kiemelt termékeink</span></h5>
          <!-- Owl Carousel-->
          <div class="owl-carousel meret_hosszu" data-items="1" data-sm-items="2" data-md-items="3" data-lg-items="4" data-dots="true" data-nav="true" data-sm-stage-padding="28" data-xl-stage-padding="0" data-loop="false" data-margin="30" data-mouse-drag="false">
          <?php 

              $query_lekerdezes = "SELECT * FROM ".$webjel."termekek WHERE kiemelt=1 AND lathato = 1 ORDER BY RAND()";
              foreach ($pdo->query($query_lekerdezes) as $row)
              {
                $query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row['csop_id'];
                $res_csop = $pdo->prepare($query_csop);
                $res_csop->execute();
                $row_csop  = $res_csop -> fetch();
                $link = ''.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row['nev_url'];
              // }
              // else
              // {
                // $link = ''.$domain.'/termekek/'.$_GET['kat_urlnev'].'/'.$row['nev_url'];
              // }
              // Kép
              $query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
              $res = $pdo->prepare($query_kep);
              $res->execute();
              $row_kep = $res -> fetch();
              $alap_kep = $row_kep['kep'];
              if ($alap_kep == '') 
              {
                $kep_link = $domain.'/webshop/images/noimage.png';
              }
              elseif ($row_kep['ovip_termek_id'] !=0)
              {
                if($row_kep['thumb'] != '')
                {
                  $kep = $row_kep['thumb'];
                }
                else
                {
                  $kep = $row_kep['kep'];
                }
                $kep_link = $kep;
              }
              else
              {
                if($row_kep['thumb'] != '')
                {
                  $kep = $row_kep['thumb'];
                }
                else
                {
                  $kep = $row_kep['kep'];
                }
                $kep_link = $domain.'/images/termekek/'.$kep;
              }
              //ÁR
              $datum = date("Y-m-d");
              if ($row['akcio_ig'] >= $datum && $row['akcio_tol'] <= $datum) //Akciós
              {
                $term_ar = '<span class="price-new" style="display:inline;">'.number_format($row['akciosar'], 0, ',', ' ').' Ft</span> <span class="price-old" style="display:inline;">'.number_format($row['ar'], 0, ',', ' ').' Ft</span>';
              }
              else //nem akciós
              {
                $term_ar = '<span class="price-normal">'.number_format($row['ar'], 0, ',', ' ').' Ft</span>';
              }          
          ?>
            <div class="owl-item">
              <div class="price-box">
                <div class="media"><a href="<?=$link?>"><img src="<?=$kep_link?>" alt="<?=$row['nev']?>" class="img-responsive" style="max-height: 250px; margin: 0 auto;"/></a></div>
                <div class="caption">
                  <div class="title"><a href="<?=$link?>"><?=$row['nev']?></a></div>
                  <p class="exeption"><?=$row['rovid_leiras']?></p>
                  <p class="price"><?=$term_ar?></p><a class="button button-primary" href="<?=$link?>">Részletek</a>
                </div>
              </div>
            </div>

            <?php 
            }
            ?>

          </div>
        </div>
      </section>

 
       
      <!-- OUR 100% healthy soaps-->
      <section class="section section-lg bg-white">
        <div class="shell text-center">
          <div class="range range-center range-0">
            <div class="cell-sm-8 cell-md-6 column-image">
              <div class="column-image-wrap"><img src="images/nyitolap_1.jpg" alt="Gyógyhatású összetevők" class="img-responsive"/>
              </div>
            </div>
            <div class="cell-sm-8 cell-md-6 section-xs-1">
              <div class="range range-center">
                <div class="cell-md-8">
                  <h5 class="heading-1 heading-decorative"><span><img class="image-center" src="images/typography-image-2-58x60.png" alt="Gyógyhatású összetevők" width="58" height="60"/>Gyógyhatású összetevők</span></h5>
                  <p>A máriatövis mag a máj védelme mellett rendkívül hatékony antioxidáns, így képes semlegesíteni azokat a veszélyes szabad gyököket, amelyek más molekulákban, sejtekben kárt tehetnek. A kendermagban alapvetően fontos aminosavak és magas tápértékű fehérjék is megtalálhatók, így nagymértékben hozzájárulhat az egészséges immunrendszer fenntartásához.</p><a class="button button-primary" href="<?=$domain?>/termekek/">Termékeink</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Take care of yourself and your skin-->
      <section class="section section-lg bg-white">
        <div class="shell text-center">
          <div class="range range-center range-0 range-md-reverse">
            <div class="cell-sm-8 cell-md-6 column-image">
              <div class="column-image-wrap"><img src="images/nyitolap_2.jpg" alt="Egyedi feldolgozás"  class="img-responsive"/>
              </div>
            </div>
            <div class="cell-sm-8 cell-md-6 section-xs-1">
              <div class="range range-center">
                <div class="cell-md-8">
                  <h5 class="heading-1 heading-decorative"><span><img class="image-center" src="images/typography-image-3-57x60.png" alt="" width="57" height="60"/>Egyedi feldolgozás</span></h5>
                  <p>Hivatalos vizsgálatok bizonyítják, hogy a HerbaPharm termékeknél alkalmazott speciális és egyedi kivonási módnak köszönhetően az alapanyagként felhasznált kendermagból, máriatövis magból, barna lenmagból, fűszerkömény magból és citromból, a hagyományos eljárásokhoz képest, sokkal nagyobb mértékben tudnak hatóanyagokat kivonni.</p><a class="button button-primary" href="<?=$domain?>/termekek/">Termékeink</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="section section-lg bg-white">
        <div class="shell text-center">
          <div class="range range-center range-0">
            <div class="cell-sm-8 cell-md-6 column-image">
              <div class="column-image-wrap"><img src="images/nyitolap_3.jpg" alt="Speciális csomagajánlatok" class="img-responsive"/>
              </div>
            </div>
            <div class="cell-sm-8 cell-md-6 section-xs-1">
              <div class="range range-center">
                <div class="cell-md-8">
                  <h5 class="heading-1 heading-decorative"><span><img class="image-center" src="images/typography-image-2-58x60.png" alt="" width="58" height="60"/>Speciális csomagajánlatok</span></h5>
                  <p>A különleges HerbaPharm termékcsomagok természetes növényi kivonatot, őrleményt és krémet is tartalmaznak, ráadásul INGYENES SZÁLLÍTÁSSAL vásárolhatók meg! Próbálja ki a HerbaPharm Növényi Kivonat csomagunkat is, és rendelje meg kedvezményesen még ma!</p><a class="button button-primary" href="<?=$domain?>/termekek/">Termékeink</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      
        */ ?>   

      <!-- Change your lifestyle-->
      <section class="section section-lg bg-image bg-image-8 context-dark">
        <div class="shell text-center">
          <div class="range range-center">
            <div class="cell-sm-10 cell-lg-8">
              <div class="range range-center">
                <div class="cell-sm-10">
                  <h2 class="heading-decorative"><span><img src="images/typography-image-5-83x72.png" alt="<?=$sz_fo_3?>" width="83" height="72"/><?=$sz_fo_3?></span></h2>
                </div>
              </div>
              <div class="highlited-box offset-1">
                <blockquote class="quote">
                  <div class="quote-icon text-white"></div>
                  <div class="quote-body">
                    <p class="big">
                      <q class="text-white"> <?=$sz_fo_4?></q>
                    </p>
                    <cite class="heading-6"><?=$sz_fo_5?></cite>
                  </div>
                </blockquote>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Change your lifestyle-->

<?php /*
      <section class="section section-lg bg-white">
        <div class="shell">
          <div class="range range-20 range-center">
            <div class="cell-md-6 cell-sm-12 cell-flex">
              <div class="cta bg-image" style="background-image: url(images/kat1.jpg)">
                <div class="cta-inner">
                  <h3 class="title">Kendermagból készült termékek</h3>
                  <h6 class="exeption">„Ma nincs pénzed az egészségre? Majd lesz holnap a betegségre!” (Paracelsus)</h6><a class="button button-primary" href="<?=$domain?>/termekek/Kendermagtermekek/">Részletek</a>
                </div>
              </div>
            </div>
                  
            <div class="cell-md-6 cell-sm-12 cell-flex">
              <div class="cta bg-image" style="background-image: url(images/kat2.jpg)">
                <div class="cta-inner">
                  <h3 class="title">Méz és a belőle készült termékek</h3>
                  <h6 class="exeption">Jótékony, egészségmegőrző vitaminraktár.</h6><a class="button button-primary" href="#">Részletek</a>
                </div>
              </div>
            </div>
            <div class="cell-md-6 cell-sm-12 cell-flex">
              <div class="cta bg-image" style="background-image: url(images/kat3.jpg)">
                <div class="cta-inner">
                  <h3 class="title">Biolekvárok</h3>
                  <h6 class="exeption">Anya! Van itthon valami fincsi édesség?</h6><a class="button button-primary" href="#">Részletek</a>
                </div>
              </div>
            </div>
            <div class="cell-md-6 cell-sm-12 cell-flex">
              <div class="cta bg-image" style="background-image: url(images/kat4.jpg)">
                <div class="cta-inner">
                  <h3 class="title">Liofilizált termékek</h3>
                  <h6 class="exeption">A tudomány az egészség szolgálatában.</h6><a class="button button-primary" href="#">Részletek</a>
                </div>
              </div>
            </div> 
                                       
          </div>
        </div>
      </section>
     */ ?> 

	<?php include $gyoker.'/module/mod_footer.php'; ?>

    </div>
	<?php include $gyoker.'/module/mod_body_end.php'; ?>

    <script>
        $(document).ready(function(){ 

            $('.meret_hosszu').each(function(){  

                var highestBox = 0;
                $('.unit__body', this).each(function(){

                    if($(this).height() > highestBox) 
                    highestBox = $(this).height(); 
                });  

                $('.unit__body',this).height(highestBox);

            });  

            $('.meret_hosszu').each(function(){  

                var highestBox = 0;
                $('.hover-heading', this).each(function(){

                    if($(this).height() > highestBox) 
                    highestBox = $(this).height(); 
                });  

                $('.hover-heading',this).height(highestBox);

            });  

            $('.meret_hosszu').each(function(){  

                var highestBox = 0;
                $('.media', this).each(function(){

                    if($(this).height() > highestBox) 
                    highestBox = $(this).height(); 
                });  

                $('.media',this).height(highestBox);

            }); 

            $('.meret_hosszu').each(function(){  

                var highestBox = 0;
                $('.title', this).each(function(){

                    if($(this).height() > highestBox) 
                    highestBox = $(this).height(); 
                });  

                $('.title',this).height(highestBox);

            }); 
            
            $('.meret_hosszu').each(function(){  

                var highestBox = 0;
                $('.exeption', this).each(function(){

                    if($(this).height() > highestBox) 
                    highestBox = $(this).height(); 
                });  

                $('.exeption',this).height(highestBox);

            });                                               
        });

    </script> 

  </body>
</html>