<?php 
	session_start();
	ob_start();
?>
<!DOCTYPE html>
<html class="wide smoothscroll wow-animation" lang="hu">
  <head>
  	<?php
  		$oldal = "informaciok";
  		include '../config.php';
  		include $gyoker.'/module/mod_head.php';       
  	?> 
      <title><?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">  	
  </head>
  <body>
  	<?php include $gyoker.'/module/mod_body_start.php'; ?>
    <!-- Page-->
    <div class="page">
      <!-- Page preloader-->

		<?php include $gyoker.'/module/mod_header.php'; ?>

      <!-- Company overview-->
      <section class="section section-lg bg-white">
        <div class="shell text-center">
          <div class="range range-xs-center">
            <div class="cell-lg-12">
              <div class="tabs-custom tabs-vertical text-left" id="tabs-2">
                <!-- Nav tabs-->
                <ul class="nav-custom nav-custom-tabs">
                  <li class="active"><a href="#osszetevok" data-toggle="tab"><?=$sz_info_1?></a></li>
                  <?php /*                  
                  <li><a href="#kendermag" data-toggle="tab">Kendermagból készült termék</a></li>
                  <li><a href="#mez" data-toggle="tab">A  MÉZ és a BELŐLE készült termékek</a></li>
                  <li><a href="#biolekvarok" data-toggle="tab">Biolekvárok</a></li>
                  <li><a href="#liofilizalt-termekek" data-toggle="tab">Liofilizált termékek</a></li>
                  */ ?>
                </ul>
                <div class="tab-content text-left">
                  <div class="tab-pane fade in active" id="osszetevok">
                    <h6><?=$sz_info_2?></h6>
                    <p><?=$sz_info_3?></p>

                    <h6><?=$sz_info_4?></h6>
                    <p><?=$sz_info_5?></p>

                    <h6><?=$sz_info_6?></h6>
                    <p><?=$sz_info_7?></p>

                    <h6><?=$sz_info_8?></h6>
                    <p><?=$sz_info_9?></p>

                    <h6><?=$sz_info_10?></h6>
                    <p><?=$sz_info_11?></p>


                    <p><?=$sz_info_12?> <a href="https://www.herbawin.hu/termekek/Termeszetes-novenyi-hatoanyagokat-tartalmazo-keszitmenyek/"><?=$sz_info_13?></a></p>
                  </div>
                  <div class="tab-pane fade" id="kendermag">
                    <h6>Kendermagból készült termékek</h6>
                    <blockquote class="quote">
                      <div class="quote-icon"></div>
                      <div class="quote-body">
                        <p class="">
                          <q>Ma nincs pénzed az egészségre? Majd lesz holnap a betegségre!</q>
                        </p>
                        <cite class="heading-6">Paracelsus</cite>
                      </div>
                    </blockquote>                    
                    <p class="">Napjainkban egyre gyakrabban fordul elő, hogy ősi, rég elfeledett növények jótékony hatásait újra felfedezi az emberiség. Így történt ez a napjainkban egyre nagyobb népszerűségnek örvendő kendermaggal is. A kendermag fogyasztásának évszázadokra visszatekintő múltja van, hiszen egy sokoldalú és rendkívül jótékony hatású magról beszélünk. A 3-4 hónap alatt teljesen kifejlődő kender amellett, hogy az iparban rendkívül hasznos növény, egészségünk megőrzésében is segíthet. A kendermag összetétele igen különleges, ezért tartják a legújabb szuperfoodnak a táplálkozástudósok. Számos kutatás kimutatta, hogy a kenderolajban megtalálható linolsav lassítja a sejtek öregedését, így valódi szuperfegyver lehet a bőröregedés gátlásában. Rendkívül erős hidratáló és bőrpuhító hatású, emellett növeli a bőr rugalmasságát és a szövetek vízmegtartó képességét is. Az áruházban található termékek egytől egyig laborteszteltek és minden tétel esetében külső labor végzi el a vizsgálatokat.</p>
                  </div>
                  <div class="tab-pane fade" id="mez">
                    <h6>A  MÉZ és a BELŐLE készült termékek</h6>
                    <p class=" text-bold">A méz a megkettőzött egészségmegőrzés, és a legtartósabb vitaminraktár, hiszen nemcsak önmagában bír jótékony,egészségmegőrző hatással, hanem magában hordozza annak a növénynek a gyógyhatásait is, amelyből készült.</p>

                    <p class="">A méz évszázadokig egyetlen édesítőszerünk volt, igen ősi emberi táplálék. A méz gyakorlatilag minden olyan anyagot tartalmaz, amelyre testünk táplálásához szükség van. A különféle cukrok mellett, fehérjék, savak, számos vitamin forrása is. A vitaminok közül jócskán találhatók benne C- és B1-, B2-, B3-, B5-, B6-, B7-vitaminok, nikotinamid, folsav, amelyek időtlen időkig elállnak. Pár evőkanál méz számos ásványi anyagból, például foszforból, vasból, kalciumból és magnéziumból fedezi a napi szükségletet. Néhány speciális hatóanyag is található a mézben. Ezek közül ki kell emelni a méhek termelte enzimeket is, amelyek baktériumölő hatása a kísérletek szerint több elterjedt antibiotikuméval vetekszik. A méz antibakteriális és fertőtlenítő hatásának alapja, hogy szinte kivonja a vizet a különböző mikroorganizmusokból, és ezzel elpusztítja azokat.Talán kevesen tudják, hogy a méz még a nyílt-és égési sebekre, fekélyekre téve is alkalmazható, fájdalomcsillapító, viszketést csökkentő hatású, a sebgyógyulást jobban segíti, mint az antibiotikumos kenőcsök.</p>

                    <p class="">MÉZRŐL ÉS JÓTÉKONY HATÁSAIRÓL ITT OLVSHAT BŐVEBBEN:</p>
                    <a href="http://www.fitt.info/component/k2/item/298-m%C3%A9z-m%C3%A9z-biom%C3%A9z?tmpl=component&print=1" target="_blank" rel="nofollow">http://www.fitt.info/component/k2/item/298-m%C3%A9z-m%C3%A9z-biom%C3%A9z?tmpl=component&print=1 </a>

                    <p><img src="<?=$domain?>/images/viragmez.jpg" class="img-responsive" alt="Virágméz"></p>

                    <p class=" text-bold">Virágméz</p>
                    <p class="">A mézet értékes mezei virágokról, gyógynövényekről gyűjtik a szorgalmas méhek, és emiatt beltartalmilag kiváló méznek számít. Szakemberek szerint a virágméz a legértékesebb mézek közé tartozik, mivel a természetgyógyászatban is használt különféle növényekről származó nektár révén a méz hatásterülete is kiszélesedik.
                    A vegyes virágméz színe: halványbarnás színtől egészen a sötétbarnáig terjed, és ez függ a benne levő mézek összetételétől. Érzékeny gyomrúak is fogyaszthatják. Erősíti a szervezet védekező-rendszerét. Natúr fogyasztásra és sütemények ízesítésére kiválóan alkalmas.</p>
                    <p class="">A virágpor értékes fehérjéket, húszféle aminosavat C- és B-vitaminokat, E- és A-vitamint, 28 különböző ásványi anyagot, még lítiumot is tartalmaz a gyűjtőterülettől függően változó arányban. A rostok kivételével minden szükséges tápanyag benne van, amire az embernek szüksége van, így vegetáriánusok ideális kiegészítő tápláléka is lehet. </p>
                    <p class="">Immunerősítő, roboráló, anyagcsere-serkentő, segíti a vérképzést, szabályozza az emésztést, májbetegségek, emésztési zavarok, érrendszeri és keringési problémák, vérszegénység, vérnyomásproblémák kezelésében bizonyított. Fokozza a szellemi teljesítőképességet, javítja a koncentrációs képességeket. </p>
                    <p class="">A vegyes virágméz jótékony hatásai között szerepelnek olyan pozitív tulajdonságok is, mint a fertőtlenítési képesség, nyugtató hatás, az erőteljes immunerősítő hatás, az általános erősítő hatás, valamint a sebkezelésre való alkalmasság is. Sebkezelés során különösen jól hasznosítható méztípusnak számít, mivel megakadályozza a seb fertőződését, valamint elősegíti a hegképződést is.</p>

                    <p class=" text-bold">Mikor és kinek ajánlott a vegyes virágméz fogyasztása?</p>
                    <p class="">Betegségek esetén már napi 2-3 evőkanálnyi mennyiség is képes megállítani a baktériumok szaporodását. A hiedelmekkel ellentétben a vegyes virágméz, ahogyan a többi méztípus sem hizlal. A benne lévő nagy mennyiségű cukor ugyanis, nem képes zsírrá átalakulni. A vegyes virágméz fogyasztása kifejezetten ajánlott a gyermekek és az idősek számára immunerősítés céljából, valamint a fertőző légúti megbetegedések kivédése vagy legyőzése céljából. A vegyes virágméz a mono mézekhez képest sokkal összetettebb, és hatékonyabb megoldást kínál az egészségügyi panaszok legyőzésére.</p>

                    <p class="">A világhírű magyar akácméz, amely 2014-ben elnyerte a HUNGARIKUM címet
                    Az akácot Tessedik Sámuel hozta be és terjesztette el  hazánkban a homok megkötése céljából. Egyes  vidékeinken  második hazájára talált, amit mutat feltűnő vitalitása és az, hogy számos változatot hozott létre viszonylag rövid idő alatt.</p>
                    <p class="">A Magyarországon előállított  mézek közül világszerte ez a legismertebb és legkeresettebb az akácméz. </p>
                    <p class="">A magyar akácméz azért vívhatta ki ezt az elismerést, mert itt volt eddig egész Európában a legkiterjedtebb akácfa állomány. Míg hazájában (Észak-Amerika) ritkás ligetekben fordul elő, nálunk összefüggő erdőségeket alkot. Egyes vidékeinken második hazájára talált, amit mutat feltűnő vitalitása és az, hogy számos változatot hozott létre viszonylag rövid idő alatt. </p>

                    <p class=" text-bold">Az akácméz tulajdonságai</p>
                    <p class="">Az akácméz színe kristálytiszta, egészen enyhén sárga, vagy egyes évjáratokban zöldes árnyalatú. Az akácméz illata akácvirág illatú. Mellék íz nélküli aromája teszi egyedivé, íze a legtöbb méznél édesebb, míg savtartalma a  legtöbb méznél alacsonyabb.  Gyümölcscukorban igen gazdag, így lassan ikrásodik, akár 1-2 évig is megőrzi állagát. A világ három legkevésbé kristályosodó fajtaméze közül az egyik az akácméz. Jó édesítő képessége mellett ezért is olyan keresett a világpiacon.</p>

                    <p class=" text-bold">Az akácméz hatásai</p>
                    <p class="">Az akácméz robinint és akacint tartalmaz, ezért jó általános  fertőtlenítő hatása van. Köhögéscsillapító hatású és segít elmulasztani a torokfájást.  Kálium és kéntartalma miatt az akácméz alkalmas leginkább a méregtelenítésre. Gyomorsav túltengés esetén is hatásos lehet. Az akácmézben lévő gyümölcscukornak májregeneráló hatása van. Nem közömbös a sportolásnál tapasztalt izomlazító képessége sem, sőt a szívizom működésére is jótékonyan hat. </p>
                    <p class="">Enyhe íze miatt szívesen használják az akácmézet süteményekbe. De nyersen, csemegeként is fogyasztható. Készítenek belőle mézes pálinkát, mézes bort és mézbort is.</p>

                    <p class=" text-bold">A selyemfűméz</p>
                    <p class="">Csak hazánkban termelhető tisztán, fajtamézként. Az egyik legfinomabb, univerzálisan használható illatos méz.</p>
                    <p class=" text-bold">Miről gyűjtik:</p>
                    <p class="">Selyemkóró (Asclepias syriaca L.) eredetileg dísznövény, majd kísérleti rostnövény. Elsősorban akácerdők alá települ. 1,0-1,5 méter magasra növő, gömb alakú, húsvörös virágzatú növény. Vadnövény, nincs növényvédelem! Klinikai kísérletek szerint gyulladáscsökkentés tekintetében hatásos mézfajta. Hatásos izomfájdalmak esetén pakolásokhoz vagy masszírozásra.</p>
                    <p class="">Fogyasztása elsősorban önmagában kanalazva ajánlott!</p>

                    <p class=" text-bold">Hárs méz jellemzői, felhasználása</p>
                    <p class="">Miről gyűjtik: Kislevelű hárs (Tilia cordata L.), Nagylevelű hárs (Tilia platyphíllos L.) és Ezüst hárs (Tilia argentea L.) fafajok alkotják hárserdeink nagy részét. Elsősorban a Dél-Dunántúlon jelentős erdőalkotó, de hazánkban általánosan elterjedt fafaj mind erdőalkotóként, mind fasoralkotó parkfaként. A hársfa 25-30 m magasra növő, sárga vagy zöldes álernyős virágzatú növény. Semmilyen mesterséges kezelést nem igényel, nincs növényvédelem! Virágát gyógynövényként júniusban gyűjtik. A hársfavirág kevés pollent és sok nektárt termel. </p>

                    <p class=" text-bold">Méz megjelenése:</p>
                    <p class="">Színe változatos, a világossárgától a borostyánságáig terjed, íze nagyon erős, kissé kesernyés, illata intenzív, hársillatú. Viszonylag nehezen kristályosodik. </p>

                    <p class=" text-bold">Méz felhasználása:</p>
                    <p class="">Illóolajai, farnezol tartalma és egyéb hatóanyagai révén jó hatása van a megfázásos tünetek enyhítésében (köhögés, hörghurut).
                    Kolin tartalma folytán gátolja a zsírlerakódást és az érelmeszesedést. Igen erős antibiotikus hatású méz.
                    Ajánlott megfázás, torokgyulladás, köhögés és lázzal járó betegségek esetén.</p>
                    <p class="">Fogyasztása idegesség, nyugtalanság, álmatlanság leküzdésére is javasolt. (Lefekvés előtt 1 kanál mézet ajánlott ilyen esetekben fogyasztani.)
                    Fogyasztása önmagában kanalazva vagy italokba keverve. Mézet csak már fogyasztható hőmérsékletű italba keverjük, ne forrázzuk!</p>

                    <p class=" text-bold">Repce (krém) méz jellemzői, felhasználása</p>
                    <p class=" text-bold">Miről gyűjtik:</p>
                    <p class="">A Káposztarepce (Brassica napus L.) termesztett növény. A növényolaj előállítás egyik fontos faja. A növény 1,0 - 2,0 m magasra növő, laza fürtös, sárga virágú növény. A magtermés jelentősen javul a méhek megporzó munkája miatt.</p>
                    <p class="">Mikor gyűjtik: Tavasszal az akác virágzását megelőzően április végén, május legelején.</p>
                    <p class="">Nagy odafigyelést igényel a méhész részéről, hogy méze ne kerülhessen bele az akácmézbe, mert annak kristályosodását okozza.</p>

                    <p class=" text-bold">Méz megjelenése:</p>
                    <p class="">Frissen pergetett mézként: Halványsárga színű, enyhe zamatú, édes illatú méz.</p>
                    <p class="">Krém mézként: A repceméz nagyon könnyen kristályosodik, kikristályosodva szinte fehérré válik, és nagyon finomszemcsés, krémes megjelenést kap. A krémméz a természetes kristályosodás közben, folyamatosan kevert mézet jelenti. A keverés hatására egyenletes, finomszemcsés, krémes lesz a kristályosodó méz. A keverésen kívül semmi más beavatkozás nem történik, nincsenek hozzáadott anyagok!</p>

                    <p class=" text-bold">Méz összetétele:</p>
                    <p class="">Repcemézre jellemző, hogy a mézben előforduló 11 cukorféle közül csak 3-at tartalmaz, a fruktóz (gyümölcscukor) és glukóz (szőlőcukor) mellett csak maltózt. Nincs benne például szaharóz, melyre a cukorbetegek érzékenyek.</p>

                    <p class=" text-bold">Méz felhasználása: </p>
                    <p class="">Gyors, finom kristályosodása miatt a krémméz alapanyaga. Fogyasztása minden formában ajánlott!
                    Alacsony savtartalma miatt ajánlott a gyomorsav-túltengésben szenvedőknek.
                    Repce krémméz segítségével nagyon egyszerű és finom édességek készíthetők.</p>

                    <p class=" text-bold">GESZTENYE MÉZ</p>
                    <p class="">Gesztenyevirág illatú, enyhén kesernyés utóízű igazi ínyenccsemege. A szelídgesztenyések nyár eleji virágaiból származó, kiemelkedően magas ásványi anyag tartalma miatt a Kárpát-medence legértékesebb méze. Jó hatású legyengült állapotot eredményező betegségek és étvágytalanság esetén.</p>
                    <p class=" text-bold">Hatásai:</p>
                    <ul class="">
                    <li>- rendszeres fogyasztása akadályozza a trombózisok kialakulását.</li>
                    <li>- viszértágulat mérséklésére is alkalmas, külsőleg is alkalmazható</li>
                    <li>- érrendszer karbantartó, érgyulladásokra külsőleg is alkalmazható</li>
                    <li>- jó hatású vérszegénység ellen</li>
                    <li>- kimerültség ellen</li>
                    <li>- levertség ellen</li>
                    <li>- legyengült szervezet erősítő</li>
                    <li>- étvágytalanság ellen</li>
                    <li>- magas ásványi anyag tartalma miatt általános erősítő</li>
                    <li>- szíverősítő</li>
                    </ul>

                    <p class=" text-bold">EZÜSTFA MÉZ</p>
                    <p class="">Igen ritka mézfajta. Az ezüstfűz méz íze és illata jellegzetes, színe szürkés-zöld, opálos,
                    aromája illatos. Közepesen, nagyjából egy éven belül kristályosodik. Vegyes mézben
                    ízjavító.</p>
                    <p class=" text-bold">Hatásai:</p>
                    <ul class="">
                    <li>- láz csillapításra</li>
                    <li>- reumára</li>
                    <li>- magas vérnyomás csökkentésére</li>
                    </ul>

                    <p class=" text-bold">BÁLVÁNYFA MÉZ</p>
                    <p class="">A bálványfa méz névadó fája Kínából díszfaként került hazánkba. Ecetfaként is emlegetik,
                    de csak hasonlít a bálványfára. Mindenhol megél, akár "gyomfának" is nevezhetnénk, ezért semmilyen növényvédelemben nem részesül.
                    Júniusban nyíló dús virágzata émelyítően nehéz illatú. Jó méhlegelő.</p>
                    <p class="">Méze csodálatosan zamatos, nyelvcsettintésre készteti az ínyenceket. Azon kívül ritkaság,
                    mivel az akáccal egy időben virágzik. Enyhén muskotályos ízű, kitűnő csemegeméz.</p>
                    <p class=" text-bold">Hatásai:</p>
                    <ul class="">
                    <li>- vesetisztító hatású</li>
                    <li>- emésztési zavarok ellen</li>
                    <li>- felső légúti megbetegedésekre</li>
                    <li>- gyomor karbantartó</li>
                    <li>- vérnyomás csökkentő</li>
                    <li>- májregeneráló</li>
                    <li>- epebántalmakra</li>
                    </ul>

                    <p class=" text-bold">ERDEI ÉDESHARMAT MÉZ</p>
                    <p class="">Az erdei méz tulajdonképpen egy vegyes méz, amit a méhek az erdőben éppen abban az
                    időben található növényekről (virágok, fák) gyűjtenek. Éppen ezért színe a világostól a
                    sötétig sokféle lehet. Például amelyikben sok a fenyő vagy a mézharmat, az sötétebb.
                    Íze is sokféle, de elmondható, hogy mindegyik aromásan kellemes.</p>
                    <p class=" text-bold">Hatásai:</p>
                    <ul class="">
                    <li>- fertőtlenítő</li>
                    <li>- szervezeterősítő</li>
                    <li>- sebgyógyító (külsőleg használva)</li>
                    <li>- ásványianyag- tartalma magas</li>
                    <li>- érrendszert karbantartja</li>
                    <li>- gyomorkarbantartó</li>
                    <li>- immunrendszer erősítő</li>
                    <li>- emésztést elősegítő</li>
                    </ul>

                    <p class="">BŐVEBBEN IS OLVASHAT:</p>
                    <a href="https://www.hazipatika.com/eletmod/termeszetes_gyogymodok/cikkek/gyogyito_mez_melyik_fajta_mire_jo/20151013145306" target="_blank" rel="nofollow">https://www.hazipatika.com/eletmod/termeszetes_gyogymodok/cikkek/gyogyito_mez_melyik_fajta_mire_jo/20151013145306</a>
                    <a href="http://xn--kogabi-vxa.hu/index.php/egeszseg/18-mezkonyv" target="_blank" rel="nofollow">http://xn--kogabi-vxa.hu/index.php/egeszseg/18-mezkonyv</a> 
                  </div>
                  <div class="tab-pane fade" id="biolekvarok">
                    <div class="range range-10">
                      <div class="cell-md-12">
                        <h6>Biolekvárok</h6>
                      </div>
                      <div class="cell-md-6">
                        <p>A bio termékek iránti kereslet évről-évre folyamatosan növekvő tendenciát mutat ma már hazánkban is. A vásárlók egy része egyre inkább tudatosan választja a vitaminokban és tápanyagokban gazdag, vegyszer-és adalékmentes élelmiszereket. A magyar biogazdaságok csak szigorúan és folyamatosan ellenőrzött körülmények között állítanak elő állati, illetve növényi eredetű termékeket, bio élelmiszereket.</p>
                        <p>Ugye neked is fontos családod egészsége?</p>
                        <p>A gondoskodó édesanyáknak mindig van pár titkos üveg a szekrényben különleges házi biolekvárjainkból, amelyeket felhasználva- akár pár perc alatt – lenyűgözően finom és egészséges finomságok kerülhetnek az asztalra. Megörvendeztetve ezzel a család apraját, de persze a nagyobbakat is. A lekváros üvegekbe nemcsak a természet kincsei kerülnek, hanem az a szeretet és gondoskodás is, amellyel készülnek.</p>                      
                      </div>                      
                      <div class="cell-md-6">
                        <img src="<?=$domain?>/images/szilvalekvar.jpg" class="img-responsive" alt="Szilvalekvár">
                      </div>
                      <div class="cell-md-12">
                        <p class="text-bold">Szilvalekvár</p>

                        <p>A szilva (Prunus domestica) a rózsafélék családjához tartozó csonthéjas gyümölcsünk.</p>
                        <p>Emésztési zavarok esetén baj lehet a vas lekötésével akkor is, ha vaskivonatot szedünk. A természetes forrásból, az aszalt szilvából, a vas felszívódása sokkal sikeresebb.
                         Nem hiába hívják a szilvát “kék gyümölcscsodának”, hiszen a benne levő mangán hatására a szervezetben az emésztést segítő enzimek lökést kapnak, és magas ballasztanyaga salaktalanítja a beleket, megszünteti a székrekedést. Megköti a felesleges zsírokat, mielőtt azok a bélből felszívódva felhalmozódnának a hasfalban, derékon, csípőtájékon, combon – tehát még a fogyni vágyók is nyugodtan ehetik.</p> 
                        <p>Idegrendszerre és agyműködésre:</p>
                        <p>A B-vitaminnak köszönhetően az idegrendszerre és az agyműködésre is jótékonyan hat. A benne lévő B-vitaminok nagyon fontosak az idegrendszer számára, ezáltal segítik az agy anyagcseréjét, így elősegítik a megfelelő gondolkodást, a memória működését.
                        Gazdag olyan B vitamin komplexekben, mint a niacin, a B6-vitamin és a pantoténsav, ezek együtt elősegítik a szervezetben a proteinek és a zsírok anyagcseréjét.
                        K-vitamint is tartalmaz, ami fontos a véralvadáshoz, a csontok fejlődéséhez és az Alzenheimer-kórban szenvedő betegek gyógyításához.</p>

                        <p>Értékes forrása az A vitaminnak és a béta-karotinnak. Az A vitaminnak jótékony hatása van a látásra, a bőrre és a nyálkahártyák egészséges megőrzésére.
                        A szilva gazdag olyan ásványi anyagokban, mint a kálium, vas és fluorid. A vasnak vörösvérsejtképző hatása van. A kálium a sejtek és a testnedvek fontos összetevője, ami szabályozza a szívverést és a vérnyomást.</p>
                        <p>Egyes megfigyelések alapján egy sor gyógyító tulajdonságát is megemlíthetjük. Csökkenti a köszvényesek reumások panaszait, jótékony a vese- és májbetegek diétájában. A szilva gyümölcssavai serkentik a nyál- és gyomorsavtermelést, étvágyfokozók is egyben. Indiai orvosok lázcsillapítónak is rendelik.</p>                      
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="liofilizalt-termekek">
                    <h6>Liofilizált termékek</h6>
                    <p>A liofilizálás, vagy kicsit magyarosabban “fagyasztva szárítás” vagy “víztelenítés”, a ma létező legkorszerűbb és legkíméletesebb fizikai tartósító eljárás. Lehetővé teszi, hogy a növényből a víz a lehető legkisebb, szinte elhanyagolható szövet- és sejtkárosodást okozva távozzon. Ezáltal a liofilizált termékeket egyedülállóan magas beltartalom és kiváló minőség jellemzi. Az eljárás során semmilyen kémiai adalékra nincsen szükség, hiszen a magas szárazanyag koncentráció következtében a mikroorganizmusok nem képesek szaporodni, ezért a liofilizált készítmények hosszú időn keresztül, légmentes csomagolásban romlás nélkül eltarthatók. Az így készült termékekben lévő beltartalmi értékek (pl. fehérjék, vitaminok és minden egyéb ásványi anyag), íz-, szín- és illatanyagok maradéktalanul megőrződnek.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

	<?php include $gyoker.'/module/mod_footer.php'; ?>

    </div>
	<?php include $gyoker.'/module/mod_body_end.php'; ?>

    <script type="text/javascript">
      window.onload = function(){  

          var url = document.location.toString();
          if (url.match('#')) {
              $('.nav-custom-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
          }

          //Change hash for page-reload
          $('.nav-custom-tabs a[href="#' + url.split('#')[1] + '"]').on('shown', function (e) {
              window.location.hash = e.target.hash;
          }); 
      }  

        $(function(){
          var hash = window.location.hash;
          hash && $('ul.nav a[href="' + hash + '"]').tab('show');

          $('.nav-custom-tabs a').click(function (e) {
            $(this).tab('show');
            var scrollmem = $('body').scrollTop() || $('html').scrollTop();
            window.location.hash = this.hash;
            $('html,body').scrollTop(scrollmem);
        });
      });     
    </script>  
  </body>
</html>