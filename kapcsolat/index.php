<?php 
	session_start();
	ob_start();
?>
<!DOCTYPE html>
<html class="wide smoothscroll wow-animation" lang="hu">
  <head>
  	<?php
  		$oldal = "kapcsolat";
  		include '../config.php';
      $title = $sz_kapcs_1;
      $description = $sz_kapcs_2;
  		include $gyoker.'/module/mod_head.php';
  	?>

      <title><?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">  	

      <script>
      function level_ellenorzes()
      {
        $('.alert').hide();
        $('.form-validation').remove();
        var alert = '<span class="form-validation"><?=$sz_kapcs_3?></span>';
        var alert2 = '<span class="form-validation"><?=$sz_kapcs_4?></span>';
        var hirl_oke = 0;
        if(document.getElementById("nev").value == '')
        {
          $('#nev').after(alert);
          var hirl_oke = 1;
        }
              
        if(document.getElementById("telefon").value == '')
        {
          $('#telefon').after(alert);
          var hirl_oke = 1;
        }

        
        if(document.getElementById("email").value == '')
        {
          $('#email').after(alert);
          var hirl_oke = 1;
        }

        if (!$('#gdpr').is(':checked')) {
          var hirl_oke = 1;        
          $('#gdrp-alert').show();
        }
       
        if(hirl_oke == 1)
        {
          return false;
        }
        else
        {
          $('#ell_div').html('<input type="hidden" name="ell_input" value="ok" />');
          return true;
        }
      }
    </script>         
  </head>
  <body>
  	<?php include $gyoker.'/module/mod_body_start.php'; ?>
    <!-- Page-->
    <div class="page">
      <!-- Page preloader-->

		<?php include $gyoker.'/module/mod_header.php'; ?>

      <section class="section section-lg bg-white">
        <div class="shell text-center">
          <div class="range range-40 text-left">
            <div class="cell-xs-12 cell-md-6">
              <h5><?=$sz_kapcs_5?></h5>
              <div class="contact-box">
                <div class="contact-item" style="max-width: 250px;">
                  <div class="contact-item-top">
                    <ul>
                      <li><span><?=$sz_kapcs_6?>:</span><a href="tel:+36706189013">+36 70 618 9013</a></li>
                    </ul>
                  </div>
                  <div class="contact-item-bottom"><a href="mailto:herbawin.webshop@gmail.com">herbawin.webshop@gmail.com</a></div>
                </div>
                <div class="info-item">
                  <ul>
                    <li>6722 Szeged,</li>
                    <li>Szentháromság utca 50.</li>
                  </ul>
                </div>
              </div>
              <a name="impresszum" class="anchor"></a>
              <h5 style="margin-top: 20px;"><?=$sz_kapcs_7?></h5>
                <p><b><?=$sz_kapcs_8?>:</b> Második Lehetőség Kereskedelmi Szociális Szövetkezet
                <br/><b><?=$sz_kapcs_9?>:</b> 06-02-000686
                <br/><b><?=$sz_kapcs_10?>:</b> 24375821-2-06
                <br/><b><?=$sz_kapcs_11?>:</b> 6725 Szeged, Alföldi utca 45. 1. em. 2.
                <br/><b><?=$sz_kapcs_12?>:</b> 6722 Szeged, Szentháromság utca 50. </p>
              <div style="margin-top: 20px;">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2759.303359058941!2d20.138588615583714!3d46.244196779117935!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4744887b74baffa5%3A0x6c60cdb873362dde!2sSzeged%2C+Szenth%C3%A1roms%C3%A1g+u.+50%2C+6722!5e0!3m2!1shu!2shu!4v1542869634813" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
              </div>
            </div>
            <div class="cell-xs-12 cell-md-6">

<?php 
  if (isset($_POST['nev']) && isset($_POST['ell_input']))
  {
  // Levélküldés
    $uzenet = '<p>Az alábbi üzenet érkezett a weboldalról:</p>
          <p>Név: '.$_POST['nev'].'
          <br>Telefon: '.$_POST['telefon'].'
          <br>E-mail: '.$_POST['email'].'';
    $uzenet .= '</p>';
    if (isset($_POST['uzenet']) && $_POST['uzenet'] != '')
    {
      $uzenet .= '<p>Üzenet:<br>'.$_POST['uzenet'].'</p>';
    }
    include $gyoker.'/module/mod_email_sajat.php';
    //phpMailer
    require_once($gyoker.'/webshop/PHPMailer-master/PHPMailerAutoload.php');
    $mail = new PHPMailer();
    $mail->isHTML(true);
    $mail->CharSet = 'UTF-8';
    if($conf_smtp == 1)
    {
      // SMTP
      $mail->IsSMTP(); // telling the class to use SMTP
      $mail->Host       = $smtp_host; // SMTP server
      $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
                             // 1 = errors and messages
                             // 2 = messages only
      $mail->SMTPAuth   = true;                  // enable SMTP authentication
      $mail->Host       = $smtp_host; // sets the SMTP server
      $mail->Port       = $smtp_port;                    // set the SMTP port for the GMAIL server
      $mail->Username   = $smtp_user; // SMTP account username
      $mail->Password   = $smtp_pass;        // SMTP account password
      $mail->SetFrom($smtp_email, $smtp_name);
      $mail->AddReplyTo($smtp_email, $smtp_name);
    }
    else
    {
      $mail->SetFrom($email, $webnev);
    }    
    $mail->AddAddress($email, $webnev);
    $mail->Subject = "Üzenet érkezett";
    $htmlmsg = $mess;
    $mail->Body = $htmlmsg;
    if(!$mail->Send()) {
      echo "Mailer Error: " . $mail->ErrorInfo;
    }

  }
?>       
              <?php if (isset($_POST['nev'])): ?>

                  <div class="alert alert-success">
                    <strong><?=$sz_kapcs_13?></strong> <?=$sz_kapcs_14?>
                  </div>  

              <?php else: ?>

              <h5><?=$sz_kapcs_15?></h5>
              <!-- RD Mailform-->
              <form method="post" action="" onsubmit="return level_ellenorzes()" style="margin-top: 17px;">
                <div class="range range-right range-10">
                  <div class="cell-sm-12">
                    <div class="form-wrap form-wrap-validation">
                      <label class="form-label" for="nev"><?=$sz_kapcs_18?></label>
                      <input class="form-input" id="nev" type="text" name="nev">
                    </div>
                  </div>
                  <div class="cell-sm-6">
                    <div class="form-wrap form-wrap-validation">
                      <label class="form-label" for="email">E-mail</label>
                      <input class="form-input" id="email" type="email" name="email">
                    </div>
                  </div>
                  <div class="cell-sm-6">
                    <div class="form-wrap form-wrap-validation">
                      <label class="form-label" for="telefon"><?=$sz_kapcs_19?></label>
                      <input class="form-input" id="telefon" type="text" name="telefon">
                    </div>
                  </div>
                  <div class="cell-sm-12">
                    <div class="form-wrap form-wrap-validation">
                      <label class="form-label" for="uzenet"><?=$sz_kapcs_20?></label>
                      <textarea class="form-input" id="uzenet" name="uzenet"></textarea>
                    </div>
                  </div>
                  <div class="cell-lg-12">                     
                    <label>
                      <input type="checkbox" id="gdpr" name="gdpr">
                      <?=$sz_regisztracio_35?> <a href="<?=$domain?>/adatkezelesi-tajekoztato" class="text-underline" target="_blank"><?=$sz_regisztracio_36?></a>, <?=$sz_regisztracio_37?>
                    </label>
                    <div class="alert alert-danger alert-dismissible" id="gdrp-alert" style="display: none;">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <?=$sz_kapcs_16?>
                    </div>                       
                  </div>                   
                </div>
                <div class="form-button">
                  <button class="button button-primary" type="submit"><?=$sz_kapcs_17?></button>
                </div>
                <div id="ell_div"></div>
              </form>

              <?php endif ?>
            </div>
          </div>
        </div>
      </section>

	<?php include $gyoker.'/module/mod_footer.php'; ?>

    </div>
	<?php include $gyoker.'/module/mod_body_end.php'; ?>
  </body>
</html>