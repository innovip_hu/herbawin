<?php 
	session_start();
	ob_start();
?>
<!DOCTYPE html>
<html class="wide smoothscroll wow-animation" lang="hu">
  <head>
  	<?php
  		$oldal = "kassza";
  		include '../config.php';
  		include $gyoker.'/module/mod_head.php';
  	?>

      <title><?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">  	
  </head>
  <body>
  	<?php include $gyoker.'/module/mod_body_start.php'; ?>
    <!-- Page-->
    <div class="page">
      <!-- Page preloader-->

		<?php include $gyoker.'/module/mod_header.php'; ?>

      <!-- Company overview-->
      <section class="section section-lg bg-white">
        <div class="shell">
            <?php include $gyoker.'/webshop/kassza_4.php'; ?>
        </div>
      </section>
      

	<?php include $gyoker.'/module/mod_footer.php'; ?>

    </div>
	<?php include $gyoker.'/module/mod_body_end.php'; ?>
  <script src="<?php print $domain; ?>/webshop/scripts/kassza_4.js" type="text/javascript"></script>
  </body>
</html>