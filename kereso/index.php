<?php 
	session_start();
	ob_start();
?>
<!DOCTYPE html>
<html class="wide smoothscroll wow-animation" lang="hu">
  <head>
  	<?php
  		$oldal = "kereso";
  		include '../config.php';
  		include $gyoker.'/module/mod_head.php';        
  	?>

      <title><?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">  	
  </head>
  <body>
  	<?php include $gyoker.'/module/mod_body_start.php'; ?>
    <!-- Page-->
    <div class="page">
      <!-- Page preloader-->

		<?php include $gyoker.'/module/mod_header.php'; ?>


      <section class="section section-lg bg-white">
        <div class="shell text-center">
          <div class="range range-30 text-left">
            <div class="cell-sm-3">
              <?php include $gyoker.'/module/mod_sidebar.php'; ?>
            </div>
            <div class="cell-sm-9">
                <?php
                  include $gyoker.'/webshop/kereso.php';
                ?>
            </div>
          </div>
        </div>
      </section>

	<?php include $gyoker.'/module/mod_footer.php'; ?>

    </div>
	<?php include $gyoker.'/module/mod_body_end.php'; ?>
<script>
  $(document).on("change", "#kereso_sorrend", function() {
    // $('#sorr_tip').val('ar_sorrend');
    $('#sorr_tip').val($('option:selected', '#kereso_sorrend').attr('attr_sorr_tip'));
    $('#sorrend').val($('option:selected', '#kereso_sorrend').attr('attr_sorrend'));
    document.getElementById('kereso_sorrend_form').submit();
  });
</script>
  </body>
</html>