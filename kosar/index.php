<?php 
	session_start();
	ob_start();
?>
<!DOCTYPE html>
<html class="wide smoothscroll wow-animation" lang="hu">
  <head>
  	<?php
  		$oldal = "kosar";
  		include '../config.php';
  		include $gyoker.'/module/mod_head.php';    
  	?>

      <title><?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">  	
  </head>
  <body>
  	<?php include $gyoker.'/module/mod_body_start.php'; ?>
    <!-- Page-->
    <div class="page">
      <!-- Page preloader-->

		<?php include $gyoker.'/module/mod_header.php'; ?>

      <!-- Company overview-->
      <section class="section section-lg bg-white">
        <div class="shell text-center">
          <div class="range range-30">
            <?php include $gyoker.'/webshop/kosar.php'; ?>
          </div>
        </div>
      </section>
      

	<?php include $gyoker.'/module/mod_footer.php'; ?>

    </div>
	<?php include $gyoker.'/module/mod_body_end.php'; ?>
  </body>
</html>