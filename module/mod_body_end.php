    <!-- PANEL-->
    <!-- END PANEL-->
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- PhotoSwipe Gallery-->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
            <button class="pswp__button pswp__button--share" title="Share"></button>
            <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
          <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Javascript-->
    <script src="<?=$domain?>/js/core.min.js"></script>
    <script src="<?=$domain?>/js/script.js"></script>
    <script src="<?=$domain?>/scripts/sajat.js"></script>


  <script>
    $(document).ready(function() {
      
      $(document).on("click",".currlink",function(){
        $('#valuta').val($(this).data("curr"));

        $('#currencyform').submit();
      }); 

      $('.filter_box h3').click(function(){
        $(this).next('div').animate({
          height: [ "toggle", "swing" ]
        }, 300);
        if ($('span',this).hasClass('fa-chevron-up'))
        {
          $('span',this).removeClass('fa-chevron-up').addClass('fa-chevron-down');
        }
        else
        {
          $('span',this).removeClass('fa-chevron-down').addClass('fa-chevron-up');
        }
      });
    });
  </script>  
  <script>
    $(document).on("change", "#szuro_form input[type=checkbox]", function() {
      szures();
    });
  </script>  
<?php
    include $gyoker.'/webshop/webshop.php';

?>
    


<div id="kinyilo_kosar_div">
  <?php
    include $gyoker.'/webshop/kinyilo_kosar.php';
  ?>
</div>     