      <!-- Page Footer-->
      <footer class="page-footer-default bg-image bg-image-21">
        <div class="shell">
          <div class="range range-30">
            <div class="cell-xs-12 cell-sm-6 cell-md-4 cell-lg-4">
              <h5 class="heading-1 heading-decorative"><?=$sz_lablec_1?></h5>
              <ul class="contact-list">
                <li>6722 Szeged, Szentháromság utca 50. </li>
                <li><a href="tel:+36706189013">+36 70 618 9013</a><br><span>E-mail:</span><a href="mailto:herbawin.webshop@gmail.com"> herbawin.webshop@gmail.com</a></li>
              </ul>
            </div>
            <div class="cell-xs-12 cell-sm-6 cell-md-4 cell-lg-4">
              <h4 class="heading-1 heading-decorative"><?=$sz_lablec_2?></h4>
              <ul class="list-marked text-primary text-bold">
                <li><a href="<?=$domain?>/aszf"><?=$sz_lablec_3?></a></li>
                <li><a href="<?=$domain?>/aszf/#szallitasi-feltetelek"><?=$sz_lablec_4?></a></li>
                <li><a href="https://ec.europa.eu/" target="_blank" rel="nofollow"><?=$sz_lablec_5?></a></li>
                <li><a href="<?=$domain?>/adatkezelesi-tajekoztato"><?=$sz_lablec_6?></a></li>
                <li><a href="<?=$domain?>/kapcsolat/#impresszum"><?=$sz_lablec_7?></a></li>
              </ul>
            </div>
            <div class="cell-xs-12 cell-sm-6 cell-md-4 cell-lg-4">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FHerbawin-298818777417873%2F&tabs&tabs=timeline&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=true&appId" width="100%" height="220" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
            </div>
          </div>
          <p class="copyright">Herbawin &#169; <span id="copyright-year"></span> | <?=$sz_lablec_8?>: <a href="https://www.innovip.hu/" target="_blank" rel="nofollow">Innovip.hu Kft.</a>
          </p>
        </div>
      </footer>

    <?php
// Cookie figyelmeztetés
    if(!isset($_COOKIE['oldal_ell']))
    {
        // setcookie('oldal_ell', 1, time() + (86400 * 150), "/");
        ?>
        <style>
            #cookie_div {
                background-color: rgba(51,51,51,0.8);
                border-radius: 0;
                box-shadow: 0 0 5px rgba(0,0,0,0.5);
                padding: 20px 0;
                position: fixed;
                left: 0;
                right: 0;
                bottom: 0;
                z-index: 9999;
                color: white;
                text-align: center;
            }
            .cookie_btn {
                background-color: #5c986a;
                padding: 3px 9px;
                cursor: pointer;
            }
        </style>
        <script>
            function cookieRendben() {
                // document.cookie="oldal_ell=1";
                document.getElementById('cookie_div').style.display = 'none';
                var expires;
                var days = 100;
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toGMTString();
                document.cookie = "oldal_ell=1" + expires + "; path=/";
            }
        </script>
        <div id="cookie_div" style=""><?=$sz_lablec_9?> <a href="<?php echo $domain; ?>/adatkezelesi-tajekoztato" style="color: white; text-decoration:underline;"><?=$sz_lablec_10?></a> <span class="cookie_btn" onClick="cookieRendben()"><?=$sz_lablec_11?></span></div>
        <?php
    }
?>     