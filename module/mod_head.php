    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="<?=$domain?>/images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:400,700%7CRoboto+Condensed:400,700%7CMerriweather:400,400i,700,700i">
    <link rel="stylesheet" href="<?=$domain?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?=$domain?>/css/style.css">
    <link rel="stylesheet" href="<?=$domain?>/css/update.css">
		<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
		<![endif]-->
	
<?php
  	// PDO
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	if($oldal=='termekek')
	{
		$description = 'Webáruházunk korszerű, jövőbe mutató, életminőség javító termékek kereskedelmével foglalkozik hozzájárulva az egészséges életmód megteremtéséhez.';
		$keywords = '';
		$title = 'Herbawin | Kendermagolaj, méz, biolekvárok, liofilizált termékek';
		// Dinamikus meta adatok
		if (isset($_GET['term_urlnev']) && $_GET['term_urlnev'] == '' && (isset($_GET['kat_urlnev']) || $_GET['kat_urlnev'] != '')) // Ha nincs termék kiválasztva
		{
			$res = $pdo->prepare("SELECT * FROM ".$webjel."term_csoportok where nev_url='".$_GET['kat_urlnev']."'");
			$res->execute();
			$row  = $res -> fetch();
			
			// Szűrő törlése
			if ((isset($_SESSION['csop_id']) && $_SESSION['csop_id'] != $row['id']) || isset($_SESSION['szuro_fooldal']))
			{
				unset($_SESSION['szuro_minimum_ar']);
				unset($_SESSION['szuro_maximum_ar']);
				unset($_SESSION['szuro_nem_ffi']);
				unset($_SESSION['szuro_nem_no']);
				unset($_SESSION['szuro_eletkor_fiatal']);
				unset($_SESSION['szuro_eletkor_felnott']);
				unset($_SESSION['szuro_eletkor_idos']);
				unset($_SESSION['termek_parameter_ertek']);
				unset($_SESSION['oldszam']);
				unset($_SESSION['keres_nev']);
			}
			//unset($_SESSION['szuro_fooldal']); // Ha belépett egy kategóriába, akkor a főoldali szűrőt töröljük
			$_SESSION['csop_id'] = $row['id'];
			
			// $description = mb_substr($row['leiras'],0,156,'UTF-8');
			// $keywords = $row['nev'];
			// $title = $row['nev'];
			$description = $row['seo_description'] ? $row['seo_description'] : mb_substr($row['leiras'],0,156,'UTF-8');
			//$keywords = $row['nev'];
			$title = $row['seo_title'] ? $row['seo_title'] : $row['nev'];
		}
		else if (isset($_GET['term_urlnev']) && $_GET['term_urlnev'] != '')// Termék is van
		{
			$res = $pdo->prepare("SELECT * FROM ".$webjel."termekek where nev_url='".$_GET['term_urlnev']."'");
			$res->execute();
			$row  = $res -> fetch();
			if(isset($row['id']))
			{
				// $description = mb_substr($row['rovid_leiras'],0,156,'UTF-8');
				$description = $row['seo_description'] ? $row['seo_description'] : mb_substr($row['rovid_leiras'],0,156,'UTF-8');
				//$keywords = $row['nev'];
				// $title = $row['nev'];
				$title = $row['seo_title'] ? $row['seo_title'] : $row['nev'];
				
				// Facebook megosztáshoz kép
				$row_kep = $pdo->query(""
						. "SELECT kep "
						. "FROM ".$webjel."termek_kepek "
						. "WHERE termek_id=".$row['id']." "
						. "ORDER BY alap DESC")
				->fetchColumn();
				if (!$row_kep)
				{
					$kep_link = ''.$domain.'/webshop/images/noimage.png';
				}
				else
				{
					$kep_link = ''.$domain.'/images/termekek/'.$row_kep;
				}
				print '<meta property="og:title" content="'.$title.'" />';
				print '<meta property="og:type" content="website"/>';
				print '<meta property="og:url" content="'.$domain.'/termekek/'.$_GET['kat_urlnev'].'/'.$_GET['term_urlnev'].'" />';
				print '<meta property="og:image" content="'.$kep_link.'" />';
				print '<meta property="og:description" content="'.$description.'" />';
			}
		}
		// Dublin core
		?>
		<meta name = "DC.Title" content = "<?php print $title; ?>">
		<meta name = "DC.Subject" content = "<?php echo $description; ?>">
		<meta name = "DC.Publisher" content = "<?php print $webnev; ?>">
		<meta name = "DC.Date" content = "<?php print date('Y-m-d'); ?>T<?php print date('h:i:s'); ?>">
		<meta name = "DC.Type" content = "website">
		<meta name = "DC.Format" content = "text/html">
		<meta name = "DC.Identifier" content = "<?php echo $domain.'/termekek/'.$_GET['kat_urlnev'].'/'.$_GET['term_urlnev']; ?>">
		<?php
		
	}
	else if($oldal=='hirek')
	{
		// Facebook megosztáshoz kép
		if (isset($_GET['nev_url']) && $_GET['nev_url'] != '')
		{
			$res = $pdo->prepare("SELECT * FROM ".$webjel."hirek WHERE nev_url='".$_GET['nev_url']."'");
			$res->execute();
			$row  = $res -> fetch();
			// Kép
			$query_kep = "SELECT * FROM ".$webjel."hir_kepek WHERE hir_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
			$res = $pdo->prepare($query_kep);
			$res->execute();
			$row_kep = $res -> fetch();
			$alap_kep = $row_kep['kep'];
			if ($alap_kep == '') 
			{
				// $kep_link = ''.$domain.'/webshop/images/noimage.png';
				$kep_link = '';
			}
			else
			{
				$kep_link = ''.$domain.'/images/termekek/'.$row_kep['kep'];
			}
			print '<meta property="og:locale" content="hu_HU" />';
			print '<meta property="og:title" content="'.$row['cim'].'" />';
			print '<meta property="og:type" content="website"/>';
			print '<meta property="og:url" content="'.$domain.'/hirek/'.$_GET['nev_url'].'" />';
			print '<meta property="og:image" content="'.$kep_link.'" />';
			print '<meta property="og:description" content="'.$row['elozetes'].'" />';
			print '<meta property="og:site_name" content="'.$webnev.'" />';
			// Dublin core
			?>
			<meta name = "DC.Title" content = "<?php print $row['cim']; ?>">
			<meta name = "DC.Subject" content = "<?php echo $row['elozetes']; ?>">
			<meta name = "DC.Publisher" content = "<?php print $webnev; ?>">
			<meta name = "DC.Date" content = "<?php print date('Y-m-d'); ?>T<?php print date('h:i:s'); ?>">
			<meta name = "DC.Type" content = "website">
			<meta name = "DC.Format" content = "text/html">
			<meta name = "DC.Identifier" content = "<?php echo $domain.'/hirek/'.$_GET['nev_url']; ?>">
			<?php

			$title = $row['cim'];
			$description = $row['elozetes'];			
		}
	}
	else if($oldal=='blog')
	{
		// Facebook megosztáshoz kép
		if (isset($_GET['nev_url']) && $_GET['nev_url'] != '')
		{
			$res = $pdo->prepare("SELECT * FROM ".$webjel."hirek2 WHERE nev_url='".$_GET['nev_url']."'");
			$res->execute();
			$row  = $res -> fetch();
			// Kép
			$query_kep = "SELECT * FROM ".$webjel."hir_kepek2 WHERE hir_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
			$res = $pdo->prepare($query_kep);
			$res->execute();
			$row_kep = $res -> fetch();
			$alap_kep = $row_kep['kep'];
			if ($alap_kep == '') 
			{
				// $kep_link = ''.$domain.'/webshop/images/noimage.png';
				$kep_link = '';
			}
			else
			{
				$kep_link = ''.$domain.'/images/termekek/'.$row_kep['kep'];
			}
			print '<meta property="og:locale" content="hu_HU" />';
			print '<meta property="og:title" content="'.$row['cim'].'" />';
			print '<meta property="og:type" content="website"/>';
			print '<meta property="og:url" content="'.$domain.'/hirek/'.$_GET['nev_url'].'" />';
			print '<meta property="og:image" content="'.$kep_link.'" />';
			print '<meta property="og:description" content="'.$row['elozetes'].'" />';
			print '<meta property="og:site_name" content="'.$webnev.'" />';
			// Dublin core
			?>
			<meta name = "DC.Title" content = "<?php print $row['cim']; ?>">
			<meta name = "DC.Subject" content = "<?php echo $row['elozetes']; ?>">
			<meta name = "DC.Publisher" content = "<?php print $webnev; ?>">
			<meta name = "DC.Date" content = "<?php print date('Y-m-d'); ?>T<?php print date('h:i:s'); ?>">
			<meta name = "DC.Type" content = "website">
			<meta name = "DC.Format" content = "text/html">
			<meta name = "DC.Identifier" content = "<?php echo $domain.'/hirek/'.$_GET['nev_url']; ?>">
			<?php

			$title = $row['cim'];
			$description = $row['elozetes'];			
		}
	}	
	else
	{
		?>
		<meta property='og:locale' content='hu_HU'>
		<meta property='og:type' content='<?php echo $og_type; ?>'/>
		<meta property='og:title' content='<?php print $title; ?>'>
		<meta property='og:description' content='<?php print $description; ?>'>
		<meta property='og:url' content='<?php echo "{$domain}/"; ?>'>
		<meta property='og:site_name' content='<?php print $title; ?>'>
		<?php
		// Dublin core
		?>
		<meta name = "DC.Title" content = "<?php print $title; ?>">
		<meta name = "DC.Subject" content = "<?php print $description; ?>">
		<meta name = "DC.Publisher" content = "<?php print $webnev; ?>">
		<meta name = "DC.Date" content = "<?php print date('Y-m-d'); ?>T<?php print date('h:i:s'); ?>">
		<meta name = "DC.Type" content = "website">
		<meta name = "DC.Format" content = "text/html">
		<meta name = "DC.Identifier" content = "<?php echo "{$domain}/"; ?>">
		<?php
	}
?>
	<script type="text/javascript" src="<?php print $domain; ?>/config.js"></script>
   
<?php 


		date_default_timezone_set('Europe/Budapest');

		if (isset($_POST['command']) && $_POST['command'] == 'LOGOUT')
		{
			unset($_SESSION['login_id']);
			unset($_SESSION['login_nev']);
			unset($_SESSION['login_tipus']);
		}
		if (!isset($_SESSION['login_id']))
		{
			if (isset($_POST['passwordText']))
			{

				$email= addslashes($_POST['email']);
				$query = "SELECT * FROM ".$webjel."users WHERE email = '$email'";
				$res = $pdo->prepare($query);
				$res->execute();
				$row  = $res -> fetch();
				$userpw = password_verify($_POST['passwordText'], $row['jelszo']);
				if ($userpw == true)
				{
					$_SESSION['login_id'] = $row['id'];
					$_SESSION['login_nev'] = $row['email'];
					if ($row['tipus'] == 'admin')
					{
						$_SESSION['login_tipus'] = 'admin';
					}
					elseif ($row['tipus'] == 'partner')
					{
						$_SESSION['login_tipus'] = 'partner';
						$_SESSION['partner_kedvezmeny'] = $row['kedvezmeny'];
					}
					
					unset($_SESSION['noreg_id']);
				}
				else
				{
					$uzenet = $sz_belepes_7;
				}
			}
		}


		// Kívánságlista törlése
		if (isset($_POST['command']) && $_POST['command'] == 'kivlistTORLES')
		{
			$deletecommand = "DELETE FROM ".$webjel."kivansag_lista WHERE id =".$_POST['id'];
			$result = $pdo->prepare($deletecommand);
			$result->execute();
		}
		
		//Kosár ürítése
		if (isset($_POST['command']) && $_POST['command'] == 'KOSAR_URIT')
		{
				$deletecommand = 'DELETE FROM '.$webjel.'kosar WHERE kosar_id='.$_SESSION['kosar_id'];
				$result = $pdo->prepare($deletecommand);
				$result->execute();
				unset($_SESSION['kosar_id']);
		}
		
		
?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W2HDTLL');</script>
<!-- End Google Tag Manager -->