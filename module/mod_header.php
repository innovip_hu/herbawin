      <div class="page-loader">
        <div>
          <div class="page-loader-body">
            <div class="cssload-loader">
              <div class="cssload-inner cssload-one"></div>
              <div class="cssload-inner cssload-two"></div>
              <div class="cssload-inner cssload-three"></div>
            </div>
          </div>
        </div>
      </div>
      <!-- Page Header-->
      <header class="section page-header">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-decorative" data-sm-stick-up-offset="1px" data-md-stick-up-offset="79px" data-lg-stick-up-offset="37px" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fullwidth" data-md-device-layout="rd-navbar-fullwidth" data-lg-device-layout="rd-navbar-static" data-lg-layout="rd-navbar-static" data-sm-stick-up="true" data-md-stick-up="true" data-lg-stick-up="true">
            <div class="rd-navbar-outer">
              <div class="rd-navbar-inner">
                <!-- RD Navbar Nav-->
                <div class="rd-navbar-item">
                  <div class="rd-navbar-column">
                    <div class="rd-navbar-nav-wrap">
                      <ul class="rd-navbar-nav rd-navbar-nav-lg">
                        <li <?php if($oldal == "cegunkrol"){echo 'class="active"';} ?>><a href="<?=$domain?>/cegunkrol"><?=$sz_menu_1?></a>
                        </li>
                        <li><a href="#"><?=$sz_menu_2?></a>
                            <ul class="rd-navbar-dropdown">
                              <?php
                                $query_csop = "SELECT * FROM ".$webjel."term_csoportok WHERE lathato = 1 AND csop_id = 0";
                                foreach ($pdo->query($query_csop) as $value_csop) { 

                                  $res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE lathato=1 AND csop_id=".$value_csop['id']." ORDER BY sorrend ASC");
                                  $res->execute();
                                  $rownum = $res->fetchColumn();

                                  ?>

                                  <li><a href="<?=$domain?>/termekek/<?=$value_csop['nev_url']?>/"><?=$value_csop['nev']?></a>
                                    <?php if ($rownum != 0): ?>
                                      <ul class="rd-navbar-dropdown">
                                        <?php
                                          $query_csop2 = "SELECT * FROM ".$webjel."term_csoportok WHERE lathato = 1 AND csop_id = ".$value_csop['id']." ORDER BY sorrend ASC";
                                          foreach ($pdo->query($query_csop2) as $value_csop2) { ?>
                                          <li><a href="<?=$domain?>/termekek/<?=$value_csop2['nev_url']?>/"><?=$value_csop2['nev']?></a></li>
                                        <?php
                                          }
                                        ?>
                                      </ul>
                                    <?php endif ?>
                                  </li>     

                              <?php    
                                }
                              ?>
                            </ul>                          
                        </li>
						            <li <?php if($oldal == "blog"){echo 'class="active"';} ?>><a href="<?=$domain?>/blog"><?=$sz_menu_3?></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="rd-navbar-column rd-navbar-column-bottom">
                    <ul class="navbar-inline-list">
                      <li class="rd-navbar-info rd-navbar-info-lg" style="font-weight: normal;">
                          <div class="sidepanel widget_search">
                            <?php include $gyoker.'/webshop/kereso_form.php'; ?>
                          </div>  
                      </li>
                    </ul>
                  </div>
                </div>
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-item rd-navbar-panel">
                  <!-- RD Navbar Toggle-->
                  <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav"><span class="toggle-icon"></span></button>
                  <!-- RD Navbar Brand--><a class="rd-navbar-brand" href="<?=$domain?>"><img class="brand-normal" src="<?=$domain?>/images/brand2-192x130.png" alt="logo" width="192" height="130"/><img class="brand-mini" src="<?=$domain?>/images/brand2-192x130.png" alt="logo" width="186" height="104"/></a>
                </div>
                <!-- RD Navbar Nav-->
                <div class="rd-navbar-item">
                  <div class="rd-navbar-column">
                    <div class="rd-navbar-nav-wrap">
                      <ul class="rd-navbar-nav rd-navbar-nav-lg">
                        <li <?php if($oldal == "informaciok" || $oldal == "aszf"){echo 'class="active"';} ?>><a href="#"><?=$sz_menu_4?></a>
                          <ul class="rd-navbar-dropdown">
                            <li><a href="<?=$domain?>/informaciok/#osszetevok"><?=$sz_menu_4_3?></a></li>
                            <?php /*                             
                            <li><a href="<?=$domain?>/informaciok/#kendermag">Kendermag</a></li>
                            <li><a href="<?=$domain?>/informaciok/#mez">Méz</a></li>
                            <li><a href="<?=$domain?>/informaciok/#biolekvarok">Biolekvár</a></li>
                            <li><a href="<?=$domain?>/informaciok/#liofilizalt-termekek">Liofilizált termékek</a></li>
                             */ ?>
                            <li><a href="<?=$domain?>/aszf"><?=$sz_menu_4_1?></a></li>
                            <li><a href="<?=$domain?>/aszf/#szallitasi-feltetelek"><?=$sz_menu_4_2?></a></li>
                          </ul>                        	
                        </li>                        
                        <li <?php if($oldal == "hirek"){echo 'class="active"';} ?>><a href="<?=$domain?>/aktualitasok"><?=$sz_menu_5?></a></li>
                        <li <?php if($oldal == "kapcsolat"){echo 'class="active"';} ?>><a href="<?=$domain?>/kapcsolat"><?=$sz_menu_6?></a></li>
                      </ul>
                      <div class="rd-navbar-nav-hidden">                         
                        <!-- RD Navbar Nav-->                          
                        <ul class="rd-navbar-nav">      
                          <div class="sidepanel widget_search">
                            <?php include $gyoker.'/webshop/kereso_form2.php'; ?>
                          </div>                                           
	                        <li <?php if($oldal == "cegunkrol"){echo 'class="active"';} ?>><a href="<?=$domain?>/cegunkrol"><?=$sz_menu_1?></a>
	                        </li>
	                        <li><a href="#"><?=$sz_menu_2?></a>
                            <ul class="rd-navbar-dropdown">
                              <?php
                                $query_csop = "SELECT * FROM ".$webjel."term_csoportok WHERE lathato = 1 AND csop_id = 0";
                                foreach ($pdo->query($query_csop) as $value_csop) { 

                                  $res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE lathato=1 AND csop_id=".$value_csop['id']." ORDER BY sorrend ASC");
                                  $res->execute();
                                  $rownum = $res->fetchColumn();

                                  ?>

                                  <li><a href="<?=$domain?>/termekek/<?=$value_csop['nev_url']?>/"><?=$value_csop['nev']?></a>
                                    <?php if ($rownum != 0): ?>
                                      <ul class="rd-navbar-dropdown">
                                        <?php
                                          $query_csop2 = "SELECT * FROM ".$webjel."term_csoportok WHERE lathato = 1 AND csop_id = ".$value_csop['id']." ORDER BY sorrend ASC";
                                          foreach ($pdo->query($query_csop2) as $value_csop2) { ?>
                                          <li><a href="<?=$domain?>/termekek/<?=$value_csop2['nev_url']?>/"><?=$value_csop2['nev']?></a></li>
                                        <?php
                                          }
                                        ?>
                                      </ul>
                                    <?php endif ?>
                                  </li>     

                              <?php    
                                }
                              ?>
                            </ul> 
	                        </li>
							            <li><a href="#"><?=$sz_menu_3?></a></li>
	                        <li><a href="#"><?=$sz_menu_4?></a>
	                          <ul class="rd-navbar-dropdown">
	                            <li><a href="<?=$domain?>/informaciok/#osszetevok"><?=$sz_menu_4_3?></a></li>
                              <?php /*
	                            <li><a href="<?=$domain?>/informaciok/#mez">Méz</a></li>
	                            <li><a href="<?=$domain?>/informaciok/#biolekvarok">Biolekvár</a></li>
	                            <li><a href="<?=$domain?>/informaciok/#liofilizalt-termekek">Liofilizált termékek</a></li>
                              */ ?>
                              <li><a href="<?=$domain?>/aszf"><?=$sz_menu_4_1?></a></li>
                              <li><a href="<?=$domain?>/aszf/#szallitasi-feltetelek"><?=$sz_menu_4_2?></a></li>
	                          </ul>                        	
	                        </li>                        
	                        <li><a href="<?=$domain?>/aktualitasok"><?=$sz_menu_5?></a></li>
	                        <li><a href="<?=$domain?>/kapcsolat"><?=$sz_menu_6?></a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!-- Collapse toggle-->
                  <button class="collapse-toggle" data-rd-navbar-toggle=".rd-navbar-panel-mobile"><span class="toggle-icon"></span></button>
                  <div class="rd-navbar-column rd-navbar-column-bottom rd-navbar-panel-mobile">
                    <ul class="navbar-inline-list">
                      <?php /*
                      <li>
                        <form id="currencyform" action="" method="post">
                          <input type="hidden" name="valuta" id="valuta" value="" />
                        </form>                             
                        <ul class="list-inline valutalinkek">
                          <li><a class="currlink <?php if (isset($_SESSION['valuta']) && $_SESSION['valuta'] == '') {echo 'active';} ?>" data-curr="">HUF</a></li>
                          <li><a class="currlink <?php if (isset($_SESSION['valuta']) && $_SESSION['valuta'] == 'eur') {echo 'active';} ?>" data-curr="eur">EUR</a></li>
                        </ul>                        
                      </li>
                      */ ?>
                      <?php if (isset($_SESSION['login_id'])): ?>
                        <li class="rd-navbar-info padding-jobb">
                          <div class="icon icon-xs fa fa-sign-out"></div><a href="<?=$domain?>/webshop/logout.php"><?=$sz_menu_7?></a>
                        </li>
                        <li class="rd-navbar-info padding-bal">
                          <div class="icon icon-xs fa fa-user"></div><a href="<?=$domain?>/profil"><?=$sz_menu_8?></a>
                        </li>                        
                      <?php else: ?>
                      <li class="rd-navbar-info padding-jobb">
                        <div class="icon icon-xs fa fa-user"></div><a href="<?=$domain?>/belepes"><?=$sz_menu_9?></a>
                      </li>
                      <li class="rd-navbar-info padding-bal">
                        <div class="icon icon-xs fa fa-user-plus"></div><a href="<?=$domain?>/regisztracio"><?=$sz_menu_10?></a>
                      </li>                        
                      <?php endif ?>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>
