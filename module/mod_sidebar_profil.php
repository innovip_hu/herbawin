	<div class="table-responsive">
	  <table class="table table-bordered table-hover text-center text-gray">
	  	<tbody>
	  		<?php if ($_SESSION['login_tipus'] == 'partner'): ?>
	  		<tr>
	  			<td style="cursor: pointer;" <?php if (isset($_GET['command']) && $_GET['command'] == 'hirek' || isset($_GET['nev_url'])) {echo 'class="text-secondary text-bold"';} ?> onclick="window.location.href='<?=$domain?>/profil/?command=hirek'">Hírek</td>
	  		</tr>	  		
			<?php endif ?>
			<?php if ($_SESSION['login_tipus'] == 'ugynok'): ?>
	  		<tr>
	  			<td style="cursor: pointer;" <?php if (isset($_GET['command']) && $_GET['command'] == 'statisztika') {echo 'class="text-secondary text-bold"';} ?> onclick="window.location.href='<?=$domain?>/profil/?command=statisztika'">Statisztika</td>
	  		</tr>
	  		<?php endif ?>
	  		<tr>
	  			<td style="cursor: pointer;" <?php if (isset($_GET['command']) && $_GET['command'] == 'profil' || !isset($_GET['command']) && !isset($_GET['nev_url'])) {echo 'class="text-secondary text-bold"';} ?> onclick="window.location.href='<?=$domain?>/profil/?command=profil'"><?=$sz_term_kat_8?></td>
	  		</tr>
	  		<tr>
	  			<td style="cursor: pointer;" <?php if (isset($_GET['command']) && $_GET['command'] == 'rendelesek') {echo 'class="text-secondary text-bold"';} ?> onclick="window.location.href='<?=$domain?>/profil/?command=rendelesek'"><?=$sz_term_kat_9?></td>
	  		</tr>	  			  		  			  		
	  		<tr>
	  			<td style="cursor: pointer;" onclick="window.location.href='<?=$domain?>/webshop/logout.php'"><?=$sz_term_kat_10?></td>
	  		</tr>	  		
	  	</tbody>
	  </table>
	</div>	