<?php 
	session_start();
	ob_start();
?>
<!DOCTYPE html>
<html class="wide smoothscroll wow-animation" lang="hu">
  <head>
  	<?php
  		$oldal = "profil";
  		include '../config.php';
  		include $gyoker.'/module/mod_head.php';

      if (!isset($_SESSION['login_id']))
      {
        header('Location:' .$domain.'/belepes/');
      }      
  	?>

      <title><?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">  	
  </head>
  <body>
  	<?php include $gyoker.'/module/mod_body_start.php'; ?>
    <!-- Page-->
    <div class="page">
      <!-- Page preloader-->

		<?php include $gyoker.'/module/mod_header.php'; ?>

      <section class="section section-lg bg-white">
        <div class="shell text-center">
          <div class="range range-30 text-left">
            <div class="cell-sm-3">
              <?php include $gyoker.'/module/mod_sidebar_profil.php'; ?>
            </div>
            <div class="cell-sm-9">
                <?php
                if (isset($_GET['command']) && $_GET['command'] == 'profil')
                {
                  include $gyoker.'/webshop/profil.php';
                }
                else if (isset($_GET['nev_url']))
                {
                  include $gyoker.'/webshop/hirek2.php'; 
                }
                elseif (isset($_GET['command']) && $_GET['command'] == 'hirek' && $_SESSION['login_tipus'] == 'partner')
                {
                  include $gyoker.'/webshop/hirek2.php';
                }                  
                elseif (isset($_GET['command']) && $_GET['command'] == 'rendelesek')
                {
                  include $gyoker.'/webshop/rendelesek.php';
                }    
                elseif (isset($_GET['command']) && $_GET['command'] == 'statisztika')
                {
                  include $gyoker.'/webshop/statisztikak.php';
                }                                                                                
                else
                {
                  include $gyoker.'/webshop/profil.php';
                }  
                ?>
            </div>
          </div>
        </div>
      </section>

	<?php include $gyoker.'/module/mod_footer.php'; ?>

    </div>
	<?php include $gyoker.'/module/mod_body_end.php'; ?> 
  <script>
    $(document).on('click', '.lenyito_sor', function(){
      var rendeles_id = $(this).attr('attr_rendeles_id');
      $('.lenyilo_sor').each(function() {
        if($(this).attr('attr_rendeles_id') == rendeles_id) {
          $(this).animate({
            height: [ "toggle", "swing" ]
          }, 300);
        }
      }); 
    });

    $(document).on('click','#mascim',function(){
      $('#eltero_szallitas').slideToggle();
    });
  </script>  
  </body>
</html>