/*-----------------------------------------------------------------------------------*/
/*	Kateg�ria lenyit�
/*-----------------------------------------------------------------------------------*/
 $(window).on("load", function() {
	$( "#mobil_kat_lenyito" ).click(function() {
		$( "#kategoria_menu" ).animate({
			height: [ "toggle", "swing" ]
		}, 300);
	});
});

/*-----------------------------------------------------------------------------------*/
/*	fekv� term�kek divje ugyanolyan magas
/*-----------------------------------------------------------------------------------*/
// Term�kek felsorol�sa a kateg�ri�ban
 $(window).on("load", function() {
    $('.termek_hosszu').each(function(){  
        var highestBox = 0;
        $('.term_hosszu_tulajd', this).each(function(){

            if($(this).height() > highestBox) 
               highestBox = $(this).height(); 
        });  
        $('.term_hosszu_tulajd',this).height(highestBox);
	});  
});
// Hasonl� term�kek k�pei
 $(window).on("load", function() {
    $('.kapcs_termekek').each(function(){  
        var highestBox = 0;
        $('.fels_termek_kep', this).each(function(){

            if($(this).height() > highestBox) 
               highestBox = $(this).height(); 
        });  
        $('.fels_termek_kep',this).height(highestBox);
	});  
});
// Hasonl� term�kek nevei
 $(window).on("load", function() {
    $('.kapcs_termekek').each(function(){  
        var highestBox = 0;
        $('.fels_termek_nev', this).each(function(){

            if($(this).height() > highestBox) 
               highestBox = $(this).height(); 
        });  
        $('.fels_termek_nev',this).height(highestBox);
	});  
});
// Hasonl� term�kek div
 $(window).on("load", function() {
    $('.kapcs_termekek').each(function(){  
        var highestBox = 0;
        $('.fels_termek_div', this).each(function(){

            if($(this).height() > highestBox) 
               highestBox = $(this).height(); 
        });  
        $('.fels_termek_div',this).height(highestBox);
	});  
});
 $(window).on("load", function() {
    $('.kapcs_termekek').each(function(){  
        var highestBox = 0;
        $('.title', this).each(function(){

            if($(this).height() > highestBox) 
               highestBox = $(this).height(); 
        });  
        $('.title',this).height(highestBox);
	});  
}); 
 $(window).on("load", function() {
	termekekIgazitasa()
});
// Term�k k�pek �s sz�vegek igaz�t�sa
	function termekekIgazitasa() {
		// felsorol�sban n�gyzetes termek k�pek 
		$('#termekek_ajax_div').each(function(){  
			var highestBox = 0;
			$('.fels_termek_kep', this).each(function(){

				if($(this).height() > highestBox) 
				   highestBox = $(this).height(); 
			});  
			$('.fels_termek_kep',this).height(highestBox);
		});  
		// felsorol�sban n�gyzetes termek adatok 
		$('#termekek_ajax_div').each(function(){  
			var highestBox = 0;
			$('.fels_termek_adatok', this).each(function(){

				if($(this).height() > highestBox) 
				   highestBox = $(this).height(); 
			});  
			$('.fels_termek_adatok',this).height(highestBox);
		}); 
		$('#termekek_ajax_div').each(function(){  
			var highestBox = 0;
			$('.title', this).each(function(){

				if($(this).height() > highestBox) 
				   highestBox = $(this).height(); 
			});  
			$('.title',this).height(highestBox);
		});		 
	}
