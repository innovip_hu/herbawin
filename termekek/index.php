<?php 
	session_start();
	ob_start();
?>
<!DOCTYPE html>
<html class="wide smoothscroll wow-animation" lang="hu">
  <head>
  	<?php
  		$oldal = "termekek";
  		include '../config.php';
      $title = $sz_term_7;
      $description = $sz_term_8;
  		include $gyoker.'/module/mod_head.php';
  	?>

      <title><?php print $title; ?></title>
      <meta name="description" content="<?php print $description; ?>">  	
      <link rel="stylesheet" href="<?php print $domain; ?>/webshop/css/rangeslider.css">
  </head>
  <body>
  	<?php include $gyoker.'/module/mod_body_start.php'; ?>
    <!-- Page-->
    <div class="page">
      <!-- Page preloader-->

    <?php 
      if (isset($_GET['term_urlnev']) && $_GET['term_urlnev'] != '' )
      {
        if(isset($_SESSION['ut_term_1']) && $_GET['term_urlnev'] == $_SESSION['ut_term_1'])
        {}
        else if(isset($_SESSION['ut_term_2']) && $_GET['term_urlnev'] == $_SESSION['ut_term_2'])
        {}
        else if(isset($_SESSION['ut_term_3']) && $_GET['term_urlnev'] == $_SESSION['ut_term_3'])
        {}
        else
        {
          if(isset($_SESSION['ut_term_2']))
          {
            $_SESSION['ut_term_3'] = $_SESSION['ut_term_2'];
          }
          if(isset($_SESSION['ut_term_1']))
          {
            $_SESSION['ut_term_2'] = $_SESSION['ut_term_1'];
          }
          $_SESSION['ut_term_1'] = $_GET['term_urlnev'];
        }
      }
    ?>

		<?php include $gyoker.'/module/mod_header.php'; ?>

    <?php /*    <section class="section section-bredcrumbs bg-image-breadcrumbs-1">
        <div class="shell">
          <div class="range range-center">
            <div class="cell-sm-10 cell-xl-8">
              <div class="breadcrumb-wrapper"><img src="<?=$domain?>/images/image-icon-1-49x43.png" alt="" width="49" height="43"/>
                <?php 
                    if (isset($_GET['kat_urlnev'])) //Termék csoportnév kiíratása
                    {
                      
                      $szulkat = '';
                      $szul_szulkat = '';
                      $szul_szul_szulkat = '';
                      $szulkat_nev = '';
                      $szul_szulkat_nev = '';
                      $szul_szul_szulkat_nev = '';
                      if (isset($_GET['kat_urlnev']))
                      {
                        $query = "SELECT * FROM ".$webjel."term_csoportok where nev_url='".$_GET['kat_urlnev']."'";
                        $res = $pdo->prepare($query);
                        $res->execute();
                        $row  = $res -> fetch();
                        $szulkat = $row['csop_id'];
                        $szulkat_nev = $row['nev'];
                        $szulkat_url = $row['nev_url'];
                        if ($szulkat > 0)
                        {
                          $query = "SELECT * FROM ".$webjel."term_csoportok where id=".$szulkat;
                          $res = $pdo->prepare($query);
                          $res->execute();
                          $row  = $res -> fetch();
                          $szul_szulkat = $row['csop_id'];
                          $szul_szulkat_nev = $row['nev'];
                          $szul_szulkat_url = $row['nev_url'];
                          if ($szul_szulkat != 0 || $szul_szulkat != '')
                          {
                            $query = "SELECT * FROM ".$webjel."term_csoportok where id=".$szul_szulkat;
                            $res = $pdo->prepare($query);
                            $res->execute();
                            $row  = $res -> fetch();
                            $szul_szul_szulkat = $row['csop_id'];
                            $szul_szul_szulkat_nev = $row['nev'];
                            $szul_szul_szulkat_url = $row['nev_url'];
                          }
                        }
                      }
                      
                      $res = $pdo->prepare("SELECT * FROM ".$webjel."term_csoportok where nev_url='".$_GET['kat_urlnev']."'");
                      $res->execute();
                      $row  = $res -> fetch();
                      // if(!isset($_GET['term_urlnev']) || $_GET['term_urlnev'] == '')
                      // {
                        print '<h2>'.$row['nev'].'</h2>';
                      // }
                      print '<ol class="breadcrumbs-custom">';
                        if($szul_szul_szulkat_nev != '' && $_GET['kat_urlnev'] != $szul_szul_szulkat_nev)
                        {
                          print '<li><a href="'.$domain.'/termekek/'.$szul_szul_szulkat_url.'/">'.$szul_szul_szulkat_nev.'</a></li>';
                        }
                        if($szul_szulkat_nev != '' && $_GET['kat_urlnev'] != $szul_szul_szulkat_nev)
                        {
                          print '<li><a href="'.$domain.'/termekek/'.$szul_szulkat_url.'/">'.$szul_szulkat_nev.'</a></li>';
                        }
                        if($szulkat_nev != '' && $_GET['kat_urlnev'] != $szul_szul_szulkat_nev)
                        {
                          print '<li><a href="'.$domain.'/termekek/'.$szulkat_url.'/">'.$szulkat_nev.'</a></li>';
                        }
                      print '</ol>';
                    }
                    else
                    { ?>
                        <h2>Termékek</h2>
                        <ol class="breadcrumbs-custom">
                          <li><a href="<?=$domain?>">Főoldal</a></li>
                          <li>Termékek
                          </li>
                        </ol>       

                        <?php               
                    }
                ?>
              </div>
            </div>
          </div>
        </div>
      </section>
      */ ?>

      <section class="section section-lg bg-white">
        <div class="shell text-center">
          <div class="range range-30 text-left">
            <?php if (isset($_GET['term_urlnev']) && $_GET['term_urlnev'] != ''): ?>
            <div class="cell-sm-12">
            <?php else: ?>
            <div class="cell-sm-3">
              <?php include $gyoker.'/module/mod_sidebar.php'; ?>
            </div>
            <div class="cell-sm-9">
            <?php endif ?>
              <?php include $gyoker.'/webshop/termekek.php'; ?>
            </div>
          </div>
        </div>
      </section>

	<?php include $gyoker.'/module/mod_footer.php'; ?>

    </div>
	<?php include $gyoker.'/module/mod_body_end.php'; ?>
  <script src="<?php print $domain; ?>/webshop/scripts/rangeslider.min.js"></script>  
  <script src="<?php print $domain; ?>/webshop/scripts/termekek.js" type="text/javascript"></script>
      <script>
      $(document).ready(function(){
      $(function() {

        var $document   = $(document),
          selector    = '[data-rangeslider]',
          $element    = $(selector);

        // Example functionality to demonstrate a value feedback
        function valueOutput(element) {
          var value = element.value,
            output = element.parentNode.getElementsByTagName('output')[0];
          output.innerHTML = value;
        }
        for (var i = $element.length - 1; i >= 0; i--) {
          valueOutput($element[i]);
        };
        $document.on('change', 'input[type="range"]', function(e) {
          valueOutput(e.target);
        });

        // Example functionality to demonstrate disabled functionality
        $document .on('click', '#js-example-disabled button[data-behaviour="toggle"]', function(e) {
          var $inputRange = $('input[type="range"]', e.target.parentNode);

          if ($inputRange[0].disabled) {
            $inputRange.prop("disabled", false);
          }
          else {
            $inputRange.prop("disabled", true);
          }
          $inputRange.rangeslider('update');
        });

        // Example functionality to demonstrate programmatic value changes
        $document.on('click', '#js-example-change-value button', function(e) {
          var $inputRange = $('input[type="range"]', e.target.parentNode),
            value = $('input[type="number"]', e.target.parentNode)[0].value;

          $inputRange.val(value).change();
        });

        // Example functionality to demonstrate destroy functionality
        $document
          .on('click', '#js-example-destroy button[data-behaviour="destroy"]', function(e) {
            $('input[type="range"]', e.target.parentNode).rangeslider('destroy');
          })
          .on('click', '#js-example-destroy button[data-behaviour="initialize"]', function(e) {
            $('input[type="range"]', e.target.parentNode).rangeslider({ polyfill: false });
          });

        // Basic rangeslider initialization
        $element.rangeslider({

          // Deactivate the feature detection
          polyfill: false,

          // Callback function
          onInit: function() {},

          // Callback function
          onSlide: function(position, value) {
            //console.log('onSlide');
            //console.log('position: ' + position, 'value: ' + value);
          },

          // Callback function
          onSlideEnd: function(position, value) {
            //console.log('onSlideEnd');
            //console.log('position: ' + position, 'value: ' + value);
            
            // document.getElementById('szuro_form').submit();
            szures();
          }
        });

      });
      });
      </script>  
        <script src="<?= $domain ?>/webshop/scripts/jquery.swipebox.js"></script>
        <script type="text/javascript">
        $( document ).ready(function() {
          /* Basic Gallery */
          $( '.swipebox' ).swipebox();
          });
        </script>      
  </body>
</html>