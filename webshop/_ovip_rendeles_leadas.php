<?php
	// $rendeles_id = 51; // ezt a változót meg kell adni az include előtt
	
	include '../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	// Rendelés adatai
	$query = "SELECT * FROM ".$webjel."rendeles WHERE id=".$rendeles_id;
	$res = $pdo->prepare($query);
	$res->execute();
	$row = $res -> fetch();
	// Felhasználó
	if ($row['noreg'] == 0)//regisztrált vevő
	{
		$query_user = "SELECT * FROM ".$webjel."users where id=".$row['user_id'];
	}
	else //noreg vevő
	{
		$query_user = "SELECT * FROM ".$webjel."users_noreg where id=".$row['user_id'];
	}
	$res = $pdo->prepare($query_user);
	$res->execute();
	$row_user  = $res -> fetch();
	$rendeles = array('rendeles_id' => $rendeles_id, 
					'vasarlo_nev' => $row['vasarlo_nev'],
					'vasarlo_irszam' => $row['vasarlo_irszam'],
					'vasarlo_varos' => $row['vasarlo_varos'],
					'vasarlo_cim' => $row['vasarlo_cim'],
					'vasarlo_adoszam' => $row['vasarlo_adoszam'],
					'vasarlo_email' => $row['vasarlo_email'],
					'vasarlo_telefon' => $row_user['telefon'],
					'vasarlo_szall_nev' => $row_user['cim_szall_nev'],
					'vasarlo_szall_varos' => $row_user['cim_szall_varos'],
					'vasarlo_szall_cim' => $row_user['cim_szall_utca'].' '.$row_user['cim_szall_hszam'],
					'vasarlo_szall_irszam' => $row_user['cim_szall_irszam'],
					'datum' => $row['datum'],
					'fiz_mod' => $row['fiz_mod'],
					'szall_mod' => $row['szall_mod'],
					'szallitasi_dij' => $row['szallitasi_dij'],
					'szallitasi_dij_afa' => $row['szallitasi_dij_afa'],
					'megjegyzes' => $row['megjegyzes'],
					'szamla_megjegyzes' => $row['szamla_megjegyzes']);
	// Rendelés tételei
	$query_tetel = "SELECT * FROM ".$webjel."rendeles_tetelek WHERE rendeles_id=".$row['id'];
	foreach ($pdo->query($query_tetel) as $row_tetel)
	{
		$query_term = "SELECT * FROM ".$webjel."termekek WHERE id=".$row_tetel['term_id'];
		$res = $pdo->prepare($query_term);
		$res->execute();
		$row_term = $res -> fetch();

		if($row_tetel['term_akcios_ar'] > 0) // Ha akciós
		{
			$term_ar = $row_tetel['term_akcios_ar'];
		}
		else
		{
			$term_ar = $row_tetel['term_ar'];
		}
		$rendeles['tetelek'][] = array('termek_nev' => $row_tetel['term_nev'], 
								'brutto_ar' => $term_ar, 
								'mennyiseg' => $row_tetel['term_db'],
								'ovip_id' => $row_term['ovip_id'],
								'afa' => $row_tetel['afa']);
	}

	// Küldés
	$target_url = 'http://localhost/ovip2/webaruhaz_rendeles_leadasa.php';
	$post = array('authCode' => $ovip_authCode, 'rendeles' => http_build_query($rendeles));
    $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$target_url);
	curl_setopt($ch, CURLOPT_POST,1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	$result=curl_exec ($ch);
	curl_close ($ch);
	
	// echo $result;
	
	// Log fájl létrehozása
	// parse_str($rendeles, $rendeles_tomb);
	$file = $gyoker.'/webshop/log_rendelesek/'.date('Ymd-His').'_'.$rendeles_id.'_'.$result.'.log';
	// $current = $result.' '.$rendeles_tomb;
	$current =  print_r($rendeles, true);
	file_put_contents($file, $current);

	// $logtext = print_r($product, true);
	// file_put_contents(Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR.'emag'.DIRECTORY_SEPARATOR.date('Ymd-His').'_'.$this->id.'.log', $logtext, FILE_APPEND | LOCK_EX);
?>
