<?php

	require_once ("Facebook/autoload.php");

	$FB = new \Facebook\Facebook([
		'app_id' => $conf_facebook_apikey,
		'app_secret' => $conf_facebook_secret,
		'default_graph_version' => 'v2.10'
		]);
	$helper = $FB->getRedirectLoginHelper();

?>