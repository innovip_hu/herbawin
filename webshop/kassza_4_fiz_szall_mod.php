<div style="color:red;" class="col-lg-12 col-md-12 riaszt_div" id="fiz_mod_riaszt"><div class="alert alert-warning alert-danger"><?=$sz_kassza_22?></div></div>
<?php
		$query_beall = 'SELECT * FROM '.$webjel.'beallitasok WHERE id=1';
		$res = $pdo->prepare($query_beall);
		$res->execute();
		$row_beall = $res -> fetch();

	//Előre utalás
		echo '<div class="col-md-6" style="margin-top:10px;"><h5 style="margin-bottom:10px;">'.$sz_kassza_23.'</h5>
			<select name="fiz_mod" id="fiz_mod_select" class="kassza_select" data-kosar="'.$_SESSION['kosar_id'].'">';
				$query2 = "SELECT * FROM ".$webjel."kassza_fiz_mod WHERE lathato=1 ORDER BY sorrend ASC";
				foreach ($pdo->query($query2) as $row2)
				{
					$select = '';
					if($row2['alap'] == 1)
					{
						$select = 'selected';
						$fiz_leiras = $row2['leiras'.$_SESSION['lang_sql']];
						$fiz_utanvet = $row2['utanvet'];
						$alap_fiz_mod = $row2['nev'.$_SESSION['lang_sql']];
					}
					echo '<option value="'.$row2['id'].'" attr_utanvet="'.$row2['utanvet'].'" attr_leiras="'.htmlentities($row2['leiras'.$_SESSION['lang_sql']]).'" '.$select.'>'.$row2['nev'.$_SESSION['lang_sql']].'</option>';
				}
			echo '</select>
			<p id="fiz_leiras_p">'.$fiz_leiras.'</p>
		</div>';
		echo '<div class="col-md-6" style="margin-top:10px;"><h5 style="margin-bottom:10px;">'.$sz_kassza_24.'</h5>
			<select name="szall_mod" id="szall_mod_select" class="kassza_select" data-kosar="'.$_SESSION['kosar_id'].'">';
				$query2 = "SELECT * FROM ".$webjel."kassza_szall_mod WHERE lathato=1 ORDER BY sorrend ASC";
				foreach ($pdo->query($query2) as $row2)
				{
					$select = '';
					if($row2['alap'] == 1)
					{
						$select = 'selected';
						$szall_leiras = $row2['leiras'.$_SESSION['lang_sql']];
						$alap_szall_mod = $row2['nev'.$_SESSION['lang_sql']];

						if ($row2['suly_alap'] == 1)
						{
							$suly = 0;
							$query = "SELECT term_id, term_db FROM ".$webjel."kosar WHERE kosar_id=".$_SESSION['kosar_id'];		
							foreach ($pdo->query($query) as $key => $value)
							{
								$query_term = "SELECT suly FROM ".$webjel."termekek where id=".$value['term_id'];
								$res = $pdo->prepare($query_term);
								$res->execute();
								$row_term = $res -> fetch();

								$suly += $value['term_db'] * $row_term['suly'];
							}

							$res = $pdo->prepare('SELECT * FROM '.$webjel.'szall_suly WHERE max_suly>='.$suly.' ORDER BY max_suly ASC LIMIT 1 ');
							$res->execute();
							$row_suly  = $res -> fetch();

							$szall_kolts = $row_suly['ar'];							
						}
						else
						{
							if($fiz_utanvet == 1)
							{
								$szall_kolts = $row2['ar_utanvet'];
							}
							else
							{
								$szall_kolts = $row2['ar'];
							}
						}						
					}

					$query_kosar = "SELECT SUM(IF(term_akcios_ar > 0, (term_akcios_ar*term_db), (term_ar*term_db))) AS szumma FROM ".$webjel."kosar WHERE kosar_id=".$_SESSION['kosar_id'];
					$res = $pdo->prepare($query_kosar);
					$res->execute();
					$row_kosar = $res -> fetch();		
					

					if ($row_beall['ingyenes_szallitas'] > 0 && $row_kosar['szumma'] >= $row_beall['ingyenes_szallitas'])
					{
						$szall_kolts = 0;
					}

					echo '<option value="'.$row2['id'].'" attr_leiras="'.$row2['leiras'.$_SESSION['lang_sql']].'" '.$select.'>'.$row2['nev'.$_SESSION['lang_sql']].'</option>';
				}
			echo '</select>
			<p id="szall_leiras_p">'.$szall_leiras.'</p>';

		print '<div class="col-lg-12 col-md-12 riaszt_div" id="glspont_riaszt" style="margin-top: 20px;"><a id="glspont_horgony"></a><div class="alert alert-warning alert-danger">GLS CsomagPontot.</div></div>';
		print '<div class="col-md-12" style="margin-top: 20px;">
			<label style="cursor: pointer; display: none;">
				<input class="szall_mod_checkbox" onClick="glsKeresoNyitas();" style="cursor:pointer; margin-right: 4px;" type="radio" id="gls_cspont" name="gls_cspont" value="GLS CsomagPont">'.$sz_kassza_28.'
				<span class="szall_kolts_arak" id="szall_kolts_ar_gls"> ('.number_format($row_beall['gls_cspont'], 0, ',', ' ').' Ft)</span>
				<span class="szall_kolts_arak" id="szall_kolts_ar_gls_utanvet"> ('.number_format($row_beall['gls_cspont_utanvet'], 0, ',', ' ').' Ft)</span>
			</label>';
			?>
			<script src="<?php print $domain; ?>/webshop/scripts/gls_cspont-valaszto.js" type="text/javascript"></script>
			<div id="gls_valaszto">
				<div class="col-lg-12">
					<select id="gls_megye" onChange="varosok_gls()">
						<option value=""><?=$sz_kassza_27?></option>
						<?php
							$query = "SELECT DISTINCT megye FROM ".$webjel."varos_megye ORDER BY megye ASC";
							foreach ($pdo->query($query) as $row)
							{
								print '<option value="'.$row['megye'].'">'.$row['megye'].'</option>';
							}
						?>
					</select>
					<div id="gls_varos_div">
						<select id="gls_varos">
							<option value=""><?=$sz_kassza_26?></option>
						</select>
					</div>
				</div>
				<div class="col-lg-12"><img src="<?php print $domain; ?>/webshop/images/gls_csomagpont.jpg"></div>
				<div id="gls_talalat_div"></div>
			</div>
			
			<input type="hidden" name="valasztott_gls_cspont" id="valasztott_gls_cspont" value="">
		</div>		
		</div>		

<?php 
    if (isset($_SESSION['valuta']) && $_SESSION['valuta'] != '')
    {
      $query_valuta = "SELECT * FROM ".$webjel."beallitasok WHERE id=1";
      $res = $pdo->prepare($query_valuta);
      $res->execute();
      $row_valuta = $res -> fetch();

      $szallitas_check = $szall_kolts / $row_valuta[$_SESSION['valuta'].'_arfolyam'];
      $valuta = $row_valuta[$_SESSION['valuta'].'_jel'];
      $arfolyamhoz = $row_valuta[$_SESSION['valuta'].'_arfolyam'];
    }
    else
    {
      $szallitas_check = $szall_kolts;
      $valuta = 'Ft';
      $arfolyamhoz = 0;
    }	
?>		

	<input type="hidden" name="valutacucc" id="valutacucc" data-valuta="<?=$valuta?>" data-arfolyam="<?=$arfolyamhoz?>">

	<div class="col-md-12 text-center" style="margin-top: 20px; float: left; width: 100%;">
		<span class="kassza_szall_kolts_info"><?=$sz_kassza_25?> <span id="fiz_szall_mod_koltseg"><?= number_format($szallitas_check, 0, ',', ' ') ?></span> <?=$valuta?></span>
	</div>
	<div class="col-md-12">
		<div class="pull-right" style="margin-top:10px; margin-bottom: 10px">
			<a  id="fiz_mod_tovabb_gomb" class="button kassza_tovabb_gomb" attr_tart_id="osszesites_tartalom" attr_horg="osszes_horgony"><?=$sz_kassza_5?></a>
		</div>		
	</div>
	<input type="hidden" name="valasztott_postapont" id="valasztott_postapont" value="">