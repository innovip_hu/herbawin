<?php
	session_start();
	include '../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}

	$query_f = "SELECT * FROM ".$webjel."kassza_fiz_mod WHERE id=".$_POST['fiz_mod'];
	$res = $pdo->prepare($query_f);
	$res->execute();
	$row_f = $res -> fetch();

	$query_sz = "SELECT * FROM ".$webjel."kassza_szall_mod WHERE id=".$_POST['szall_mod'];
	$res = $pdo->prepare($query_sz);
	$res->execute();
	$row_sz = $res -> fetch();

	$query_beall = "SELECT ingyenes_szallitas FROM ".$webjel."beallitasok WHERE id=1";
	$res = $pdo->prepare($query_beall);
	$res->execute();
	$row_beall = $res -> fetch();	

	$query_kosar = "SELECT SUM(IF(term_akcios_ar > 0, (term_akcios_ar*term_db), (term_ar*term_db))) AS szumma FROM ".$webjel."kosar WHERE kosar_id=".$_SESSION['kosar_id'];
	$res = $pdo->prepare($query_kosar);
	$res->execute();
	$row_kosar = $res -> fetch();	

	if ($row_beall['ingyenes_szallitas'] > 0 && $row_kosar['szumma'] >= $row_beall['ingyenes_szallitas'])
	{
		echo 0;
	}
	elseif ($row_sz['suly_alap'] == 1)
	{
		$suly = 0;
		$query = "SELECT term_id, term_db FROM ".$webjel."kosar WHERE kosar_id=".$_POST['kosar_id'];		
		foreach ($pdo->query($query) as $key => $value)
		{
			$query_term = "SELECT suly FROM ".$webjel."termekek where id=".$value['term_id'];
			$res = $pdo->prepare($query_term);
			$res->execute();
			$row_term = $res -> fetch();

			$suly += $value['term_db'] * $row_term['suly'];
		}

		$res = $pdo->prepare('SELECT * FROM '.$webjel.'szall_suly WHERE max_suly>='.$suly.' ORDER BY max_suly ASC LIMIT 1 ');
		$res->execute();
		$row_suly  = $res -> fetch();

		echo $row_suly['ar'];
	}	
	else
	{
		if($row_f['utanvet'] == 1)
		{
			echo $row_sz['ar_utanvet'];
		}
		else
		{
			echo $row_sz['ar'];
		}
	}

?>