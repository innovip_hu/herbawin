	<?php
		if ($_GET['keres_nev'] == '')
		{
			print '<p>Nincs megadva keresési feltétel!</p>';
		}
		else
		{
			$keres_szoveg = str_replace(" ", "%", $_GET['keres_nev']);
		// Kategóriák
			print '<div class="kereso_talalat_kategoria">';
			
			$keres_szoveg_tomb = array();
			$keres_szoveg_tomb = explode(" ",$_GET['keres_nev']);
			$feltetel = '';
			foreach($keres_szoveg_tomb as $row_tomb)
			{
				$feltetel .= " AND (nev LIKE '%".$row_tomb."%' OR nev_en LIKE '%".$row_tomb."%' OR nev_de LIKE '%".$row_tomb."%')";
			}
			$query = "SELECT DISTINCT csop_id FROM ".$webjel."termekek WHERE lathato = 1 ".$feltetel;
			foreach ($pdo->query($query) as $row)
			{
				$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."termekek WHERE lathato = 1 ".$feltetel." AND csop_id=".$row['csop_id']);
				$res->execute();
				$rownum = $res->fetchColumn();
	
				$query_kat = "SELECT * FROM ".$webjel."term_csoportok WHERE id=".$row['csop_id'];
				$res = $pdo->prepare($query_kat);
				$res->execute();
				$row_kat = $res -> fetch();
				print '<a href="'.$domain.'/termekek/'.$row_kat['nev_url'].'/?keres_nev='.$_GET['keres_nev'].'">'.$row_kat['nev'.$_SESSION['lang_sql']].' ('.$rownum.')</a> ';
			}
			print '</div>';
			
		// Termékek
			if (isset($_GET['sorr_tip']))
			{
				$sorr_tip = $_GET['sorr_tip'];
			}
			else
			{
				$sorr_tip = 'ar_sorrend'; // Alap rendezési feltétel
			}
			if (isset($_GET['sorrend']))
			{
				$sorrend = $_GET['sorrend'];
			}
			else
			{
				$sorrend = 'ASC'; // Alap rendezési feltétel
			}
			print '<form action="" method="GET" id="kereso_sorrend_form"><select class="form-input" id="kereso_sorrend" name="sorrend">';
				if ($sorr_tip == 'ar_sorrend' && $sorrend == 'ASC')
				{
					print '<option attr_sorrend="ASC" attr_sorr_tip="ar_sorrend" value="ar_nov" selected>'.$sz_term_kat_1.'</option>';
				}
				else
				{
					print '<option attr_sorrend="ASC" attr_sorr_tip="ar_sorrend" value="ar_nov">'.$sz_term_kat_1.'</option>';
				}
				if ($sorr_tip == 'ar_sorrend' && $sorrend == 'DESC')
				{
					print '<option attr_sorrend="DESC" attr_sorr_tip="ar_sorrend" value="ar_csokk" selected>'.$sz_term_kat_2.'</option>';
				}
				else
				{
					print '<option attr_sorrend="DESC" attr_sorr_tip="ar_sorrend" value="ar_csokk">'.$sz_term_kat_2.'</option>';
				}
				if ($sorr_tip == 'nev' && $sorrend == 'ASC')
				{
					print '<option attr_sorrend="ASC" attr_sorr_tip="nev" value="nev_nov" selected>'.$sz_term_kat_3.'</option>';
				}
				else
				{
					print '<option attr_sorrend="ASC" attr_sorr_tip="nev" value="nev_nov">'.$sz_term_kat_3.'</option>';
				}
				if ($sorr_tip == 'nev' && $sorrend == 'DESC')
				{
					print '<option attr_sorrend="DESC" attr_sorr_tip="nev" value="nev_csokk" selected>'.$sz_term_kat_4.'</option>';
				}
				else
				{
					print '<option attr_sorrend="DESC" attr_sorr_tip="nev" value="nev_csokk">'.$sz_term_kat_4.'</option>';
				}
				if ($sorr_tip == 'id' && $sorrend == 'DESC')
				{
					print '<option attr_sorrend="DESC" attr_sorr_tip="id" value="id_csokk" selected>'.$sz_term_kat_7.'</option>';
				}
				else
				{
					print '<option attr_sorrend="DESC" attr_sorr_tip="id" value="id_csokk">'.$sz_term_kat_7.'</option>';
				}
			print '</select>
				<input type="hidden" name="keres_nev" value="'.$_GET['keres_nev'].'" />
				<input type="hidden" name="sorr_tip" id="sorr_tip" value="" />
				<input type="hidden" name="sorrend" id="sorrend" value="" />
			</form>';
			if (isset($_GET['sorr_tip']))
			{
				$sorr_tip = $_GET['sorr_tip'];
			}
			else
			{
				$sorr_tip = 'ar_sorrend'; // Alap rendezési feltétel
			}
			if (isset($_GET['sorrend']))
			{
				$sorrend = $_GET['sorrend'];
			}
			else
			{
				$sorrend = 'ASC'; // Alap rendezési feltétel
			}
		
			// Generálás
			$query = "SELECT *, IF(akciosar > 0 AND akcio_ig >= '".date('Y-m-d')."', akciosar, ar) AS ar_sorrend FROM ".$webjel."termekek WHERE lathato = 1 ".$feltetel." ORDER BY ".$sorr_tip." ".$sorrend;
			$a = 0;
			print '<div class="range range-50 kapcs_termekek" style="margin-top: 0px;">';
			foreach ($pdo->query($query) as $row)
			{
				$a = 1;
				include $gyoker.'/webshop/termek.php';
			}
			print '</div>';
			if ($a == 0)
			{
				print '<p>'.$sz_kereso_3.'</p>';
			}
		}
	?>
