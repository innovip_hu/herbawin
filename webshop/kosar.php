	<?php
		if (isset($_SESSION['kosar_id']) && $_SESSION['kosar_id'] != 0)
		{
			$kupon = '';
			if(isset($_POST['kupon_kod']))
			{
				$query = "SELECT * FROM ".$webjel."kuponok WHERE kod='".$_POST['kupon_kod']."' AND rendeles_id=0 AND indul<='".date("Y-m-d")."' AND vege>='".date("Y-m-d")."'";
				$res = $pdo->prepare($query);
				$res->execute();
				$row = $res -> fetch();
				
				if(isset($row['id']))
				{
					if($row['min_kosar_ertek'] > $_POST['kosar_erteke'])
					{
						print '<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							A kosár értéke nem éri el a kuponban meghatározott minimum értéket ('.$row['min_kosar_ertek'].'Ft)!
						</div>';
					}
					else
					{
						$updatecommand = "UPDATE ".$webjel."kosar SET kupon_id='".$row['id']."' WHERE kosar_id=".$_SESSION['kosar_id'];
						$result = $pdo->prepare($updatecommand);
						$result->execute();
						$kupon = 'ok';
						if($row['egyszeri'] == 1) // Ha egyszeri, akkor érvényteleníteni kell
						{
							$updatecommand = "UPDATE ".$webjel."kuponok SET rendeles_id='".$_SESSION['kosar_id']."' WHERE id=".$row['id'];
							$result = $pdo->prepare($updatecommand);
							$result->execute();
						}
					}
				}
				else
				{
					print '<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							A megadott kód hibás, vagy már nem érvényes!
					</div>';
				}
			}
	?>
		<script language="javascript">
			//kosár elemei darabszám beírásra
				function showTetelek(str)
				{
					if (window.XMLHttpRequest)
					  {// code for IE7+, Firefox, Chrome, Opera, Safari
					  xmlhttp=new XMLHttpRequest();
					  }
					else
					  {// code for IE6, IE5
					  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
					  }
					xmlhttp.onreadystatechange=function()
					  {
					  if (xmlhttp.readyState==4 && xmlhttp.status==200)
						{
							document.getElementById("kosar_elemei").innerHTML=xmlhttp.responseText;
							document.getElementById("kosar_erteke").value = document.getElementById("kosar_erteke_kuponhoz").value;
						}
					  }
					xmlhttp.open("GET","<?php print $domain; ?>/webshop/module/mod_kosar_elemei.php?darab="+document.getElementById("darab_"+str).value+"&id="+str,true);
					xmlhttp.send();
				}
			//kosár elemei PLUSZ
				function showTetelekPlusz(str,term_id)
				{
					if(Number(document.getElementById("darab_"+str).value) < Number(document.getElementById("raktaron_"+str).value)){
						if (window.XMLHttpRequest)
						  {// code for IE7+, Firefox, Chrome, Opera, Safari
						  xmlhttp=new XMLHttpRequest();
						  }
						else
						  {// code for IE6, IE5
						  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
						  }
						xmlhttp.onreadystatechange=function()
						  {
						  if (xmlhttp.readyState==4 && xmlhttp.status==200)
							{
								document.getElementById("kosar_elemei").innerHTML=xmlhttp.responseText;
								document.getElementById("kosar_erteke").value = document.getElementById("kosar_erteke_kuponhoz").value;
							}
						  }
						xmlhttp.open("GET","<?php print $domain; ?>/webshop/module/mod_kosar_elemei.php?muvelet=plusz&regidarab="+document.getElementById("darab_"+str).value+"&id="+str+"&term_id="+term_id,true);
						xmlhttp.send();
					}
				}
			//kosár elemei MINUSZ
				function showTetelekMinusz(str,term_id)
				{
					if(document.getElementById("darab_"+str).value - 1 < 1)
						{
									return;
						}
					if (window.XMLHttpRequest)
					  {// code for IE7+, Firefox, Chrome, Opera, Safari
					  xmlhttp=new XMLHttpRequest();
					  }
					else
					  {// code for IE6, IE5
					  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
					  }
					xmlhttp.onreadystatechange=function()
					  {
					  if (xmlhttp.readyState==4 && xmlhttp.status==200)
						{
							document.getElementById("kosar_elemei").innerHTML=xmlhttp.responseText;
							document.getElementById("kosar_erteke").value = document.getElementById("kosar_erteke_kuponhoz").value;
						}
					  }
					xmlhttp.open("GET","<?php print $domain; ?>/webshop/module/mod_kosar_elemei.php?muvelet=minusz&regidarab="+document.getElementById("darab_"+str).value+"&id="+str+"&term_id="+term_id,true);
					xmlhttp.send();
				}
		</script>
	<?php
			//termék törlése
			if (isset($_POST['command']) && $_POST['command'] == "TORLES")
			{
				$deletecommand = 'DELETE FROM '.$webjel.'kosar WHERE id = '.$_POST['id'];
				$stmt = $pdo->prepare($deletecommand);
				$stmt->bindParam(':id', $_POST['id'], PDO::PARAM_INT);	
				$stmt->execute();
				// Kupon törlése
				$updatecommand = "UPDATE ".$webjel."kosar SET kupon_id=0 WHERE kosar_id=".$_SESSION['kosar_id'];
				$result = $pdo->prepare($updatecommand);
				$result->execute();
			}
			// Van-e termék a kosárban
			$res = $pdo->prepare('SELECT * FROM '.$webjel.'kosar WHERE kosar_id='.$_SESSION['kosar_id']);
			$res->execute();
			$rownum = $res->fetchColumn();
			if ($rownum == 0)
			{
				$pdo->exec('DELETE FROM '.$webjel.'kosar WHERE kosar_id='.$_SESSION['kosar_id']);
				unset($_SESSION['kosar_id']);
				
				header('Location: '.$domain.'/kosar/');
			}
			else
			{
				print '
					<div class="cell-lg-8 cell-md-8">';
						print '<div class="table-responsive" id="kosar_elemei">';
							include ('module/mod_kosar_elemei.php');
						print '</div>';
					print '</div>';
					?>
					<div class="cell-lg-4 cell-md-4">
						<div class="row">
							<div class="col-md-12 margbot10"><a href="<?php print $domain; ?>/kassza/" class="button button-primary full_szeles_gomb"><?=$sz_kosar_1?></a></div>
							<?php
								if(isset($_SESSION['link_ut']))
								{
									print '<div class="col-md-12 margbot10"><a href="'.$_SESSION['link_ut'].'" class="button button-white full_szeles_gomb">'.$sz_kosar_3.'</a></div>';
								}
							?>
							<div class="col-md-12 margbot10">
								<form id="kosar_uritese" action="" method="post">
									<a onclick="document.getElementById('kosar_uritese').submit();" class="button button-mandy full_szeles_gomb" ><?=$sz_kosar_2?></a>
									<input type="hidden" name="command" value="KOSAR_URIT"/>
								</form>
							</div>
						</div>
						<?php
						if($config_kupon == 1)
						{
							// Kupon beváltás
							print '<div class="panel-group kupon_div margtop20">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title text-center">Kuponkód beváltása</h4>
									</div>
									<div id="collapse-coupon" class="panel-collapse collapse in" style="">
										<div class="panel-body">
											<div class="col-sm-12 margbot10">
												<form method="POST" action="" id="kupon_form">
													<input type="text" name="kupon_kod" value="" placeholder="Kupon kód" id="input-coupon" class="form-control" style="width:100%;">
													<input type="hidden" id="kosar_erteke" name="kosar_erteke" value="'.$ar.'" />
												</form>
											</div>
											<div class="col-sm-12">';
												?>
												<a onclick="document.getElementById('kupon_form').submit();" class="btn btn-sm w100sz" >Bevált</a>
												<?php
											print '</div>
										</div>
									</div>
								</div>
							</div>';
						}
						?>
					</div>
				<?php
				// Kupon beváltás
			}
		}
		else
		{
			print '<div class="cell-md-12"><h3>'.$sz_kinyil_4.'</h3>';
			print '<img src="'.$domain.'/webshop/images/ures_kosar.png" border="0"></div>';
		}
	?>
	