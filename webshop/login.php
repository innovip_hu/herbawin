<?php

	require_once "fbconfig.php";

	$redirectURL = $domain."/webshop/fb-callback.php";
	$permissions = ['email'];
	$loginURL = $helper->getLoginUrl($redirectURL,$permissions);

	if (isset($_SESSION['kosar_id']) && isset($_SESSION['login_id']))
	{
		header('Location: '.$domain.'/kassza/');
	}
	else if (isset($_SESSION['login_id']))
	{
		header('Location: '.$domain.'/');
	}
	else
	{
		?>
		<div class="belepes_box">
			<form id="loginForm" action="" method="post">
				<div class="belepes_header">
					<?=$sz_belepes_1?>
					<span><?=$sz_belepes_2?></span>
				</div>
				<input type="text" name="email" value="" class="form-input" placeholder="E-mail">
				<input type="password" name="passwordText" value="" class="form-input" placeholder="<?=$sz_belepes_3?>">
				<p><?=$sz_belepes_4?> <a href="<?php print $domain; ?>/jelszo-emlekezteto/"><?=$sz_belepes_5?></a></p>
				<div id="login_riaszt">
					<?php
						if(isset($uzenet))
						{
							print '<div class="alert alert-danger alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								'.$uzenet.'
							</div>';
						}
					?>
				</div>
				<button type="submit" class="button button-primary" /><?=$sz_belepes_6?></button>
				<input type="hidden" name="command" value="LOGIN">

			</form>
		</div>
		<?php
	}
?>
