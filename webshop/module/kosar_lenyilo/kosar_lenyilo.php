				<script type="text/javascript" src="http://<?php print $domain; ?>/webshop/module/kosar_lenyilo/script.js"></script>
				<style>
					.block-cart-header { /*Legördülö kosár látható fejléce*/
						float: right;
						position: relative;
						background-image: url('http://<?php print $domain; ?>/webshop/module/kosar_lenyilo/bg_button.png');
						background: #ffffff;
						background: -moz-linear-gradient(top, #ffffff 0%, #fafafa 50%, #f4f4f4 100%);
						background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(50%,#fafafa), color-stop(100%,#f4f4f4));
						background: -webkit-linear-gradient(top, #ffffff 0%,#fafafa 50%,#f4f4f4 100%);
						background: -o-linear-gradient(top, #ffffff 0%,#fafafa 50%,#f4f4f4 100%);
						background: -ms-linear-gradient(top, #ffffff 0%,#fafafa 50%,#f4f4f4 100%);
						background: linear-gradient(to bottom, #ffffff 0%,#fafafa 50%,#f4f4f4 100%);
						height: 31px;
						padding: 8px 46px 0 16px;
						font-size: 14px;
						color: #353535;
						margin: 9px 0 0 0;
						border: 1px solid #cac9c9;
					}
					.block-cart-header h3 { /* "Kosár:" felirat*/
						float: left;
						margin: 0 7px 0 0;

						font-size: 16px;
						line-height: normal;
						color: #50749b;
						font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
					}
					.block-cart-header .block-content { /*Legördülö kosár legördülő ablaka*/
						float: left;
						color: #333;
					}
					.block-cart-header .amount-2 { /*Termékdarab és ár*/
						line-height: normal;
						margin: 0;
						margin: 4px 0 0 0;
					}
					.block-cart-header .amount-2 a {
						color: #353535;
						text-decoration: none;
					}
					.block-cart-header .amount-2 a:hover {
						text-decoration: underline;
					}
					.cart-inner { /*Legördülő rész*/
						position: relative;
						z-index: 9999;
					}
					.block-subtitle{
						text-decoration: underline;
						font-weight: bold;
						color: #50749b;
					}
					.block-cart-header .cart-content {
						position: absolute;
						right: -46px;
						top: 9px;
						z-index: 9999;
						width: 260px;
					}
					.cart-indent {
						background: #F8F8F8;
						padding: 10px;
						border: 1px solid #cac9c9;
					}
					.block-cart-header .actions { /*"Kosár" gomb*/
						text-align: center;
					}
					.btn-remove {/*"Eltávolít" gomb*/
						display: block;
						width: 19px;
						height: 19px;
						font-size: 0;
						line-height: 0;
						background: url('http://<?php print $domain; ?>/webshop/module/kosar_lenyilo/btn_remove.gif') 0 0 no-repeat;
						background-position: 0 top;
						text-indent: -999em;
						overflow: hidden;
						margin-left: 2px;
					}
					.btn-remove:hover {
						background-position: 0 bottom;
					}
				</style>
				<?php
					$db = 0;
					$ar = 0;
					if (isset($_SESSION['kosar_id']))
					{
						/*mysql_connect($dbhost,$dbuser,$dbpass);
						mysql_select_db($dbname);
						mysql_query('SET NAMES utf8');*/
						
						$query = "SELECT * FROM ".$webjel."kosar WHERE kosar_id=".$_SESSION['kosar_id'];
						foreach ($pdo->query($query) as $row)
						{
							$db = $db + $row['term_db'];
							if($row['term_akcios_ar'] == 0) //ha nem akciós
							{
								$ar = $ar + ($row['term_ar'] * $row['term_db']);
							}
							else //ha akciós
							{
								$ar = $ar + ($row['term_akcios_ar'] * $row['term_db']);
							}
						}
					}
				?>
				<div class="block-cart-header" <?php if (isset($_SESSION['kosar_id'])) { print 'style="background: #d7e1ed url(http://'.$domain.'/webshop/module/kosar_lenyilo/bg_button.png) no-repeat 100% 0;"'; } ?>>
					<div class="block-cart-wrap">
						<div class="cart_icon"></div>
						<h3>Kosár:</h3>
						<div class="block-content">
							<div>
								<p class="amount-2"><a href="http://<?php print $domain; ?>/kosar/"><?php print $db; ?> termék</a> - <span class="price"><?php print number_format($ar, 0, ',', ' '); ?> Ft</span></p>
							</div>
							<?php if ($db > 0) { ?>
							<div class="cart-inner">
								<div class="cart-content" style="display: none;">
									<div class="cart-indent">
										<p class="block-subtitle">Kosárban lévő termék(ek)</p>
										<table align="center" cellpadding="6" cellspacing="0">
											<?php
												$query = "SELECT * FROM ".$webjel."kosar WHERE kosar_id=".$_SESSION['kosar_id'];
												foreach ($pdo->query($query) as $row)
												{
													if ($config_ovip == 'I')
													{
														$query_termlink = "SELECT * FROM ".$ovipjel."termekek where id=".$row['term_id'];
													}
													else
													{
														$query_termlink = "SELECT * FROM ".$webjel."termekek where id=".$row['term_id'];
													}
													$res_termlink = $pdo->prepare($query_termlink);
													$res_termlink->execute();
													$row_termlink  = $res_termlink -> fetch();
													
													print '<tr valign="top">';
													$query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_termlink['csop_id'];
													$res_csop = $pdo->prepare($query_csop);
													$res_csop->execute();
													$row_csop  = $res_csop -> fetch();
													print '<td><a href="http://'.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row_termlink['nev_url'].'" title="'.$row['term_nev'].'" class="product-image">';
													if ($row['term_kep'] == "")
													{
														print '<img src="http://'.$domain.'/webshop/images/noimage.png" width="84" alt="'.$row['term_nev'].'">';
													}
													else
													{
														print '<img src="http://'.$domain.'/images/termekek/'.$row['term_kep'].'" width="84" alt="'.$row['term_nev'].'">';
													}
													print '</a></td>';
													print '<td>
																'.$row['term_nev'].'
																<br>
																'.$row['term_db'].' db
																<br>';
																if($row['term_akcios_ar'] == 0) //ha nem akciós
																{
																	print number_format(($row['term_ar'] * $row['term_db']), 0, ',', ' ').' Ft';
																}
																else
																{
																	print number_format(($row['term_akcios_ar'] * $row['term_db']), 0, ',', ' ').' Ft';
																}
																/*print '<br>
																<p><a href="" title="Termék eltávolítása" onclick="return confirm';
																print "('Biztos törli a terméket a kosárból?')";
																print ';" class="btn-remove">Termék eltávolítása</a></p>*/
															print '</td>
														</tr>';
												}
											?>
										</table>
										<div class="actions">
											<button type="button" title="Kosár" class="reszletek_gomb" onclick="location.href='http://<?php print $domain; ?>/kosar/';"><span><span>Kosár</span></span></button>
											
											<button type="button" title="Kassza" class="reszletek_gomb" onclick="location.href='http://<?php print $domain; ?>/kassza/';"><span><span>Kassza</span></span></button>
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
