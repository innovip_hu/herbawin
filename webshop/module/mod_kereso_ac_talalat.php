<?php
	session_start();
	ob_start();
	include '../../config.php';
	$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
	try
	{
		$pdo = new PDO(
		$dsn, $dbuser, $dbpass,
		Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
		);
	}
	catch (PDOException $e)
	{
		die("Nem lehet kapcsolódni az adatbázishoz!");
	}
	
	$keres_szoveg_tomb = array();
	$keres_szoveg_tomb = explode(" ",$_POST['keres_nev']);
	$feltetel = '';
	foreach($keres_szoveg_tomb as $row_tomb)
	{
		$feltetel .= " AND (".$webjel."termekek.nev LIKE '%".$row_tomb."%' OR ".$webjel."termekek.nev_en LIKE '%".$row_tomb."%' OR ".$webjel."termekek.nev_de LIKE '%".$row_tomb."%')";
	}
			
	$query = "SELECT *, IF(akciosar > 0 AND akcio_ig >= '".date('Y-m-d')."', akciosar, ar) AS ar_sorrend 
					, ".$webjel."termekek.id AS id
					, ".$webjel."termekek.nev AS nev
					, ".$webjel."term_csoportok.nev AS csop_nev
					, ".$webjel."termekek.nev_url AS nev_url
					, ".$webjel."term_csoportok.nev_url AS nev_url_csop
		FROM ".$webjel."termekek 
		LEFT JOIN ".$webjel."term_csoportok 
		ON ".$webjel."termekek.csop_id = ".$webjel."term_csoportok.id 
		WHERE ".$webjel."termekek.lathato = 1 ".$feltetel." 
		ORDER BY akciosar ASC";
	$a = 0;
	$i = 0;
	$i_kat = 0;
	$termekek = array();
	$kategoriak = array();
	$akciosok = array();
	foreach ($pdo->query($query) as $row)
	{
		$a = 1;
		$i++;
		$str = mb_strtolower($row['nev'], 'UTF-8');
		$str_csop = mb_strtolower($row['csop_nev'], 'UTF-8');
		if($i <= 8) // max termék találat
		{
			$termekek[] = '<a href="'.$domain.'/termekek/'.$row['nev_url_csop'].'/'.$row['nev_url'].'"><li>'.$str.'</li></a>';
		}
		// Kategória
		$kat_link = '<a href="'.$domain.'/termekek/'.$row['nev_url_csop'].'/"><li>'.$str_csop.'</li></a>';
		$vane = 0;
		// $vane = array_search($kat_link, $kategoriak); // Innovip szerveren nem működött!
		foreach($kategoriak as $kat) // Keresünka t
		{
			if($kat == $kat_link) { $vane = 1; }
		}
		if($vane == 0) // Ha nincs még ez a kategória a tömbben
		{
			$i_kat++;
			if($i_kat <= 6) // max kategória találat
			{
				$kategoriak[] = $kat_link;
			}
		}
		// Akcióstermékek
		if($row['akcio_ig'] >= date('Y-m-d') && $row['akcio_tol'] <= date('Y-m-d'))
		{
			$akciosok[] = array("nev" => $row['nev'.$_SESSION['lang_sql']], 
								"link" => $domain.'/termekek/'.$row['nev_url_csop'].'/'.$row['nev_url'], 
								"ar" => $row['ar'], 
								"akciosar" => $row['akciosar'], 
								"ar_sorrend" => $row['ar_sorrend'], 
								"id" => $row['id']);
		}
	}
	if ($a == 1) // Ha van találat
	{
		print '<div class="kereso_ac_doboz_termekek"><ul>';
		foreach($termekek as $term)
		{
			print $term;
		}
		print '</ul>';
		// Kategóriák
		print '<h5 class="kereso_ac_doboz_kategoria_title">'.$sz_kereso_1.'</h5><ul>';
		foreach($kategoriak as $kat)
		{
			print $kat;
		}
		print '</ul></div>';
		// Akciós termékek
		if(!empty($akciosok))
		{
			shuffle($akciosok);
			print '<div class="kereso_ac_doboz_akciok"><ul>';
			print '<h5>'.$sz_kereso_2.'</h5>';
			for ($i = 0; $i < 4; $i++) {
				if(isset($akciosok[$i]))
				{
					// Kép
					$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$akciosok[$i]['id']." ORDER BY alap DESC LIMIT 1";
					$res = $pdo->prepare($query_kep);
					$res->execute();
					$row_kep = $res -> fetch();
					$alap_kep = $row_kep['kep'];
					if ($alap_kep == '') 
					{
						$kep_link = $domain.'/webshop/images/noimage.png';
					}
					else
					{
						if($row_kep['thumb'] != '')
						{
							$kep = $row_kep['thumb'];
						}
						else
						{
							$kep = $row_kep['kep'];
						}
						$kep_link = $domain.'/images/termekek/'.$kep;
					}
					// print '<p><a href="'.$akciosok[$i]['link'].'"><img src="'.$kep_link.'" />'.$akciosok[$i]['nev'].'</a></p>';
					print '<div class="kereso_ac_doboz_akciok_termekek"><a href="'.$akciosok[$i]['link'].'"><div class="kereso_ac_kep_helye"><img src="'.$kep_link.'" /></div>'.$akciosok[$i]['nev'].'<br/>'.number_format($akciosok[$i]['ar_sorrend'], 0, ',', ' ').' Ft</a></div>';
					
					// adatok
					
				}
			}
			print '</ul></div>';
		}
	}
				
?>