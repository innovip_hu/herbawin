<?php
	if (isset($_GET['id']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
		if (isset($_GET['darab']))
		{
			$term_db = $_GET['darab'];
			$id = $_GET['id'];
			$query = "UPDATE ".$webjel."kosar SET term_db=? WHERE id=?";
			$result = $pdo->prepare($query);
			$result->execute(array($term_db,$id));
			// Kupon törlése
			$updatecommand = "UPDATE ".$webjel."kosar SET kupon_id=0 WHERE kosar_id=".$_SESSION['kosar_id'];
			$result = $pdo->prepare($updatecommand);
			$result->execute();
		}
		else if ($_GET['muvelet'] == 'plusz')
		{
			$term_db = $_GET['regidarab'] + 1;
			$id = $_GET['id'];
			$query = "UPDATE ".$webjel."kosar SET term_db=? WHERE id=?";
			$result = $pdo->prepare($query);
			$result->execute(array($term_db,$id));
			// Kupon törlése
			$updatecommand = "UPDATE ".$webjel."kosar SET kupon_id=0 WHERE kosar_id=".$_SESSION['kosar_id'];
			$result = $pdo->prepare($updatecommand);
			$result->execute();
		}
		else if ($_GET['muvelet'] == 'minusz')
		{
			$term_db = $_GET['regidarab'] - 1;
			$id = $_GET['id'];
			$query = "UPDATE ".$webjel."kosar SET term_db=? WHERE id=?";
			$result = $pdo->prepare($query);
			$result->execute(array($term_db,$id));
			// Kupon törlése
			$updatecommand = "UPDATE ".$webjel."kosar SET kupon_id=0 WHERE kosar_id=".$_SESSION['kosar_id'];
			$result = $pdo->prepare($updatecommand);
			$result->execute();
		}
	}
				$query = "SELECT *, ".$webjel."kosar.id as id FROM ".$webjel."kosar 
						LEFT JOIN ".$webjel."kosar_tetel_termek_parameter_ertek 
						ON ".$webjel."kosar.id=".$webjel."kosar_tetel_termek_parameter_ertek.kosar_tetel_id 
						WHERE kosar_id=".$_SESSION['kosar_id']."
						GROUP BY ".$webjel."kosar.id";
				$db = 0;
				$ar = 0;
				print '<table class="table table-bordered">
					<thead>
						<tr>
							<td style="padding: 12px; background-color: #5c986a; color: #fff !important;" class="text-center kosar_mobilon_eltunik">'.$sz_kosar_8.'</td>
							<td style="padding: 12px; background-color: #5c986a; color: #fff !important;" class="text-left">'.$sz_kosar_4.'</td>
							<td style="padding: 12px; background-color: #5c986a; color: #fff !important;" class="text-left kosar_mennyiseg_oszlop">'.$sz_kosar_5.'</td>
							<td style="padding: 12px; background-color: #5c986a; color: #fff !important;" class="text-right kosar_mobilon_eltunik">'.$sz_kosar_6.'</td>
							<td style="padding: 12px; background-color: #5c986a; color: #fff !important;" class="text-right kosar_mobilon_eltunik">'.$sz_kosar_7.'</td>
							<td style="padding: 12px; background-color: #5c986a; color: #fff !important;" class="kosar_mobilon_eltunik"></td>
						</tr>
					</thead>
					<tbody>';
				foreach ($pdo->query($query) as $row)
				{
					//termék adatai
					$query_term = "SELECT * FROM ".$webjel."termekek WHERE id=".$row['term_id'];
					$res_term = $pdo->prepare($query_term);
					$res_term->execute();
					$row_term  = $res_term -> fetch();
					
					$db = $db + 1;
					// Ha OVIP termék
					if ($row_term['ovip_id'] !=0)
					{
						$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row_term['id']." ORDER BY alap DESC LIMIT 1";
						$res = $pdo->prepare($query_kep);
						$res->execute();
						$row_kep = $res -> fetch();
						$alap_kep = $row_kep['kep'];
						if ($alap_kep == '') 
						{
							$kep_link = $domain.'/webshop/images/noimage.png';
						}
						else
						{
							if($row_kep['thumb'] != '')
							{
								$kep = $row_kep['thumb'];
							}
							else
							{
								$kep = $row_kep['kep'];
							}
							$kep_link = $kep;
						}
						$kep = $kep_link;
					}
					elseif ($row['term_kep'] == "")
					{
						$kep = $domain.'/webshop/images/noimage.png';
					}
					else
					{
						$kep = $domain.'/images/termekek/'.$row['term_kep'];
					}
					if($row['term_akcios_ar'] == 0) //ha nem akciós
					{
						$term_ar = $row['term_ar'];
					}
					else //ha akciós
					{
						$term_ar = $row['term_akcios_ar'];
					}
					$query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_term['csop_id'];
					$res_csop = $pdo->prepare($query_csop);
					$res_csop->execute();
					$row_csop  = $res_csop -> fetch();

					if (isset($_SESSION['valuta']) && $_SESSION['valuta'] != '')
					{
						$query_valuta = "SELECT * FROM ".$webjel."beallitasok WHERE id=1";
						$res = $pdo->prepare($query_valuta);
						$res->execute();
						$row_valuta = $res -> fetch();

						$term_ar = $term_ar / $row_valuta[$_SESSION['valuta'].'_arfolyam'];
						$valuta = $row_valuta[$_SESSION['valuta'].'_jel'];
					}
					else
					{
						$valuta = 'Ft';
					}					
					
					print '<tr>
						<td class="text-center kosar_mobilon_eltunik">
							<div class="image"><a href="'.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row_term['nev_url'].'"><img style="width: 80px;" src="'.$kep.'" alt="'.$row['term_nev'].'" title="'.$row['term_nev'].'" class="img-thumbnail"></a></div>
						</td>
						<td class="text-left"><a href="'.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row_term['nev_url'].'">
							<h6 class="margbot10">'.$row_term['nev'.$_SESSION['lang_sql']].'</h6>
							<div class="kosar_mobilon_ar"><div class="price">'.number_format($term_ar, 0, ',', ' ').' '.$valuta.'</div></div>';
						// Jellemzők
						$query_jell = "SELECT * FROM ".$webjel."kosar_tetel_termek_parameter_ertek WHERE kosar_tetel_id = ".$row['id']." ORDER BY termek_parameter_nev ASC";
						$elso = 1;
						foreach ($pdo->query($query_jell) as $row_jell)
						{
							if($elso == 1) { print ''; $elso = 0; } else { print '<br/>'; }
							print $row_jell['termek_parameter_nev'].': '.$row_jell['termek_parameter_ertek_nev'];
						}						
							
						print '</a></td>
						<td class="text-left">';
							if($config_keszlet_kezeles == 'I')
							{
								print '<input type="hidden" name="raktaron" id="raktaron_'.$row['id'].'" value="'.$row_term['raktaron'].'"/>';
							}
							else
							{
								print '<input type="hidden" name="raktaron" id="raktaron_'.$row['id'].'" value="999999999"/>';
							}
							print '<div class="input-group btn-block" style="max-width: 200px;">
								<p class="clearfix"><input onchange="showTetelek('.$row['id'].')" type="text" name="darab" id="darab_'.$row['id'].'" value="'.$row['term_db'].'" size="1" class="form-control cart-q" readonly></p>
								<div class="btn-group">
									<a onclick="showTetelekPlusz('.$row['id'].')" class="btn btn-sm"><span class="fa fa-plus"></a>
									<a onclick="showTetelekMinusz('.$row['id'].')" class="btn btn-sm"><span class="fa fa-minus"></a>
									<div class="kosar_mobilon_torol_gomb">';
										?><a onclick="document.getElementById('tartalomTorlese_<?php print $row['id']; ?>').submit();" class="btn btn-sm" ><span class="fa fa-trash"></a><?php
									print '</div>
								</div>
							</div>
						</td>
						<td class="text-right kosar_mobilon_eltunik"><div class="price">'.number_format($term_ar, 0, ',', ' ').' '.$valuta.'</div></td>
						<td class="text-right kosar_mobilon_eltunik"><div class="price price-total">'.number_format($term_ar * $row['term_db'], 0, ',', ' ').' '.$valuta.'</div></td>
						<td class="kosar_mobilon_eltunik" style="width:42px;"><form id="tartalomTorlese_'.$row['id'].'" action="" method="post">';
								?>
									<a onclick="document.getElementById('tartalomTorlese_<?php print $row['id']; ?>').submit();" class="btn" ><span class="fa fa-trash"></span></a>
								<?php
								print '<input type="hidden" name="id" value="'.$row['id'].'"/>
								<input type="hidden" name="command" value="TORLES"/>
								</form></td>
					</tr>';
					$ar = $ar + ($term_ar * $row['term_db']);
				}
				print '<input type="hidden" id="kosar_erteke_kuponhoz" value="'.$ar.'" />';
				
				$ingyen_szalitas = 0;
				$kedv_ar = 0;
				if($row['kupon_id'] > 0)
				{
					print '<tr>
						<td></td>
						<td>
							Kupon kedvezmény:<br>';
							$query_kupon = "SELECT * FROM ".$webjel."kuponok WHERE id=".$row['kupon_id'];
							$res = $pdo->prepare($query_kupon);
							$res->execute();
							$row_kupon = $res -> fetch();
							if($row_kupon['kupon_fajta'] == 'Ingyenes szállítás')
							{
								print 'Ingyenes szállítás';
								$kedv_ar = 0;
							}
							else if($row_kupon['kupon_fajta'] == 'Kedvezmény')
							{
								print '-'.$row_kupon['kedvezmeny'];
								if ($row_kupon['kedv_tipus'] == 'szazalek')
								{
									print '% kedvezmény';
									$kedv_ar = $ar*$row_kupon['kedvezmeny']/100*(-1);
								}
								else if ($row_kupon['kedv_tipus'] == 'osszeg')
								{
									print 'Ft kedvezmény';
									$kedv_ar = $row_kupon['kedvezmeny']*(-1);
								}
							}
							else if($row_kupon['kupon_fajta'] == 'Ajándék termék')
							{
								$query_term = "SELECT * FROM ".$webjel."termekek WHERE id=".$row_kupon['termek_id'];
								$res = $pdo->prepare($query_term);
								$res->execute();
								$row_term = $res -> fetch();
								$query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_term['csop_id'];
								$res_csop = $pdo->prepare($query_csop);
								$res_csop->execute();
								$row_csop  = $res_csop -> fetch();
								print 'Ajándék: <a href="'.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row_term['nev_url'].'">'.$row_term['nev'].'</a>';
								$kedv_ar = 0;
							}
							else
							{
								print $row_kupon['kupon_fajta'];
							}
						
						print '</td>
						<td></td>
						<td class="text-right"><div class="price">'.number_format($kedv_ar, 0, ',', ' ').' '.$valuta.'</div></td>
						<td class="text-right"><div class="price price-total">'.number_format($kedv_ar, 0, ',', ' ').' '.$valuta.'</div></td>
						<td></td>
					</tr>';
				}
				
				print '<tr>
							<td colspan="6" class="text-right"><div class="price price-total" style="color:#505050;">Fizetendő: '.number_format($ar+$kedv_ar, 0, ',', ' ').' '.$valuta.'</div></td>
						</tr>';
				$res = $pdo->prepare('SELECT * FROM '.$webjel.'beallitasok WHERE id=1');
				$res->execute();
				$row_beall  = $res -> fetch();
				if ($row_beall['ingyenes_szallitas'] > 0 && $ingyen_szalitas == 0) // Ha van ingyenes szállítás
				{
					$kosar_kulonbseg = $row_beall['ingyenes_szallitas'] - ($ar+$kedv_ar);
					if($kosar_kulonbseg > 0)
					{
						$ingy_szallhoz = number_format($kosar_kulonbseg, 0, ',', ' ');
						$ingyszall_szoveg = 'Az ingyenes kiszállításhoz még '.$ingy_szallhoz.' '.$valuta.'-ért válassz terméket.';
					}
					else
					{
						$ingyszall_szoveg = 'A kiszállítás ingyenes.';
					}

					print '<tr>
								<td colspan="6" class="text-center"><div class="price price-total" >'.$ingyszall_szoveg.'</div></td>
							</tr>';
				}
				print '</tbody></table>';

				
?>