<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
	print '<div id="osszehas_box_div">';
	
	if (isset($_SESSION['osszahas_1']) || isset($_SESSION['osszahas_2']) || isset($_SESSION['osszahas_3']) || isset($_SESSION['osszahas_4']))
	{
		print '<div id="osszehas_box">';
			print '<div class="osszehas_elemei">';
				// print '<form action="" method="POST">';
							print '<input type="submit" value="Kijelöltek összehasonlítása" id="osszeh_tovabb" class="osszeh_tovabb">
							<input type="hidden" name="osszehasonlitas" value="">';
				// print '</form>';
			print '</div>';
			print '<div class="osszehas_elemei" style="margin:0;">';
				// 1.
				if (isset($_SESSION['osszahas_1']))
				{
					$query = 'SELECT * FROM '.$webjel.'termekek WHERE id='.$_SESSION['osszahas_1'];
					$res = $pdo->prepare($query);
					$res->execute();
					$row  = $res -> fetch();
					print '<div class="osszehas_elemei term">
						<form action="" method="POST">';
							// Kép
							$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
							$res = $pdo->prepare($query_kep);
							$res->execute();
							$row_kep = $res -> fetch();
							$alap_kep = $row_kep['kep'];
							if ($alap_kep == '') 
							{
								$kep_link = ''.$domain.'/webshop/images/noimage.png';
							}
							else
							{
								$kep_link = ''.$domain.'/images/termekek/'.$row_kep['kep'];
							}
							print '<input type="image" name="osszahas_torol_id" src="'.$kep_link.'" width="40">';
							print '<input type="hidden" name="session_szam" value="osszahas_1">
						</form>
					</div>';
				}
				else
				{
					print '<div class="osszehas_elemei term">&nbsp;</div>';
				}
				// 2.
				if (isset($_SESSION['osszahas_2']))
				{
					$query = 'SELECT * FROM '.$webjel.'termekek WHERE id='.$_SESSION['osszahas_2'];
					$res = $pdo->prepare($query);
					$res->execute();
					$row  = $res -> fetch();
					print '<div class="osszehas_elemei term">
						<form action="" method="POST">';
							// Kép
							$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
							$res = $pdo->prepare($query_kep);
							$res->execute();
							$row_kep = $res -> fetch();
							$alap_kep = $row_kep['kep'];
							if ($alap_kep == '') 
							{
								$kep_link = ''.$domain.'/webshop/images/noimage.png';
							}
							else
							{
								$kep_link = ''.$domain.'/images/termekek/'.$row_kep['kep'];
							}
							print '<input type="image" name="osszahas_torol_id" src="'.$kep_link.'" width="40">';
							print '<input type="hidden" name="session_szam" value="osszahas_2">
						</form>
					</div>';
				}
				else
				{
					print '<div class="osszehas_elemei term">&nbsp;</div>';
				}
				// 3.
				if (isset($_SESSION['osszahas_3']))
				{
					$query = 'SELECT * FROM '.$webjel.'termekek WHERE id='.$_SESSION['osszahas_3'];
					$res = $pdo->prepare($query);
					$res->execute();
					$row  = $res -> fetch();
					print '<div class="osszehas_elemei term">
						<form action="" method="POST">';
							// Kép
							$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
							$res = $pdo->prepare($query_kep);
							$res->execute();
							$row_kep = $res -> fetch();
							$alap_kep = $row_kep['kep'];
							if ($alap_kep == '') 
							{
								$kep_link = ''.$domain.'/webshop/images/noimage.png';
							}
							else
							{
								$kep_link = ''.$domain.'/images/termekek/'.$row_kep['kep'];
							}
							print '<input type="image" name="osszahas_torol_id" src="'.$kep_link.'" width="40">';
							print '<input type="hidden" name="session_szam" value="osszahas_3">
						</form>
					</div>';
				}
				else
				{
					print '<div class="osszehas_elemei term">&nbsp;</div>';
				}
				// 4.
				if (isset($_SESSION['osszahas_4']))
				{
					$query = 'SELECT * FROM '.$webjel.'termekek WHERE id='.$_SESSION['osszahas_4'];
					$res = $pdo->prepare($query);
					$res->execute();
					$row  = $res -> fetch();
					print '<div class="osszehas_elemei term">
						<form action="" method="POST">';
							// Kép
							$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
							$res = $pdo->prepare($query_kep);
							$res->execute();
							$row_kep = $res -> fetch();
							$alap_kep = $row_kep['kep'];
							if ($alap_kep == '') 
							{
								$kep_link = ''.$domain.'/webshop/images/noimage.png';
							}
							else
							{
								$kep_link = ''.$domain.'/images/termekek/'.$row_kep['kep'];
							}
							print '<input type="image" name="osszahas_torol_id" src="'.$kep_link.'" width="40">';
							print '<input type="hidden" name="session_szam" value="osszahas_4">
						</form>
					</div>';
				}
				else
				{
					print '<div class="osszehas_elemei term">&nbsp;</div>';
				}
			print '</div>';
			// Törlés
			print '<div class="osszehas_elemei" style="margin-right:0;">
					<form action="" method="POST">
						<input type="submit" name="osszahas_osszes_torol" class="osszeh_torles" value="X">
					</form>
				</div>';
		print '</div>';
	}
?>
</div>