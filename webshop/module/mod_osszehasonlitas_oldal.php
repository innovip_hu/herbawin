<?php
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	
	print '<div class="row"><div class="col-lg-12"><a href="" class="btn margbot10" style="margin-bottom:4px;">Vissza</a></div></div>';
	
	// Összehasonlított termék számának meghatározása
	$termek_szam = 0;
	$jellemzok_1 = array();
	if (isset($_SESSION['osszahas_1']))
	{
		$termek_szam = $termek_szam + 1;
		$query_1 = 'SELECT * FROM '.$webjel.'termekek WHERE id='.$_SESSION['osszahas_1'];
		$res_1 = $pdo->prepare($query_1);
		$res_1->execute();
		$row_1  = $res_1 -> fetch();

		$query_csop_1 = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_1['csop_id'];
		$res_csop_1 = $pdo->prepare($query_csop_1);
		$res_csop_1->execute();
		$row_csop_1  = $res_csop_1 -> fetch();
		// Paraméterek
		$query_jell = "SELECT * FROM ".$webjel."termek_parameterek 
			INNER JOIN ".$webjel."termek_termek_parameter_ertekek 
			ON ".$webjel."termek_parameterek.id=".$webjel."termek_termek_parameter_ertekek.termek_parameter_id 
			WHERE ".$webjel."termek_parameterek.tipus='egyedi' AND ".$webjel."termek_termek_parameter_ertekek.termek_id=".$row_1['id']."
			ORDER BY ".$webjel."termek_parameterek.nev ASC";
		foreach ($pdo->query($query_jell) as $row_jell)
		{
			$jellemzok_1[] = array("nev" => $row_jell['nev'], "ertek" => $row_jell['ertek'].' '.$row_jell['mertekegyseg']);
		}
	}
	$jellemzok_2 = array();
	if (isset($_SESSION['osszahas_2']))
	{
		$termek_szam = $termek_szam + 1;
		$query_2 = 'SELECT * FROM '.$webjel.'termekek WHERE id='.$_SESSION['osszahas_2'];
		$res_2 = $pdo->prepare($query_2);
		$res_2->execute();
		$row_2  = $res_2 -> fetch();

		$query_csop_2 = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_2['csop_id'];
		$res_csop_2 = $pdo->prepare($query_csop_2);
		$res_csop_2->execute();
		$row_csop_2  = $res_csop_2 -> fetch();
		// Jellemzők
		foreach($jellemzok_1 as $tomb_row) // végigmegyek az előző tömbön és belerakom ennek a terméknek a jellemzőit
		{
			/* $query_jell = "SELECT ".$webjel."jellemzok.ertek, 
						".$webjel."jellemzok.mertekegyseg, 
						".$webjel."jellemzok.jellemzo_id, 
						".$webjel."jellemzok.termek_cikkszam, 
						".$webjel."jellemzo_tipusok.nev, 
						".$webjel."jellemzo_tipusok.iml_id 
				FROM ".$webjel."jellemzok 
				INNER JOIN ".$webjel."jellemzo_tipusok 
				ON ".$webjel."jellemzok.jellemzo_id=".$webjel."jellemzo_tipusok.iml_id 
				WHERE ".$webjel."jellemzok.termek_cikkszam='".$row_2["cikkszam"]."' AND ".$webjel."jellemzo_tipusok.nev='".$tomb_row['nev']."' 
				ORDER BY ".$webjel."jellemzo_tipusok.nev ASC"; */
			$query_jell = "SELECT * FROM ".$webjel."termek_parameterek 
				INNER JOIN ".$webjel."termek_termek_parameter_ertekek 
				ON ".$webjel."termek_parameterek.id=".$webjel."termek_termek_parameter_ertekek.termek_parameter_id 
				WHERE ".$webjel."termek_parameterek.tipus='egyedi' AND ".$webjel."termek_termek_parameter_ertekek.termek_id=".$row_2['id']." AND ".$webjel."termek_parameterek.nev='".$tomb_row['nev']."'
				ORDER BY ".$webjel."termek_parameterek.nev ASC";
			$res = $pdo->prepare($query_jell);
			$res->execute();
			$row_jell = $res -> fetch();
			
			if(isset($row_jell['nev']))
			{
				$jellemzok_2[] = array("nev" => $row_jell['nev'], "ertek" => $row_jell['ertek'].' '.$row_jell['mertekegyseg']);
			}
			else
			{
				$jellemzok_2[] = array("nev" => $row_jell['nev'], "ertek" => 'n.a.');
			}
		}
		$query_jell = "SELECT * FROM ".$webjel."termek_parameterek 
			INNER JOIN ".$webjel."termek_termek_parameter_ertekek 
			ON ".$webjel."termek_parameterek.id=".$webjel."termek_termek_parameter_ertekek.termek_parameter_id 
			WHERE ".$webjel."termek_parameterek.tipus='egyedi' AND ".$webjel."termek_termek_parameter_ertekek.termek_id=".$row_2['id']."
			ORDER BY ".$webjel."termek_parameterek.nev ASC";
		foreach ($pdo->query($query_jell) as $row_jell)
		{
			$a = 0;
			foreach($jellemzok_2 as $tomb_row) // Ellenőrzöm, hogy belekerült-e már ez a jellemző a tömbbe
			{
				if($tomb_row['nev'] == $row_jell['nev']) // találat
				{
					$a = 1;
				}
			}
			if($a == 0) // ha nics találat
			{
				$jellemzok_1[] = array("nev" => $row_jell['nev'], "ertek" => 'n.a.');
				$jellemzok_2[] = array("nev" => $row_jell['nev'], "ertek" => $row_jell['ertek'].' '.$row_jell['mertekegyseg']);
			}
		}
	}
	$jellemzok_3 = array();
	if (isset($_SESSION['osszahas_3']))
	{
		$termek_szam = $termek_szam + 1;
		$query_3 = 'SELECT * FROM '.$webjel.'termekek WHERE id='.$_SESSION['osszahas_3'];
		$res_3 = $pdo->prepare($query_3);
		$res_3->execute();
		$row_3  = $res_3 -> fetch();

		$query_csop_3 = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_3['csop_id'];
		$res_csop_3 = $pdo->prepare($query_csop_3);
		$res_csop_3->execute();
		$row_csop_3  = $res_csop_3 -> fetch();
		// Jellemzők
		foreach($jellemzok_1 as $tomb_row) // végigmegyek az előző tömbön és belerakom ennek a terméknek a jellemzőit
		{
			/* $query_jell = "SELECT ".$webjel."jellemzok.ertek, 
						".$webjel."jellemzok.mertekegyseg, 
						".$webjel."jellemzok.jellemzo_id, 
						".$webjel."jellemzok.termek_cikkszam, 
						".$webjel."jellemzo_tipusok.nev, 
						".$webjel."jellemzo_tipusok.iml_id 
				FROM ".$webjel."jellemzok 
				INNER JOIN ".$webjel."jellemzo_tipusok 
				ON ".$webjel."jellemzok.jellemzo_id=".$webjel."jellemzo_tipusok.iml_id 
				WHERE ".$webjel."jellemzok.termek_cikkszam='".$row_3["cikkszam"]."' AND ".$webjel."jellemzo_tipusok.nev='".$tomb_row['nev']."' 
				ORDER BY ".$webjel."jellemzo_tipusok.nev ASC"; */
			$query_jell = "SELECT * FROM ".$webjel."termek_parameterek 
				INNER JOIN ".$webjel."termek_termek_parameter_ertekek 
				ON ".$webjel."termek_parameterek.id=".$webjel."termek_termek_parameter_ertekek.termek_parameter_id 
				WHERE ".$webjel."termek_parameterek.tipus='egyedi' AND ".$webjel."termek_termek_parameter_ertekek.termek_id=".$row_3['id']." AND ".$webjel."termek_parameterek.nev='".$tomb_row['nev']."'
				ORDER BY ".$webjel."termek_parameterek.nev ASC";
			$res = $pdo->prepare($query_jell);
			$res->execute();
			$row_jell = $res -> fetch();
			
			if(isset($row_jell['nev']))
			{
				$jellemzok_3[] = array("nev" => $row_jell['nev'], "ertek" => $row_jell['ertek'].' '.$row_jell['mertekegyseg']);
			}
			else
			{
				$jellemzok_3[] = array("nev" => $row_jell['nev'], "ertek" => 'n.a.');
			}
		}
		$query_jell = "SELECT * FROM ".$webjel."termek_parameterek 
			INNER JOIN ".$webjel."termek_termek_parameter_ertekek 
			ON ".$webjel."termek_parameterek.id=".$webjel."termek_termek_parameter_ertekek.termek_parameter_id 
			WHERE ".$webjel."termek_parameterek.tipus='egyedi' AND ".$webjel."termek_termek_parameter_ertekek.termek_id=".$row_3['id']."
			ORDER BY ".$webjel."termek_parameterek.nev ASC";
		foreach ($pdo->query($query_jell) as $row_jell)
		{
			$a = 0;
			foreach($jellemzok_3 as $tomb_row) // Ellenőrzöm, hogy belekerült-e már ez a jellemző a tömbbe
			{
				if($tomb_row['nev'] == $row_jell['nev']) // találat
				{
					$a = 1;
				}
			}
			if($a == 0) // ha nics találat
			{
				$jellemzok_1[] = array("nev" => $row_jell['nev'], "ertek" => 'n.a.');
				$jellemzok_2[] = array("nev" => $row_jell['nev'], "ertek" => 'n.a.');
				$jellemzok_3[] = array("nev" => $row_jell['nev'], "ertek" => $row_jell['ertek'].' '.$row_jell['mertekegyseg']);
			}
		}
	}
	$jellemzok_4 = array();
	if (isset($_SESSION['osszahas_4']))
	{
		$termek_szam = $termek_szam + 1;
		$query_4 = 'SELECT * FROM '.$webjel.'termekek WHERE id='.$_SESSION['osszahas_4'];
		$res_4 = $pdo->prepare($query_4);
		$res_4->execute();
		$row_4  = $res_4 -> fetch();
		
		$query_csop_4 = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_4['csop_id'];
		$res_csop_4 = $pdo->prepare($query_csop_4);
		$res_csop_4->execute();
		$row_csop_4  = $res_csop_4 -> fetch();
		// Jellemzők
		foreach($jellemzok_1 as $tomb_row) // végigmegyek az előző tömbön és belerakom ennek a terméknek a jellemzőit
		{
			/* $query_jell = "SELECT ".$webjel."jellemzok.ertek, 
						".$webjel."jellemzok.mertekegyseg, 
						".$webjel."jellemzok.jellemzo_id, 
						".$webjel."jellemzok.termek_cikkszam, 
						".$webjel."jellemzo_tipusok.nev, 
						".$webjel."jellemzo_tipusok.iml_id 
				FROM ".$webjel."jellemzok 
				INNER JOIN ".$webjel."jellemzo_tipusok 
				ON ".$webjel."jellemzok.jellemzo_id=".$webjel."jellemzo_tipusok.iml_id 
				WHERE ".$webjel."jellemzok.termek_cikkszam='".$row_4["cikkszam"]."' AND ".$webjel."jellemzo_tipusok.nev='".$tomb_row['nev']."' 
				ORDER BY ".$webjel."jellemzo_tipusok.nev ASC"; */
			$query_jell = "SELECT * FROM ".$webjel."termek_parameterek 
				INNER JOIN ".$webjel."termek_termek_parameter_ertekek 
				ON ".$webjel."termek_parameterek.id=".$webjel."termek_termek_parameter_ertekek.termek_parameter_id 
				WHERE ".$webjel."termek_parameterek.tipus='egyedi' AND ".$webjel."termek_termek_parameter_ertekek.termek_id=".$row_4['id']." AND ".$webjel."termek_parameterek.nev='".$tomb_row['nev']."'
				ORDER BY ".$webjel."termek_parameterek.nev ASC";
			$res = $pdo->prepare($query_jell);
			$res->execute();
			$row_jell = $res -> fetch();
			if(isset($row_jell['nev']))
			{
				$jellemzok_4[] = array("nev" => $row_jell['nev'], "ertek" => $row_jell['ertek'].' '.$row_jell['mertekegyseg']);
			}
			else
			{
				$jellemzok_4[] = array("nev" => $row_jell['nev'], "ertek" => 'n.a.');
			}
		}
		$query_jell = "SELECT * FROM ".$webjel."termek_parameterek 
			INNER JOIN ".$webjel."termek_termek_parameter_ertekek 
			ON ".$webjel."termek_parameterek.id=".$webjel."termek_termek_parameter_ertekek.termek_parameter_id 
			WHERE ".$webjel."termek_parameterek.tipus='egyedi' AND ".$webjel."termek_termek_parameter_ertekek.termek_id=".$row_4['id']."
			ORDER BY ".$webjel."termek_parameterek.nev ASC";
		foreach ($pdo->query($query_jell) as $row_jell)
		{
			$a = 0;
			foreach($jellemzok_4 as $tomb_row) // Ellenőrzöm, hogy belekerült-e már ez a jellemző a tömbbe
			{
				if($tomb_row['nev'] == $row_jell['nev']) // találat
				{
					$a = 1;
				}
			}
			if($a == 0) // ha nics találat
			{
				$jellemzok_1[] = array("nev" => $row_jell['nev'], "ertek" => 'n.a.');
				$jellemzok_2[] = array("nev" => $row_jell['nev'], "ertek" => 'n.a.');
				$jellemzok_3[] = array("nev" => $row_jell['nev'], "ertek" => 'n.a.');
				$jellemzok_4[] = array("nev" => $row_jell['nev'], "ertek" => $row_jell['ertek'].' '.$row_jell['mertekegyseg']);
			}
		}
	}
	
	// foreach($jellemzok_3 as $tomb_row)
	// {
		// print '<br/>'.$tomb_row['nev'].': '.$tomb_row['ertek'];
	// }
	
	if (isset($_SESSION['osszahas_1'])) { $tomb_hossz = count($jellemzok_1); }
	else if (isset($_SESSION['osszahas_2'])) { $tomb_hossz = count($jellemzok_2); }
	else if (isset($_SESSION['osszahas_3'])) { $tomb_hossz = count($jellemzok_3); }
	else if (isset($_SESSION['osszahas_4'])) { $tomb_hossz = count($jellemzok_4); }
	
	// print $tomb_hossz;
?>

<div class="table-responsive">
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<td></td>
				<?php
					if (isset($_SESSION['osszahas_1']))
					{
						print '<td><a href="'.$domain.'/termekek/'.$row_csop_1['nev_url'].'/'.$row_1['nev_url'].'">';
							$alap_kep = '';
							$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row_1['id']." ORDER BY alap DESC LIMIT 1";
							$res = $pdo->prepare($query_kep);
							$res->execute();
							$row_kep = $res -> fetch();
							$alap_kep = $row_kep['kep'];
							if ($alap_kep == '') 
							{
								print '<img src="'.$domain.'/webshop/images/noimage.png" />';
							}
							else
							{
								print '<img src="'.$domain.'/images/termekek/'.$alap_kep.'" />';
							}
						print '</a></td>';
					}
					if (isset($_SESSION['osszahas_2']))
					{
						print '<td><a href="'.$domain.'/termekek/'.$row_csop_2['nev_url'].'/'.$row_2['nev_url'].'">';
							$alap_kep = '';
							$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row_2['id']." ORDER BY alap DESC LIMIT 1";
							$res = $pdo->prepare($query_kep);
							$res->execute();
							$row_kep = $res -> fetch();
							$alap_kep = $row_kep['kep'];
							if ($alap_kep == '') 
							{
								print '<img src="'.$domain.'/webshop/images/noimage.png" />';
							}
							else
							{
								print '<img src="'.$domain.'/images/termekek/'.$alap_kep.'" />';
							}
						print '</a></td>';
					}
					if (isset($_SESSION['osszahas_3']))
					{
						print '<td><a href="'.$domain.'/termekek/'.$row_csop_3['nev_url'].'/'.$row_3['nev_url'].'">';
							$alap_kep = '';
							$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row_3['id']." ORDER BY alap DESC LIMIT 1";
							$res = $pdo->prepare($query_kep);
							$res->execute();
							$row_kep = $res -> fetch();
							$alap_kep = $row_kep['kep'];
							if ($alap_kep == '') 
							{
								print '<img src="'.$domain.'/webshop/images/noimage.png" />';
							}
							else
							{
								print '<img src="'.$domain.'/images/termekek/'.$alap_kep.'" />';
							}
						print '</a></td>';
					}
					if (isset($_SESSION['osszahas_4']))
					{
						print '<td><a href="'.$domain.'/termekek/'.$row_csop_4['nev_url'].'/'.$row_4['nev_url'].'">';
							$alap_kep = '';
							$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row_4['id']." ORDER BY alap DESC LIMIT 1";
							$res = $pdo->prepare($query_kep);
							$res->execute();
							$row_kep = $res -> fetch();
							$alap_kep = $row_kep['kep'];
							if ($alap_kep == '') 
							{
								print '<img src="'.$domain.'/webshop/images/noimage.png" />';
							}
							else
							{
								print '<img src="'.$domain.'/images/termekek/'.$alap_kep.'" />';
							}
						print '</a></td>';
					}
				?>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th>Név</th>
				<?php
					if (isset($_SESSION['osszahas_1']))
					{
						print '<td><h5><a href="'.$domain.'/termekek/'.$row_csop_1['nev_url'].'/'.$row_1['nev_url'].'">'.$row_1['nev'].'</a></h5></td>';
					}
					if (isset($_SESSION['osszahas_2']))
					{
						print '<td><h5><a href="'.$domain.'/termekek/'.$row_csop_2['nev_url'].'/'.$row_2['nev_url'].'">'.$row_2['nev'].'</a></h5></td>';
					}
					if (isset($_SESSION['osszahas_3']))
					{
						print '<td><h5><a href="'.$domain.'/termekek/'.$row_csop_3['nev_url'].'/'.$row_3['nev_url'].'">'.$row_3['nev'].'</a></h5></td>';
					}
					if (isset($_SESSION['osszahas_4']))
					{
						print '<td><h5><a href="'.$domain.'/termekek/'.$row_csop_4['nev_url'].'/'.$row_4['nev_url'].'">'.$row_4['nev'].'</a></h5></td>';
					}
				?>
			</tr>
			<?php
				// Jellemzők
				for ($i=0; $i<=($tomb_hossz-1); $i++)
				{
					$a = 0;
					print '<tr>';
					if (isset($_SESSION['osszahas_1']))
					{
						print '<th>'.$jellemzok_1[$i]['nev'].'</th>';
						$a = 1;
						print '<td>'.$jellemzok_1[$i]['ertek'].'</td>';
					}
					if (isset($_SESSION['osszahas_2']))
					{
						if($a == 0)
						{
							print '<th>'.$jellemzok_2[$i]['nev'].'</th>';
							$a = 1;
						}
						print '<td>'.$jellemzok_2[$i]['ertek'].'</td>';
					}
					if (isset($_SESSION['osszahas_3']))
					{
						if($a == 0)
						{
							print '<th>'.$jellemzok_3[$i]['nev'].'</th>';
							$a = 1;
						}
						print '<td>'.$jellemzok_3[$i]['ertek'].'</td>';
					}
					if (isset($_SESSION['osszahas_4']))
					{
						if($a == 0)
						{
							print '<th>'.$jellemzok_4[$i]['nev'].'</th>';
							$a = 1;
						}
						print '<td>'.$jellemzok_4[$i]['ertek'].'</td>';
					}
					print '</tr>';
				}

			?>
			<tr>
				<th>Ár</th>
				<?php
					if (isset($_SESSION['osszahas_1']))
					{
						if ($row_1['akcio_ig'] >= date('Y-m-d') && $row_1['akcio_tol'] <= date('Y-m-d')) //Akciós
						{
							print '<td><span class="price-old">'.number_format($row_1['ar'], 0, ',', ' ').' Ft</span><span class="price-new">'.number_format($row_1['akciosar'], 0, ',', ' ').' Ft</span></td>';
						}
						else
						{
							print '<td><span class="price-normal">'.number_format($row_1['ar'], 0, ',', ' ').' Ft</span></td>';
						}
					}
					if (isset($_SESSION['osszahas_2']))
					{
						if ($row_2['akcio_ig'] >= date('Y-m-d') && $row_2['akcio_tol'] <= date('Y-m-d')) //Akciós
						{
							print '<td><span class="price-old">'.number_format($row_2['ar'], 0, ',', ' ').' Ft</span><span class="price-new">'.number_format($row_2['akciosar'], 0, ',', ' ').' Ft</span></td>';
						}
						else
						{
							print '<td><span class="price-normal">'.number_format($row_2['ar'], 0, ',', ' ').' Ft</span></td>';
						}
					}
					if (isset($_SESSION['osszahas_3']))
					{
						if ($row_3['akcio_ig'] >= date('Y-m-d') && $row_3['akcio_tol'] <= date('Y-m-d')) //Akciós
						{
							print '<td><span class="price-old">'.number_format($row_3['ar'], 0, ',', ' ').' Ft</span><span class="price-new">'.number_format($row_3['akciosar'], 0, ',', ' ').' Ft</span></td>';
						}
						else
						{
							print '<td><span class="price-normal">'.number_format($row_3['ar'], 0, ',', ' ').' Ft</span></td>';
						}
					}
					if (isset($_SESSION['osszahas_4']))
					{
						if ($row_4['akcio_ig'] >= date('Y-m-d') && $row_4['akcio_tol'] <= date('Y-m-d')) //Akciós
						{
							print '<td><span class="price-old">'.number_format($row_4['ar'], 0, ',', ' ').' Ft</span><span class="price-new">'.number_format($row_4['akciosar'], 0, ',', ' ').' Ft</span></td>';
						}
						else
						{
							print '<td><span class="price-normal">'.number_format($row_4['ar'], 0, ',', ' ').' Ft</span></td>';
						}
					}
				?>
			</tr>
		</tbody>
	</table>
</div>

