<!--Ár szerint-->
	<?php
		if (isset($_GET['kat_urlnev'])) //ha van kategória kiválasztva
		{
			$res = $pdo->prepare("SELECT * FROM ".$webjel."term_csoportok where nev_url='".$_GET['kat_urlnev']."'");
			$res->execute();
			$row  = $res -> fetch();
			$where = ' WHERE csop_id='.$row['id'];
		}
		else
		{
			$where = '';
		}
	// Maximum ár meghatározása
		$res = $pdo->prepare('SELECT MAX(IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar)) as max_ar FROM '.$webjel.'termekek'.$where);
		$res->execute();
		$row  = $res -> fetch();
		$szuro_max_ar = ceil($row['max_ar'] / 1000) * 1000;
	// Minimum ár meghatározása
		$res = $pdo->prepare('SELECT MIN(IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar)) as min_ar FROM '.$webjel.'termekek'.$where);
		$res->execute();
		$row  = $res -> fetch();
		$szuro_min_ar = floor($row['min_ar'] / 1000) * 1000;
	// Aktuális minimum és maximum
		if (isset($_SESSION['szuro_minimum_ar']) || isset($_SESSION['szuro_maximum_ar']))
		{
			$szuro_minimum_ar = $_SESSION['szuro_minimum_ar'];
			$szuro_maximum_ar = $_SESSION['szuro_maximum_ar'];
		}
		else
		{
			$szuro_minimum_ar = $szuro_min_ar;
			$szuro_maximum_ar = $szuro_max_ar;
		}
		
	?>
	<div class="box szuro">
		<div class="box-heading"><h3><i class="flaticon-filter20"></i>Szűrő</h3></div>
		<div class="box-content">
			<div id="filter-box">                                                           
				<script>
					function szuro_ellenorzes()
					{
						var mehet = 0;
						// Ha egy feltétel sincs megadva, akkor nem szűr
						if(document.getElementById("sex_male").checked == false && document.getElementById("sex_female").checked == false && document.getElementById("age_young").checked == false && document.getElementById("age_middle").checked == false && document.getElementById("age_old").checked == false && document.getElementById("szuro_minimum_ar").value == <?php print $szuro_min_ar; ?> && document.getElementById("szuro_maximum_ar").value == <?php print $szuro_max_ar; ?>)
						{
							var mehet = 1;
						}
						if(mehet == 0)
						{
							document.getElementById('szuro_form').submit();
						}
					}
				</script>
				<form id="szuro_form" action="<?php if($oldal != 'termekek'){print 'http://'.$domain.'/termekek/';} ?>" method="POST" >
					<div class="filter_group_title">Nem</div>
					<div class="filter_group sex">
						<input type="checkbox" id="sex_male" name="szuro_nem_ffi" value="11" <?php if(isset($_SESSION['szuro_nem_ffi']) && $_SESSION['szuro_nem_ffi']=='11'){print 'checked';} ?>/>
						<input type="checkbox" id="sex_female" name="szuro_nem_no" value="12" <?php if(isset($_SESSION['szuro_nem_no']) && $_SESSION['szuro_nem_no']=='12'){print 'checked';} ?>/>
						<label data-for="sex_male" id="filter_11"><i class="male"></i><br>Férfi</label><label data-for="sex_female" id="filter_12"><i class="female"></i><br>Nő</label>
					</div>
					<div class="filter_group_title">Életkor</div>
					<div class="filter_group age">
						<input type="checkbox" id="age_young" name="szuro_eletkor_fiatal" value="21" <?php if(isset($_SESSION['szuro_eletkor_fiatal']) && $_SESSION['szuro_eletkor_fiatal']=='21'){print 'checked';} ?>/>
						<input type="checkbox" id="age_middle" name="szuro_eletkor_felnott" value="22" <?php if(isset($_SESSION['szuro_eletkor_felnott']) && $_SESSION['szuro_eletkor_felnott']=='22'){print 'checked';} ?>/>
						<input type="checkbox" id="age_old" name="szuro_eletkor_idos" value="23" <?php if(isset($_SESSION['szuro_eletkor_idos']) && $_SESSION['szuro_eletkor_idos']=='23'){print 'checked';} ?>/>
						<label data-for="age_young" id="filter_21"><i class="young"></i><br>Fiatal</label><label data-for="age_middle" id="filter_22"><i class="middle"></i><br>Felnőtt</label><label data-for="age_old" id="filter_23"><i class="old"></i><br>Idős</label>
					</div>
					<div class="filter_group_title">Minimum ár</div>
					<div class="filter_group age">
						<input id="szuro_minimum_ar" name="szuro_minimum_ar" type="range" min="<?php print $szuro_min_ar; ?>" max="<?php print $szuro_max_ar; ?>" step="1000" value="<?php print $szuro_minimum_ar; ?>" data-rangeslider>
						<p><output class="szuro_ar_osszeg"></output> Ft</p>
					</div>
					<div class="filter_group_title">Maximum ár</div>
					<div class="filter_group age">
						<input id="szuro_maximum_ar" name="szuro_maximum_ar" type="range" min="<?php print $szuro_min_ar; ?>" max="<?php print $szuro_max_ar; ?>" step="1000" value="<?php print $szuro_maximum_ar; ?>" data-rangeslider>
						<p><output class="szuro_ar_osszeg"></output> Ftgggg</p>
					</div>
					<input type="hidden" name="szuro_inditasa" value="ok">
				</form>
				<form id="szuro_torles_form" action="" method="POST">
					<input type="hidden" name="szuro_torlese" value="">
				</form>
				
				<input onclick="szuro_ellenorzes()" type="submit" value="Keresés" class="btn btn-primary" />
				<input onclick="document.getElementById('szuro_torles_form').submit();" type="submit" value="Törlés" class="btn btn-active" style="float:right;" />
					
			</div>
		</div>
	</div>
	
    <script>
    $(function() {

        var $document   = $(document),
            selector    = '[data-rangeslider]',
            $element    = $(selector);

        // Example functionality to demonstrate a value feedback
        function valueOutput(element) {
            var value = element.value,
                output = element.parentNode.getElementsByTagName('output')[0];
            output.innerHTML = value;
        }
        for (var i = $element.length - 1; i >= 0; i--) {
            valueOutput($element[i]);
        };
        $document.on('change', 'input[type="range"]', function(e) {
            valueOutput(e.target);
        });

        // Example functionality to demonstrate disabled functionality
        $document .on('click', '#js-example-disabled button[data-behaviour="toggle"]', function(e) {
            var $inputRange = $('input[type="range"]', e.target.parentNode);

            if ($inputRange[0].disabled) {
                $inputRange.prop("disabled", false);
            }
            else {
                $inputRange.prop("disabled", true);
            }
            $inputRange.rangeslider('update');
        });

        // Example functionality to demonstrate programmatic value changes
        $document.on('click', '#js-example-change-value button', function(e) {
            var $inputRange = $('input[type="range"]', e.target.parentNode),
                value = $('input[type="number"]', e.target.parentNode)[0].value;

            $inputRange.val(value).change();
        });

        // Example functionality to demonstrate destroy functionality
        $document
            .on('click', '#js-example-destroy button[data-behaviour="destroy"]', function(e) {
                $('input[type="range"]', e.target.parentNode).rangeslider('destroy');
            })
            .on('click', '#js-example-destroy button[data-behaviour="initialize"]', function(e) {
                $('input[type="range"]', e.target.parentNode).rangeslider({ polyfill: false });
            });

        // Basic rangeslider initialization
        $element.rangeslider({

            // Deactivate the feature detection
            polyfill: false,

            // Callback function
            onInit: function() {},

            // Callback function
            onSlide: function(position, value) {
                console.log('onSlide');
                console.log('position: ' + position, 'value: ' + value);
            },

            // Callback function
            onSlideEnd: function(position, value) {
                console.log('onSlideEnd');
                console.log('position: ' + position, 'value: ' + value);
            }
        });

    });
    </script>
	
