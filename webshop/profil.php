<?php
	if (isset($_SESSION['login_id']))
	{
		if (isset($_POST['command']) && $_POST['command'] == "MOD")
		{
			include $gyoker.'/webshop/profil_check.php';
			$jelszo = $_POST['jszo1'];
			if (profil_check($_POST) == "rendben")
			{
				if (isset($_POST['mascim']) && $_POST['mascim'] == 'ok')
				{
					$cim_szall_nev = $_POST['cim_szall_nev'];
					$cim_szall_varos = $_POST['cim_szall_varos'];
					$cim_szall_utca = $_POST['cim_szall_utca'];
					$cim_szall_hszam = $_POST['cim_szall_hszam'];
					$cim_szall_irszam = $_POST['cim_szall_irszam'];
				}
				else
				{
					$cim_szall_nev = '';
					$cim_szall_varos = '';
					$cim_szall_utca = '';
					$cim_szall_hszam = '';
					$cim_szall_irszam = '';
				}
				if($jelszo == "" )
				{
					$email = $_POST['email'];
					$telefon = $_POST['telefon'];
					$vezeteknev = $_POST['vezeteknev'];
					$cim_varos = $_POST['cim_varos'];
					$cim_utca = $_POST['cim_utca'];
					$cim_hszam = $_POST['cim_hszam'];
					$cim_irszam = $_POST['cim_irszam'];
					$cim_szall_nev = $_POST['cim_szall_nev'];
					$cim_szall_varos = $_POST['cim_szall_varos'];
					$cim_szall_utca = $_POST['cim_szall_utca'];
					$cim_szall_hszam = $_POST['cim_szall_hszam'];
					$cim_szall_irszam = $_POST['cim_szall_irszam'];
					$id = $_SESSION['login_id'];
					$query = 'UPDATE '.$webjel.'users SET email=?, telefon=?, vezeteknev=?, cim_varos=?, cim_utca=?, cim_hszam=?, cim_irszam=?, cim_szall_nev=?, cim_szall_varos=?, cim_szall_utca=?, cim_szall_hszam=?, cim_szall_irszam=? WHERE id=?';
					$result = $pdo->prepare($query);
					$result->execute(array($email,$telefon,$vezeteknev,$cim_varos,$cim_utca,$cim_hszam,$cim_irszam,$cim_szall_nev,$cim_szall_varos,$cim_szall_utca,$cim_szall_hszam,$cim_szall_irszam,$id));
				}
				else
				{
					$email = $_POST['email'];
					$telefon = $_POST['telefon'];
					$vezeteknev = $_POST['vezeteknev'];
					$cim_varos = $_POST['cim_varos'];
					$cim_utca = $_POST['cim_utca'];
					$cim_hszam = $_POST['cim_hszam'];
					$cim_irszam = $_POST['cim_irszam'];
					$cim_szall_nev = $_POST['cim_szall_nev'];
					$cim_szall_varos = $_POST['cim_szall_varos'];
					$cim_szall_utca = $_POST['cim_szall_utca'];
					$cim_szall_hszam = $_POST['cim_szall_hszam'];
					$cim_szall_irszam = $_POST['cim_szall_irszam'];
					$id = $_SESSION['login_id'];
					$jelszo = password_hash($jelszo, PASSWORD_DEFAULT);
					$query = 'UPDATE '.$webjel.'users SET email=?, telefon=?, vezeteknev=?, cim_varos=?, cim_utca=?, cim_hszam=?, cim_irszam=?, cim_szall_nev=?, cim_szall_varos=?, cim_szall_utca=?, cim_szall_hszam=?, cim_szall_irszam=?, jelszo=? WHERE id=?';
					$result = $pdo->prepare($query);
					$result->execute(array($email,$telefon,$vezeteknev,$cim_varos,$cim_utca,$cim_hszam,$cim_irszam,$cim_szall_nev,$cim_szall_varos,$cim_szall_utca,$cim_szall_hszam,$cim_szall_irszam,$jelszo,$id));
				}
				print '<div class="alert alert-success alert-dismissable"><p>Az adatok sikeresen elmentve!</p></div>';
				if (isset($_SESSION['kosar_id']))
				{
					header ('Location: '.$domain.'/kassza/');
				}
			}
			else
			{
				print '<div class="alert alert-danger alert-dismissable"><p>'.profil_check($_POST).'</p></div>';
			}
		}
		$query = "SELECT * FROM ".$webjel."users where id=".$_SESSION['login_id'];
		$res = $pdo->prepare($query);
		$res->execute();
		$row  = $res -> fetch();
?>
		<form id="mentesForm" action="" method="post">
			<div class="col-lg-6 col-md-6">
				<h4><?=$sz_regisztracio_12?></h4>
				<div class="row" style="margin-top: 20px;">
					<div class="col-lg-12 col-md-12">
						<div class="form-wrap form-wrap-validation">
	                  		<label class="form-label" for="felhnev"><?=$sz_regisztracio_39?></label>
	                  			<input type="text" name="felhnev" id="felhnev" value="<?php print $row['felhnev']; ?>" class="form-input" readonly >
	                  	</div>						
					</div>
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-lg-12 col-md-12">
						<div class="form-wrap form-wrap-validation">
	                  		<label class="form-label" for="jszo1"><?=$sz_regisztracio_17?></label>
	                  			<input type="password" name="jszo1" id="jszo1" value="" class="form-input" >
	                  	</div>						
					</div>
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-lg-12 col-md-12">
						<div class="form-wrap form-wrap-validation">
	                  		<label class="form-label" for="jszo2"><?=$sz_regisztracio_19?></label>
	                  			<input type="password" name="jszo2" id="jszo2" value="" class="form-input" >
	                  	</div>
	                </div> 
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-lg-12 col-md-12">
						<div class="form-wrap form-wrap-validation">
	                  		<label class="form-label" for="vezeteknev"><?=$sz_regisztracio_21?> <span class="text-danger">*</span></label>
	                  			<input type="text" name="vezeteknev" id="vezeteknev" value="<?php print $row['vezeteknev']; ?>" class="form-input" >
	                  	</div>
					</div>
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-lg-12 col-md-12">
						<div class="form-wrap form-wrap-validation">
	                  		<label class="form-label" for="email"><?=$sz_regisztracio_14?> <span class="text-danger">*</span></label>
	                  			<input type="text" name="email" id="email" value="<?php print $row['email']; ?>" class="form-input" >
	                  	</div>						
					</div>
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-lg-12 col-md-12">
						<div class="form-wrap form-wrap-validation">
	                  		<label class="form-label" for="telefon"><?=$sz_regisztracio_23?></label>
	                  			<input type="text" name="telefon" id="telefon" value="<?php print $row['telefon']; ?>" class="form-input" >
	                  	</div>							
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6">
				<h4><?=$sz_regisztracio_24?></h4>
				<div class="row" style="margin-top: 20px;">
					<div class="col-lg-12 col-md-12">
						<div class="form-wrap form-wrap-validation">
	                  		<label class="form-label" for="cim_varos"><?=$sz_regisztracio_26?></label>
	                  			<input type="text" name="cim_varos" id="cim_varos" value="<?php print $row['cim_varos']; ?>" class="form-input" >
	                  	</div>							
					</div>
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-lg-12 col-md-12">
						<div class="form-wrap form-wrap-validation">
	                  		<label class="form-label" for="cim_utca"><?=$sz_regisztracio_28?></label>
	                  			<input type="text" name="cim_utca" id="cim_utca" value="<?php print $row['cim_utca']; ?>" class="form-input" >
	                  	</div>						
					</div>
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-lg-12 col-md-12">
						<div class="form-wrap form-wrap-validation">
	                  		<label class="form-label" for="cim_hszam"><?=$sz_regisztracio_30?></label>
	                  			<input type="text" name="cim_hszam" id="cim_hszam" value="<?php print $row['cim_hszam']; ?>" class="form-input" >
	                  	</div>						
					</div>
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-lg-12 col-md-12">
						<div class="form-wrap form-wrap-validation">
	                  		<label class="form-label" for="cim_irszam"><?=$sz_regisztracio_32?></label>
	                  			<input type="text" name="cim_irszam" id="cim_irszam" value="<?php print $row['cim_irszam']; ?>" class="form-input" >
	                  	</div>							
					</div>
				</div>
				<div class="row" style="margin-top: 10px;">
					<div class="col-lg-12 col-md-12">
				        <label class="chechack">
				          <input type="checkbox" name="mascim" id="mascim" value="ok" <?php if ($row['cim_szall_varos'] != '') {print 'checked';} ?>>
				          <?=$sz_regisztracio_33?>
				          <span class="checkmark"></span>
				        </label>						
						<?php if ($row['cim_szall_varos'] == '') {$display = 'style="display:none"';} else {$display = '';} ?>
					</div>
				</div>
				<div id="eltero_szallitas" <?php print $display; ?>>
					<div class="row" style="margin-top: 10px;">
						<div id="div2" class="col-lg-12 col-md-12">
							<div class="form-wrap form-wrap-validation">
		                  		<label class="form-label" for="cim_szall_nev"><?=$sz_regisztracio_21?></label>
		                  			<input type="text" name="cim_szall_nev" id="cim_szall_nev" value="<?php print $row['cim_szall_nev']; ?>" class="form-input" >
		                  	</div>							
						</div>
					</div>
					<div class="row" style="margin-top: 10px;">
						<div id="div4" class="col-lg-12 col-md-12">
							<div class="form-wrap form-wrap-validation">
		                  		<label class="form-label" for="cim_szall_varos"><?=$sz_regisztracio_26?></label>
		                  			<input type="text" name="cim_szall_varos" id="cim_szall_varos" value="<?php print $row['cim_szall_varos']; ?>" class="form-input" >
		                  	</div>							
						</div>
					</div>
					<div class="row" style="margin-top: 10px;">
						<div id="div6" class="col-lg-12 col-md-12">
							<div class="form-wrap form-wrap-validation">
		                  		<label class="form-label" for="cim_szall_utca"><?=$sz_regisztracio_28?></label>
		                  			<input type="text" name="cim_szall_utca" id="cim_szall_utca" value="<?php print $row['cim_szall_utca']; ?>" class="form-input" >
		                  	</div>								
						</div>
					</div>
					<div class="row" style="margin-top: 10px;">
						<div id="div8" class="col-lg-12 col-md-12">
							<div class="form-wrap form-wrap-validation">
		                  		<label class="form-label" for="cim_szall_hszam"><?=$sz_regisztracio_30?></label>
		                  			<input type="text" name="cim_szall_hszam" id="cim_szall_hszam" value="<?php print $row['cim_szall_hszam']; ?>" class="form-input" >
		                  	</div>							
						</div>
					</div>
					<div class="row" style="margin-top: 10px;">
						<div id="div10" class="col-lg-12 col-md-12">
							<div class="form-wrap form-wrap-validation">
		                  		<label class="form-label" for="cim_szall_irszam"><?=$sz_regisztracio_32?></label>
		                  			<input type="text" name="cim_szall_irszam" id="cim_szall_irszam" value="<?php print $row['cim_szall_irszam']; ?>" class="form-input" >
		                  	</div>							
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-12 col-md-12" style="margin-bottom:10px; margin-top: 20px; float: left;">
					<a onClick="document.getElementById('mentesForm').submit();" class="button button-primary"/><?=$sz_regisztracio_40?></a>
					<input type="hidden" name="command" value="MOD">
			</div>
		</form>
<?php		
	}
	else
	{
		print '<p style="text-align:center;">Kérem jelentkezzen be!</p>';
	}
?>