// Városok generálása
	function varosok_gls() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("gls_varos_div").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("POST","../webshop/module/glscsomagpont_varosok.php?script=ok",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("megye="+encodeURIComponent(document.getElementById("gls_megye").value));
	}
// Pontok keresése
	function glsKeres() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("gls_talalat_div").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("POST","../webshop/module/glscsomagpont_talalatok.php?script=ok",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("varos="+encodeURIComponent(document.getElementById("gls_varos").value));
	}
// Pont kiválasztása
	function glsKivalaszt(id) {
		document.getElementById("valasztott_gls_cspont").value = document.getElementById("gls_adat_"+id).value;
	}
// Kereső megnyitása
	function glsKeresoNyitas() {
		$( "#gls_valaszto" ).slideDown( 300, function() { });
	}
// Kereső zárása
	function glsKeresoZaras() {
		$( "#gls_valaszto" ).slideUp( 300, function() { });
		document.getElementById("valasztott_gls_cspont").value = '';
		document.getElementById("gls_varos").value = 0;
		document.getElementById("gls_talalat_div").innerHTML='';
		document.getElementById("gls_riaszt").innerHTML='';
	}
// Ellenőrzés a gls_cspont miatt
	function gls_elenorzes() {
		if(document.getElementById("gls_cspont").checked == true && document.getElementById("valasztott_gls_cspont").value == '') {
			document.getElementById("gls_riaszt").innerHTML='<div class="alert alert-danger alert-dismissable">Válassz GLS CsomagPontot!</p></div>';
			return false;
		}
		else
		{
			return true;
		}
	}
