// Tartalmak nyitása zárása
	$(document).on("click", ".kassza_title", function() {
		$( "#adatok_tartalom" ).slideUp( 300, function() { });
		$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
		$( "#osszesites_tartalom" ).slideUp( 300, function() { });
		$( "#"+$(this).attr('attr_tart_id') ).slideDown( 300, function() { });
	});
// Tovább gombok
	$(document).on("click", ".kassza_tovabb_gomb", function() {
		var tartalom_id = $(this).attr('attr_tart_id');
		var horgony_id = $(this).attr('attr_horg');
		$( "#adatok_tartalom" ).slideUp( 300, function() { });
		$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
		$( "#osszesites_tartalom" ).slideUp( 300, function() { });
		$( "#" + tartalom_id).slideDown( 300, function() {
			$('html, body').animate({
				scrollTop: $("#"+horgony_id).offset().top - 80
				// scrollTop: $("#fiz_mod_horgony").offset().top - 80
			}, 500);
		});
	});
// Regisztráció checkbox
	$(document).on("click", "#regisztracio_checkbox", function() {
		if ($(this).is(":checked")) {
			$( "#jelszo_div" ).slideDown( 300, function() { });
			$( "#jelszo2_div" ).slideDown( 300, function() { });
			$( "#feltetel_div" ).slideDown( 300, function() { });
		}
		else {
			$( "#jelszo_div" ).slideUp( 300, function() { });
			$( "#jelszo2_div" ).slideUp( 300, function() { });
			$( "#feltetel_div" ).slideUp( 300, function() { });
			$( "#jelszo" ).val('');
			$( "#jelszo2" ).val('');
		}
	});
// Eltérő cím nyítás/csukás
	$(document).on("click", "#mascim", function() {
		if ($(this).is(":checked")) {
			$( "#eltero_cim" ).slideDown( 300, function() { });
			$( "#szallcim1_span" ).html($("#cim_szall_irszam").val() + " " + $("#cim_szall_varos").val());
			$( "#szallcim2_span" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());
			$( "#szallcim1_span2" ).html($("#cim_szall_irszam").val() + " " + $("#cim_szall_varos").val());
			$( "#szallcim2_span2" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());
		}
		else
		{
			$( "#eltero_cim" ).slideUp( 300, function() { });
			$( "#cim_szall_nev" ).val('');
			$( "#cim_szall_varos" ).val('');
			$( "#cim_szall_utca" ).val('');
			$( "#cim_szall_hszam" ).val('');
			$( "#cim_szall_irszam" ).val('');
			$( "#szallcim1_span" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
			$( "#szallcim2_span" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
			$( "#szallcim1_span2" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
			$( "#szallcim2_span2" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
			$( "#nev2_span" ).html('');
			$( "#nev2_span2" ).html('');
		}
	});
// Adatok beírása az összesítésbe
	// Név
	$(document).on("change", "#nev", function() {
		$( "#nev_span" ).html($(this).val());
		$( "#nev1_span" ).html($(this).val());
		$( "#nev1_span2" ).html($(this).val());
	});
	// Email
	$(document).on("change", "#email", function() {
		$( "#email_span" ).html($(this).val());
	});
	// Telefon
	$(document).on("change", "#telefon", function() {
		$( "#telefon_span" ).html($(this).val());
	});
	// cim_varos
	$(document).on("change", "#cim_varos", function() {
		$( "#cim1_span" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
		$( "#cim1_span2" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
		if ($("#mascim").not(":checked")) {
			$( "#szallcim1_span" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
			$( "#szallcim1_span2" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
		}
	});
	// cim_irszam
	$(document).on("change", "#cim_irszam", function() {
		$( "#cim1_span" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
		$( "#cim1_span2" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
		if ($("#mascim").not(":checked")) {
			$( "#szallcim1_span" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
			$( "#szallcim1_span2" ).html($("#cim_irszam").val() + " " + $("#cim_varos").val());
		}
	});
	// cim_utca
	$(document).on("change", "#cim_utca", function() {
		$( "#cim2_span" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
		$( "#cim2_span2" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
		if ($("#mascim").not(":checked")) {
			$( "#szallcim2_span" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
			$( "#szallcim2_span2" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
		}
	});
	// cim_hszam
	$(document).on("change", "#cim_hszam", function() {
		$( "#cim2_span" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
		$( "#cim2_span2" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
		if ($("#mascim").not(":checked")) {
			$( "#szallcim2_span" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
			$( "#szallcim2_span2" ).html($("#cim_utca").val() + " " + $("#cim_hszam").val());
		}
	});
	// Száll Név
	$(document).on("change", "#cim_szall_nev", function() {
		$( "#nev2_span" ).html($(this).val());
		$( "#nev2_span2" ).html($(this).val());
	});
	// cim_varos
	$(document).on("change", "#cim_szall_varos", function() {
		$( "#szallcim1_span" ).html($("#cim_szall_irszam").val() + " " + $("#cim_szall_varos").val());
	});
	// cim_irszam
	$(document).on("change", "#cim_szall_irszam", function() {
		$( "#szallcim1_span" ).html($("#cim_szall_irszam").val() + " " + $("#cim_szall_varos").val());
	});
	// cim_utca
	$(document).on("change", "#cim_szall_utca", function() {
		$( "#szallcim2_span" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());
		$( "#szallcim2_span2" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());
	});
	// cim_hszam
	$(document).on("change", "#cim_szall_hszam", function() {
		$( "#szallcim2_span" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());
		$( "#szallcim2_span2" ).html($("#cim_szall_utca").val() + " " + $("#cim_szall_hszam").val());
	});
	// Szállítási mód	
	/* $(document).on("click", ".szall_mod_checkbox", function() {
		$( "#szall_mod_span" ).html($(this).val());
		if($(this).attr('id') != 'postapont'){
			$( "#szall_mod_pp_span" ).html('');
		}
		else if($(this).attr('id') != 'gls_cspont'){
			$( "#szall_mod_gls_span" ).html('');
		}
	}); */
	$(document).on("click", ".pp_talalat", function() {
		$( "#szallcim1_span" ).html($("#valasztott_postapont").val());
		$( "#szallcim2_span" ).html('');
		$( "#szallcim1_span2" ).html($("#valasztott_postapont").val());
		$( "#szallcim2_span2" ).html('');
		$( "#gls_span" ).html('');
		$( "#pp_span" ).html('<b>Választott PostaPont:</b> ' + $("#valasztott_postapont").val());
	});
	$(document).on("click", ".gls_talalat", function() {
		$( "#szallcim1_span" ).html($("#valasztott_gls_cspont").val());
		$( "#szallcim2_span" ).html('');
		$( "#szallcim1_span2" ).html($("#valasztott_gls_cspont").val());
		$( "#szallcim2_span2" ).html('');
		$( "#pp_span" ).html('');
		$( "#gls_span" ).html('<b>Választott GLS csomagpont:</b> ' + $("#valasztott_gls_cspont").val());
	});
	// Fizetése mód	
	/* $(document).on("click", ".fiz_mod_checkbox", function() {
		$( "#fiz_mod_span" ).html($(this).val());
	}); */
	
// Fizetési mód kiválasztása	
	$(document).on("change", "#fiz_mod_select, #szall_mod_select", function() {
		// alert(8);
		$.post(domain+'/webshop/kassza_4_mod_szall_koltseg_meghat.php',{
				kosar_id : $(this).data('kosar'),
				fiz_mod : $('#fiz_mod_select').val(),
				szall_mod : $('#szall_mod_select').val()
			},function(response,status){ // Required Callback Function
				var szall_koltseg = response;
				var osszesen = $('#vegosszeghez_osszesen').val();
				var ar_kedvezmeny = $('#vegosszeghez_ar_kedvezmeny').val();
				var fiezetendo = Number(szall_koltseg) + Number(osszesen) + Number(ar_kedvezmeny);
				if ($('#valutacucc').data('valuta') != 'Ft')
				{
					var szall_koltseg = Math.round(szall_koltseg / $('#valutacucc').data('arfolyam'));
					var osszesen = Math.round(osszesen / $('#valutacucc').data('arfolyam'));
					var ar_kedvezmeny = Math.round(ar_kedvezmeny / $('#valutacucc').data('arfolyam'));
					var fiezetendo = Math.round(fiezetendo / $('#valutacucc').data('arfolyam'));
				}				
				$('#fiz_szall_mod_koltseg').html(szall_koltseg.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
				// Összesítéshez
				$('#szall_kolts_span').html(szall_koltseg.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
				$('#fizetendo_span').html(fiezetendo.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
				$('#fiz_mod_span').html($('#fiz_mod_select option:selected').text());
				$('#szall_mod_span').html($('#szall_mod_select option:selected').text());
			});

	});
	$(document).on("change", "#fiz_mod_select", function() {
		$( "#fiz_leiras_p" ).animate({
			height: [ "toggle", "swing" ]
		}, 300, function(){
			$('#fiz_leiras_p').html($('#fiz_mod_select option:selected').attr('attr_leiras'));
			$( "#fiz_leiras_p" ).animate({
				height: [ "toggle", "swing" ]
			}, 300);
		});
	});
	$(document).on("change", "#szall_mod_select", function() {
		if ($('#szall_mod_select').val() == 4)
		{
			$('#gls_cspont').trigger('click');
		}
		else
		{
			$('#gls_cspont').prop('checked', false);
			glsKeresoZaras();
		}
		$( "#szall_leiras_p" ).animate({
			height: [ "toggle", "swing" ]
		}, 300, function(){
			$('#szall_leiras_p').html($('#szall_mod_select option:selected').attr('attr_leiras'));
			$( "#szall_leiras_p" ).animate({
				height: [ "toggle", "swing" ]
			}, 300);
		});
	});	
	

// Rendelés leadása
	$(document).on("click", "#rendeles_leadasa", function() {
		$( ".riaszt_div" ).css('display', 'none');
		alert1 = '<span class="form-validation">A mező kitöltése kötelező!</span>';
		if($('#belepve').val() == 0)
		{
			if($('#nev').val() == ''){
				$( "#nev_riaszt" ).css('display', 'block');
				$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
				$( "#osszesites_tartalom" ).slideUp( 300, function() { });
				$( "#adatok_tartalom" ).slideDown( 300, function() { });
				$( "#nev" ).focus();
				return false;
			}
			else if($('#email').val() == ''){
				$( "#email_riaszt" ).css('display', 'block');
				$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
				$( "#osszesites_tartalom" ).slideUp( 300, function() { });
				$( "#adatok_tartalom" ).slideDown( 300, function() { });
				$( "#email" ).focus();
				return false;
			}
			else if($('#telefon').val() == ''){
				$( "#telefon_riaszt" ).css('display', 'block');
				$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
				$( "#osszesites_tartalom" ).slideUp( 300, function() { });
				$( "#adatok_tartalom" ).slideDown( 300, function() { });
				$( "#telefon" ).focus();
				return false;
			}
			else if($('#cim_irszam').val() == ''){
				$( "#cim_irszam_riaszt" ).css('display', 'block');
				$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
				$( "#osszesites_tartalom" ).slideUp( 300, function() { });
				$( "#adatok_tartalom" ).slideDown( 300, function() { });
				$( "#cim_irszam" ).focus();
				return false;
			}
			else if($('#cim_varos').val() == ''){
				$( "#cim_varos_riaszt" ).css('display', 'block');
				$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
				$( "#osszesites_tartalom" ).slideUp( 300, function() { });
				$( "#adatok_tartalom" ).slideDown( 300, function() { });
				$( "#cim_varos" ).focus();
				return false;
			}
			else if($('#cim_utca').val() == ''){
				$( "#cim_utca_riaszt" ).css('display', 'block');
				$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
				$( "#osszesites_tartalom" ).slideUp( 300, function() { });
				$( "#adatok_tartalom" ).slideDown( 300, function() { });
				$( "#cim_utca" ).focus();
				return false;
			}
			else if($('#cim_hszam').val() == ''){
				$( "#cim_hszam_riaszt" ).css('display', 'block');
				$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
				$( "#osszesites_tartalom" ).slideUp( 300, function() { });
				$( "#adatok_tartalom" ).slideDown( 300, function() { });
				$( "#cim_hszam" ).focus();
				return false;
			}
			// Ha regisztrál
			else if ($("#regisztracio_checkbox").is(":checked")) {
				if($('#jelszo').val() == ''){
					$( "#jelszo_riaszt" ).css('display', 'block');
					$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
					$( "#osszesites_tartalom" ).slideUp( 300, function() { });
					$( "#adatok_tartalom" ).slideDown( 300, function() { });
					$( "#jelszo" ).focus();
					return false;
				}
				else if($('#jelszo').val() != $('#jelszo2').val()){
					$( "#jelszo_riaszt2" ).css('display', 'block');
					$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
					$( "#osszesites_tartalom" ).slideUp( 300, function() { });
					$( "#adatok_tartalom" ).slideDown( 300, function() { });
					$( "#jelszo" ).focus();
					return false;
				}
				else if($('#feltetel').is(":checked") != true){
					$( "#feltetel_riaszt" ).css('display', 'block');
					$( "#szall_mod_tartalom" ).slideUp( 300, function() { });
					$( "#osszesites_tartalom" ).slideUp( 300, function() { });
					$( "#adatok_tartalom" ).slideDown( 300, function() { });
					$( "#feltetel" ).focus();
					return false;
				}
			};
		}
		// Postapont
		/* if($("#postapont").is(":checked") && $('#valasztott_postapont').val() == '')
		{
			// document.getElementById("pp_riaszt").innerHTML='<div class="alert alert-danger alert-dismissable">Válassz Posta Pontot!</p></div>';
			$( "#postapont_riaszt" ).css('display', 'block');
			$( "#osszesites_tartalom" ).slideUp( 300, function() { });
			$( "#adatok_tartalom" ).slideUp( 300, function() { });
			$( "#szall_mod_tartalom" ).slideDown( 300, function() { });
			$('html, body').animate({
				scrollTop: $('#postapont_horgony').offset().top - 80
			}, 500);
			return false;
		} */
		// GLS pont
		if(document.getElementById("gls_cspont").checked == true && document.getElementById("valasztott_gls_cspont").value == '')
		if($("#gls_cspont").is(":checked") && $('#valasztott_gls_cspont').val() == '')
		{
			// document.getElementById("pp_riaszt").innerHTML='<div class="alert alert-danger alert-dismissable">Válassz Posta Pontot!</p></div>';
			$( "#glspont_riaszt" ).css('display', 'block');
			$( "#osszesites_tartalom" ).slideUp( 300, function() { });
			$( "#adatok_tartalom" ).slideUp( 300, function() { });
			$( "#szall_mod_tartalom" ).slideDown( 300, function() { });
			$('html, body').animate({
				scrollTop: $('#glspont_horgony').offset().top - 80
			}, 500);
			return false;
		}
		if($('#vas_fel').is(":checked") != true){
			$( "#vas_fel_riaszt" ).css('display', 'block');
			return false;
		}	

		if($('#gdpr').is(":checked") != true){
			$( "#gdpr_riaszt" ).css('display', 'block');
			return false;
		}			
		
		// Minden OK
		$("#rendeles_gomb_div").html('<div class="loader"></div>');
		$("#rendeles_leadas_form").submit();
	});

	