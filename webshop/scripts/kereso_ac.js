$(document).ready(function(){
	// AC doboz nyitása
	$("#keres_nev").keyup(function(){
		if($("#keres_nev").val().length > 2) {
			// $( "#kereso_ac_doboz" ).slideDown( 260, function() {
				// AJAX
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						document.getElementById("kereso_ac_doboz").innerHTML=xmlhttp.responseText;
						// Ha nincs találat, akkor összecsukjuk
						if($('#kereso_ac_doboz').is(':empty'))
						{
							$( "#kereso_ac_doboz" ).slideUp(260);
						}
						else
						{
							$( "#kereso_ac_doboz" ).slideDown(260);
						}
						// Vastagítás
						$('#kereso_ac_doboz li').each(function() {
							var text = $(this).text();
							var keres_nev = $("#keres_nev").val().toLowerCase()
							$(this).html(text.replace( keres_nev, '<span class="kereso_ac_doboz_kiemelt_talalat">'+keres_nev+'</span>' )); 
						});
						// Ha nincs akciós termék, akkor kisebbre veszi a találati ablakot
						if (($(".kereso_ac_doboz_akciok").length > 0)){
						   $('#kereso_ac_doboz').css('width', '600px')
						}
						else {
						   $('#kereso_ac_doboz').css('width', '300px')
						}
					}
				  }
				xmlhttp.open("POST",domain+"/webshop/module/mod_kereso_ac_talalat.php?script=ok",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send("keres_nev="+encodeURIComponent(document.getElementById("keres_nev").value));
			// });
		}
		else {
			$( "#kereso_ac_doboz" ).slideUp(260);
		}
	});

});

$(document).ready(function(){
	// AC doboz nyitása
	$("#keres_nev2").keyup(function(){
		if($("#keres_nev2").val().length > 2) {
			// $( "#kereso_ac_doboz" ).slideDown( 260, function() {
				// AJAX
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						document.getElementById("kereso_ac_doboz2").innerHTML=xmlhttp.responseText;
						// Ha nincs találat, akkor összecsukjuk
						if($('#kereso_ac_doboz2').is(':empty'))
						{
							$( "#kereso_ac_doboz2" ).slideUp(260);
						}
						else
						{
							$( "#kereso_ac_doboz2" ).slideDown(260);
						}
						// Vastagítás
						$('#kereso_ac_doboz li').each(function() {
							var text = $(this).text();
							var keres_nev = $("#keres_nev").val().toLowerCase()
							$(this).html(text.replace( keres_nev, '<span class="kereso_ac_doboz_kiemelt_talalat">'+keres_nev+'</span>' )); 
						});
						// Ha nincs akciós termék, akkor kisebbre veszi a találati ablakot
						if (($(".kereso_ac_doboz_akciok").length > 0)){
						   $('#kereso_ac_doboz2').css('width', '600px')
						}
						else {
						   $('#kereso_ac_doboz2').css('width', '300px')
						}
					}
				  }
				xmlhttp.open("POST",domain+"/webshop/module/mod_kereso_ac_talalat.php?script=ok",true);
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send("keres_nev="+encodeURIComponent(document.getElementById("keres_nev2").value));
			// });
		}
		else {
			$( "#kereso_ac_doboz2" ).slideUp(260);
		}
	});

});
// AC doboz bezárása
$(document).click(function(e) {
	if( e.target.id != 'kereso_ac_doboz' && e.target.id != 'keres_nev') {
		$( "#kereso_ac_doboz" ).slideUp(260);
	}
	if( e.target.id != 'kereso_ac_doboz2' && e.target.id != 'keres_nev2') {
		$( "#kereso_ac_doboz2" ).slideUp(260);
	}	
});