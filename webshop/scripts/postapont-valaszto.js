// Városok generálása
	function varosok() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("pp_varos_div").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("POST","../webshop/module/postapont_varosok.php?script=ok",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("megye="+encodeURIComponent(document.getElementById("pp_megye").value));
	}
// Pontok keresése
	function ppKeres() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("pp_talalat_div").innerHTML=xmlhttp.responseText;
			}
		  }
		xmlhttp.open("POST","../webshop/module/postapont_talalatok.php?script=ok",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlhttp.send("varos="+encodeURIComponent(document.getElementById("pp_varos").value)+"&pp_ossz="+encodeURIComponent(document.getElementById("pp_ossz").checked)+"&pp_posta="+encodeURIComponent(document.getElementById("pp_posta").checked)+"&pp_mol="+encodeURIComponent(document.getElementById("pp_mol").checked)+"&pp_autom="+encodeURIComponent(document.getElementById("pp_autom").checked)+"&pp_coop="+encodeURIComponent(document.getElementById("pp_coop").checked));
	}
// Pont kiválasztása
	function ppKivalaszt(id) {
		document.getElementById("valasztott_postapont").value = document.getElementById("pp_adat_"+id).value;
	}
// Kereső megnyitása
	function ppKeresoNyitas() {
		// document.getElementById("pp_valaszto").style.display = 'block';
		$( "#pp_valaszto" ).slideDown( 300, function() { });
	}
// Kereső zárása
	function ppKeresoZaras() {
		// document.getElementById("pp_valaszto").style.display = 'none';
		$( "#pp_valaszto" ).slideUp( 300, function() { });
		document.getElementById("valasztott_postapont").value = '';
		document.getElementById("pp_varos").value = 0;
		document.getElementById("pp_talalat_div").innerHTML='';
		document.getElementById("pp_riaszt").innerHTML='';
	}
// Ellenőrzés a postapont miatt
	function pp_elenorzes() {
		if(document.getElementById("postapont").checked == true && document.getElementById("valasztott_postapont").value == '') {
			document.getElementById("pp_riaszt").innerHTML='<div class="alert alert-danger alert-dismissable">Válassz Posta Pontot!</p></div>';
			return false;
		}
		else
		{
			return true;
		}
	}
