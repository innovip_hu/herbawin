// Az oldalon megjelenő termékek mennyisége
	$(document).on("change", "#old_db", function() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("termekek_ajax_div").innerHTML = xmlhttp.responseText;
				termekekIgazitasa();
			}
		  }
		xmlhttp.open("GET",domain+"/webshop/termek_kategoriak.php?script=ok&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value)+"&old_db="+encodeURIComponent(document.getElementById("old_db").value),true);
		xmlhttp.send();
	});
// Sorba rendezés
	$(document).on("change", "#sorrend_uj", function() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("termekek_ajax_div").innerHTML = xmlhttp.responseText;
				termekekIgazitasa();
			}
		  }
		xmlhttp.open("GET",domain+"/webshop/termek_kategoriak.php?script=ok&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value)+"&sorrend_uj="+encodeURIComponent(document.getElementById("sorrend_uj").value),true);
		xmlhttp.send();
	});
// Lapozás
	function lapozas(oldszam) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("termekek_ajax_div").innerHTML = xmlhttp.responseText;
				termekekIgazitasa();
				$('html,body').scrollTop(0);
				// var stateObj = { foo: "webshop" };
				// history.pushState(stateObj, "", domain+"/webshop/termek_kategoriak.php?script=ok&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value)+"&oldszam="+oldszam);
			}
		  }
		xmlhttp.open("GET",domain+"/webshop/termek_kategoriak.php?script=ok&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value)+"&oldszam="+oldszam,true);
		xmlhttp.send();
	};
// Összehasonlításba tesz, vagy kivesz
	$(document).on("click", ".osszeh_check", function() {
		var osszeh_termek = jQuery(this).attr( "attr_term_id" );
		var osszeh_checked = jQuery(this).attr( "attr_checked" );
		
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("termekek_ajax_div").innerHTML = xmlhttp.responseText;
				termekekIgazitasa();
				// Összehasonlító div újratöltése
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						document.getElementById("osszehas_ajax_div").innerHTML = xmlhttp.responseText;
					}
				  }
				xmlhttp.open("GET",domain+"/webshop/module/mod_osszehasonlitas_kicsi.php?script=ok",true);
				xmlhttp.send();
			}
		  }
		xmlhttp.open("GET",domain+"/webshop/termek_kategoriak.php?script=ok&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value)+"&osszeh_termek="+osszeh_termek+"&osszeh_checked="+osszeh_checked,true);
		xmlhttp.send();
	});
// Összehasonlítás megnyitása
	$(document).on("click", "#osszeh_tovabb", function() {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("termekek_ajax_div").innerHTML = xmlhttp.responseText;
			}
		  }
		xmlhttp.open("GET",domain+"/webshop/module/mod_osszehasonlitas_oldal.php?script=ok",true);
		xmlhttp.send();
	});
// Kívánságlista
	function kivansaglistabaRakas(kivansaglista_termek) {
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("termekek_ajax_div").innerHTML = xmlhttp.responseText;
				termekekIgazitasa();
			}
		  }
		xmlhttp.open("GET",domain+"/webshop/termek_kategoriak.php?script=ok&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value)+"&kivansaglista_termek="+kivansaglista_termek,true);
		xmlhttp.send();
	};
// Szűrő
	function szures() {
		if (document.getElementById("term_urlnev").value == '') {
			var str = $( "#szuro_form" ).serialize();
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("termekek_ajax_div").innerHTML = xmlhttp.responseText;
					window.history.replaceState("valami", "Termékek", domain+"/termekek/"+encodeURIComponent(document.getElementById("kat_urlnev").value)+"/?"+str);
					termekekIgazitasa();
				}
			  }
			/* xmlhttp.open("POST",domain+"/webshop/termek_kategoriak.php?script=ok&command=szures&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value),true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send($("#szuro_form").serialize()); */
			xmlhttp.open("POST",domain+"/webshop/termek_kategoriak.php?script=ok&command=szures&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value)+"&"+str,true);
			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			xmlhttp.send("szures_most=ok");
		}
		else
		{
			document.getElementById('szuro_form').submit();
		}
	};
// Nézet módosítása
	$(document).on("click", "#nezet_gomb", function() {
		var nezet_uj = jQuery(this).attr( "attr_nezet" );
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				document.getElementById("termekek_ajax_div").innerHTML = xmlhttp.responseText;
				termekekIgazitasa();
			}
		  }
		xmlhttp.open("GET",domain+"/webshop/termek_kategoriak.php?script=ok&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value)+"&nezet_uj="+nezet_uj,true);
		xmlhttp.send();
	});
// Kosárba helyezés
	$(document).on("click", "#kosarba_gomb", function() {
		// Form adatai
		var formData = new FormData();
		// Paraméterek
		var nincs_param = 0;
		var felar = 0;
		$(".termek_parameterek_kosar").each(function(){
			formData.append($(this).attr('id'), ($(this).val()));
			if($(this).val() == '')
			{
				nincs_param = 1;
			}
			felar = felar + Number($(this).children('option[value="'+$(this).val()+'"]').attr('data-felar'));
			
			par_id = $(this).attr('attr_id');
			formData.append("termek_parameter["+par_id+"]", (document.getElementById("termek_parameter"+par_id).value));
		});
		formData.append("term_id", (document.getElementById("term_id").value));
		formData.append("term_nev", (document.getElementById("term_nev").value));
		formData.append("term_kep", (document.getElementById("term_kep").value));
		formData.append("term_ar", (Number(document.getElementById("term_ar").value) + felar));
		formData.append("term_afa", (document.getElementById("term_afa").value));
		if(document.getElementById("term_akcios_ar").value == 0) // Nem akciós
		{
			formData.append("term_akcios_ar", (document.getElementById("term_akcios_ar").value));
		}
		else // Akciós
		{
			formData.append("term_akcios_ar", (Number(document.getElementById("term_akcios_ar").value) + felar));
		}
		formData.append("command", (document.getElementById("command").value));
		formData.append("darab", (document.getElementById("darab").value));
		
		if(nincs_param == 1)
		{
			$('#parameter_riaszt').css('display', 'block');
		}
		else
		{
			if (window.XMLHttpRequest)
			  {// code for IE7+, Firefox, Chrome, Opera, Safari
			  xmlhttp=new XMLHttpRequest();
			  }
			else
			  {// code for IE6, IE5
			  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			  }
			xmlhttp.onreadystatechange=function()
			  {
			  if (xmlhttp.readyState==4 && xmlhttp.status==200)
				{
					document.getElementById("termekek_ajax_div").innerHTML = xmlhttp.responseText;
					// Repülő efekt
					var cart = $('#iderepul');
					var imgtofly = $(document.getElementById("ez"));
					// var form = $('#kosarba_form'); // A form ID-je
					if (imgtofly) {
						var imgclone = imgtofly.clone()
						.offset({ top:imgtofly.offset().top, left:imgtofly.offset().left })
						.css({'opacity':'0.7', 'position':'absolute', 'height':'150px', 'width':'150px', 'z-index':'1000'})
						.appendTo($('body'))
						.animate({
						'top':cart.offset().top + 10,
						'left':cart.offset().left + 30,
						'width':55,
						'height':55
						}, 1000, 'swing'
						   , function () // ez a callback, ami az animáció vége után lefut
							{
								//Kinyíló kosár újra generálása
								if (window.XMLHttpRequest)
								  {// code for IE7+, Firefox, Chrome, Opera, Safari
								  xmlhttp=new XMLHttpRequest();
								  }
								else
								  {// code for IE6, IE5
								  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
								  }
								xmlhttp.onreadystatechange=function()
								  {
								  if (xmlhttp.readyState==4 && xmlhttp.status==200)
									{
										document.getElementById("kinyilo_kosar_div").innerHTML=xmlhttp.responseText;
										$('.slide-out-div').tabSlideOut({
											 tabHandle: '.handle',                              //class of the element that will be your tab
											 pathToTabImage: domain+'/webshop/images/contact_tab_on.png',   //path to the image for the tab (optionaly can be set using css)
											 imageHeight: '172px',                               //height of tab image
											 imageWidth: '48px',                               //width of tab image    
											 tabLocation: 'right',                               //side of screen where tab lives, top, right, bottom, or left
											 speed: 300,                                        //speed of animation
											 action: 'click',                                   //options: 'click' or 'hover', action to trigger animation
											 topPos: '30vh',                                   //position from the top
											 fixedPosition: true                               //options: true makes it stick(fixed position) on scroll
										});
									}
								  }
								xmlhttp.open("GET",domain+"/webshop/kinyilo_kosar.php?script=ok",true);
								xmlhttp.send();
							}
						);
						imgclone.animate({'width':0, 'height':0}, function(){ $(this).detach() });
					};
					termekekIgazitasa();
				}
			  }
			xmlhttp.open("POST",domain+"/webshop/termek_info.php?script=ok&kat_urlnev="+encodeURIComponent(document.getElementById("kat_urlnev").value)+"&term_urlnev="+encodeURIComponent(document.getElementById("term_urlnev").value),true);
			xmlhttp.send(formData);
		}
	});
// Szűrő nyitása
	$(document).on("click", "#szuro_lenyito", function() {
		/* $("#szuro_div_section").css('display', 'block'); */
		$("#szuro_div_section").css('z-index', '999999');
		$("#szuro_div_section").animate({
			opacity: '1',
		});
	});
// Szűrő zárása
	$(document).on("click", "#szuro_bezar_gomb", function() {
		/* $("#szuro_div_section").css('display', 'none'); */
		$("#szuro_div_section").animate({
			opacity: '0',
		}
			, function() {
				$("#szuro_div_section").css('z-index', '-1');
			}
		);
		/* $("#szuro_div_section").animate({
			opacity: '0',
		}, 500); */
		/* $("#szuro_div_section").css('z-index', '-1'); */
	});
