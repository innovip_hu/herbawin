<?php
 	require_once 'sdk/config.php';
	require_once 'sdk/SimplePayment.class.php';	
	//DEMO language file
	if (file_exists ('demo/demo_functions.php')) {
		require_once 'demo/demo_functions.php';
	}
	
	$orderCurrency = (isset($_REQUEST['order_currency'])) ? $_REQUEST['order_currency'] : 'N/A';
	$orderRef = (isset($_REQUEST['order_ref'])) ? $_REQUEST['order_ref'] : 'N/A'; 	
	
	$backref = new SimpleBackRef($config, $orderCurrency );		
	$backref->order_ref = $orderRef;	
	
	$links = "";	
	if($backref->checkResponse()){	
		$backStatus = $backref->backStatusArray;
		$message = '';		 
		//CCVISAMC
		if ($backStatus['PAYMETHOD'] == 'Visa/MasterCard/Eurocard') {
			$message .= '<b><font color="green">'.$sz_kassza_45.'</font></b><br/>';
/*			if ($backStatus['ORDER_STATUS'] == 'IN_PROGRESS') {
				$message .= '<b><font color="green">' . WAITING_FOR_IPN . '</font></b><br/>';
			} elseif ($backStatus['ORDER_STATUS' ] == 'PAYMENT_AUTHORIZED') {
				$message .= '<b><font color="green">' . WAITING_FOR_IPN . '</font></b><br/>';
			} elseif ($backStatus['ORDER_STATUS'] == 'COMPLETE') {
				$message .= '<b><font color="green">' . CONFIRMED_IPN . '</font></b><br/>';
			}*/
		}
		//WIRE
		elseif ($backStatus['PAYMETHOD'] == 'Bank/Wire transfer') {
			//$message = '<b><font color="green">' . SUCCESSFUL_WIRE . '</font></b><br/>';
			if ($backStatus['ORDER_STATUS'] == 'PAYMENT_AUTHORIZED' || $backStatus['ORDER_STATUS'] == 'COMPLETE') {
				//$message .= '<b><font color="green">' . CONFIRMED_WIRE . '</font></b><br/>';
			} 			
		}
		$links .= '<a href="irn.php?order_ref=' . $backStatus['REFNOEXT'] . '&payrefno=' . $backStatus['PAYREFNO'] . '&ORDER_AMOUNT=1207&AMOUNT=1207&ORDER_CURRENCY=' . $orderCurrency . '">IRN</a>';
		$links .= ' | <a href="idn.php?order_ref=' . $backStatus['REFNOEXT'] . '&payrefno=' . $backStatus['PAYREFNO'] . '&ORDER_AMOUNT=1207&ORDER_CURRENCY=' . $orderCurrency . '">IDN</a>';
	} else {
		$backStatus = $backref->backStatusArray;		
		$message = '<b><font color="red">'.$sz_kassza_46.'</font></b><br/>';
		//$message .= '<b><font color="red">' . END_OF_TRANSACTION . '</font></b><br/>';
		$message .= $sz_kassza_47.'<br/><br/>';		
	}
	
	$links .= ' | <a href="ios.php?simpleid=' . $backStatus['PAYREFNO'] . '&order_ref=' . $backStatus['REFNOEXT'] . '&ORDER_CURRENCY=' . $orderCurrency . '">IOS</a>';	
	$message .= $sz_kassza_48.': <b>' . $backStatus['PAYREFNO'] . '</b><br/>'; 
	$message .= $sz_kassza_49.': <b>' . $backStatus['REFNOEXT'] . '</b><br/>';
	$message .= $sz_kassza_50.': <b>' . $backStatus['BACKREF_DATE'] . '</b><br/>';
	$backref->errorLogger();  
	echo $message;	 
?>

			
