<?php
    require_once 'sdk/config.php';
    require_once 'sdk/SimplePayment.class.php';
	
    if (!isset($OrderId)) {
        include '../../config.php';
        header('Location: '.$domain);
    }

    if (isset($_SESSION['valuta']) && $_SESSION['valuta'] != '')
    {
        $orderCurrency = strtoupper($_SESSION['valuta']);
    }
    else
    {
        $orderCurrency = 'HUF';
    }    
	
    $lu = new SimpleLiveUpdate($config, $orderCurrency);       	
    $lu->setField("ORDER_REF", $OrderId);  

    if (isset($_SESSION['lang']) && $_SESSION['lang'] != '')
    {
        $lu->setField("LANGUAGE",  strtoupper($_SESSION['lang']));       
    }
    else
    {
        $lu->setField("LANGUAGE", "HU");
    } 
	
    foreach ($termekek as $value) {
        $lu->addProduct($value);        
    }

    $query_ord = "SELECT * FROM ".$webjel."rendeles WHERE id=".$OrderId;
    $res_ord = $pdo->prepare($query_ord);
    $res_ord->execute();
    $row_ord = $res_ord -> fetch();
	
    if (isset($row_ord['szallitasi_dij'])) {
        if (isset($_SESSION['valuta']) && $_SESSION['valuta'] != '')
        {
            $query_valuta = "SELECT * FROM ".$webjel."beallitasok WHERE id=1";
            $res = $pdo->prepare($query_valuta);
            $res->execute();
            $row_valuta = $res -> fetch();

            $ar_szall_valuta = $row_ord['szallitasi_dij'] / $row_valuta[$_SESSION['valuta'].'_arfolyam'];
            $lu->setField("ORDER_SHIPPING", $ar_szall_valuta);
        }
        else
        {
            $lu->setField("ORDER_SHIPPING", $row_ord['szallitasi_dij']);
        }        
    }

    $query_megye = "SELECT megye FROM ".$webjel."varos_megye WHERE varos LIKE '".$szla_vasarlo_varos."'";
    $res_megye = $pdo->prepare($query_megye);
    $res_megye->execute();
    $row_megye = $res_megye -> fetch();

    if ($cim_szall_varos['nev'] != '')
    {
        $query_megye = "SELECT megye FROM ".$webjel."varos_megye WHERE varos LIKE '".$deli_varos."'";
        $res_megye = $pdo->prepare($query_megye);
        $res_megye->execute();
        $row_megye_del = $res_megye -> fetch();        
    } 
    else
    {
        $row_megye_del['megye'] = '';
    }   

    $reg_nev = $szla_vasarlo_nev;
    $reg_email = $szla_vasarlo_email;
    $reg_telefon = $telefon;
    $reg_cim_irszam = $szla_vasarlo_irszam;
    $bill_cim = $cim2;
    $bill_varos = $szla_vasarlo_varos;
    $bill_megye = $row_megye['megye'];

    $deli_varos_nev = $deli_varos;
    $deli_megye = $row_megye_del['megye'];
    $del_cim = $szallcim2;
    $reg_szall_cim_irszam = $deli_irsz;

    if ($row_ord['glscspont'] != '')
    {
        $del_cim = $row_ord['glscspont'];
    }

	//Billing data
    $lu->setField("BILL_FNAME", "");
    $lu->setField("BILL_LNAME", $reg_nev);
    $lu->setField("BILL_EMAIL", $reg_email); 
    $lu->setField("BILL_PHONE", $reg_telefon);
    //$lu->setField("BILL_COMPANY", "Company name");          		// optional
    //$lu->setField("BILL_FISCALCODE", " ");                  		// optional
    $lu->setField("BILL_COUNTRYCODE", "HU");
    $lu->setField("BILL_STATE", $bill_megye);
    $lu->setField("BILL_CITY", $bill_varos); 
    $lu->setField("BILL_ADDRESS", $bill_cim); 
    //$lu->setField("BILL_ADDRESS2", "Second line address");      	// optional
    $lu->setField("BILL_ZIPCODE", $reg_cim_irszam);          
	
    //Delivery data
    $lu->setField("DELIVERY_FNAME", ""); 
    $lu->setField("DELIVERY_LNAME", $reg_nev); 
    $lu->setField("DELIVERY_EMAIL", ""); 
    $lu->setField("DELIVERY_PHONE", $reg_telefon); 
    $lu->setField("DELIVERY_COUNTRYCODE", "HU");
    $lu->setField("DELIVERY_STATE", $deli_megye);
    $lu->setField("DELIVERY_CITY", $deli_varos_nev);
    $lu->setField("DELIVERY_ADDRESS", $del_cim); 
    //$lu->setField("DELIVERY_ADDRESS2", "Second line address");  	// optional
    $lu->setField("DELIVERY_ZIPCODE", $reg_szall_cim_irszam); 
	
    $display = $lu->createHtmlForm('SimplePayForm', 'auto', 'Fizetés indítása');   // format: link, button, auto (auto is redirects to payment page immediately )
	$lu->errorLogger(); 	
	echo $display;
?>
