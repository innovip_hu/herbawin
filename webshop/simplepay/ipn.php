<?php
	require_once 'sdk/config.php';
	require_once 'sdk/SimplePayment.class.php';
	
	$orderCurrency = (isset($_REQUEST['CURRENCY'])) ? $_REQUEST['CURRENCY'] : 'N/A';
	
	$ipn = new SimpleIpn($config, $orderCurrency);		
	if($ipn->validateReceived()){	

		$ipn->confirmReceived();

		include '../../config.php';

        //$current =  print_r($_REQUEST, true);
        
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}

		//Sikeres IPN esetén ide jön a feldolgozás.
		$pdo->exec("UPDATE ".$webjel."rendeles SET fizetve_otp=1 WHERE id=".$_REQUEST['REFNOEXT']);

	}

	$ipn->errorLogger();
	
    if ($ipn->debug) {
        print $ipn->getDebugMessage();
    }
    if (count($ipn->errorMessage) > 0) {
        print $ipn->getErrorMessage();
    }

?>


