<?php
	require_once("sdk/config.php");
	require_once 'sdk/SimplePayment.class.php';

	$orderCurrency = (isset($_REQUEST['order_currency'])) ? $_REQUEST['order_currency'] : 'N/A';
	$orderRef = (isset($_REQUEST['order_ref'])) ? $_REQUEST['order_ref'] : 'N/A'; 
	
	$timeOut = new SimpleLiveUpdate($config, $orderCurrency);
	$timeOut->commMethod = 'timeout';
	$timeOut->order_ref = $orderRef;
	
	$message = "";
	if (@$_REQUEST['redirect'] == 1) {
		$message = '<b><font color="red">'.$sz_kassza_51.'</font></b><br/>';
		$log['TRANSACTION'] = 'ABORT';
	} else {
		$message = '<b><font color="red">'.$sz_kassza_52.'</font></b><br/>';
		$message .= $sz_kassza_53.'<br/><br/>';			
		$log['TRANSACTION'] = 'TIMEOUT';
	} 
	
	$message .= $sz_kassza_50.': <b>' . date('Y-m-d H:i:s', time()) . '</b><br/>';
	$message .= $sz_kassza_49.': <b>' . $_REQUEST['order_ref'] . '</b><br/>';
	
	$log['ORDER_ID'] = $orderRef;
	$log['CURRENCY'] = $orderCurrency;
	$log['REDIRECT'] = (isset($_REQUEST['redirect'])) ? $_REQUEST['redirect'] : '0';
	$timeOut->logFunc("Timeout", $log, $log['ORDER_ID']); 
	$timeOut->errorLogger(); 
	
	echo $message;			 
?>

