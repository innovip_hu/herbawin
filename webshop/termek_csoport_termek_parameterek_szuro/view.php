<div class="term_cim sidebar_term_cim" id="szuro_lenyito">Szűrő <span class="mobil_kat_lenyito"><i class="fa fa-bars" style="font-style: normal;"></i></span></div>
<div id="szuro_div_section">
	<div class="szuro_mobil_title">Szűrő<img id="szuro_bezar_gomb" src="<?php print $domain; ?>/webshop/images/delete-button.png" /></div>
	<div id="szuro_div">
		<form id="szuro_form" action="<?php if($oldal != 'termekek'){print ''.$domain.'/termekek/';} ?>" method="GET" >
			<?php
			$termek_csoport_id = isset($_GET['kat_urlnev']) ? $pdo->query("SELECT id FROM ".$webjel."term_csoportok WHERE nev_url='".$_GET['kat_urlnev']."'")->fetchColumn() : FALSE;
			// $where = $termek_csoport_id ? ' WHERE csop_id='.$termek_csoport_id : '';
			$where = $termek_csoport_id ? ' WHERE (csop_id='.$termek_csoport_id.' OR EXISTS (SELECT 1 FROM '.$webjel.'termek_termek_csoportok WHERE '.$webjel.'termek_termek_csoportok.termek_id='.$webjel.'termekek.id AND '.$webjel.'termek_termek_csoportok.termek_csoport_id='.$termek_csoport_id.'))' : '';
			// Maximum ár meghatározása
			$res = $pdo->prepare('SELECT MAX(IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar)) as max_ar FROM '.$webjel.'termekek'.$where);
			$res->execute();
			$row  = $res -> fetch();
			$szuro_max_ar = ceil($row['max_ar'] / 1000) * 1000;
			// Minimum ár meghatározása
			$res = $pdo->prepare('SELECT MIN(IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar)) as min_ar FROM '.$webjel.'termekek'.$where);
			$res->execute();
			$row  = $res -> fetch();
			$szuro_min_ar = floor($row['min_ar'] / 1000) * 1000;
			// Aktuális minimum és maximum
			if (isset($_SESSION['szuro_minimum_ar']) || isset($_SESSION['szuro_maximum_ar'])) {
				$szuro_minimum_ar = $_SESSION['szuro_minimum_ar'];
				$szuro_maximum_ar = $_SESSION['szuro_maximum_ar'];
			}
			else {
				$szuro_minimum_ar = $szuro_min_ar;
				$szuro_maximum_ar = $szuro_max_ar;
			}
			?>
			<div class="filter_box accordion">
				<h3 class="h4 text-gray-light" style="cursor: pointer; margin: 0px;">Ár<span class="fa fa-chevron-up fa-pull-right text-primary"></span></h3>
				<div style="margin-top: 5px;">
					<p>
						Minimum ár
						<input id="szuro_minimum_ar" name="szuro_minimum_ar" type="range" min="<?php print $szuro_min_ar; ?>" max="<?php print $szuro_max_ar; ?>" step="1000" value="<?php print $szuro_minimum_ar; ?>" data-rangeslider>
						<output class="szuro_ar_osszeg"></output> Ft
					</p>
					<p>
						Maximum ár
						<input id="szuro_maximum_ar" name="szuro_maximum_ar" type="range" min="<?php print $szuro_min_ar; ?>" max="<?php print $szuro_max_ar; ?>" step="1000" value="<?php print $szuro_maximum_ar; ?>" data-rangeslider>
						<output class="szuro_ar_osszeg"></output> Ft
					</p>
				</div>
			</div>
			
			<?php
			$termek_csoport_id = $pdo->query("SELECT id FROM ".$webjel."term_csoportok WHERE nev_url='".$_GET['kat_urlnev']."'")->fetchColumn();
			$query = "SELECT DISTINCT tp.id, tp.nev, tp.valaszthato, tp.mertekegyseg, tp.szuro, tp.szuro_sorrend 
				FROM ".$webjel."termek_uj_parameter_ertekek tpe 
				JOIN ".$webjel."termekek t ON tpe.termek_id=t.id
				JOIN ".$webjel."termek_uj_parameterek tp ON tp.id=tpe.parameter_id 
				WHERE (t.csop_id=".$termek_csoport_id." OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok WHERE ".$webjel."termek_termek_csoportok.termek_id=t.id AND ".$webjel."termek_termek_csoportok.termek_csoport_id=".$termek_csoport_id.")) AND t.lathato=1 AND tp.valaszthato=0 AND tp.szuro=1 
				ORDER BY tp.szuro_sorrend ASC, tp.nev ASC";
			foreach ($pdo->query($query) as $row)
			{
				if (isset($_SESSION['termek_parameter_ertek'][$row['id']]))
				{
					$sima_paramater_nem_latszik = '';
					$chevron = "fa-chevron-up";
				}
				else
				{
					$sima_paramater_nem_latszik = 'style="display: none;"';	
					$chevron = "fa-chevron-down";
				}

				echo '<div class="filter_box accordion szuro_divek">
					<h3 class="h4 text-gray-light">'.$row['nev'].'<span class="fa '.$chevron.' fa-pull-right text-primary"></span></h3>
					<div '.$sima_paramater_nem_latszik.' class="text-gray-light">';
						$query2 = "SELECT tpe.id, tpe.ertek 
							FROM ".$webjel."termek_uj_parameter_ertekek tpe 
							JOIN ".$webjel."termekek t ON tpe.termek_id=t.id
							WHERE (t.csop_id=".$termek_csoport_id." OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok WHERE ".$webjel."termek_termek_csoportok.termek_id=t.id AND ".$webjel."termek_termek_csoportok.termek_csoport_id=".$termek_csoport_id.")) AND t.lathato=1 AND tpe.parameter_id=".$row['id']." 
							GROUP BY tpe.ertek 
							ORDER BY tpe.ertek ASC";
						foreach ($pdo->query($query2) as $row2)
						{
							if (isset($_SESSION['termek_parameter_ertek'][$row['id']]) AND in_array($row2['ertek'], $_SESSION['termek_parameter_ertek'][$row['id']])) { $checked = 'checked'; }
							else { $checked = ''; }
							echo '<p><label class="chechack"><input type="checkbox" name="termek_parameter_ertek['.$row['id'].'][]" value="'.$row2['ertek'].'" '.$checked.'>'.$row2['ertek'].' '.$row['mertekegyseg'].'<span class="checkmark"></span></label></p>';
						}
					echo '</div>
				</div>';
			}
			?>
		</form>
	</div>
</div>
