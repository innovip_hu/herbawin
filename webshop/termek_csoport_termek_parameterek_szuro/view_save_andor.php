<script src="<?php print $domain; ?>/webshop/scripts/rangeslider.min.js"></script>
<link rel="stylesheet" href="<?php print $domain; ?>/webshop/css/rangeslider.css">
<form id="szuro_form" action="<?php if($oldal != 'termekek'){print ''.$domain.'/termekek/';} ?>" method="POST" >
<?php
$termek_csoport_id = isset($_GET['kat_urlnev']) ? $pdo->query("SELECT id FROM ".$webjel."term_csoportok WHERE nev_url='".$_GET['kat_urlnev']."'")->fetchColumn() : FALSE;
$where = $termek_csoport_id ? ' WHERE csop_id='.$termek_csoport_id : '';
// Maximum ár meghatározása
$res = $pdo->prepare('SELECT MAX(IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar)) as max_ar FROM '.$webjel.'termekek'.$where);
$res->execute();
$row  = $res -> fetch();
$szuro_max_ar = ceil($row['max_ar'] / 1000) * 1000;
// Minimum ár meghatározása
$res = $pdo->prepare('SELECT MIN(IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar)) as min_ar FROM '.$webjel.'termekek'.$where);
$res->execute();
$row  = $res -> fetch();
$szuro_min_ar = floor($row['min_ar'] / 1000) * 1000;
// Aktuális minimum és maximum
if (isset($_SESSION['szuro_minimum_ar']) || isset($_SESSION['szuro_maximum_ar'])) {
	$szuro_minimum_ar = $_SESSION['szuro_minimum_ar'];
	$szuro_maximum_ar = $_SESSION['szuro_maximum_ar'];
}
else {
	$szuro_minimum_ar = $szuro_min_ar;
	$szuro_maximum_ar = $szuro_max_ar;
}
?>
<div class="filter_box accordion">
	<h3>Ár</h3>
	<div>
		<p>
			Minimum ár
			<input id="szuro_minimum_ar" name="szuro_minimum_ar" type="range" min="<?php print $szuro_min_ar; ?>" max="<?php print $szuro_max_ar; ?>" step="1000" value="<?php print $szuro_minimum_ar; ?>" data-rangeslider>
			<output class="szuro_ar_osszeg"></output> Ft
		</p>
		<p>
			Maximum ár
			<input id="szuro_maximum_ar" name="szuro_maximum_ar" type="range" min="<?php print $szuro_min_ar; ?>" max="<?php print $szuro_max_ar; ?>" step="1000" value="<?php print $szuro_maximum_ar; ?>" data-rangeslider>
			<output class="szuro_ar_osszeg"></output> Ft
		</p>
	</div>
</div>
<script>
$(document).ready(function(){
$(function() {

	var $document   = $(document),
		selector    = '[data-rangeslider]',
		$element    = $(selector);

	// Example functionality to demonstrate a value feedback
	function valueOutput(element) {
		var value = element.value,
			output = element.parentNode.getElementsByTagName('output')[0];
		output.innerHTML = value;
	}
	for (var i = $element.length - 1; i >= 0; i--) {
		valueOutput($element[i]);
	};
	$document.on('change', 'input[type="range"]', function(e) {
		valueOutput(e.target);
	});

	// Example functionality to demonstrate disabled functionality
	$document .on('click', '#js-example-disabled button[data-behaviour="toggle"]', function(e) {
		var $inputRange = $('input[type="range"]', e.target.parentNode);

		if ($inputRange[0].disabled) {
			$inputRange.prop("disabled", false);
		}
		else {
			$inputRange.prop("disabled", true);
		}
		$inputRange.rangeslider('update');
	});

	// Example functionality to demonstrate programmatic value changes
	$document.on('click', '#js-example-change-value button', function(e) {
		var $inputRange = $('input[type="range"]', e.target.parentNode),
			value = $('input[type="number"]', e.target.parentNode)[0].value;

		$inputRange.val(value).change();
	});

	// Example functionality to demonstrate destroy functionality
	$document
		.on('click', '#js-example-destroy button[data-behaviour="destroy"]', function(e) {
			$('input[type="range"]', e.target.parentNode).rangeslider('destroy');
		})
		.on('click', '#js-example-destroy button[data-behaviour="initialize"]', function(e) {
			$('input[type="range"]', e.target.parentNode).rangeslider({ polyfill: false });
		});

	// Basic rangeslider initialization
	$element.rangeslider({

		// Deactivate the feature detection
		polyfill: false,

		// Callback function
		onInit: function() {},

		// Callback function
		onSlide: function(position, value) {
			//console.log('onSlide');
			//console.log('position: ' + position, 'value: ' + value);
		},

		// Callback function
		onSlideEnd: function(position, value) {
			//console.log('onSlideEnd');
			//console.log('position: ' + position, 'value: ' + value);
			
			// document.getElementById('szuro_form').submit();
			szures();
		}
	});

});
});
</script>

<?php if ($termek_csoport_id): ?>
<?php $termek_parameterek = $pdo->query(""
		. "SELECT tp.id, tp.nev, tp.tipus, tp.mertekegyseg, tcstp.id as termek_csoport_termek_parameter_id "
		. "FROM ".$webjel."termek_parameterek tp "
		. "JOIN ".$webjel."termek_csoport_termek_parameterek tcstp ON tp.id=tcstp.termek_parameter_id AND tcstp.termek_csoport_id=".$termek_csoport_id." "
		. "ORDER BY "
		. "CASE WHEN tp.nev LIKE '".TERMEK_PARAMETER_NEV_GYARTO."' THEN 1 ELSE 2 END, "
		. "tp.nev")->fetchAll(); ?>
<?php if (empty($termek_parameterek)): ?>
<?php $termek_parameterek = $pdo->query(""
		. "SELECT DISTINCT tp.id, tp.nev, tp.tipus, tp.mertekegyseg, 0 as termek_csoport_termek_parameter_id "
		. "FROM ".$webjel."termek_parameterek tp "
		. "JOIN ".$webjel."termek_termek_parameter_ertekek ttpe ON tp.id=ttpe.termek_parameter_id "
		. "JOIN ".$webjel."termekek t ON ttpe.termek_id=t.id "
		. "WHERE (t.csop_id=".$termek_csoport_id." OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok WHERE ".$webjel."termek_termek_csoportok.termek_id=t.id AND ".$webjel."termek_termek_csoportok.termek_csoport_id=".$termek_csoport_id.")) AND t.lathato=1 "
		. "ORDER BY "
		. "CASE WHEN tp.nev LIKE '".TERMEK_PARAMETER_NEV_GYARTO."' THEN 1 ELSE 2 END, "
		. "tp.nev")->fetchAll(); ?>
<?php endif; ?>
<?php if (!empty($termek_parameterek)): ?>
<?php foreach ($termek_parameterek as $termek_parameter): ?>
<div class="filter_box accordion szuro_divek">
	<h3><?php echo $termek_parameter['nev']; ?></h3>
	<div>
		<?php $termek_csoport_termek_parameter_ertekek = $pdo->query(""
				. "SELECT * "
				. "FROM ".$webjel."termek_csoport_termek_parameter_ertekek tcstpe "
				. "WHERE tcstpe.termek_csoport_termek_parameter_id=".$termek_parameter['termek_csoport_termek_parameter_id']."")->fetchAll(); ?>
		<?php if (!empty($termek_csoport_termek_parameter_ertekek)): ?>
		<?php foreach ($termek_csoport_termek_parameter_ertekek as $termek_csoport_termek_parameter_ertek): ?>
		<p>
			<input type="checkbox" name="termek_parameter_ertek[<?php echo $termek_parameter['id']; ?>][]" value="<?php echo $termek_csoport_termek_parameter_ertek['tipus'].'|'.$termek_csoport_termek_parameter_ertek['ertek']; ?>" <?php if (isset($_SESSION['termek_parameter_ertek'][$termek_parameter['id']]) AND in_array($termek_csoport_termek_parameter_ertek['tipus'].'|'.$termek_csoport_termek_parameter_ertek['ertek'], $_SESSION['termek_parameter_ertek'][$termek_parameter['id']])): ?>checked<?php endif; ?>>
			<?php if ($termek_csoport_termek_parameter_ertek['tipus'] == TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_NAGYOBB): ?>
				<?php echo $termek_csoport_termek_parameter_ertek['ertek']; ?>
				<?php echo $termek_parameter['mertekegyseg']; ?>
				felett
			<?php elseif ($termek_csoport_termek_parameter_ertek['tipus'] == TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_KISEBB): ?>
				<?php echo $termek_csoport_termek_parameter_ertek['ertek']; ?>
				<?php echo $termek_parameter['mertekegyseg']; ?>
				alatt
			<?php elseif ($termek_csoport_termek_parameter_ertek['tipus'] == TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_KOZOTT): ?>
				<?php $ertek = explode('-', $termek_csoport_termek_parameter_ertek['ertek']); ?>
				<?php echo $ertek[0]; ?> és <?php echo $ertek[1]; ?>
				<?php echo $termek_parameter['mertekegyseg']; ?>
				között
			<?php endif; ?>
			
			<?php
			$szuro_sql = '';
			if (isset($_SESSION['szuro_minimum_ar'])) // Ár szerinti szűrés
			{
				$szuro_sql = ' AND IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar) >= '.$_SESSION['szuro_minimum_ar'].' AND IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar) <= '.$_SESSION['szuro_maximum_ar'].' ';
			}
			if (isset($_SESSION['termek_parameter_ertek'])) {
				$szuro_termek_parameter_ertek = " AND (";
				foreach ($_SESSION['termek_parameter_ertek'] as $termek_parameter_id => $termek_parameter_ertekek) {
					if (reset($_SESSION['termek_parameter_ertek']) !== $termek_parameter_ertekek) {
						$szuro_termek_parameter_ertek .= " AND ";
					}
					$szuro_termek_parameter_ertek .= "EXISTS ("
							. "SELECT 1 FROM ".$webjel."termek_termek_parameter_ertekek ttpe WHERE t.id=ttpe.termek_id AND (";
					$szuro_termek_parameter_ertek .= "ttpe.termek_parameter_id=".$termek_parameter_id." AND (";
					foreach ($termek_parameter_ertekek as $termek_parameter_ertek1) {
						if (reset($termek_parameter_ertekek) !== $termek_parameter_ertek1) {
							$szuro_termek_parameter_ertek .= " OR ";
						}
						if (strpos($termek_parameter_ertek1, '|') === FALSE) {
							$szuro_termek_parameter_ertek .= "ttpe.ertek='".$termek_parameter_ertek1."'";
						}
						else {
							$termek_csoport_termek_parameter_ertek_tipus = explode('|', $termek_parameter_ertek1);
							$termek_csoport_termek_parameter_ertek_tipus = reset($termek_csoport_termek_parameter_ertek_tipus);
							$termek_parameter_ertek1 = explode('|', $termek_parameter_ertek1);
							$termek_parameter_ertek1 = end($termek_parameter_ertek1);
							if ($termek_csoport_termek_parameter_ertek_tipus == TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_NAGYOBB) {
								$szuro_termek_parameter_ertek .= "ttpe.ertek>=".$termek_parameter_ertek1."";
							}
							elseif ($termek_csoport_termek_parameter_ertek_tipus == TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_KISEBB) {
								$szuro_termek_parameter_ertek .= "ttpe.ertek<=".$termek_parameter_ertek1."";
							}
							elseif ($termek_csoport_termek_parameter_ertek_tipus == TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_KOZOTT) {
								$termek_parameter_ertek1 = explode('-', $termek_parameter_ertek1);
								$szuro_termek_parameter_ertek .= "(ttpe.ertek>=".$termek_parameter_ertek1[0]." AND ttpe.ertek<=".$termek_parameter_ertek1[1].")";
							}
						}
					}
					$szuro_termek_parameter_ertek .= ")))";
				}
				$szuro_termek_parameter_ertek .= ")";
			}
			else {
				$szuro_termek_parameter_ertek = "";
			}
			?>
			<?php if ($termek_csoport_termek_parameter_ertek['tipus'] == TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_NAGYOBB): ?>
				(<?php echo $pdo->query(""
					. "SELECT COUNT(t.id) "
					. "FROM ".$webjel."termekek t "
					. "JOIN ".$webjel."termek_termek_parameter_ertekek ttpe ON t.id=ttpe.termek_id AND ttpe.termek_parameter_id=".$termek_parameter['id']." AND ttpe.ertek>=".$termek_csoport_termek_parameter_ertek['ertek']." "
					. "WHERE (t.csop_id=".$termek_csoport_id." OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok WHERE ".$webjel."termek_termek_csoportok.termek_id=t.id AND ".$webjel."termek_termek_csoportok.termek_csoport_id=".$termek_csoport_id.")) AND t.lathato=1".$szuro_sql.$szuro_termek_parameter_ertek)->fetchColumn(); ?>)
			<?php elseif ($termek_csoport_termek_parameter_ertek['tipus'] == TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_KISEBB): ?>
				(<?php echo $pdo->query(""
					. "SELECT COUNT(t.id) "
					. "FROM ".$webjel."termekek t "
					. "JOIN ".$webjel."termek_termek_parameter_ertekek ttpe ON t.id=ttpe.termek_id AND ttpe.termek_parameter_id=".$termek_parameter['id']." AND ttpe.ertek<=".$termek_csoport_termek_parameter_ertek['ertek']." "
					. "WHERE (t.csop_id=".$termek_csoport_id." OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok WHERE ".$webjel."termek_termek_csoportok.termek_id=t.id AND ".$webjel."termek_termek_csoportok.termek_csoport_id=".$termek_csoport_id.")) AND t.lathato=1".$szuro_sql.$szuro_termek_parameter_ertek)->fetchColumn(); ?>)
			<?php elseif ($termek_csoport_termek_parameter_ertek['tipus'] == TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_KOZOTT): ?>
				<?php $ertek = explode('-', $termek_csoport_termek_parameter_ertek['ertek']); ?>
				(<?php echo $pdo->query(""
					. "SELECT COUNT(t.id) "
					. "FROM ".$webjel."termekek t "
					. "JOIN ".$webjel."termek_termek_parameter_ertekek ttpe ON t.id=ttpe.termek_id AND ttpe.termek_parameter_id=".$termek_parameter['id']." AND ttpe.ertek>=".$ertek[0]." AND ttpe.ertek<=".$ertek[1]." "
					. "WHERE (t.csop_id=".$termek_csoport_id." OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok WHERE ".$webjel."termek_termek_csoportok.termek_id=t.id AND ".$webjel."termek_termek_csoportok.termek_csoport_id=".$termek_csoport_id.")) AND t.lathato=1".$szuro_sql.$szuro_termek_parameter_ertek)->fetchColumn(); ?>)
			<?php endif; ?>
		</p>
		<?php endforeach; ?>
		<?php else: ?>
		<?php $termek_parameter_ertekek = $pdo->query(""
				. "SELECT ttpe.termek_parameter_id, ttpe.ertek, tpe.nev "
				. "FROM ".$webjel."termek_termek_parameter_ertekek ttpe "
				. "JOIN ".$webjel."termekek t ON ttpe.termek_id=t.id AND (t.csop_id=".$termek_csoport_id." OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok WHERE ".$webjel."termek_termek_csoportok.termek_id=t.id AND ".$webjel."termek_termek_csoportok.termek_csoport_id=".$termek_csoport_id.")) "
				. "LEFT JOIN ".$webjel."termek_parameter_ertekek tpe ON ttpe.termek_parameter_id=tpe.termek_parameter_id AND ttpe.ertek=tpe.id "
				. "WHERE ttpe.termek_parameter_id=".$termek_parameter['id']." "
				. "GROUP BY ttpe.ertek")->fetchAll(); ?>
		<?php foreach ($termek_parameter_ertekek as $termek_parameter_ertek): ?>
		<p>
			<input type="checkbox" name="termek_parameter_ertek[<?php echo $termek_parameter['id']; ?>][]" value="<?php echo $termek_parameter_ertek['ertek']; ?>" <?php if (isset($_SESSION['termek_parameter_ertek'][$termek_parameter['id']]) AND in_array($termek_parameter_ertek['ertek'], $_SESSION['termek_parameter_ertek'][$termek_parameter['id']])): ?>checked<?php endif; ?>>
			<?php if ($termek_parameter_ertek['nev']): ?>
				<?php echo $termek_parameter_ertek['nev']; ?>
			<?php else: ?>
				<?php if ($termek_parameter['tipus'] == TERMEK_PARAMETER_TIPUS_IGEN_NEM): ?>
				<?php echo $termek_parameter_ertek['ertek'] === '1' ? 'Igen' : 'Nem'; ?>
				<?php else: ?>
				<?php echo $termek_parameter_ertek['ertek']; ?>
				<?php endif; ?>
			<?php endif; ?>
			<?php echo $termek_parameter['mertekegyseg']; ?>
			
			<?php
			$szuro_sql = '';
			if (isset($_SESSION['szuro_minimum_ar'])) // Ár szerinti szűrés
			{
				$szuro_sql = ' AND IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar) >= '.$_SESSION['szuro_minimum_ar'].' AND IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar) <= '.$_SESSION['szuro_maximum_ar'].' ';
			}
			if (isset($_SESSION['termek_parameter_ertek'])) {
				$szuro_termek_parameter_ertek = " AND (";
				foreach ($_SESSION['termek_parameter_ertek'] as $termek_parameter_id => $termek_parameter_ertekek) {
					if (reset($_SESSION['termek_parameter_ertek']) !== $termek_parameter_ertekek) {
						$szuro_termek_parameter_ertek .= " AND ";
					}
					$szuro_termek_parameter_ertek .= "EXISTS ("
							. "SELECT 1 FROM ".$webjel."termek_termek_parameter_ertekek ttpe WHERE t.id=ttpe.termek_id AND (";
					$szuro_termek_parameter_ertek .= "ttpe.termek_parameter_id=".$termek_parameter_id." AND (";
					foreach ($termek_parameter_ertekek as $termek_parameter_ertek1) {
						if (reset($termek_parameter_ertekek) !== $termek_parameter_ertek1) {
							$szuro_termek_parameter_ertek .= " OR ";
						}
						if (strpos($termek_parameter_ertek1, '|') === FALSE) {
							$szuro_termek_parameter_ertek .= "ttpe.ertek='".$termek_parameter_ertek1."'";
						}
						else {
							$termek_csoport_termek_parameter_ertek_tipus = explode('|', $termek_parameter_ertek1);
							$termek_csoport_termek_parameter_ertek_tipus = reset($termek_csoport_termek_parameter_ertek_tipus);
							$termek_parameter_ertek1 = explode('|', $termek_parameter_ertek1);
							$termek_parameter_ertek1 = end($termek_parameter_ertek1);
							if ($termek_csoport_termek_parameter_ertek_tipus == TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_NAGYOBB) {
								$szuro_termek_parameter_ertek .= "ttpe.ertek>=".$termek_parameter_ertek1."";
							}
							elseif ($termek_csoport_termek_parameter_ertek_tipus == TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_KISEBB) {
								$szuro_termek_parameter_ertek .= "ttpe.ertek<=".$termek_parameter_ertek1."";
							}
							elseif ($termek_csoport_termek_parameter_ertek_tipus == TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_KOZOTT) {
								$termek_parameter_ertek1 = explode('-', $termek_parameter_ertek1);
								$szuro_termek_parameter_ertek .= "(ttpe.ertek>=".$termek_parameter_ertek1[0]." AND ttpe.ertek<=".$termek_parameter_ertek1[1].")";
							}
						}
					}
					$szuro_termek_parameter_ertek .= ")))";
				}
				$szuro_termek_parameter_ertek .= ")";
			}
			else {
				$szuro_termek_parameter_ertek = "";
			}
			?>
			(<?php echo $pdo->query(""
					. "SELECT COUNT(t.id) "
					. "FROM ".$webjel."termekek t "
					. "JOIN ".$webjel."termek_termek_parameter_ertekek ttpe ON t.id=ttpe.termek_id AND ttpe.termek_parameter_id=".$termek_parameter['id']." AND ttpe.ertek='".$termek_parameter_ertek['ertek']."' "
					. "WHERE (t.csop_id=".$termek_csoport_id." OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok WHERE ".$webjel."termek_termek_csoportok.termek_id=t.id AND ".$webjel."termek_termek_csoportok.termek_csoport_id=".$termek_csoport_id.")) AND t.lathato=1".$szuro_sql.$szuro_termek_parameter_ertek)->fetchColumn(); ?>)
		</p>
		<?php endforeach; ?>
		<?php endif; ?>
	</div>
</div>
<?php endforeach; ?>
<script src="<?php print $domain; ?>/webshop/termek_csoport_termek_parameterek_szuro/jquery-ui.min.js"></script>
<script>
	$(function() {
		$(".filter_box.accordion").accordion({
			collapsible: true
		});
	});
</script>
<style>
	.filter_box.accordion h3 {
		background-position: right center;
		background-repeat: no-repeat;
		background-size: contain;
	}
	.filter_box.accordion h3.ui-corner-top {
		background-image: url("<?php print $domain; ?>/webshop/termek_csoport_termek_parameterek_szuro/sort_down.png");
	}
	.filter_box.accordion h3.ui-corner-all {
		background-image: url("<?php print $domain; ?>/webshop/termek_csoport_termek_parameterek_szuro/sort_right.png");
	}
</style>
<?php endif; ?>
<?php endif; ?>
</form>
<script>
	$(document).on("change", "#szuro_form input[type=checkbox]", function() {
		// document.getElementById('szuro_form').submit();
		szures();
	});
</script>