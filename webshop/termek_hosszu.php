<?php
	// Link
	if ($config_ovip == 'I')
	{
		$query_csop = 'SELECT * FROM '.$ovipjel.'term_csoportok WHERE id='.$row['csop_id'];
	}
	else
	{
		$query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row['csop_id'];
	}
	$res_csop = $pdo->prepare($query_csop);
	$res_csop->execute();
	$row_csop  = $res_csop -> fetch();
	$link = ''.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row['nev_url'];
	// Kép
	$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
	$res = $pdo->prepare($query_kep);
	$res->execute();
	$row_kep = $res -> fetch();
	$alap_kep = $row_kep['kep'];
	if ($alap_kep == '') 
	{
		$kep_link = $domain.'/webshop/images/noimage.png';
	}
	else
	{
		if($row_kep['thumb'] != '')
		{
			$kep = $row_kep['thumb'];
		}
		else
		{
			$kep = $row_kep['kep'];
		}
		$kep_link = $domain.'/images/termekek/'.$kep;
	}
	//ÁR
	$datum = date("Y-m-d");
	if ($row['akcio_ig'] >= $datum && $row['akcio_tol'] <= $datum) //Akciós
	{
		$term_ar = '<span class="price-old">'.number_format($row['ar'], 0, ',', ' ').' Ft</span><span class="price-new">'.number_format($row['akciosar'], 0, ',', ' ').' Ft</span>';
	}
	else //nem akciós
	{
		$term_ar = '<span class="price-normal">'.number_format($row['ar'], 0, ',', ' ').' Ft</span>';
	}
	//Szállítási idő
	if ($row['raktaron'] > 0) // ha van raktáron
	{
		$szall_ido = '1-2 nap';
	}
	else 
	{
		$szall_ido = '3-4 nap';
	}
	
	print '<div class="row termek_hosszu">
		<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6 term_hosszu_tulajd">
			<a href="'.$link.'"><img alt="'.$row['nev'].'" src="'.$kep_link.'"></a>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 term_hosszu_tulajd">
			<h5><a href="'.$link.'">'.$row['nev'].'</a></h5><p class="rovid_leiras">'.$row['rovid_leiras'].'</p>
		</div>
		<div class="col-lg-2 col-md-0 col-sm-0 col-xs-0 center szall_ido ">
			<p>Szállítási idő:</p>
			<p>'.$szall_ido.'</p>
			<img src="'.$domain.'/webshop/images/szall_ido.png">
		</div>
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 center ">
			<p class="term_hosszu_ar">'.$term_ar.'</p>';
		// Összehasonlítás
			if ($config_osszehasonlito == 'I')
			{
				if ((isset($_SESSION['osszahas_1']) && $_SESSION['osszahas_1'] == $row['id']) || (isset($_SESSION['osszahas_2']) && $_SESSION['osszahas_2'] == $row['id']) || (isset($_SESSION['osszahas_3']) && $_SESSION['osszahas_3'] == $row['id']) || (isset($_SESSION['osszahas_4']) && $_SESSION['osszahas_4'] == $row['id']))
				{
					$osszeh_check = 'checked';
					$attr_checked = 'ok';
				}
				else
				{
					$osszeh_check = '';
					$attr_checked = 'no';
				}
				print '<p>
					összehasonlítás <input type="checkbox" class="osszeh_check" attr_term_id="'.$row['id'].'" attr_checked="'.$attr_checked.'" '.$osszeh_check.'>';
				print'</p>';
			}
		print '</div>
		<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6 ">
			<a href="'.$link.'" class="btn btn-active margbot10">Tovább</a>';
			?>
			<br/><a onclick="kivansaglistabaRakas(<?php print $row['id']; ?>)" class="btn btn-active btn-kivlista ">Kívánságlista</a>
			<?php
		print '</div>
	</div>';
	
?>
