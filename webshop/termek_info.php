<?php	
	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	?><link rel="stylesheet" href="<?= $domain ?>/webshop/css/swipebox.css"><?php
	// VÁSÁRLÁS
	if (isset($_POST['command']) && $_POST['command'] == 'VASARLAS')
	{	
		$kod = time().rand(1, 9999);
		$term_id = $_POST['term_id'];
		$term_db = $_POST['darab'];
		$datum = date('Y-m-d');
		$term_ar = $_POST['term_ar'];
		$term_akcios_ar = $_POST['term_akcios_ar'];
		$ido = date('H:i:s');
		$term_nev = $_POST['term_nev'];
		$term_kep = $_POST['term_kep'];
		$term_afa = $_POST['term_afa'];
		if (isset($_SESSION['kosar_id'])) //ha nem üres a kosár
		{
			$kosar_id = $_SESSION['kosar_id'];
			$query = "INSERT INTO ".$webjel."kosar (term_id,term_db,datum,term_ar,term_akcios_ar,ido,kosar_id,term_nev,term_kep,term_afa) VALUES (:term_id,:term_db,:datum,:term_ar,:term_akcios_ar,:ido,:kosar_id,:term_nev,:term_kep,:term_afa)";
			$result = $pdo->prepare($query);
			$result->execute(array(':term_id'=>$term_id,
								':term_db'=>$term_db,
								':datum'=>$datum,
								':term_ar'=>$term_ar,
								':term_akcios_ar'=>$term_akcios_ar,
								':ido'=>$ido,
								':kosar_id'=>$kosar_id,
								':term_nev'=>$term_nev,
								':term_kep'=>$term_kep,
								':term_afa'=>$term_afa));
			$kosar_tetel_id = $pdo->lastInsertId();
		}
		else //ha üres a kosár
		{
			$query = "INSERT INTO ".$webjel."kosar (term_id,term_db,datum,term_ar,term_akcios_ar,ido,kod,term_nev,term_kep,term_afa) VALUES (:term_id,:term_db,:datum,:term_ar,:term_akcios_ar,:ido,:kod,:term_nev,:term_kep,:term_afa)";
			$result = $pdo->prepare($query);
			$result->execute(array(':term_id'=>$term_id,
								':term_db'=>$term_db,
								':datum'=>$datum,
								':term_ar'=>$term_ar,
								':term_akcios_ar'=>$term_akcios_ar,
								':ido'=>$ido,
								':kod'=>$kod,
								':term_nev'=>$term_nev,
								':term_kep'=>$term_kep,
								':term_afa'=>$term_afa));
			$kosar_tetel_id = $pdo->lastInsertId();
									
			$query = "SELECT * FROM ".$webjel."kosar where kod='".$kod."'";
			$res = $pdo->prepare($query);
			$res->execute();
			$row  = $res -> fetch();
			$kosar_id = $row['id'];
			
			$id = $kosar_id;
			$query = "UPDATE ".$webjel."kosar SET kosar_id=? WHERE id=?";
			$result = $pdo->prepare($query);
			$result->execute(array($kosar_id,$id));
									
			$_SESSION['kosar_id'] = $kosar_id;
		}
		if (isset($_POST['termek_parameter'])) {
			foreach ($_POST['termek_parameter'] as $termek_parameter_id => $termek_parameter_ertek_id) {
				$insert = $pdo->prepare("INSERT INTO ".$webjel."kosar_tetel_termek_parameter_ertek ("
						. "kosar_tetel_id, "
						. "termek_parameter_id, "
						. "termek_parameter_nev, "
						. "termek_parameter_ertek_id, "
						. "termek_parameter_ertek_nev"
						. ") values ("
						. "".$kosar_tetel_id.", "
						. "".$termek_parameter_id.", "
						. "'".$pdo->query("SELECT nev FROM ".$webjel."termek_uj_parameterek WHERE id=".$termek_parameter_id)->fetchColumn()."', "
						. "".$termek_parameter_ertek_id.", "
						. "'".$pdo->query("SELECT ertek 
									FROM ".$webjel."termek_uj_parameter_ertekek 
									WHERE id=".$termek_parameter_ertek_id)->fetchColumn()."'"
						. ")");
				$insert->execute();
			}
		}
		
		// Ha kívánságlistából jött, akkor törölni kell onnan
		if (isset($_POST['command2']) && $_POST['command2'] == 'kivansaglistabol')
		{
			$deletecommand = "DELETE FROM ".$webjel."kivansag_lista WHERE termek_id =".$_POST['term_id']." AND user_id=".$_SESSION['login_id'];
			$result = $pdo->prepare($deletecommand);
			$result->execute();
		}
		// Kupon törlése
		$updatecommand = "UPDATE ".$webjel."kosar SET kupon_id=0 WHERE kosar_id=".$_SESSION['kosar_id'];
		$result = $pdo->prepare($updatecommand);
		$result->execute();
	}
	
	// print '<pre>'.print_r ($_POST,true).'</pre>';
	
	print '<input type="hidden" id="kat_urlnev" value="'.$_GET['kat_urlnev'].'" />';
	print '<input type="hidden" id="term_urlnev" value="'.$_GET['term_urlnev'].'" />';
	
	// Kívánság lista
	if (isset($_POST['command']) && $_POST['command'] == 'kivansag_lista')
	{
		if(isset($_SESSION['login_id']))
		{
			$rownum = 0;
			$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."kivansag_lista WHERE termek_id = ".$_POST['id']." AND user_id = ".$_SESSION['login_id']);
			$res->execute();
			$rownum = $res->fetchColumn();
			if ($rownum == 0) // Ha még nincs a listában
			{
				$insertcommand = "INSERT INTO ".$webjel."kivansag_lista (termek_id,user_id) VALUES (".$_POST['id'].",".$_SESSION['login_id'].")";
				$result = $pdo->prepare($insertcommand);
				$result->execute();
			}
			print '<div class="alert alert-success alert-dismissable"><p>A terméket sikeresen a kívánságlistádra helyezted. Reméljük, hamarosan átkerül a kosaradba.</p></div>';
		}
		else
		{
			print '<div class="alert alert-danger alert-dismissable">'
				. '<p>'
					. 'Helyezd az általad választott terméket egyszerűen a kívánságlistádra és add le rendelésed akkor, amikor már biztos vagy a dolgodban.<br>'
					. 'A kívánságlistádon elhelyezett termékeid aktuális állapotát érdemes figyelni nehogy lemaradj a rendelésről, hiszen termékkínálatunkat folyamatosan változtatjuk.<br>'
					. '<b>A kívánságlista használatához regisztráció szükséges. <a href="'.$domain.'/regisztracio/">Regisztrálj!</a></b>'
				. '</p>'
			. '</div>';
		}
	}
	$query = "SELECT *, ".$webjel."afa.afa as term_afa, ".$webjel."termekek.id as id 
		FROM ".$webjel."termekek 
		INNER JOIN ".$webjel."afa 
		ON ".$webjel."termekek.afa=".$webjel."afa.id 
		WHERE ".$webjel."termekek.nev_url='".$_GET['term_urlnev']."'";
	$res = $pdo->prepare($query);
	$res->execute();
	$row  = $res -> fetch();
	if(isset($row['id']) && $row['lathato'] == 1)
	{
		print '<div class="row" style="margin-top: 0px;">';
		//KÉP
			print '<div class="col-lg-6 col-md-6 col-sm-12">';
				$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
				$res = $pdo->prepare($query_kep);
				$res->execute();
				$row_kep = $res -> fetch();
				$alap_kep = $row_kep['thumb'];

					?>
						<div class="termek_plecsnik">
							<?php
								$query = "SELECT * 
								FROM ".$webjel."termek_uj_parameter_ertekek
								INNER JOIN ".$webjel."termek_uj_parameterek
								ON ".$webjel."termek_uj_parameter_ertekek.parameter_id = ".$webjel."termek_uj_parameterek.id
								WHERE termek_id=".$row['id']." AND ".$webjel."termek_uj_parameterek.plecsni = 1";
								foreach ($pdo->query($query) as $value) { ?>

									<img src="<?=$domain?>/images/termekek/<?=$value['ikon']?>" alt="<?=$value['ikon']?>" style="margin: 5px 0;"><br>
									
							<?php	 }
							?>
						</div>

					<?php				
				if ($alap_kep == '') 
				{					
					print '<img id="ez" src="'.$domain.'/webshop/images/noimage.png">';
				} elseif ($row_kep['ovip_termek_id'] != 0)
				{
					$query_kepek = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." ORDER BY alap DESC";
					$elso = 1;
					$kiskepek = 0;
					foreach ($pdo->query($query_kepek) as $row_kepek)
					{
						if($elso == 1) // Ha az első
						{
							// Alap kép
							print '<a href="'.$row_kepek['kep'].'" class="swipebox" title="'.$row['nev'].'"><img id="ez" src="'.$row_kepek['kep'].'"  alt="'.$row['nev'].'"></a>';
							$elso = 0;
						}
						else
						{
							if($kiskepek == 0) // Ha az első
							{
								print '<div class="row">';
								$kiskepek = 1;
							}
							print '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">';
							print '<a href="'.$row_kepek['kep'].'" class="swipebox" title="'.$row['nev'].'"><img id="ez" src="'.$row_kepek['thumb'].'"  alt="'.$row['nev'].'"></a>';
							print '</div>';
						}
					}
					if($kiskepek == 1) // Ha volt kiskép
					{
						print '</div>';
					}					
				}
				else
				{
				
					$query_kepek = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row['id']." ORDER BY alap DESC";
					$elso = 1;
					$kiskepek = 0;
					foreach ($pdo->query($query_kepek) as $row_kepek)
					{
						if($elso == 1) // Ha az első
						{
							// Alap kép
							print '<a href="'.$domain.'/images/termekek/'.$row_kepek['kep'].'" class="swipebox" title="'.$row['nev'].'"><img id="ez" src="'.$domain.'/images/termekek/'.$row_kepek['kep'].'"  alt="'.$row['nev'].'"></a>';
							$elso = 0;
						}
						else
						{
							if($kiskepek == 0) // Ha az első
							{
								print '<div class="row margtop20">';
								$kiskepek = 1;
							}
							print '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="margin-bottom: 10px;">';
							print '<a href="'.$domain.'/images/termekek/'.$row_kepek['kep'].'" class="swipebox" title="'.$row['nev'].'"><img id="ez" src="'.$domain.'/images/termekek/'.$row_kepek['thumb'].'"  alt="'.$row['nev'].'"></a>';
							print '</div>';
						}
					}
					if($kiskepek == 1) // Ha volt kiskép
					{
						print '</div>';
					}
				}
			print '</div>';
				
		//ADATOK
		print '<div class="col-lg-6 col-md-6 col-sm-12 temek_info">';
			if ($row['nev'.$_SESSION['lang_sql']] != '')
			{
				print '<h1 class="h1_termeknev">'.$row['nev'.$_SESSION['lang_sql']].'</h1>';
			}
			if ($row['rovid_leiras'.$_SESSION['lang_sql']] != '')
			{
				print '<p>'.nl2br($row['rovid_leiras'.$_SESSION['lang_sql']]).'</p>';
			}
		// Összehasonlítás
			if ($config_osszehasonlito == 'I')
			{
				if ((isset($_SESSION['osszahas_1']) && $_SESSION['osszahas_1'] == $row['id']) || (isset($_SESSION['osszahas_2']) && $_SESSION['osszahas_2'] == $row['id']) || (isset($_SESSION['osszahas_3']) && $_SESSION['osszahas_3'] == $row['id']) || (isset($_SESSION['osszahas_4']) && $_SESSION['osszahas_4'] == $row['id']))
				{
					$osszeh_check = 'checked';
				}
				else
				{
					$osszeh_check = '';
				}
				print '<p><form action="" method="POST">
							összehasonlítás <input type="checkbox" onclick="form.submit()" '.$osszeh_check.'>';
							if ($osszeh_check == '')
							{
								print '<input type="hidden" name="osszahas_id" value="'.$row['id'].'">';
							}
							else
							{
								print '<input type="hidden" name="no_osszahas_id" value="'.$row['id'].'">';
							}
				print'</form></p>';
			}
			
			$datum = date("Y-m-d");
			
		// Megtakarítás
				/* if ($row['akcio_ig'] >= $datum && $row['akcio_tol'] <= $datum) //Akciós
				{
					$megtak = 100 - ((100 * $row['akciosar']) / $row['ar']);
					print '<p>
									<div style="width:75px; height:30px; margin-top: 4px; background: url('.$domain.'/webshop/images/bg_megtak.png) no-repeat; color:white; padding-top:24px; font-size:16px; font-weight:bold; text-align:center;">
										'.number_format($megtak, 0, ',', ' ').'%
									</div>
							</p>';
				} */
		//ÁR
			if ((isset($_SESSION['login_tipus']) && $_SESSION['login_tipus'] == 'partner') && (isset($_SESSION['partner_kedvezmeny']) && $_SESSION['partner_kedvezmeny'] != 0))
			{
				$kedv_d = $_SESSION['partner_kedvezmeny'];
				$row['ar'] = $row['ar'] * ((100 - $kedv_d) / 100);
				$row['akciosar'] = $row['akciosar'] * ((100 - $kedv_d) / 100);
			}


			if (isset($_SESSION['valuta']) && $_SESSION['valuta'] != '')
			{
				$query_valuta = "SELECT * FROM ".$webjel."beallitasok WHERE id=1";
				$res = $pdo->prepare($query_valuta);
				$res->execute();
				$row_valuta = $res -> fetch();

				$akcios_ar_valuta = $row['akciosar'] / $row_valuta[$_SESSION['valuta'].'_arfolyam'];
				$ar_valuta = $row['ar'] / $row_valuta[$_SESSION['valuta'].'_arfolyam'];
				$valuta = $row_valuta[$_SESSION['valuta'].'_jel'];
			}
			else
			{
				$akcios_ar_valuta = $row['akciosar'];
				$ar_valuta = $row['ar'];
				$valuta = 'Ft';
			}			
				
			if ($row['akcio_ig'] >= $datum && $row['akcio_tol'] <= $datum) //ha akciós
			{
				print '<p class="term_info_regiar">'.number_format($ar_valuta, 0, ',', ' ').' '.$valuta.' </p>';
				print '<p class="term_info_ujar">'.number_format($akcios_ar_valuta, 0, ',', ' ').' '.$valuta.' </p>';
				$term_akcios_ar = $row['akciosar'];
			}
			else //ha nem akciós
			{
				// if ($row['ar'] > 0)
				// {
				print '<p class="term_info_ar text-gray">'.number_format($ar_valuta, 0, ',', ' ').' '.$valuta.' </p>';
					$term_akcios_ar = 0;
				// }
			}
			$query_beall = "SELECT * FROM ".$webjel."beallitasok where id = 1";
			$res_beall = $pdo->prepare($query_beall);
			$res_beall->execute();
			$row_beall  = $res_beall -> fetch();
			
		//DARAB
?>
			<div class="row"><div class="col-lg-12">
				<?php
					$termek_parameterek_kosar = $pdo->query("SELECT *, ".$webjel."termek_uj_parameterek.id AS id 
						FROM ".$webjel."termek_uj_parameterek 
						INNER JOIN ".$webjel."termek_uj_parameter_ertekek
						ON ".$webjel."termek_uj_parameterek.id = ".$webjel."termek_uj_parameter_ertekek.parameter_id 
							AND  ".$webjel."termek_uj_parameter_ertekek.termek_id = ".$row['id']."
						WHERE valaszthato=1 
						GROUP BY ".$webjel."termek_uj_parameterek.id 
						ORDER BY nev ASC")->fetchAll();
				?>
				<script type="text/javascript">
					function kosarba_repules() {
						<?php if (!empty($termek_parameterek_kosar)): ?>
						var noError = true;
						$('.termek_parameterek_kosar').each(function() {
							var error = $('#error_'+$(this).attr('id'));
							if ($(error).css('display')=='none') {
								var error_hide = true;
							}
							else
								var error_hide = false;
							if ($(this).val() == '') {
								noError = false;
								if (error_hide){
									$(error).show();
								}
							}
							else {
								if (!error_hide)
									$(error).hide();
							}
						});
						if (!noError)
							return false;
						<?php endif; ?>
					  var cart = $('#iderepul');
					  var imgtofly = $(document.getElementById("ez"));
					  var form = $('#kosarba_form'); // valahogy meg kell keresni a formot a DOM-ban, legegyszerűbb ha adsz neki ID-t
					  if (imgtofly) {
						var imgclone = imgtofly.clone()
						.offset({ top:imgtofly.offset().top, left:imgtofly.offset().left })
						.css({'opacity':'0.7', 'position':'absolute', 'height':'150px', 'width':'150px', 'z-index':'1000'})
						.appendTo($('body'))
						.animate({
						'top':cart.offset().top + 10,
						'left':cart.offset().left + 30,
						'width':55,
						'height':55
						}, 1000, 'swing',
						   function () { document.getElementById("kosarba_form").submit(); } // ez a callback, ami az animáció vége után lefut
						);
						imgclone.animate({'width':0, 'height':0}, function(){ $(this).detach() });
					  }

					  return false; // az onsubmit eseménykor, ez kerül visszaadásra azonnal a fgv által. Ha ez true, akkor a egyből elküldi a formot
					};
				</script>
				<?php
					if($config_keszlet_kezeles != 'I' || ($config_keszlet_kezeles == 'I' && $row['raktaron'] > 0))
					{
						?>
						<form name="kosarba_form" id="kosarba_form" action="" method="post" >
							<?php
								if($config_keszlet_kezeles == 'I')
								{
									print '<input type="hidden" name="raktaron" id="raktaron" value="'.$row['raktaron'].'"/>';
								}
								else
								{
									print '<input type="hidden" name="raktaron" id="raktaron" value="999999999"/>';
								}
							?>
							<div class="table-responsive termek_info_tabla">
							<table cellpadding="4" align="left" class="table">
							
									<script type="text/javascript">
										function subtractQty(){
											$('#darab_riaszt').css('display','none');
											if(document.getElementById("darab").value - 1 < 1)
											{
												return;
											}
											else
											{
												document.getElementById("darab").value--;
											}
										}
										function darabNoveles() {
											if(Number(document.getElementById("darab").value) == Number(document.getElementById("raktaron").value)){
												$('#darab_riaszt').css('display','block');
											}
											else {
												document.getElementById("darab").value++;
												$('#darab_riaszt').css('display','none');
											}
										}
									</script>
									<tr valign="middle">
										<td>Mennyiség:</td>
										<td>
												<input type="text" name="darab" id="darab" value="1" class="term_info_darab" readonly/>
												<button type="button" class="btn term_info_plusz button-sm" name='add' onclick='darabNoveles()'><span class="fa fa-plus"></span></button>
												<button type="button" class="btn term_info_minusz button-sm" name='subtract' onclick='javascript: subtractQty();'><span class="fa fa-minus"></span></button>												
										</td>
									</tr>
									<tr valign="middle" height="40" id="darab_riaszt">
										<td colspan="2"><div class="alert alert-danger" style="padding:6px 15px; margin-bottom:0;">A maximum megvásárolható mennyiség <?php echo $row['raktaron']; ?> db!</div></td>
									</tr>
	<?php /*<select attr_id="13" id="termek_parameter13" name="termek_parameter[13]" class="termek_parameterek_kosar">
		<option value="" data-felar="0">Válasszon</option>
		<option value="185" data-felar="0">Fehér</option>
		<option value="186" data-felar="0">Zöld</option>
	</select>*/ ?>
									<?php 
									foreach ($termek_parameterek_kosar as $termek_parameter)
									{
										?>
										<tr id="error_termek_parameter<?php echo $termek_parameter['id']; ?>" style="display: none;"> 
											<td colspan="2">
												<div class="alert alert-danger" style="padding:6px 15px; margin-bottom:0;">Kötelező kiválasztani!</div>
											</td>
										</tr>
										<tr>
											<td>
												<?php echo $termek_parameter['nev']; ?>
											</td>
											<td style="padding: 6px 0 6px 6px;">
												<select attr_id="<?= $termek_parameter['id'] ?>" id="termek_parameter<?= $termek_parameter['id'] ?>" name="termek_parameter[<?= $termek_parameter['id'] ?>]" class="termek_parameterek_kosar">
													<option value="" data-felar="0">Válasszon</option>
													<?php
													$termek_parameter_ertekek = $pdo->query("SELECT * 
																FROM ".$webjel."termek_uj_parameter_ertekek 
																WHERE parameter_id=".$termek_parameter['id']."  AND termek_id = ".$row['id']."
																ORDER BY ertek"
													)->fetchAll();
													foreach ($termek_parameter_ertekek as $termek_parameter_ertek)
													{
														if (isset($_SESSION['valuta']) && $_SESSION['valuta'] != '')
														{
															$query_valuta = "SELECT * FROM ".$webjel."beallitasok WHERE id=1";
															$res = $pdo->prepare($query_valuta);
															$res->execute();
															$row_valuta = $res -> fetch();

															$parameter_felar = $termek_parameter_ertek['felar'] / $row_valuta[$_SESSION['valuta'].'_arfolyam'];
															$valuta = $row_valuta[$_SESSION['valuta'].'_jel'];
														}
														else
														{
															$parameter_felar = $termek_parameter_ertek['felar'];
															$valuta = 'Ft';
														}

														?>
														<option
															value="<?php echo $termek_parameter_ertek['id']; ?>"
															data-felar="<?php echo $termek_parameter_ertek['felar']; ?>"
														>
															<?php echo $termek_parameter_ertek['ertek']; ?>
															<?php if ($termek_parameter_ertek['felar']): ?>
															(<?php echo number_format($parameter_felar, 0, ',', ' '); ?> <?=$valuta?> felár)
															<?php endif; ?>
														</option>
														<?php
													}
													?>
												</select>
											</td>
										</tr>
										<?php 
									}
									?>
							</table>
							</div>
							<input type='hidden' id='term_id' name='term_id' value='<?php print $row['id']; ?>'/>
							<input type='hidden' id='term_nev'  name='term_nev' value='<?php print $row['nev']; ?>'/>
							<input type='hidden' id='term_kep' name='term_kep' value='<?php print $alap_kep; ?>'/>
							<input type='hidden' id='term_ar' name='term_ar' value='<?php print $row["ar"]; ?>'/>
							<input type='hidden' id='term_afa' name='term_afa' value='<?php print $row["term_afa"]; ?>'/>
							<input type='hidden' id='term_akcios_ar' name='term_akcios_ar' value='<?php print $term_akcios_ar; ?>'/>
							<input type='hidden' id='command' name='command' value='VASARLAS'/>
						</form>
						<?php
					}
					else
					{
						print 'A termék jelenleg nem rendelhető!';
					}
					?>
				<form id="kivansag_lista_form" action="" method="post" >
					<input type='hidden' name='command' value='kivansag_lista'/>
					<input type='hidden' name='id' value='<?php print $row['id']; ?>'/>
				</form>
			</div></div>
			<div class="row margtop10" id="parameter_riaszt" style="display:none; margin-top: 10px;">
				<div class="col-lg-12 col-md-12">
					<div class="alert alert-warning alert-danger" style="margin-bottom:0;"><p>Válassz paramétert.</p></div>
				</div>
			</div>
			<div class="row margtop10" style="margin-top: 10px;">
				<?php
				if($config_keszlet_kezeles != 'I' || ($config_keszlet_kezeles == 'I' && $row['raktaron'] > 0))
				{ ?>
					<div class="col-lg-6 col-md-6 col-sm-12"><a id="kosarba_gomb" class="button button-primary term_info_kosarba" onclick="fbq('track', 'AddToCart');"><?=$sz_term_1?></a></div>
					<?php
				}
				?>
			</div>
		</div>
		</div>
		<div class="row margtop30">
			<div class="col-lg-12">
              <div class="tabs-custom tabs-horizontal text-left" id="tabs-1">
                <!-- Nav tabs-->
                <ul class="nav-custom nav-custom-tabs">
                  <li class="active"><a href="#leiras" data-toggle="tab"><?=$sz_term_2?></a></li>
                  <li><a href="#technikai_adatok" data-toggle="tab"><?=$sz_term_3?></a></li>
                </ul>
                <div class="tab-content text-left">
                  <div class="tab-pane fade in active" id="leiras">
						<?= $row["leiras".$_SESSION['lang_sql']] ?>
                  </div>
                  <div class="tab-pane fade temek_info" id="technikai_adatok">
							<?php
								$query_jell = "SELECT * 
									FROM ".$webjel."termek_uj_parameter_ertekek 
									INNER JOIN ".$webjel."termek_uj_parameterek 
									ON ".$webjel."termek_uj_parameterek.id=".$webjel."termek_uj_parameter_ertekek.parameter_id 
									WHERE termek_id=".$row['id']." AND valaszthato=0";
								echo '<div class="table-responsive"><table class="table table-bordered table-striped"><tbody>';
								$tr = 1;
								foreach ($pdo->query($query_jell) as $row_jell)
								{
									if($tr == 1)
									{
										echo '<tr>';
										$tr = 0;
									}
									else { $tr = 1; }
									echo '<td style="width:50%;">';
										if($row_jell['ikon'] != '')
										{
											echo '<img src="'.$domain.'/images/termekek/'.$row_jell['ikon'].'" />';
										}
										echo '<b>'.$row_jell['nev'].':</b>
										<span>'.$row_jell['ertek'].' '.$row_jell['mertekegyseg'].'</span>
									</td>';
									if($tr == 1) { echo '</tr>'; }
								}
								echo '</tbody></table></div>';
							?>
                  </div>
                </div>
              </div>				
			</div>
		</div>
<?php	
		// Kapcsolódó termékek
		print '<h4 class="text-center" style="margin-top: 50px;">'.$sz_term_4.'</h4>';
		$csop_id = $row['csop_id'];
		$termek_id = $row['id'];
		include $gyoker.'/webshop/kapcsolodo_termekek.php';
	}
	else
	{
		print '<h5>'.$sz_term_5.'</h5>';
	}
?>