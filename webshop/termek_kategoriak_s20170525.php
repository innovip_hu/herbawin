<?php

	if (isset($_GET['script']))
	{
		session_start();
		ob_start();
		include '../config.php';
		$dsn = sprintf("mysql:host=%s;port=3306;dbname=%s", $dbhost, $dbname);
		try
		{
			$pdo = new PDO(
			$dsn, $dbuser, $dbpass,
			Array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8")
			);
		}
		catch (PDOException $e)
		{
			die("Nem lehet kapcsolódni az adatbázishoz!");
		}
	}
	// Kereső
	if(isset($_GET['keres_nev']))
	{
		$_SESSION['keres_nev'] = $_GET['keres_nev'];
	}
	// Szűrés
	if(isset($_GET['command']) && $_GET['command'] == 'szures')
	{
		$oldal = 'termekek';
		include $gyoker.'/webshop/webshop.php';
	}
	// Nézet
	if(isset($_GET['nezet_uj']))
	{
		$_SESSION['nezet'] = $_GET['nezet_uj'];
	}
	// Kívánságlista
	if (isset($_GET['kivansaglista_termek']))
	{
		if(isset($_SESSION['login_id']))
		{
			$rownum = 0;
			$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."kivansag_lista WHERE termek_id = ".$_GET['kivansaglista_termek']." AND user_id = ".$_SESSION['login_id']);
			$res->execute();
			$rownum = $res->fetchColumn();
			if ($rownum == 0) // Ha még nincs a listában
			{
				$insertcommand = "INSERT INTO ".$webjel."kivansag_lista (termek_id,user_id) VALUES (".$_GET['kivansaglista_termek'].",".$_SESSION['login_id'].")";
				$result = $pdo->prepare($insertcommand);
				$result->execute();
			}
			print '<div class="alert alert-success alert-dismissable"><p>A terméket sikeresen a kívánságlistádra helyezted. Reméljük, hamarosan átkerül a kosaradba.</p></div>';
		}
		else
		{
			print '<div class="alert alert-danger alert-dismissable"><p>Helyezd az általad választott terméket egyszerűen a kívánságlistádra és add le rendelésed akkor, amikor már biztos vagy a dolgodban. A kívánságlistádon elhelyezett termékeid aktuális állapotát érdemes figyelni nehogy lemaradj a rendelésről, hiszen termékkínálatunkat folyamatosan változtatjuk. A kívánságlista használatához regisztráció szükséges. <a href="'.$domain.'/regisztracio/">Regisztrálj!</a></p></div>';
		}
	}
	// Összehasonlítás
	if (isset($_GET['osszeh_termek']))
	{
		if($_GET['osszeh_checked'] == 'no') // Bekapcsolás
		{
			if (!isset($_SESSION['osszahas_1']))
			{
				$_SESSION['osszahas_1'] = $_GET['osszeh_termek'];
			}
			else if (!isset($_SESSION['osszahas_2']))
			{
				$_SESSION['osszahas_2'] = $_GET['osszeh_termek'];
			}
			else if (!isset($_SESSION['osszahas_3']))
			{
				$_SESSION['osszahas_3'] = $_GET['osszeh_termek'];
			}
			else if (!isset($_SESSION['osszahas_4']))
			{
				$_SESSION['osszahas_4'] = $_GET['osszeh_termek'];
			}
		}
		else
		{
			if (isset($_SESSION['osszahas_1']) && $_SESSION['osszahas_1'] == $_GET['osszeh_termek'])
			{
				unset($_SESSION['osszahas_1']);
			}
			else if (isset($_SESSION['osszahas_2']) && $_SESSION['osszahas_2'] == $_GET['osszeh_termek'])
			{
				unset($_SESSION['osszahas_2']);
			}
			else if (isset($_SESSION['osszahas_3']) && $_SESSION['osszahas_3'] == $_GET['osszeh_termek'])
			{
				unset($_SESSION['osszahas_3']);
			}
			else if (isset($_SESSION['osszahas_4']) && $_SESSION['osszahas_4'] == $_GET['osszeh_termek'])
			{
				unset($_SESSION['osszahas_4']);
			}
		}
	}
	
		
	print '<input type="hidden" id="kat_urlnev" value="'.$_GET['kat_urlnev'].'" />';
	print '<input type="hidden" id="term_urlnev" value="" />';
	
	$csop_id = 0;
	$csop_leiras = '';
	if (isset($_GET['kat_urlnev'])) // Kategória leírása a felsorolás előtt
	{
		$res = $pdo->prepare("SELECT * FROM ".$webjel."term_csoportok where nev_url='".$_GET['kat_urlnev']."'");
		$res->execute();
		$row  = $res -> fetch();
		$csop_id = $row['id'];
		$csop_nev = $row['nev'];
		$csop_leiras = $row['leiras'];
		$csop_kep = $row['kep'];
	}
	
	$res = $pdo->prepare("SELECT COUNT(*) FROM ".$webjel."term_csoportok WHERE csop_id = ".$csop_id." AND lathato=1 ORDER BY sorrend asc");
	$res->execute();
	$rownum = $res->fetchColumn();
// Kategóriák generálása
	if ( $rownum > 0 && $csop_id > 0)
	{
		print '<div class="row kapcs_termekek">';
		$query1 = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id = ".$csop_id." AND lathato=1 ORDER BY sorrend asc";
		foreach ($pdo->query($query1) as $row1)
		{
			if ($row1['kep'] == "")
			{
				$csop_kep_link = ''.$domain.'/webshop/images/noimage.png';
			}
			else
			{
				$csop_kep_link = ''.$domain.'/images/termekek/'.$row1['kep'];
			}
			print '<div class="product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12 fels_termek_div">
					<a href="'.$domain.'/termekek/'.$row1['nev_url'].'/"><div class="fels_termek_kep">
						<img alt="'.$row1['nev'].'" class="img-responsive lazy" data-src="'.$csop_kep_link.'" src="'.$csop_kep_link.'" style="opacity: 1;">
					</div></a>
					<div class="fels_termek_adatok">
						<h5 class="fels_termek_nev center"><a href="'.$domain.'/termekek/'.$row1['nev_url'].'/">'.$row1['nev'].'</a></h5>
					</div>
			</div>';
		}
		print '</div>';
	}
// Termékek generálása
	else
	{
	// Kategória leírása
		if ($csop_leiras != '')
		{
			print '<div class="row margbot20">';
				if ($csop_kep != '')
				{
					print '<div class="col-sm-2">
						<div class="image">
							<img src="'.$domain.'/images/termekek/'.$csop_kep.'" class="img-thumbnail">
						</div>
					</div>
					<div class="col-sm-10">';
				}
				else
				{
					print '<div class="col-sm-12">';
				}
					print $csop_leiras.'
				</div>
			</div>';
		}
	//oldalankénti darabszám
		if (isset($_GET['old_db']))
		{
			$_SESSION['old_db'] = $_GET['old_db'];
			unset($_SESSION['oldszam']);
		}
		if (isset($_SESSION['old_db']))
		{
			$db_per_oldal = $_SESSION['old_db'];
		}
		else
		{
			$db_per_oldal = 24; //alapból ennyi termék jelenik meg
		}
	// Szűrő
		$szuro_sql = '';
		if (isset($_SESSION['szuro_minimum_ar'])) // Ár szerinti szűrés
		{
			$szuro_sql = ' AND IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar) >= '.$_SESSION['szuro_minimum_ar'].' AND IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar) <= '.$_SESSION['szuro_maximum_ar'].' ';
		}
	// Kereső
		if (isset($_SESSION['keres_nev']))
		{
			$szuro_sql .=  " AND nev LIKE '%".str_replace(" ", "%", $_SESSION['keres_nev'])."%' ";
		}
		
		if (isset($_SESSION['termek_parameter_ertek'])) {
			$szuro_termek_parameter_ertek = " AND (";
			$i = 0;
			foreach ($_SESSION['termek_parameter_ertek'] as $termek_parameter_id => $termek_parameter_ertekek)
			{
				/* if (reset($_SESSION['termek_parameter_ertek']) !== $termek_parameter_ertekek) {
					$szuro_termek_parameter_ertek .= " AND ";
				} */
				if ($i > 0) {
					$szuro_termek_parameter_ertek .= " AND ";
				}
				$i++;
				$szuro_termek_parameter_ertek .= "EXISTS ("
						. "SELECT 1 FROM ".$webjel."termek_termek_parameter_ertekek ttpe WHERE ".$webjel."termekek.id=ttpe.termek_id AND (";
				$szuro_termek_parameter_ertek .= "ttpe.termek_parameter_id=".$termek_parameter_id." AND (";
				foreach ($termek_parameter_ertekek as $termek_parameter_ertek) {
					if (reset($termek_parameter_ertekek) !== $termek_parameter_ertek) {
						$szuro_termek_parameter_ertek .= " OR ";
					}
					if (strpos($termek_parameter_ertek, '|') === FALSE) {
						$szuro_termek_parameter_ertek .= "ttpe.ertek='".$termek_parameter_ertek."'";
					}
					else {
						$termek_csoport_termek_parameter_ertek_tipus = explode('|', $termek_parameter_ertek);
						$termek_csoport_termek_parameter_ertek_tipus = reset($termek_csoport_termek_parameter_ertek_tipus);
						$termek_parameter_ertek = explode('|', $termek_parameter_ertek);
						$termek_parameter_ertek = end($termek_parameter_ertek);
						if ($termek_csoport_termek_parameter_ertek_tipus == TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_NAGYOBB) {
							$szuro_termek_parameter_ertek .= "ttpe.ertek>=".$termek_parameter_ertek."";
						}
						elseif ($termek_csoport_termek_parameter_ertek_tipus == TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_KISEBB) {
							$szuro_termek_parameter_ertek .= "ttpe.ertek<=".$termek_parameter_ertek."";
						}
						elseif ($termek_csoport_termek_parameter_ertek_tipus == TERMEK_CSOPORT_TERMEK_PARAMETER_ERTEK_TIPUS_KOZOTT) {
							$termek_parameter_ertek = explode('-', $termek_parameter_ertek);
							$szuro_termek_parameter_ertek .= "(ttpe.ertek>=".$termek_parameter_ertek[0]." AND ttpe.ertek<=".$termek_parameter_ertek[1].")";
						}
					}
				}
				$szuro_termek_parameter_ertek .= ")))";
			}
			$szuro_termek_parameter_ertek .= ")";
		}
		else {
			$szuro_termek_parameter_ertek = "";
		}
		
	//rekordok száma
		$res = $pdo->prepare("SELECT COUNT(*), IF(akciosar > 0 AND akcio_ig >= NOW(), akciosar, ar) AS ar_sorrend FROM ".$webjel."termekek WHERE (csop_id = ".$csop_id." OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok WHERE ".$webjel."termek_termek_csoportok.termek_id=".$webjel."termekek.id AND ".$webjel."termek_termek_csoportok.termek_csoport_id=".$csop_id.")) AND lathato=1".$szuro_sql.$szuro_termek_parameter_ertek);
		$res->execute();
		$rownum = $res->fetchColumn();
	//kezdés meghatározása
		if (isset($_GET['oldszam']))
		{
			$_SESSION['oldszam'] = $_GET['oldszam'];
			$_SESSION['csop_id'] = $csop_id;
		}
		if (isset($_SESSION['oldszam']) && !isset($_POST['szuro_maximum_ar']))
		{
			$kezd = $_SESSION['oldszam'];
		}
		else
		{
			$kezd = 0;
		}
	//LAPOZÁS
		print '<div class="row lapozo_sav">';
			print	'<div class="col-lg-5 col-md-5 col-sm-12">';
				//Oldalankénti darabszám
						print '<span>
								<select class="form-control" id="old_db" name="old_db">';
								/* if (isset($_SESSION['old_db']) && $_SESSION['old_db'] == 3)
								{
									print '<option value="3" selected>3</option>';
								}
								else
								{
									print '<option value="3">3</option>';
								}
								if (isset($_SESSION['old_db']) && $_SESSION['old_db'] == 12)
								{
									print '<option value="12" selected>12</option>';
								}
								else
								{
									print '<option value="12">12</option>';
								} */
								if (isset($_SESSION['old_db']) && $_SESSION['old_db'] == 24)
								{
									print '<option value="24" selected>24</option>';
								}
								else
								{
									print '<option value="24">24</option>';
								}
								if (isset($_SESSION['old_db']) && $_SESSION['old_db'] == 48)
								{
									print '<option value="48" selected>48</option>';
								}
								else
								{
									print '<option value="48">48</option>';
								}
								if (isset($_SESSION['old_db']) && $_SESSION['old_db'] == 100)
								{
									print '<option value="100" selected>100</option>';
								}
								else
								{
									print '<option value="100">100</option>';
								}
								if (isset($_SESSION['old_db']) && $_SESSION['old_db'] == 500)
								{
									print '<option value="500" selected>500</option>';
								}
								else
								{
									print '<option value="500">500</option>';
								}
								print '</select>';
						print '</span>';
				//Sorrend
					
					if (isset($_GET['sorrend_uj']))
					{
						if ($_GET['sorrend_uj'] == 'nev_nov')
						{
							$_SESSION['sorr_tip'] = 'nev';
							$_SESSION['sorrend'] = 'asc';
						}
						else if ($_GET['sorrend_uj'] == 'nev_csokk')
						{
							$_SESSION['sorr_tip'] = 'nev';
							$_SESSION['sorrend'] = 'desc';
						}
						else if ($_GET['sorrend_uj'] == 'ar_nov')
						{
							$_SESSION['sorr_tip'] = 'ar_sorrend';
							$_SESSION['sorrend'] = 'asc';
						}
						else if ($_GET['sorrend_uj'] == 'ar_csokk')
						{
							$_SESSION['sorr_tip'] = 'ar_sorrend';
							$_SESSION['sorrend'] = 'desc';
						}
					}
					if (isset($_SESSION['sorr_tip']))
					{
						$sorr_tip = $_SESSION['sorr_tip'];
					}
					else
					{
						$sorr_tip = 'ar_sorrend'; // Alap rendezési feltétel
					}
					if (isset($_SESSION['sorrend']))
					{
						$sorrend = $_SESSION['sorrend'];
					}
					else
					{
						$sorrend = 'ASC'; // Alap rendezési feltétel
					}
					
					print '<span>
							<select class="form-control" id="sorrend_uj" name="sorrend_uj">';
								if ($sorr_tip == 'ar_sorrend' && $sorrend == 'asc')
								{
									print '<option value="ar_nov" selected>Ár szerint növekvő</option>';
								}
								else
								{
									print '<option value="ar_nov">Ár szerint növekvő</option>';
								}
								if ($sorr_tip == 'ar_sorrend' && $sorrend == 'desc')
								{
									print '<option value="ar_csokk" selected>Ár szerint csökkenő</option>';
								}
								else
								{
									print '<option value="ar_csokk">Ár szerint csökkenő</option>';
								}
								if ($sorr_tip == 'nev' && $sorrend == 'asc')
								{
									print '<option value="nev_nov" selected>Név szerint növekvő</option>';
								}
								else
								{
									print '<option value="nev_nov">Név szerint növekvő</option>';
								}
								if ($sorr_tip == 'nev' && $sorrend == 'desc')
								{
									print '<option value="nev_csokk" selected>Név szerint csökkenő</option>';
								}
								else
								{
									print '<option value="nev_csokk">Név szerint csökkenő</option>';
								}
							print '</select>
							</span>';
							// Nézet
							if(isset($_SESSION['nezet']))
							{
								$nezet_fajl = $_SESSION['nezet'];
								if($_SESSION['nezet'] == 'termek')
								{
									print '<img id="nezet_gomb" attr_nezet="termek_hosszu" class="nezet_gomb" src="'.$domain.'/webshop/images/g_term_hosszu.png" />';
								}
								else if($_SESSION['nezet'] == 'termek_hosszu')
								{
									print '<img id="nezet_gomb" attr_nezet="termek" class="nezet_gomb" src="'.$domain.'/webshop/images/g_term_negyzetes.png" />';
								}
							}
							else
							{
								// $nezet_fajl = 'termek_hosszu'; // alap nézet
								$nezet_fajl = 'termek'; // alap nézet
								print '<img id="nezet_gomb" attr_nezet="termek" class="nezet_gomb" src="'.$domain.'/webshop/images/g_term_negyzetes.png" />';
							}
						print '</div>';
					
			//oldalszám kiírása
					print '<div class="col-lg-7 col-md-7 col-sm-12"><div class="lapozo_ikonok_div">';
					if (isset($_SESSION['sorrend'])) { $old_sorrend = '&sorrend='.$_SESSION['sorrend'];}
					if (isset($_SESSION['sorr_tip'])) { $old_sorr_tip = '&sorr_tip='.$_SESSION['sorr_tip'];}
					if (isset($_SESSION['old_db'])) { $old_old_db = '&old_db='.$_SESSION['old_db'];}
					
					$aktual_oldszam = ($kezd + $db_per_oldal) / $db_per_oldal;
					$oldal_elso = '';
					$oldal_minusz_1 = '';
					$oldal_minusz_2 = '';
					$oldal_minusz_3 = '';
					$pontok_eleje = '';
					$oldal_utolso = '';
					$pontok_vege = '';
					$oldal_plusz_1 = '';
					$oldal_plusz_2 = '';
					$oldal_plusz_3 = '';
					$oldal_aktualis = '';
					
				//Lapozó ikonok
					if (isset($_SESSION['oldszam']) &&  $_SESSION['oldszam'] > 0)
					{
						print '<span>
							<input onClick="lapozas('.($kezd - $db_per_oldal).')" type="submit" title="" value="<" class="lapozo_szamok"/>
						</span>';
					}
					
					if (($aktual_oldszam) > 1)
					{
						$oldal_elso = '<span><input onClick="lapozas(0)" type="submit" title="" value="1" class="lapozo_szamok" /></span>';

					}
					if($rownum > $db_per_oldal) // Aktuális oldalszám, ha nem fér el egy oldalra az össz termék
					{
						$oldal_aktualis = '<span><font class="lapozo_szamok_aktualis">'.$aktual_oldszam.'</font></span>';
					}
					if (($aktual_oldszam - 1) > 1)
					{
						$oldal_minusz_1 = '<span>
							<input onClick="lapozas('.(($aktual_oldszam - 2)*$db_per_oldal).')" type="submit" title="" value="'.($aktual_oldszam - 1).'" class="lapozo_szamok"/>
						</span>';
					}
					if (($aktual_oldszam - 2) > 1)
					{
						$oldal_minusz_2 = '<span>
							<input onClick="lapozas('.(($aktual_oldszam - 3)*$db_per_oldal).')" type="submit" title="" value="'.($aktual_oldszam - 2).'" class="lapozo_szamok" />
						</span>';
					}
					if (($aktual_oldszam - 3) > 1)
					{
						$oldal_minusz_3 = '<span>
							<input onClick="lapozas('.(($aktual_oldszam - 4)*$db_per_oldal).')" type="submit" title="" value="'.($aktual_oldszam - 3).'" class="lapozo_szamok" />
						</span>';
					}
					if (($aktual_oldszam) > 5)
					{
						$pontok_eleje = '<span>...</span>';
					}
					$utolso_oldszam = ceil($rownum / $db_per_oldal);
					if (($aktual_oldszam) < $utolso_oldszam)
					{
						$oldal_utolso = '<span>
							<input onClick="lapozas('.(($utolso_oldszam - 1)*$db_per_oldal).')" type="submit" title="" value="'.$utolso_oldszam.'" class="lapozo_szamok" />
						</span>';
					}
					if (($aktual_oldszam) < ($utolso_oldszam - 4))
					{
						$pontok_vege = '<span>...</span>';
					}
					if (($aktual_oldszam + 1) < $utolso_oldszam)
					{
						$oldal_plusz_1 = '<span>
							<input onClick="lapozas('.(($aktual_oldszam + 0)*$db_per_oldal).')" type="submit" title="" value="'.($aktual_oldszam + 1).'" class="lapozo_szamok" />
						</span>';
					}
					if (($aktual_oldszam + 2) < $utolso_oldszam)
					{
						$oldal_plusz_2 = '<span>
							<input onClick="lapozas('.(($aktual_oldszam + 1)*$db_per_oldal).')" type="submit" title="" value="'.($aktual_oldszam + 2).'" class="lapozo_szamok" />
						</span>';
					}
					if (($aktual_oldszam + 3) < $utolso_oldszam)
					{
						$oldal_plusz_3 = '<span>
							<input onClick="lapozas('.(($aktual_oldszam + 2)*$db_per_oldal).')" type="submit" title="" value="'.($aktual_oldszam + 3).'" class="lapozo_szamok" />
						</span>';
					}
					
					print '<span>'.$oldal_elso.$pontok_eleje.$oldal_minusz_2.$oldal_minusz_1.$oldal_aktualis.$oldal_plusz_1.$oldal_plusz_2.$pontok_vege.$oldal_utolso.'</span><span></span>';
					
				//Lapozó ikonok
					if (isset($_SESSION['oldszam']) &&  $_SESSION['oldszam'] > 0)
					{
						print '<span>';
						if (($kezd + $db_per_oldal) < $rownum)
						{
							print	'<input onClick="lapozas('.($kezd + $db_per_oldal).')" type="submit" title="" value=">" class="lapozo_szamok" id="balrabutton" />';
						}
						print '</span>';
					}
					else
					{
						print '<span></span>';
						if (($kezd + $db_per_oldal) < $rownum)
						{
							print '<span width="24">
								<input onClick="lapozas('.($kezd + $db_per_oldal).')" type="submit" title="" value=">" class="lapozo_szamok" id="balrabutton" />
							</span>';
						}
					}
				print	'</div></div></div>';
	//TERMÉKEK
		$datum = date('Y-m-d');
		$query = "SELECT *, IF(akciosar > 0 AND akcio_ig >= '".$datum."', akciosar, ar) AS ar_sorrend FROM ".$webjel."termekek WHERE (csop_id=".$csop_id." OR EXISTS (SELECT 1 FROM ".$webjel."termek_termek_csoportok WHERE ".$webjel."termek_termek_csoportok.termek_id=".$webjel."termekek.id AND ".$webjel."termek_termek_csoportok.termek_csoport_id=".$csop_id.")) AND lathato=1 ".$szuro_sql.$szuro_termek_parameter_ertek." ORDER BY ".$sorr_tip." ".$sorrend." LIMIT ".$kezd.",".$db_per_oldal;
		foreach ($pdo->query($query) as $row)
		{
			print '<div id="termek_felsorolas">';
			include $gyoker.'/webshop/'.$nezet_fajl.'.php';
			// include $gyoker.'/webshop/termek_hosszu.php';
			print '</div>';
		}
		
	//LAPOZÁS az alján is
		print '<div class="row lapozo_sav"><div class="col-lg-12"><div class="lapozo_ikonok_div">';
					
				//Lapozó ikonok
					if (isset($_SESSION['oldszam']) &&  $_SESSION['oldszam'] > 0)
					{
						print '<span>
							<input onClick="lapozas('.($kezd - $db_per_oldal).')" type="submit" title="" value="<" class="lapozo_szamok" id="balrabutton" />
						</span>';
					}
					
				//oldalszám kiírása
					
					print '<span align="right" colspan="4">'.$oldal_elso.$pontok_eleje.$oldal_minusz_3.$oldal_minusz_2.$oldal_minusz_1.$oldal_aktualis.$oldal_plusz_1.$oldal_plusz_2.$oldal_plusz_3.$pontok_vege.$oldal_utolso.'<span></span>';
					
				//Lapozó ikonok
					if (isset($_SESSION['oldszam']) &&  $_SESSION['oldszam'] > 0)
					{
						print '<span>';
						if (($kezd + $db_per_oldal) < $rownum)
						{
							print	'<input onClick="lapozas('.($kezd + $db_per_oldal).')" type="submit" title="" value=">" class="lapozo_szamok" id="balrabutton" />';
						}
						print '</span>';
					}
					else
					{
						if (($kezd + $db_per_oldal) < $rownum)
						{
							print '<span width="24">
								<input onClick="lapozas('.($kezd + $db_per_oldal).')" type="submit" title="" value=">" class="lapozo_szamok" id="balrabutton" />
							</span>';
						}
					}
			print '</div></div></div>';
	}
?>