<?php
	// Link
	if (isset($_GET['keres_nev']) || isset($_GET['keres_nev']) || isset($akcios_termek) || (isset($legujabb_termek) && $legujabb_termek == 'ok') || (isset($legnepszerubb_termekek) && $legnepszerubb_termekek == 'ok'))
	{
		if ($config_ovip == 'I')
		{
			$query_csop = 'SELECT * FROM '.$ovipjel.'term_csoportok WHERE id='.$row['csop_id'];
		}
		else
		{
			$query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row['csop_id'];
		}
		$res_csop = $pdo->prepare($query_csop);
		$res_csop->execute();
		$row_csop  = $res_csop -> fetch();
		$link = 'http://'.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row['nev_url'];
	}
	else
	{
		$link = 'http://'.$domain.'/termekek/'.$_GET['kat_urlnev'].'/'.$row['nev_url'];
	}
	// Kép
	if ($row['kep'] == "")
	{
		$kep_link = 'http://'.$domain.'/webshop/images/noimage.png';
	}
	else
	{
		$kep_link = 'http://'.$domain.'/images/termekek/'.$row['kep'];
		
	}
	//ÁR
	$datum = date("Y-m-d");
	if ($row['akcio_ig'] >= $datum && $row['akcio_tol'] <= $datum) //Akciós
	{
		$term_ar = '<span class="price-new">'.number_format($row['akciosar'], 0, ',', ' ').' Ft</span> <span class="price-old">'.number_format($row['ar'], 0, ',', ' ').' Ft</span>';
	}
	else //nem akciós
	{
		$term_ar = '<span class="price-new">'.number_format($row['ar'], 0, ',', ' ').' Ft</span>';
	}
	print '<div class="product-layout product-grid col-lg-4 col-md-4 col-sm-4 col-xs-12">
		<div class="product-thumb transition maxheight2">';
			// print '<div class="new_pr">New</div>';
			print '<div class="image">
				<a href="'.$link.'"><img alt="'.$row['nev'].'" class="img-responsive lazy" data-src="'.$kep_link.'" src="'.$kep_link.'" style="opacity: 1;"></a>
			</div>
			<div class="caption">
				<div class="price">
					'.$term_ar.'
				</div>
				<div class="name"><a href="'.$link.'">'.$row['nev'].'</a></div>
				<div class="description">'.$row['rovid_leiras'].'</div>
			</div>
		</div>
	</div>';
	
	
	
	/* print '<div class="ws_termek">'; 
	//KÉP
	print '<div style="margin:auto; height:148px;">'; // KÉP
			if (isset($_GET['keres_nev']) || isset($_GET['keres_nev']) || isset($akcios_termek) || (isset($legujabb_termek) && $legujabb_termek == 'ok') || (isset($legnepszerubb_termekek) && $legnepszerubb_termekek == 'ok'))
			{
				if ($config_ovip == 'I')
				{
					$query_csop = 'SELECT * FROM '.$ovipjel.'term_csoportok WHERE id='.$row['csop_id'];
				}
				else
				{
					$query_csop = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row['csop_id'];
				}
				$res_csop = $pdo->prepare($query_csop);
				$res_csop->execute();
				$row_csop  = $res_csop -> fetch();
				$link = 'http://'.$domain.'/termekek/'.$row_csop['nev_url'].'/'.$row['nev_url'];
			}
			else
			{
				$link = 'http://'.$domain.'/termekek/'.$_GET['kat_urlnev'].'/'.$row['nev_url'];
			}
	if ($row['kep'] == "")
	{
		print '<a href="'.$link.'"><img src="http://'.$domain.'/webshop/images/noimage.png" border="0" width="140" style="margin-bottom: 2px;"></a>';
	}
	else
	{
		print '<a href="'.$link.'"><img src="http://'.$domain.'/images/termekek/'.$row['kep'].'" border="0" style="margin-bottom: 2px; max-height: 140px; max-width: 140px;" alt="'.$row['nev'].'"></a>';
		
	}
	print '</a></div>';
	
	
	// NÉV
	print '<div style="text-align:center; width:100%; min-height: 48px;">'; 
		print '<p><a href="'.$link.'" style="font-size:15px; font-family: '; print "'Open Sans'"; print ', sans-serif; margin-bottom: 0; text-transform: capitalize; color: #8d9b00; text-decoration:none;">'.$row['nev'].'</a></p>';
	print '</div>';
	// Megtakarítás
	$datum = date("Y-m-d");
	if ($row['akcio_ig'] >= $datum && $row['akcio_tol'] <= $datum) //Akciós
	{
		$megtak = 100 - ((100 * $row['akciosar']) / $row['ar']);
		print '<div style="width:100%;">
					<center>
						<div style="width:75px; height:30px; margin-top: 4px; background: url(http://'.$domain.'/webshop/images/bg_megtak.png) no-repeat; color:white; padding-top:24px; font-size:16px; font-weight:bold;">
							'.number_format($megtak, 0, ',', ' ').'%
						</div>
					</center>
				</div>';
	}
	// MENÜ és ár
	if ($config_osszehasonlito == 'I')
	{
		if ((isset($_SESSION['osszahas_1']) && $_SESSION['osszahas_1'] == $row['id']) || (isset($_SESSION['osszahas_2']) && $_SESSION['osszahas_2'] == $row['id']) || (isset($_SESSION['osszahas_3']) && $_SESSION['osszahas_3'] == $row['id']) || (isset($_SESSION['osszahas_4']) && $_SESSION['osszahas_4'] == $row['id']))
		{
			$osszeh_check = 'checked';
		}
		else
		{
			$osszeh_check = '';
		}
	}
	print '<div style="min-height:47px">
			<table align="center" cellpadding="0" style="padding-top: 4px;">';
				if ($config_osszehasonlito == 'I')
				{
					print '<tr>
						<td>
							<form action="" method="POST">
								összehasonlítás <input type="checkbox" onclick="form.submit()" '.$osszeh_check.'>';
								if ($osszeh_check == '')
								{
									print '<input type="hidden" name="osszahas_id" value="'.$row['id'].'">';
								}
								else
								{
									print '<input type="hidden" name="no_osszahas_id" value="'.$row['id'].'">';
								}
							print'</form>
						</td>
					</tr>';
				}
				print '<tr>';
				
			//ÁR
			$datum = date("Y-m-d");
			if ($row['akcio_ig'] >= $datum && $row['akcio_tol'] <= $datum) //Akciós
			{
				if ($row['ar'] > 0)
				{
					print '<td><font style="text-decoration: line-through;">'.number_format($row['ar'], 0, ',', ' ').'</font><span class="term_ar" style="color: #FB1F78;"> '.number_format($row['akciosar'], 0, ',', ' ').'</span> Ft</td>';
				}
				else
				{
					print '<td class="term_ar"><font style="color: #FB1F78;"> '.number_format($row['akciosar'], 0, ',', ' ').'</font> Ft</td>';
				}
			}
			else //nem akciós
			{
				if ($row['ar'] > 0)
				{
					print '<td class="term_ar">'.number_format($row['ar'], 0, ',', ' ').' Ft</td>';
				}
				else
				{
					print '<td class="term_ar"></td>';
				}
			}
			print '</tr><tr>';
			
			//RÉSZLETEK
			print '<td align="center"><a href="'.$link.'" class="reszletek_gomb">Részletek</a></td>';
					
	print		'</tr>
			</table>
		</div>';
	print '</div>'; */
?>