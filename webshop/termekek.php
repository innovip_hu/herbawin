<div id="termekek_ajax_div">
<?php
	// Kategóriábam felsorolás (termék vagy további kategória)
	if (isset($_GET['kat_urlnev']) && isset($_GET['term_urlnev']) && $_GET['kat_urlnev'] != '' && $_GET['term_urlnev'] == '') 
	{
		include $gyoker.'/webshop/termek_kategoriak.php';
	}
//Termék infó
	else if (isset($_GET['term_urlnev'])) 
	{
		include $gyoker.'/webshop/termek_info.php';
	}
//KEZDŐLAP - TERMÉKEK
	else 
	{
		print '<div class="row kapcs_termekek">';
				$query1 = "SELECT * FROM ".$webjel."term_csoportok WHERE csop_id = 0 AND lathato=1 ORDER BY sorrend asc";
				foreach ($pdo->query($query1) as $row1)
				{
					if ($row1['kep'] == "")
					{
						$csop_kep_link = ''.$domain.'/webshop/images/noimage.png';
					}
					else
					{
						$csop_kep_link = ''.$domain.'/images/termekek/'.$row1['kep'];
					}
					print '<div class="product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12 fels_termek_div">
							<a href="'.$domain.'/termekek/'.$row1['nev_url'].'/"><div class="fels_termek_kep">
								<img alt="'.$row1['nev'].'" class="img-responsive lazy" data-src="'.$csop_kep_link.'" src="'.$csop_kep_link.'" style="opacity: 1;">
							</div></a>
							<div class="fels_termek_adatok">
								<h5 class="fels_termek_nev center"><a href="'.$domain.'/termekek/'.$row1['nev_url'].'/">'.$row1['nev'].'</a></h5>
							</div>
					</div>';
				}
		print '</div>';
	}
?>
</div>
