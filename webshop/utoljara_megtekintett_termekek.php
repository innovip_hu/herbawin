<?php
	// Utoljára megnézett termékek

	
	if(isset($_SESSION['ut_term_1']))
	{
		print '<div class="row utnez_term_divek">';
		$query_ut_term = "SELECT * FROM ".$webjel."termekek where nev_url='".$_SESSION['ut_term_1']."'";
		$res = $pdo->prepare($query_ut_term);
		$res->execute();
		$row_ut_term = $res -> fetch();
		$query_csop_ut = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_ut_term['csop_id'];
		$res_csop = $pdo->prepare($query_csop_ut);
		$res_csop->execute();
		$row_csop_ut  = $res_csop -> fetch();
		$link = ''.$domain.'/termekek/'.$row_csop_ut['nev_url'].'/'.$row_ut_term['nev_url'];
		$link_ut = ''.$domain.'/termekek/'.$row_csop_ut['nev_url'].'/'.$row_ut_term['nev_url'];
		$_SESSION['link_ut'] = ''.$domain.'/termekek/'.$row_csop_ut['nev_url'].'/'.$row_ut_term['nev_url'];
		// Kép
		print '<div class="col-lg-4 col-md-4 col-sm-4 col-sx-4"><a href="'.$link.'" >';
			$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row_ut_term['id']." ORDER BY alap DESC LIMIT 1";
			$res = $pdo->prepare($query_kep);
			$res->execute();
			$row_kep = $res -> fetch();
			$alap_kep = $row_kep['kep'];
			if ($alap_kep == '') 
			{
				print '<img alt="'.$row_ut_term['nev_url'].'" src="'.$domain.'/webshop/images/noimage.png" />';
			} elseif ($row_kep['ovip_termek_id'] != 0) {
				if($row_kep['thumb'] != '')
				{
					$kep = $row_kep['thumb'];
				}
				else
				{
					$kep = $row_kep['kep'];
				}
				print '<img alt="'.$row_ut_term['nev_url'].'" src="'.$kep.'" />';
			}
			else
			{
				if($row_kep['thumb'] != '')
				{
					$kep = $row_kep['thumb'];
				}
				else
				{
					$kep = $row_kep['kep'];
				}
				print '<img alt="'.$row_ut_term['nev_url'].'" src="'.$domain.'/images/termekek/'.$kep.'" />';
			}		
		print '</a></div>';
		// Név
		if ((isset($_SESSION['login_tipus']) && $_SESSION['login_tipus'] == 'partner') && (isset($_SESSION['partner_kedvezmeny']) && $_SESSION['partner_kedvezmeny'] != 0))
		{
			$kedv_d = $_SESSION['partner_kedvezmeny'];
			$row_ut_term['ar'] = $row_ut_term['ar'] * ((100 - $kedv_d) / 100);
			$row_ut_term['akciosar'] = $row_ut_term['akciosar'] * ((100 - $kedv_d) / 100);
		}			
		print '<div class="col-lg-8 col-md-8 col-sm-8 col-sx-3">';
			print '<p><a href="'.$link.'">';
				if (strlen($row_ut_term['nev'.$_SESSION['lang_sql']]) > 46)
				{
					print mb_substr($row_ut_term['nev'.$_SESSION['lang_sql']],0,46,'UTF-8').'...';
				}
				else
				{
					print $row_ut_term['nev'.$_SESSION['lang_sql']];
				}
			print '</a></p>';

			if (isset($_SESSION['valuta']) && $_SESSION['valuta'] != '')
			{
				$query_valuta = "SELECT * FROM ".$webjel."beallitasok WHERE id=1";
				$res = $pdo->prepare($query_valuta);
				$res->execute();
				$row_valuta = $res -> fetch();

				$akcios_ar_valuta = $row_ut_term['akciosar'] / $row_valuta[$_SESSION['valuta'].'_arfolyam'];
				$ar_valuta = $row_ut_term['ar'] / $row_valuta[$_SESSION['valuta'].'_arfolyam'];
				$valuta = $row_valuta[$_SESSION['valuta'].'_jel'];
			}
			else
			{
				$akcios_ar_valuta = $row_ut_term['akciosar'];
				$ar_valuta = $row_ut_term['ar'];
				$valuta = 'Ft';
			}

			// Ár
			$datum = date('Y-m-d');
			if ($row_ut_term['akcio_ig'] >= $datum && $row_ut_term['akcio_tol'] <= $datum) //Akciós
			{
				print '<span class="price-new" style="display:inline;">'.number_format($akcios_ar_valuta, 0, ',', ' ').' '.$valuta.'</span> <span class="price-old" style="display:inline;">'.number_format($ar_valuta, 0, ',', ' ').' '.$valuta.'</span>';
			}
			else //nem akciós
			{
				print '<span class="price-normal">'.number_format($ar_valuta, 0, ',', ' ').' '.$valuta.'</span>';
			}
		print '</div>';
		print '</div>';
	}
	
	if(isset($_SESSION['ut_term_2']))
	{
		print '<div class="row utnez_term_divek">';
		$query_ut_term = "SELECT * FROM ".$webjel."termekek where nev_url='".$_SESSION['ut_term_2']."'";
		$res = $pdo->prepare($query_ut_term);
		$res->execute();
		$row_ut_term = $res -> fetch();
		$query_csop_ut = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_ut_term['csop_id'];
		$res_csop = $pdo->prepare($query_csop_ut);
		$res_csop->execute();
		$row_csop_ut  = $res_csop -> fetch();
		$link = ''.$domain.'/termekek/'.$row_csop_ut['nev_url'].'/'.$row_ut_term['nev_url'];
		// Kép
		print '<div class="col-lg-4 col-md-4 col-sm-4 col-sx-4"><a href="'.$link.'" >';
			$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row_ut_term['id']." ORDER BY alap DESC LIMIT 1";
			$res = $pdo->prepare($query_kep);
			$res->execute();
			$row_kep = $res -> fetch();
			$alap_kep = $row_kep['kep'];
			if ($alap_kep == '') 
			{
				print '<img alt="'.$row_ut_term['nev_url'].'" src="'.$domain.'/webshop/images/noimage.png" />';
			} elseif ($row_kep['ovip_termek_id'] != 0) {
				if($row_kep['thumb'] != '')
				{
					$kep = $row_kep['thumb'];
				}
				else
				{
					$kep = $row_kep['kep'];
				}
				print '<img alt="'.$row_ut_term['nev_url'].'" src="'.$kep.'" />';
			}
			else
			{
				if($row_kep['thumb'] != '')
				{
					$kep = $row_kep['thumb'];
				}
				else
				{
					$kep = $row_kep['kep'];
				}
				print '<img alt="'.$row_ut_term['nev_url'].'" src="'.$domain.'/images/termekek/'.$kep.'" />';
			}
		print '</a></div>';
		// Név
		if ((isset($_SESSION['login_tipus']) && $_SESSION['login_tipus'] == 'partner') && (isset($_SESSION['partner_kedvezmeny']) && $_SESSION['partner_kedvezmeny'] != 0))
		{
			$kedv_d = $_SESSION['partner_kedvezmeny'];
			$row_ut_term['ar'] = $row_ut_term['ar'] * ((100 - $kedv_d) / 100);
			$row_ut_term['akciosar'] = $row_ut_term['akciosar'] * ((100 - $kedv_d) / 100);
		}			
		print '<div class="col-lg-8 col-md-8 col-sm-8 col-sx-3">';
			print '<p><a href="'.$link.'">';
				if (strlen($row_ut_term['nev'.$_SESSION['lang_sql']]) > 46)
				{
					print mb_substr($row_ut_term['nev'.$_SESSION['lang_sql']],0,46,'UTF-8').'...';
				}
				else
				{
					print $row_ut_term['nev'.$_SESSION['lang_sql']];
				}
			print '</a></p>';

			if (isset($_SESSION['valuta']) && $_SESSION['valuta'] != '')
			{
				$query_valuta = "SELECT * FROM ".$webjel."beallitasok WHERE id=1";
				$res = $pdo->prepare($query_valuta);
				$res->execute();
				$row_valuta = $res -> fetch();

				$akcios_ar_valuta = $row_ut_term['akciosar'] / $row_valuta[$_SESSION['valuta'].'_arfolyam'];
				$ar_valuta = $row_ut_term['ar'] / $row_valuta[$_SESSION['valuta'].'_arfolyam'];
				$valuta = $row_valuta[$_SESSION['valuta'].'_jel'];
			}
			else
			{
				$akcios_ar_valuta = $row_ut_term['akciosar'];
				$ar_valuta = $row_ut_term['ar'];
				$valuta = 'Ft';
			}	

			// Ár
			$datum = date('Y-m-d');
			if ($row_ut_term['akcio_ig'] >= $datum && $row_ut_term['akcio_tol'] <= $datum) //Akciós
			{
				print '<span class="price-new" style="display:inline;">'.number_format($akcios_ar_valuta, 0, ',', ' ').' '.$valuta.'</span> <span class="price-old" style="display:inline;">'.number_format($ar_valuta, 0, ',', ' ').' '.$valuta.'</span>';
			}
			else //nem akciós
			{
				print '<span class="price-normal">'.number_format($ar_valuta, 0, ',', ' ').' '.$valuta.'</span>';
			}
		print '</div>';
		print '</div>';
	}
	
	if(isset($_SESSION['ut_term_3']))
	{
		print '<div class="row utnez_term_divek">';
		$query_ut_term = "SELECT * FROM ".$webjel."termekek where nev_url='".$_SESSION['ut_term_3']."'";
		$res = $pdo->prepare($query_ut_term);
		$res->execute();
		$row_ut_term = $res -> fetch();
		$query_csop_ut = 'SELECT * FROM '.$webjel.'term_csoportok WHERE id='.$row_ut_term['csop_id'];
		$res_csop = $pdo->prepare($query_csop_ut);
		$res_csop->execute();
		$row_csop_ut  = $res_csop -> fetch();
		$link = ''.$domain.'/termekek/'.$row_csop_ut['nev_url'].'/'.$row_ut_term['nev_url'];
		// Kép
		print '<div class="col-lg-4 col-md-4 col-sm-4 col-sx-4"><a href="'.$link.'" >';
			$query_kep = "SELECT * FROM ".$webjel."termek_kepek WHERE termek_id=".$row_ut_term['id']." ORDER BY alap DESC LIMIT 1";
			$res = $pdo->prepare($query_kep);
			$res->execute();
			$row_kep = $res -> fetch();
			$alap_kep = $row_kep['kep'];
			if ($alap_kep == '') 
			{
				print '<img alt="'.$row_ut_term['nev_url'].'" src="'.$domain.'/webshop/images/noimage.png" />';
			} elseif ($row_kep['ovip_termek_id'] != 0) {
				if($row_kep['thumb'] != '')
				{
					$kep = $row_kep['thumb'];
				}
				else
				{
					$kep = $row_kep['kep'];
				}
				print '<img alt="'.$row_ut_term['nev_url'].'" src="'.$kep.'" />';
			}
			else
			{
				if($row_kep['thumb'] != '')
				{
					$kep = $row_kep['thumb'];
				}
				else
				{
					$kep = $row_kep['kep'];
				}
				print '<img alt="'.$row_ut_term['nev_url'].'" src="'.$domain.'/images/termekek/'.$kep.'" />';
			}
		print '</a></div>';
		// Név

		if ((isset($_SESSION['login_tipus']) && $_SESSION['login_tipus'] == 'partner') && (isset($_SESSION['partner_kedvezmeny']) && $_SESSION['partner_kedvezmeny'] != 0))
		{
			$kedv_d = $_SESSION['partner_kedvezmeny'];
			$row_ut_term['ar'] = $row_ut_term['ar'] * ((100 - $kedv_d) / 100);
			$row_ut_term['akciosar'] = $row_ut_term['akciosar'] * ((100 - $kedv_d) / 100);
		}			
		print '<div class="col-lg-8 col-md-8 col-sm-8 col-sx-3">';
			print '<p><a href="'.$link.'">';
				if (strlen($row_ut_term['nev'.$_SESSION['lang_sql']]) > 46)
				{
					print mb_substr($row_ut_term['nev'.$_SESSION['lang_sql']],0,46,'UTF-8').'...';
				}
				else
				{
					print $row_ut_term['nev'.$_SESSION['lang_sql']];
				}
			print '</a></p>';

			if (isset($_SESSION['valuta']) && $_SESSION['valuta'] != '')
			{
				$query_valuta = "SELECT * FROM ".$webjel."beallitasok WHERE id=1";
				$res = $pdo->prepare($query_valuta);
				$res->execute();
				$row_valuta = $res -> fetch();

				$akcios_ar_valuta = $row_ut_term['akciosar'] / $row_valuta[$_SESSION['valuta'].'_arfolyam'];
				$ar_valuta = $row_ut_term['ar'] / $row_valuta[$_SESSION['valuta'].'_arfolyam'];
				$valuta = $row_valuta[$_SESSION['valuta'].'_jel'];
			}
			else
			{
				$akcios_ar_valuta = $row_ut_term['akciosar'];
				$ar_valuta = $row_ut_term['ar'];
				$valuta = 'Ft';
			}	

			// Ár
			$datum = date('Y-m-d');
			if ($row_ut_term['akcio_ig'] >= $datum && $row_ut_term['akcio_tol'] <= $datum) //Akciós
			{
				print '<span class="price-new" style="display:inline;">'.number_format($akcios_ar_valuta, 0, ',', ' ').' '.$valuta.'</span> <span class="price-old" style="display:inline;">'.number_format($ar_valuta, 0, ',', ' ').' '.$valuta.'</span>';
			}
			else //nem akciós
			{
				print '<span class="price-normal">'.number_format($ar_valuta, 0, ',', ' ').' '.$valuta.'</span>';
			}
			print '</div>';
			print '</div>';
		}
			
	?>
