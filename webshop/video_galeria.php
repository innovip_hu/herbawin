<?php
	if (isset($_GET['nev_url']) && $_GET['nev_url'] != '') //  belépve
	{
		$res = $pdo->prepare("SELECT * FROM ".$webjel."yt_galeria WHERE nev_url='".$_GET['nev_url']."'");
		$res->execute();
		$row  = $res -> fetch();
		// print '<h3>'.$row['nev'].'</h3>';
		print nl2br('<p class="margbot20">'.$row['leiras'].'</p>');
		// Képek
		$query_kep = "SELECT * FROM ".$webjel."yt_galeria_linkek WHERE galeria_id=".$row['id']." ORDER BY id DESC";
		print '<div class="row">';
		foreach ($pdo->query($query_kep) as $row_kep)
		{
			print '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margbot10">';
				$video = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe width=\"100%\" height=\"210\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>",$row_kep['link']);
				print $video;
			print '</div>';
		}
		print '</div>';
		
		print '<p style="height:20px;"><a href="'.$domain.'/video-galeria/" class="btn btn-sm btn-primary-variant-1 btn-min-width-sm margtop10 margbot20" >Vissza</a></p>';
		
	}
	else // Galéria lista
	{
		// print '<h3>Galéria</h3>';
		$query = "SELECT * FROM `".$webjel."yt_galeria` ORDER BY `nev` DESC";
		print '<div class="row">';
		foreach ($pdo->query($query) as $row)
		{
			print '<a href="'.$domain.'/video-galeria/'.$row['nev_url'].'"><div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margbot10">';
					// Kép
					$query_kep = "SELECT * FROM ".$webjel."yt_galeria_linkek WHERE galeria_id=".$row['id']." ORDER BY alap DESC LIMIT 1";
					$res = $pdo->prepare($query_kep);
					$res->execute();
					$row_kep = $res -> fetch();
					$alap_kep = $row_kep['link'];
					if ($alap_kep != '') 
					{
						$video = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe width=\"100%\" height=\"210\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>",$alap_kep);
						print $video;
					}
					print '<h5 class="margtop10" style="text-align: center;">'.$row['nev'].'</h5>
					
			</div></a>';
		}
		print '</div>';
	}
?>
